# IFT1025

Notes de cours de IFT1025 - Programmation 2

## Modifier

Les slides sont écrites au format
[markdown](https://daringfireball.net/projects/markdown/), puis
compilées en slides PDF via `beamer`

## Générer les slides

```bash
./build.sh 00-PlanDeCours.md
```

### Dépendances


Nécessaires :

- `pandoc` pour compiler les slides Markdown en pdf via `beamer`
- Module de filtres `pandoc` pour python

```
pip install pandocfilters
```

Facultatifs :

- [`mk`](https://github.com/9fans/plan9port) pour générer
  automatiquement ce qui a changé seulement (à la `make`, mais plus
  chouette)
- [`entr`](http://eradman.com/entrproject/) pour générer
  automatiquement ce qui a changé à la modification d'un fichier (via
  la recette `mk auto`)
- `graphviz` pour générer les diagrammes

## Hacks

Pas très élégant, mais efficace : les non-breaking spaces (`&nbsp;`)
sont utilisés de façon hackish pour espacer des blocs (en générant un
paragraphe quasi-vide) :

    bla bla

    &nbsp;

    bla bla

ou pour forcer une image à être en mode inline :

    ![description invisible](img/truc.png)&nbsp;

puisqu'une image collée à du texte est automatiquement inlinée (ça ne
crée pas de **Figure N**, ça ne centre pas l'élément).

## TODO

Il manque certains exemples de code, des démonstrations et autres
compléments
