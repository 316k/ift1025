#!/usr/bin/python3
import pandocfilters as pf
import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def latex(s):
    return pf.RawBlock('latex', s)

delete = False

def mk_comments(k, v, f, m):
    global delete
    if k == "Para":
        value = pf.stringify(v)
        if value.startswith('[') and value.endswith(']'):
            content = value[1:-1]
            if content == "comment":
                delete = True
                return latex('')
            elif content == "/comment":
                delete = False
                return latex('')
    if delete:
        return latex('')


if __name__ == "__main__":
    pf.toJSONFilter(mk_comments)
