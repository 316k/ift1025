import java.io.IOException;
import java.util.Scanner;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONException;
import org.json.JSONObject;

public class ExceptionsC {

    public final static String url = "https://cat-fact.herokuapp.com/facts/random";
    // public final static String url = "http://perdu.com";

    public static String getFact() throws IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();

        String messageBrut = response.body().string();

        JSONObject messageJSON = new JSONObject(messageBrut);

        return messageJSON.getString("text");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean recommencer = true;

        do {
            recommencer = false;

            try {
                String catFact = getFact();
                System.out.println("Voici un fait intéressant sur les chats:");
                System.out.println();
                System.out.println("> " + catFact);
            } catch (IOException io) {
                System.err.println("ERREUR: Le site " + url + " n'est pas disponible");
                System.err.println("Vérifiez votre connexion internet et entrez OK pour réessayer : ");

                if (scanner.hasNext() && scanner.nextLine().equals("OK")) {
                    recommencer = true;
                }

            } catch (JSONException jsonException) {
                System.err.println("ERREUR: Le site ne retourne pas un message au format JSON");
            }
        } while (recommencer);
    }
}
