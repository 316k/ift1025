import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;

public class ExceptionsA {

    public final static String url = "https://cat-fact.herokuapp.com/facts/random";

    public static String getFact() {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();

        String messageBrut = response.body().string();

        JSONObject messageJSON = new JSONObject(messageBrut);

        return messageJSON.getString("text");
    }

    public static void main(String[] args) {
        String catFact = getFact();
        System.out.println("Voici un fait intéressant sur les chats:");
        System.out.println();
        System.out.println("> " + catFact);
    }
}
