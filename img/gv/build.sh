#!/bin/bash

for i in "$1"*.dot
do
    # Cool home-made flags
    flags=""
    flagline="$(sed 1q $i | grep FLAG)"
    base="${i%.dot}"
    outfile="$base.png"

    if echo "$flagline" | grep pos > /dev/null
    then
        flags="-Kneato $flags"
    fi

    # Home-made animation
    # Use : // line-to-uncomment-at-step-n //%n
    if echo "$flagline" | grep animate > /dev/null
    then
        for n in $(grep -E '//.+//%[0-9]+$' $i | awk '{print substr($NF, 4); }')
        do
            tmp=$(mktemp)
            sed -r '; s,//(.+)//%'$n',\1,' $i > $tmp
            dot $tmp -Gdpi=200 -Tpng $flags -o $base.$n.png
            rm $tmp
        done

        continue
    fi

    dot $i -Gdpi=200 -Tpng $flags -o $outfile
done

