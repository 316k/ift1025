# -*- mode: shell-script -*-
THEME=Rochester
PANDOC_STYLE="--filter ./columns.py --filter ./stop.py --no-tex-ligatures -t beamer+strikeout -V theme:$THEME -V colortheme:dolphin"
PANDOC_CODE="-H make-code-smaller.tex --highlight-style=tango -H header_pagenbr.tex"

FILES="00-PlanDeCours.pdf" \
      "01-Java.pdf" \
      "02-ProgrammationOrientéeObjet-1.pdf" "03-ProgrammationOrientéeObjet-2.pdf" \
      "04-Héritage.pdf" \
      "05-StructuresDeDonnées.pdf" \
      "06-Exceptions.pdf" "07-Fichiers.pdf" \
      "08-Evenements.pdf" \
      "08-Anonymes.pdf" \
      "09-GUI.pdf" \
      "10-Infographie.pdf" \
      "10b-Infographie-extra.pdf" \
      "11-Multithread.pdf" \
      "12-Tris.pdf" \
      "13-Regex.pdf" \
      "06b-Exceptions-complement.html"

#      "02a-Exercices objets.pdf" "03a-Exercices héritage.pdf" "03a-Exercices héritage - min.pdf"

.all:V: $FILES

# nup:V: "01-Java-nup.pdf" \
#        "02-Programmation Orientée Objet - 1-nup.pdf" "03-Programmation Orientée Objet - 2-nup.pdf" \
#        "04-Heritage-nup.pdf" \
#        "05-Structures de Données-nup.pdf" \
#        "06-Exceptions-nup.pdf" "07-Fichiers-nup.pdf" \
#        "08-Evenements-nup.pdf" \
#        "09-GUI-nup.pdf" \
#        "10-Infographie-nup.pdf" \
#        "11-Multithread-nup.pdf" \
#        "12-Tris-nup.pdf" \
#        "13-Regex-nup.pdf"

bundle:V: $FILES
    zip bundle.zip $FILES

valid:V:
    # Trailing dots
    grep -En '[^.]{2}\.$' [01]*md
    # stuff: b
    grep -En '[^ ]:' *.md

auto:V:
    ls *.md *.tex | entr mk

&-nup.pdf: &.pdf
    pdfnup --nup 1x3 --no-landscape "$stem.pdf"

&.pdf: &.md
    echo "$stem"
    time (pandoc $PANDOC_STYLE $PANDOC_CODE "$stem.md" -o "$stem.pdf" || (echo Pas de code dans les slides; pandoc $PANDOC_STYLE "$stem.md" -o "$stem.pdf"))

&.html: &.md
    pandoc -s "$stem.md" -o "$stem.html"
