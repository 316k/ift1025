---
title: Classes anonymes et interfaces fonctionnelles
author: Par Nicolas Hurtubise
header-includes:
    - \usepackage{textcomp}
date: IFT1025 - Programmation 2
---

# Au programme...

- Quelques notions de programmation fonctionnelle en Java

&nbsp;

`=>` Préalable aux interfaces graphiques

# Fonctions en tant qu'objets?

En JavaScript, les fonctions sont des objets comme les autres:

```javascript
var direBonjour = function(nom) {
    console.log("Bonjour " + nom + "!");
};

var direByebye = function(nom) {
    console.log("Bye bye " + nom + "!");
};

var fct;
if(Math.random() < 0.5) {
    fct = direByebye;
} else {
    fct = direBonjour;
}

fct("Twado"); // "Bonjour Twado!" ou "Bye bye Twado!"
```

# Fonctions en tant qu'objets?

En JavaScript, les fonctions sont des objets comme les autres:

```javascript
var nombrePair = function(x) {
    return x % 2 == 0;
};

var nombrePositif = function(x) {
    return x >= 0;
};

var tab = [-1, 10, 5, 3, 2, -6];

console.log(tab.filter(nombrePair));
// => [10, 2, -6]

console.log(tab.filter(nombrePositif));
// => [ 10, 5, 3, 2 ]


```

# Fonctions en tant qu'objets?

Cette fonctionnalité est particulièrement pratique dans le contexte de
la *programmation événementielle*.

&nbsp;

## Programmation événementielle

- Paradigme de programmation qui focus sur les événements qui peuvent
  se produire pendant l'exécution du programme.

- S'oppose à la programmation *séquentielle*, où tout s'exécute dans
  un ordre à peu près prédéfini

&nbsp;

Par exemple : sur une page web, un utilisateur peut cliquer sur
n'importe quel bouton n'importe quand.

# Fonctions en tant qu'objets?

La programmation événementielle repose sur *définir des actions à
prendre en réaction à ce que les utilisateurs font* :

- Si on clique sur la page `=>` afficher un popup
- Si on appuie sur la touche Espace `=>` affiche "Bonjour!" dans la
  console

&nbsp;

# Fonctions en tant qu'objets?

JavaScript a été conçu spécifiquement pour ce genre de cas
d'utilisation :

```javascript

var popup = function() {
    alert("Ceci est un popup!");
};

/* Si on clique sur la page,
   la fonction pop doit être appelée */
document.addEventListener("click", popup);

/* Si on appuie sur un bouton Espace,
   on affiche "Bonjour!" dans la console */
document.addEventListener("keyup", function() {
    if(e.keyCode == 32) {
        console.log("Bonjour!");
    }
});
```

# Fonctions en tant qu'objets?

En Java, les fonctions ne sont *pas des objets*. Ce sont des
procédures stockées qui ne peuvent pas être assignées à des
variables...

&nbsp;

```java
class Test {

    public static void direBonjour(String nom) { ... }

    public static void direByebye(String nom) { ... }

    public static void main(String[] args) {
        *** Invalide! ***
        Function test = direBonjour;
        test("Twado");
    }
}
```

# Fonctions en tant qu'objets?

*Mais*...

&nbsp;

On peut contourner le problème grâce aux interfaces et à l'héritage!

```java
// Fichier Function.java
public interface Function {
    public void execute(String arg);
}
```

# Fonctions en tant qu'objets?

```java
// Fichier DireBonjour.java
public class DireBonjour implements Function {
    public void execute(String arg) {
        System.out.println("Bonjour " + arg + "!");
    }
}

// Fichier DireByebye.java
public class DireByebye implements Function {
    public void execute(String arg) {
        System.out.println("Bye bye " + arg + "!");
    }
}
```

# Fonctions en tant qu'objets?

```java
public static void main(String[] args) {
    Function f;

    if(Math.random() < 0.5) {
        f = new DireBonjour();
    } else {
        f = new DireByebye();
    }

    f.execute("Twado");
}
```

Ça marche... Mais c'est *lourd*

- On a besoin de 4 fichiers pour faire ce qu'on faisait en ~10 lignes
de JavaScript
-  2 classes sont instanciées une seule fois dans tout le programme...


# Classe anonyme

Pour simplifier ça on peut profiter du concet de **classe
anonyme**. \newline Il s'agit d'une classe...

- Qui ne porte pas de nom
- **Concrète** (toutes les méthodes implantées, rien d'`abstract`)
- Qui **hérite** d'une autre classe (typiquement abstraite)
  \underline{ou} qui **implémente** une interface
- Qui est définie seulement *au moment de l'utiliser* (directement
  dans la méthode qui l'utilise) :

```java
Function f = new Function() {
    public void execute(String arg) {
        System.out.println("Bonjour " + arg + "!");
    }
};
```

# Classe anonyme

```java
public static void main(String[] args) {
    Function f;

    if(Math.random() < 0.5) {
        f = new Function() {
            public void execute(String arg) {
                System.out.println("Bonjour " + arg + "!");
            }
        };
    } else {
        f = new Function() {
            public void execute(String arg) {
                System.out.println("Bye bye " + arg + "!");
            }
        };
    }

    f.execute();
}
```


# Classe anonyme

Déjà mieux! Mais ça reste visuellement plus lourd qu'en JavaScript...

&nbsp;

On répète la signature `public void execute(String arg)` partout, en plus du
`new Function()`.

&nbsp;

Vu le contexte dans lequel on écrit notre classe anonyme, **il n'y a
qu'une seule possibilité** :

```javascript
/* Puisqu'on assigne la classe anonyme à une variable de
   type `Function`, on va **nécessairement** avoir à écrire 
   tout ça tout le temps... */
Function f = new Function() {
    public void execute(String arg) {
        ...
    }
};
```

# Interfaces fonctionnelles

- Pour avoir de quoi de plus joli que des classes/interfaces anonymes,
  Java 8 définit du *sucre syntaxique* pour ça.

- Si on définit notre `Function` comme étant une interface, on peut la
qualifier d'**interface fonctionnelle**.

- Une interface fonctionnelle doit avoir *exactement une seule méthode
  sans implantation* et utilise l'annotation `@FunctionalInterface`.

```java
@FunctionalInterface
public interface Function {
    public void execute(String arg);
}
```

# Interfaces fonctionnelles

Une fois qu'on a défini notre interface fonctionnelle, Java fait un
peu de magie pour nous permettre de simplifier l'écriture de nos
classes anonymes :

```java
Function f = (nom) -> {
    System.out.println("Bonjour " + nom + "!");
};
```

# Interfaces fonctionnelles

Le code devient :

```java
public static void main(String[] args) {
    Function f;

    if(Math.random() < 0.5) {
        f = (nom) -> {
            System.out.println("Bonjour " + nom + "!");
        };
    } else {
        f = (nom) -> {
            System.out.println("Bye bye " + nom + "!");
        };
    }

    f.execute("Twado");
}
```

# Interfaces fonctionnelles

## Question....

- Est-ce que `nom` peut maintenant être de n'importe quel type ? On ne
  spécifie pas que ça doit être une `String`!


```java
    f = (nom) -> {
        System.out.println("Bye bye " + nom + "!");
    };

    ...

    f(10); // ?
```

# Interfaces fonctionnelles

## Réponse

- Pas du tout !
- On parle de *sucre syntaxique* parce que ça n'impacte que la
  *syntaxe* du langage
- Java fait de l'*inférence de type* pour détecter ce qui devrait être
  écrit à la place de notre fonction anonyme \newline `(nom) -> { ... }`

# Interfaces fonctionnelles

## Réponse

Avec une petite analyser du code, on se rend compte que la seule
chose valide à cet endroit est un : \newline

\centerline{\texttt{new Function() \{ ... \}}}

&nbsp;

qui contient une seule fonction : \newline

\centerline{\texttt{public void execute(String arg);}}

&nbsp;

# Interfaces fonctionnelles

En bref, quand Java lit :

```java
    f = (arg) -> {
        System.out.println("Bonjour " + arg + "!");
    };



```

&nbsp;

# Interfaces fonctionnelles

Java analyse le contexte et comprend que ça veut dire :

```java
    f = new Function() {
        public void execute(String arg) {
            System.out.println("Bonjour " + arg + "!");
        }
    };

```

`=>` Ça nous permet d'écrire la même chose, avec moins de code
