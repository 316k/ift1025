# Exceptions -- Complément sur le `finally`

> Comme le `finally` est mentionné très rapidement dans les slides,
> c'est difficile de vraiment comprendre à quoi ça peut bien servir
> (par rapport à simplement mettre du code après le bloc `try...catch`
> directement
> 
> Voici une petite explication avec un exemple concret. Notez que le
> code ci-bas reste du pseudo-code style Java qui est avant tout fait
> pour être lu, vous n'arriverez pas à le compiler et à l'exécuter.

Imaginons une situation où on veut un programme pour gérer les
emprunts de livres dans une bibliothèque.


Pour réserver un livre dans ce système, il faut :

1. Ouvrir le dossier de la personne qui veut emprunter le livre
2. Ajouter le livre à réserver au dossier
3. Refermer le dossier

Si on oublie l'étape 3, le dossier n'est pas sauvegardé correctement
(et donc le livre n'est pas réservé pour vrai).


```java
class Personne {
    public void emprunterLivre(Livre livre) {

        // On doit demander un accès au dossier de la personne
        dossier = bibliotheque.ouvrirDossier(this);

        dossier.reserverLivre(livre);

        // Si on oublie cette étape
        // le livre n'est pas ajouté aux réservations!
        dossier.fermer();
    }
}
```

Une raison pour avoir un design du genre pourrait être parce que la
base de données est disponible sur internet : plusieurs personnes
utilisent le système de réservations de la bibliothèque *en même
temps*.

Un cas qui n'est pas normal mais qui pourrait arriver (donc un cas
exceptionnel) serait si plusieurs personnes ouvraient le même dossier
en même temps et essayaient de le modifier au même moment.

### Utilisation du programme par deux personnes en même temps{.center}

| # | Personne 1                                          | Personne 2                                          |
|---+-----------------------------------------------------+-----------------------------------------------------|
| 1 | Ouvrir le dossier <br> reçu: `dossier={Livre A}`    |                                                     |
| 2 |                                                     | Ouvrir le dossier <br> reçu: `dossier={Livre A}`    |
| 3 | Ajouter : Livre B <br> `dossier={Livre A, Livre B}` |                                                     |
| 4 |                                                     | Ajouter : Livre C <br> `dossier={Livre A, Livre C}` |
| 5 | Enregistrer dossier                                 |                                                     |
| 6 |                                                     | Enregistrer dossier                                 |


À la fin de ces deux modifications, le dossier contient `{Livre A,
Livre C}` et le *Livre B* a été perdu...


On peut utiliser les exceptions pour gérer ce cas : notre fonction
peut lancer une exception personnalisée `DoubleEmpruntException` à la
fonction appelante dans le cas rare où on essaie de modifier le
dossier pendant qu'il est déjà ouvert.

```java
class Personne {
    public void emprunterLivre(Livre livre) throws DoubleEmpruntException {

        // Si le dossier est déjà ouvert, on ne peut pas l'ouvrir en double
        if(bibliotheque.dossierDejaOuvert(this)) {
            throw new DoubleEmpruntException("Le dossier est déjà ouvert ailleurs!");
        }

        // On doit demander un accès au dossier de la personne
        dossier = bibliotheque.ouvrirDossier(this);

        dossier.reserverLivre(livre);

        // Si on oublie cette étape
        // le livre n'est pas ajouté aux réservations!
        dossier.fermer();
    }
}
```

De cette façon, on sait que deux personnes ne peuvent pas ouvrir le
même dossier en double et causer des problèmes : si le dossier est
déjà ouvert, on ne peut pas le rouvrir une deuxième fois.


Imaginons maintenant qu'une autre situation exceptionnelle se
produit : quelqu'un ouvre le compte, commence à réserver, mais pour
une raison quelconque, la fonction reserverLivre() lance elle-même une
IOException :


```java
class Personne {
    public void emprunterLivre(Livre livre) throws DoubleEmpruntException {

        // Si le dossier est déjà ouvert, on ne peut pas l'ouvrir en double
        if(bibliotheque.dossierDejaOuvert(this)) {
            throw new DoubleEmpruntException("Le dossier est déjà ouvert ailleurs!");
        }

        // On doit demander un accès au dossier de la personne
        dossier = bibliotheque.ouvrirDossier(this);


        *** La fonction reserverLivre() lance une IOException ici ***
        dossier.reserverLivre(livre);

        *** => L'IOException remonte à la fonction appelante ***


        // Si on oublie cette étape
        // le livre n'est pas ajouté aux réservations!
        dossier.fermer();
    }
}
```


On pourrait mettre un `try...catch` pour gérer la nouvelle exception :


```java
class Personne {
    public void emprunterLivre(Livre livre) throws DoubleEmpruntException {

        // Si le dossier est déjà ouvert, on ne peut pas l'ouvrir en double
        if(bibliotheque.dossierDejaOuvert(this)) {
            throw new DoubleEmpruntException("Le dossier est déjà ouvert ailleurs!");
        }

        // On doit demander un accès au dossier de la personne
        dossier = bibliotheque.ouvrirDossier(this);

        try {
            *** La fonction reserverLivre() lance une IOException ici ***
            dossier.reserverLivre(livre);


            // Si on oublie cette étape
            // le livre n'est pas ajouté aux réservations!
            dossier.fermer();
        } catch(IOException e) {
            *** L'IOException est attrapée ici ***
            System.out.println("Erreur de réseau");
        }
    }
}
```

Mais ici, on ne ferme le dossier que si le `try` réussit à se
terminer, ce qui le rend impossible à ré-ouvrir par la suite!

On devrait donc fermer le dossier après le `try...catch` plutôt que
dans le `try` seulement :

```java
class Personne {
    public void emprunterLivre(Livre livre) throws DoubleEmpruntException {

        // Si le dossier est déjà ouvert, on ne peut pas l'ouvrir en double
        if(bibliotheque.dossierDejaOuvert(this)) {
            throw new DoubleEmpruntException("Le dossier est déjà ouvert ailleurs!");
        }

        // On doit demander un accès au dossier de la personne
        dossier = bibliotheque.ouvrirDossier(this);

        try {
            *** La fonction reserverLivre() lance une IOException ici ***
            dossier.reserverLivre(livre);


        } catch(IOException e) {
            *** L'IOException est attrapée ici ***
            System.out.println("Erreur de réseau");
        }

        *** On continue l'exécution après => on ferme le dossier ***

        // Si on oublie cette étape
        // le livre n'est pas ajouté aux réservations!
        dossier.fermer();
    }
}
```

Après un bloc `try...catch`, tout continue de s'exécuter normalement,
donc c'est une bonne solution ici.


Supposons qu'on a un autre problème... Ici, attraper l'erreur et la
gérer tout de suite n'est peut-être pas pratique. Si c'était une
exception *imprévisible* plutôt qu'une exception rattrapable comme
IOException :


```java
class Personne {
    public void emprunterLivre(Livre livre) throws DoubleEmpruntException {

        // Si le dossier est déjà ouvert, on ne peut pas l'ouvrir en double
        if(bibliotheque.dossierDejaOuvert(this)) {
            throw new DoubleEmpruntException("Le dossier est déjà ouvert ailleurs!");
        }

        // On doit demander un accès au dossier de la personne
        dossier = bibliotheque.ouvrirDossier(this);

        try {
            *** La fonction reserverLivre() comporte un bug ***
            *** Cette ligne lance une ArrayIndexOutOfBoundsException ***
            dossier.reserverLivre(livre);

            *** => L'ArrayIndexOutOfBoundsException remonte à la fonction appelante ***



        } catch(IOException e) {
            *** Le catch n'est pas fait pour les ArrayIndexOutOfBoundsException ***
            *** donc on n'attrappe pas cette exception ***
            System.out.println("Erreur de réseau");
        }

        *** On ne va PAS continuer l'exécution, puisque ***
        *** l'exception est remontée à la fonction appelante ***

        // Si on oublie cette étape
        // le livre n'est pas ajouté aux réservations!
        dossier.fermer();
    }
}
```

Notre programme plante, mais c'est impossible de prévoir ça d'avance :
on ne veut pas mettre des try catch partout pour toutes les erreurs de
programmation possibles, ça serait une mauvaise idée d'entourer le
tout d'un `try...catch`, ça rendrait le code moche et difficile à
débugger.

*Mais*, on a quand même le problème que tout à l'heure : le dossier de
la personne est resté ouvert et plus personne nulle part ne peut
l'utiliser.

&nbsp;

*On a besoin d'un mécanisme pour exécuter du code **peu importe** ce
qui va se passer*. Peu importe si la fonction retourne prématurément à
cause d'une exception, peu importe si on break d'une boucle, peu
importe tout ce qui se passe. C'est à ça que le `finally` sert :


```java
class Personne {
    public void emprunterLivre(Livre livre) throws DoubleEmpruntException {

        // Si le dossier est déjà ouvert, on ne peut pas l'ouvrir en double
        if(bibliotheque.dossierDejaOuvert(this)) {
            throw new DoubleEmpruntException("Le dossier est déjà ouvert ailleurs!");
        }

        // On doit demander un accès au dossier de la personne
        dossier = bibliotheque.ouvrirDossier(this);

        try {
            *** La fonction reserverLivre() comporte un bug ***
            *** Cette ligne lance une ArrayIndexOutOfBoundsException ***
            dossier.reserverLivre(livre);

            *** => L'ArrayIndexOutOfBoundsException remonte à la fonction appelante



        } catch(IOException e) {
            *** Le catch n'est pas fait pour les ArrayIndexOutOfBoundsException ***
            *** donc on n'attrappe pas cette exception ***
            System.out.println("Erreur de réseau");
        } finally {
            *** Ce bloc s'exécute peu importe si le try s'est terminé correctement ou non ***
            *** Le dossier est fermé correctement dans *tous* les cas ***
            dossier.fermer();
        }

        *** On ne va PAS continuer l'exécution, puisque ***
        *** l'exception est remontée à la fonction appelante ***
        // ...
    }
}
```

Notez bien que le `finally` sert dans certains contextes très précis
et que **vous ne risquez pas d'en avoir besoin dans le cadre du
cours**. C'est cependant pratique dans des cas où on utilise des
ressources qui doivent être fermées, désallouées ou nettoyées d'une
quelconque façon, même s'il y a eu une erreur.

Notez que le bloc `try...finally` n'a pas forcément besoin d'un catch
et que le `finally` s'exécute vraiment **peu importe comment ça s'est
terminé**, ce qui peut donner lieu à du code particulièrement
bizarre :

```java
    public static int fonctionEtrange() {
        while (true) {
            try {
                return 10;
            } finally {
                break;
            }
        }

        return 30;
    }

    public static void main(String[] args) {
        System.out.println(fonctionEtrange());
        => Affiche 30
    }
```

(Note: cela va sans dire que de faire de quoi du genre n'est pas une
idée...)

<style>
body {
    max-width: 800px;
    padding: 15px;
    width: 100%;
    margin: auto;
}
pre {
    font-size: large;
    background-color: #CCE7F0;
    padding: 10px;
}
table {
    border-collapse: collapse;
    margin: auto;
}
table, td, th {
    border-top: 1px solid black;
    border-bottom: 1px solid black;
    padding: 10px 15px;
}
td:first-child, th:first-child {
    color: gray;
}
blockquote {
    font-style: italic;
    border: 1px dotted black;
    padding: 15px;
}
.center {
    text-align: center;
}
</style>
