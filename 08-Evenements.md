---
title: Programmation Réseau et Événementielle
author: Par Nicolas Hurtubise
header-includes:
    - \usepackage{textcomp}
date: IFT1025 - Programmation 2
---

# Au programme...

- Architecture Client/Serveur
- Sockets
- Programmation événementielle
- Interfaces fonctionnelles

# Architecture Client/Serveur

On va écrire un petit système où deux programmes différents
communiquent :

&nbsp;

- Un premier programme qui *envoie des commandes*
- Un autre programme qui *agit selon les commandes reçues*

# Architecture Client/Serveur

- Le **serveur** est le programme qui attend de recevoir des commandes
  pour s'exécuter
- Le **client** est le programme qui se connecte au serveur pour lui
  envoyer des requêtes


# Architecture Client/Serveur

\center

![](img/gv/server-client.1.png){width=65%}&nbsp;

*Schéma typique de la communication entre un client et un serveur*

# Architecture Client/Serveur

\center

![](img/gv/server-client.2.png){width=65%}&nbsp;

*Schéma typique de la communication entre un client et un serveur*

# Architecture Client/Serveur

\center

![](img/gv/server-client.3.png){width=65%}&nbsp;

*Schéma typique de la communication entre un client et un serveur*

# Architecture Client/Serveur

\center

![](img/gv/server-client.4.png){width=65%}&nbsp;

*Schéma typique de la communication entre un client et un serveur*

# Architecture Client/Serveur

\center

![](img/gv/server-client.5.png){width=65%}&nbsp;

*Schéma typique de la communication entre un client et un serveur*

# Architecture Client/Serveur

\center

![](img/gv/server-client.6.png){width=65%}&nbsp;

*Schéma typique de la communication entre un client et un serveur*

# Architecture Client/Serveur

Dans beaucoup de cas, le client et le serveur seront des programmes
sur **différentes machines** qui communiqueront via **internet** :

&nbsp;

- Serveur web -- Navigateur
- Serveur de courriels -- Logiciel de courriels
- Serveur de jeu multijoueur pour *Minecraft* -- client *Minecraft*
- ...

# Protocole

Le client et le serveur doivent s'entendre sur un *protocole* à
respecter lors de la communication, aka, sur la façon dont les données
seront échangées

## Serveur web

- [Protocole `HTTP`](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Message_format)
- Requête textuelle envoyée par le client :
    - Ligne de requête (ex.: `GET /images/logo.png HTTP/1.1`)
    - Lignes d'en-têtes (ex.: `Accept-Language: en`)
    - Ligne vide
    - Corps de la requête (au besoin, par exemple pour transmettre des
      champs de formulaire)

&nbsp;

&nbsp;

&nbsp;

# Protocole

Le client et le serveur doivent s'entendre sur un *protocole* à
respecter lors de la communication, aka, sur la façon dont les données
seront échangées

## Serveur web

La réponse envoyée par le serveur est définie de manière similaire

```bash
HTTP/1.1 200 OK
Date: Sun, 10 Oct 2010 23:26:07 GMT
Server: Apache/2.2.8 (Ubuntu) mod_ssl/2.2.8 OpenSSL/0.9.8g
Last-Modified: Sun, 26 Sep 2010 22:04:35 GMT
ETag: "45b6-834-49130cc1182c0"
Accept-Ranges: bytes
Content-Length: 12
Connection: close
Content-Type: text/html

<b>Hello world!</b>
```

# Protocole

Les différents types de serveurs définissent des protocoles différents

&nbsp;

- Serveur de courriels : plusieurs protocols possibles (selon ce qu'on
  souhaite faire exactement)
    - `IMAP`
    - `SMTP`
    - `POP3`
    - ...
- Serveur de Minecraft
    - Protocole `SMP` (*Survival Multiplayer*)

# Architecture Client/Serveur en Java

Pour créer un serveur simple en Java, on va utiliser des *Sockets*

&nbsp;

`=>` Abstraction pour de la communication sur un réseau

&nbsp;

> A socket is an endpoint for communication between two machines.
>
> ~ ~ Documentation de Java


# `Socket`

On peut utiliser des sockets pour ouvrir une connexion entre deux
machines sur un même réseau (par exemple, entre votre ordinateur et un
serveur de Google, tous deux connectés à l'Internet)

&nbsp;

On peut utiliser l'objet `Socket` pour ouvrir une connexion :

```java
Socket clientSocket = new Socket({IP}, {PORT});
```
&nbsp;

Par exemple, pour ouvrir une connexion sur le serveur web du site
`perdu.com`, on peut spécifier l'ip du serveur et le port 80 :

```java
Socket clientSocket = new Socket("208.97.177.124", 80);
```

# `Socket`

Pour simplifier nos tests, on va lancer notre client et notre serveur
sur la même machine

&nbsp;

On va utiliser l'adresse IP **`127.0.0.1`** comme adresse de
connexion

&nbsp;

`127.0.0.1` `=>` Adresse spéciale qui correspond *toujours* à la
machine locale (localhost)

# `Socket`

En Java, un objet `Socket` est associé à un ***stream* d'input** et un
***stream* d'output**

```java
Socket clientSocket = new Socket("127.0.0.1", 80);

// Obtenir le stream d'input (données reçues du serveur)
clientSocket.getInputStream()


// Obtenir le stream d'output (pour envoyer des données au serveur)
clientSocket.getOutputStream()
```

# Client

```java
public class JavaClient {

    public static void main(String[] args) {
        try {
            Socket clientSocket = new Socket("127.0.0.1", 1337);

            OutputStreamWriter os = new OutputStreamWriter(
                clientSocket.getOutputStream()
            );
            
            BufferedWriter writer = new BufferedWriter(os);

            /* Voir le chapitre sur les fichiers si les
               OutputStreamWriter et BufferedWriter ne vous
               disent rien */
            
            // ...
```

# Client

```java
            // Envoie au serveur les lignes tapée sur la console
            Scanner scanner = new Scanner(System.in);
            
            while(scanner.hasNext()) {
                String line = scanner.nextLine();
                System.out.println("Envoi de : " + line);
                writer.append(line + "\n");

                // Vider le buffer : envoyer la requête tout de suite
                writer.flush();
            }
            
            writer.close();
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
```

# Serveur

Du côté du serveur, on doit démarrer un `ServerSocket` et attendre
qu'un client se connecte.

```java
/* Création d'un serveur qui écoute sur le port spécifié
   {BACKLOG} == nombre de clients permis dans la
   file d'attente du serveur */
ServerSocket server = new ServerSocket({PORT}, {BACKLOG});

// Attente d'une connexion d'un client
Socket client = server.accept();

// ... traitement de la requête ...
```

# Serveur

```java
public class JavaServer {
    public static void main(String[] args) {
        try {
            // Création d'un serveur sur le port 1337 (max 1 client)
            ServerSocket server = new ServerSocket(1337, 1);

            // Attente d'une connexion d'un client
            Socket client = server.accept();
            
            System.out.println("Connexion !");
            System.out.println(client);
            // => Affiche des informations sur le client
            // Socket[addr=/127.0.0.1,port=45896,localport=1337]

            InputStreamReader is = 
                new InputStreamReader(client.getInputStream());

            BufferedReader reader = new BufferedReader(is);
            // ...
```

# Serveur

```java
            // (suite)

            String line;

            /* Tant que le client envoie des données, 
               on les affiche */
            while ((line = reader.readLine()) != null) {
                System.out.println("Reçu : " + line);
            }
            
            // Dès que le client se déconnecte, le serveur se ferme
            System.out.println("Fin");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
```

# Ajouter des fonctionnalités au serveur

On a un serveur simple qui affiche le message reçu

&nbsp;

Le gros du traitement (de base pour l'instant) se fait dans la boucle
qui lit les données envoyées :

```java
while ((line = reader.readLine()) != null) {
    System.out.println("Reçu : " + line);
}
```

# Ajouter des fonctionnalités au serveur

Le serveur est un peu simple...

&nbsp;

On va définir des commandes que le client peut envoyer pour faire
faire des choses au serveur :

- `echo [argument]` : Afficher l'argument envoyé sur la console
- `reverse [argument]` : Afficher l'argument à l'envers
- `date` : Afficher la date
- `count [argument]` : Afficher le nombre de caractères dans
  l'argument

# Ajouter des fonctionnalités au serveur

On peut commencer par analyser la ligne reçue :

```java
while ((line = reader.readLine()) != null) {
    String[] parts = line.split(" ");
    String commande = parts[0];
    String argument = "";

    for(int i=1; i<parts.length; i++) {
        argument += parts[i];
        if(i < parts.length - 1)
            argument += " ";
    }

    // *** Traitement de commande + argument ici ***
}
```

Puis tester la commande reçue pour faire une action en conséquence

# Ajouter des fonctionnalités au serveur


```java
    // Traitement de commande + argument :
    switch (commande) {
        case "echo":
            System.out.println(argument);
            break;
        case "date":
            System.out.println(new Date()); // .toString() affiché
            break;
        case "count":
            System.out.println(argument.length());
            break;
        case "reverse":
            // ...

        default:
            System.out.println("Commande inconnue");
    }
```

# Ajouter des fonctionnalités au serveur

Le pattern général de notre code devient :

```java
while( on écoute les commandes ) {
    
    commande = prochaineCommande();

    switch(commande) {
        case Commande1:
            ...
        case Commande2:
            ...
        
        ...

        case CommandeN:
            ...
    }
}
```


# Programmation Événementielle

On peut généraliser l'idée de recevoir une commande à un *événement
quelconque* qui pourrait arriver pendant l'exécution de notre
programme.

- Requêtes sur un serveur
- Tâches à exécuter selon l'heure de la journée
    - Faire un backup des fichiers à minuit tous les soirs
    - Envoyer une alerte quand c'est l'heure d'un rendez-vous
- *Interfaces graphiques*
    - Clic sur un bouton
    - Mouvement de souris
    - Entrée de texte ou d'un raccourcis clavier
- Internet of Things (IoT)
    - Alerte lorsque quelqu'un entre chez vous pendant votre absence
    - Votre Toaster "Intelligent" qui averti un serveur de la NSA que vos
      toasts sont prêtes

# Programmation Événementielle

C'est ce qu'on appelle de la *programmation événementielle*

&nbsp;

- On ne sait pas d'avance la séquence d'événements qui va se produire
- On définit des *actions* à prendre en *réaction* à des événements


# Boucle d'événements

Le pattern :

```java
    while(continuer) {
        // Attendre le prochain événement
        event = waitForNextEvent();
        
        // Traiter l'événement
        switch(event) {
            ...
        }
    }
```

est généralement la base d'un programme événementiel et constitue ce
qu'on appelle la *boucle d'événements*

# Boucle d'événements

Tous les serveurs qu'on pourrait vouloir définir vont avoir une boucle
de ce type...

&nbsp;

&nbsp;

**Idée** : abstraire ça dans un objet

&nbsp;

`->` On pourrait coder un objet `Server` que n'importe qui peut
utiliser, sans avoir besoin de comprendre les sockets et les streams

# Boucle d'événements

On peut coder cet objet assez facilement, simplement en déplaçant la
boucle d'événements dans la méthode d'un nouvel objet :

```java
public class Server {
    private ServerSocket server;

    public Server(int port) throws IOException {
        // Crée le serveur sur le port spécifié
        this.server = new ServerSocket(port, 1);
        
        // Attends une connexion...
    }

    public void listen() {
        // *** Boucle d'événements ***
        while ((line = reader.readLine()) != null) {
            // Lire et exécuter la prochaine commande
            this.process(line);
        }
    }
}
```

# Boucle d'événements

On créerait alors un serveur simplement avec :

```java
// Crée le serveur
Server s = new Server(1337);

// Démarre la boucle d'événements
s.listen();
```

`=>` Problème : comment définir les traitements à faire selon les
événements reçus ?

# Boucle d'événements

**Idée 1** : sous-classer la classe `Server` et `@Override` la méthode
qui sert à traiter la prochaine commande reçue :

```java
public class MonServeur extends Server {
    public MonServeur(int port) throws IOException {
        super(port);
    }
    
    @Override
    public void process(String line) {
        String commande = // ...
        String argument = // ...
        
        switch(commande) { // Traitement de la commande
            // un case pour chaque commande...
        }
    }
}
```

# Boucle d'événements

**Idée 2** : on pourrait avoir une liste de méthodes à appeler
lorsqu'une nouvelle commande arrive :

```c
- méthode qui affiche son argument si commande=echo
- méthode qui affiche l'argument inversé si commande=reverse
- méthode qui affiche la date si commande=date
- etc
```


# Boucle d'événements

On peut définir des objets qui contiennent ces méthodes qui réagissent
aux événements : des `EventHandler`s

&nbsp;

Le serveur pourrait avoir une liste de ces objets. Lorsqu'un événement
survient, on peut avertir tous les `EventHandler` et les laisser
réagir au besoin.

```java
Server s = new Server(1337);

// Ajoute les EventHandlers
s.addEventHandler(eventHandlerA);
s.addEventHandler(eventHandlerB);
...
s.addEventHandler(eventHandlerZ);

s.listen();
```

# Boucle d'événements

- On définirait une classe abstraite :

```java

public abstract class EventHandler {

    public abstract void handle(String command, String argument);

}
```

Qui sera *spécialisée en une nouvelle sous-classe* pour chaque type
d'événement

- Du côté du serveur, on aurait par exemple un
  `ArrayList<EventHandler>` qui garde trace des handlers

# Boucle d'événements

```java
public class Server {
    private ServerSocket server;
    private ArrayList<EventHandler> handlers;

    // ... (Constructeur ici) ...

    // Ajoute un handler à la liste
    public void addEventHandler(EventHandler handler) {
        this.handlers.add(handler);
    }

    public void listen() {
        while ((line = reader.readLine()) != null) {
            /* Boucle d'événements : on avertit tous les handlers
               lorsqu'un événement survient */
            this.callHandles(command, argument);
        }
    }
    // ...
```

# Boucle d'événements

```java
    /*
     * Appelle la fonction `handle()` de tous les
     * handlers définis avec la commande et l'argument spécifié
     */
    private void callHandles(String command, String argument) {
        for (EventHandler handler : handlers) {
            handler.handle(command, argument);
        }
    }
}

```

# Boucle d'événements

On peut se définir une nouvelle sous-classe de `EventHandler` pour
chaque événement :

```java
// Fichier DateHandler.java
public class DateHandler extends EventHandler {
    @Override
    public void handle(String command, String argument) {
        if (command.equals("date")) {
            System.out.println(new Date());
        }
    }
}
```

&nbsp;

# Boucle d'événements

On peut se définir une nouvelle sous-classe de `EventHandler` pour
chaque événement :

```java
// Fichier CountHandler.java
public class CountHandler extends EventHandler {
    @Override
    public void handle(String command, String argument) {
        if (command.equals("count")) {
            System.out.println(argument.length());
        }
    }
}
```

Et ainsi de suite pour `ReverseHandler`, `EchoHandler`, ...


# Boucle d'événements

*Avantage de cette abstraction* : notre serveur est alors complètement
défini avec du code beaucoup plus clair et concis !

```java
    public static void main(String[] args) {
        try {
            Server server = new Server(1337);
            
            server.addEventHandler(new EchoHandler());
            server.addEventHandler(new ReverseHandler());
            server.addEventHandler(new DateHandler());
            server.addEventHandler(new CountHandler());
            
            server.listen();
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
```

<!-- Directement passer aux classes anonymes -->

<!-- # Classes internes -->

<!-- - Supposons qu'on a 60 événements à gérer, ça nous fait 60 classes -->
<!-- différentes `=>` 60 fichiers... -->

<!-- &nbsp; -->

<!-- - On peut faire de quoi d'un peu plus joli : définir des *classes -->
<!--   internes* -->
<!--     - Classes définies à l'intérieur d'une autre classe plutôt que -->
<!--       dans un fichier distinct -->

<!-- # Classes internes -->

<!-- ```java -->
<!-- public class JavaServer4 { -->
<!--     public static void main(String[] args) { -->
<!--         Server server = new Server(1337); -->
    
<!--         server.addEventHandler(new EchoHandler()); -->
<!--         server.addEventHandler(new ReverseHandler()); -->
<!--         // ... -->
<!--         server.listen(); -->
<!--     } -->
<!--     public static class EchoHandler extends EventHandler { -->
<!--         // ... -->
<!--     } -->
<!--     // ... -->
<!-- } -->
<!-- ``` -->

<!-- \footnotesize -->

<!-- *Note sur le `static class`* : il existe également des classes -->
<!-- internes non-static, mais le but du cours n'est pas d'analyser tous -->
<!-- les recoins des détails d'implantation de Java... Comprennez le -->
<!-- concept général de définir une classe dans une autre classe, pas -->
<!-- besoin de vous attarder sur le mot-clé ici. -->

# Boucle d'événements

Mais...

&nbsp;

&nbsp;

On se rend compte qu'on a besoin de définir beaucoup de sous-classes
de `EventHandler`... pour les utiliser une seule fois dans tout le
programme !

&nbsp;

Si on a 30 événements à gérer \newline `=>` 30 classes définies dans 30
fichiers éparpillés dans le projet

# Classe anonyme

Pour simplifier ça on peut profiter du concet de **classe
anonyme**. \newline Il s'agit d'une classe...

- Qui ne porte pas de nom
- **Concrète** (toutes les méthodes implantées, rien d'`abstract`)
- Qui **hérite** d'une autre classe (typiquement abstraite)
  \underline{ou} qui **implémente** une interface
- Qui est définie seulement *au moment de l'utiliser* (directement
  dans la méthode qui l'utilise) :

```java
    EventHandler dateHandler = new EventHandler() {
        @Override
        public void handle(String command, String argument) {
            if (command.equals("date")) {
                System.out.println(new Date());
            }
        }
    };
```

# Classe anonyme

```java
public static void main(String[] args) {
    Server server = new Server(1337);

    // Définition + ajout d'un événement
    server.addEventHandler(new EventHandler() {
        @Override
        public void handle(String command, String argument) {
            if (command.equals("echo")) {
                System.out.println(argument);
            }
        }
    });

    // Autre événement...
    server.addEventHandler(new EventHandler() {
        // ...
    });

    // ...
```


# Interfaces fonctionnelles

Ça reste cependant visuellement un peu lourd...

&nbsp;

En JavaScript, on peut passer simplement une fonction en paramètre à
d'autres fonctions, ce qui est utile pour définir des actions lors
d'événements :

```javascript
function pageClicked() {
    alert('Clic sur la page !');
}

document.addEventListener("click", pageClicked);

// Fonction anonyme
document.addEventListener("click", function() {
    alert("Clic sur la page !");
});
```

# Interfaces fonctionnelles

- Pour avoir de quoi de plus joli que des classes/interfaces anonymes,
  Java 8 définit du *sucre syntaxique* pour ça.

- Si on définit notre `EventHandler` comme étant une interface plutôt
qu'une classe abstraite, on peut la qualifier d'**interface
fonctionnelle**.

- Une interface fonctionnelle doit avoir *exactement une seule méthode
  sans implantation* et utilise l'annotation `@FunctionalInterface`.

```java
@FunctionalInterface
public interface EventHandler {

    public void handle(String command, String argument);

}
```

# Interfaces fonctionnelles

Une fois qu'on a défini notre interface fonctionnelle, Java fait un
peu de magie pour nous permettre de simplifier l'écriture de nos
classes anonymes :

```java
    Server server = new Server(1337);

    // Définition + ajout d'un événement
    server.addEventHandler((cmd, arg) -> {
        if (cmd.equals("echo")) {
            System.out.println(arg);
        }
    });
```

# Interfaces fonctionnelles

## Question....

- Est-ce que `cmd` et `arg` peuvent maintenant être de n'importe quel
  type ? On ne spécifie pas que ça doit être des `String` !


```java
    server.addEventHandler((cmd, arg) -> {
        if (cmd.equals("echo")) {
            System.out.println(arg);
        }
    });
```

# Interfaces fonctionnelles

## Réponse

- Pas du tout !
- On parle de *sucre syntaxique* parce que ça n'impacte que la
  *syntaxe* du langage
- Java fait de l'*inférence de type* pour deviner ce qui devrait être
  écrit à la place de notre fonction anonyme \newline `(cmd, arg) -> { ... }`
- Après une petite analyse, on se rend compte que la seule chose
  valide à cet endroit est un : \newline \centerline{\texttt{new EventHandler() \{ ... \}}} \newline qui
  contient une seule fonction : \newline \centerline{\texttt{public void handle(String cmd, String arg) \{...\}}}

# Interfaces fonctionnelles

En bref, quand Java lit :

```java
    server.addEventHandler((cmd, arg) -> {
        if (cmd.equals("echo")) {
            System.out.println(arg);
        }
    });




```

&nbsp;

# Interfaces fonctionnelles

Java analyse le contexte et comprend que ça veut dire :

```java
    server.addEventHandler(new EventHandler() {
        @Override
        public void handle(String cmd, String arg) {
            if (cmd.equals("echo")) {
                System.out.println(arg);
            }
        }
    });
```

`=>` Ça nous permet d'écrire la même chose, avec moins de code

# \textcolor{gray}{Autre question...}

- \textcolor{gray}{Qu'est-ce qui se passe si on essaie de faire un long calcul lors
  d'un événement ?}


```java
    server.addEventHandler((cmd, arg) -> {
        if (cmd.equals("fibonacci")) {
            /* Fonction qui prend 3 semaines
               à s'exécuter... */
            fibonacciRecursif(80);
        }
    });
```

\textcolor{gray}{Envoi des commandes au serveur :}

```
fibonacci
date
echo salut !
```


# \textcolor{gray}{Autre question...}

\textcolor{gray}{Le serveur ne peut exécuter qu'une seule requête à la
fois... Si quelqu'un envoie une commande qui prend beaucoup de temps à
s'exécuter, tout le monde doit attendre}

&nbsp;

&nbsp;

\textcolor{gray}{Solution : *Multithreading* (qu'on verra plus tard,
si le temps le permet)}
