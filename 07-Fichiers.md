---
title: Fichiers et Streams
author: Par Nicolas Hurtubise$\newline$\footnotesize{Basé en partie sur les notes de Jian-Yun Nie}
header-includes:
    - \usepackage{textcomp}
date: IFT1025 - Programmation 2
---

# Au programme...

- Mémoire vive vs Mémoire persistante
- Systèmes de fichiers
- Fichiers
- Streams
- Sérialisation
- Lecture/Écriture non séquentielle & Index


# Mémoire vive

- Un programme a généralement besoin de stocker des informations
  durant son exécution
    - Variables
    - Tableaux et objets alloués en mémoire
    - ...

```java
int a = 10; // Besoin de noter la valeur 10
int b = 100;

// Besoin d'accéder à la valeur stockée sous le nom "a"
System.out.println(a);
```

- À la fin d'un programme, la mémoire utilisée pour stocker les
  variables et autres objets alloués est récupérée pour les prochains
  programmes qui vont s'exécuter


# Mémoire vive

- Toute cette information doit être stockée physiquement quelque part
- `=>` dans la *Mémoire vive* de l'ordinateur
    - En anglais, *RAM* : *Random Access Memory*

\center

![](img/ram.jpg){width=60%}&nbsp;

# Mémoire vive

- La mémoire vive permet accès à des endroits **arbitraires** et
  **aléatoires**
    - Les accès se font **rapidement**, mais il s'agit de mémoire
      **volatile**
    - Si on coupe l'alimentation électrique, les bits d'information
      stockés s'effacent

# Mémoire persistante

- On ne peut donc pas se fier à la *RAM* pour conserver des données
  sur le long terme...
- On a besoin d'une composante de l'ordinateur qui peut conserver des
  données indépendamment de si l'ordinateur est alimenté électriquement
  ou non

&nbsp;

- On a besoin de  **mémoire persistante**
    - Peut prendre plusieurs formes : Ruban magnétique, Disquette,
      Disque Dur, Mémoire flash, ...
    - Typiquement, l'accès à des données sur un support persistant
      sera plus lent que pour de la mémoire vive

# Mémoire persistante

![Agrillo Mario, https://commons.wikimedia.org/wiki/File:Disque_dur_0005.JPG](img/hdd.jpg)

# Système de fichiers

- Similairement aux variables, tableaux et objets qui permettent
  d'abstraire des adresses dans la mémoire vive, le **Système de
  fichiers** permet d'avoir une abstraction de la mémoire persistante

# Système de fichiers

- Le *système de fichiers* a une structure d'arbre qui contient :
    - Des *fichiers* (des données)
    - Des *répertoires*, qui regroupent des fichiers + d'autres
      répertoires

# Système de fichiers

![](img/gv/08.fs.png)&nbsp;


# Fichier

- Un fichier est simplement une séquence ordonnée de *bytes*
    - 1 *byte* = 8 bits (en français : 1 octet = 8 bits)
- Les *bytes* d'un fichier peuvent représenter du texte, une image, du
  code machine à exécuter, une chanson, ou quoi que ce soit d'autre

\center

![](img/gv/08.mem.png)&nbsp;


# Fichier

- Sur les systèmes d'exploitation basés sur `Unix` (GNU/Linux, Mac,
  ...), la commande `cat` permet d'afficher
  *textuellement*\footnote{plus précisément, de con`cat`éner le
  contenu des fichiers} le contenu des fichiers passés en argument


```java
$ cat hello
Bonjour tout le monde !

$ cat hello sava hello
Bonjour tout le monde !
Comment allez-vous ?
Bonjour tout le monde !
```


# Fichier

- Afficher "textuellement" un fichier correspond simplement à
  interpréter les bytes qui le constituent comme étant une suite de
  caractères (ASCII, UTF-8 ou autre)

# Fichier

- La commande `od` permet d'afficher les bytes du fichier dans un
  format octal, hexadécimal, décimal ou autre (`od` = octal dump)

```bash
$ od --endian=big -x hello
0000000  426f    6e6a    6f75    7220    746f    7574    206c    6520
0000020  6d6f    6e64    6520    210a
0000030
```

# Fichier

- On peut combiner plusieurs options :

```bash
$ od --endian=big -x -c hello
0000000   426f    6e6a    6f75    7220    746f    7574    206c    6520
         B   o   n   j   o   u   r       t   o   u   t       l   e
0000020   6d6f    6e64    6520    210a
         m   o   n   d   e       !  \n
0000030
```

# Fichier

- On peut tenter d'afficher le contenu textuel de n'importe quel
  fichier (ce qui ne donne pas un résultat particulièrement
  intéressant)


```bash
$ od --endian=big -x -c RobotFindsKitten.class
0000000   cafe    babe    0000    0034    009a    0a00    2400    4b07
       312 376 272 276  \0  \0  \0   4  \0 232  \n  \0   $  \0   K  \a
0000020   004c    0900    4d00    4e0a    0002    004f    0900    4d00
        \0   L  \t  \0   M  \0   N  \n  \0 002  \0   O  \t  \0   M  \0
0000040   5008    0051    0a00    5200    5308    0054    0700    550a
```

# Fichiers en Java

En Java, on peut manipuler un fichier avec la classe `FileReader` :

```java
    try {
        FileReader fr = new FileReader("hello");
        int value = 0;
        char c;

        while (true) {
            value = fr.read(); // Lit 1 caractère du fichier
            if(value == -1) // -1 est la valeur pour "Fin de fichier"
                break;
            c = (char) value;
            System.out.print(c);
        }
        fr.close();
    } catch (IOException ex) {
        System.out.println("Erreur à l'ouverture du fichier");
    }
// Affiche "Bonjour tout le monde !" caractère par caractère
```

# Fichiers en Java

Variante :

```java
    try {
        FileReader fr = new FileReader("hello");

        int value = 0;
        char c;

        while ((value = fr.read()) != -1) {
            c = (char) value;

            System.out.print(c);
        }

        fr.close();
    } catch (IOException ex) {
        System.out.println("Erreur à l'ouverture du fichier");
    }
```

# Fichiers en Java

- `FileReader` est un objet assez bas niveau...
    - `read()` permet de lire un caractère à la fois, retourne un `int`
    - `read(char[] c)` permet de lire plusieurs caractères à la fois
      dans un tableau de `char` (pas directement une `String`)

&nbsp;

- `BufferedReader` est une classe fournie qui permet d'abstraire un
  peu tout ça et de plutôt lire un fichier ligne par ligne, au format
  `String`

# Fichiers en Java

On peut lire une ligne d'un fichier en `String` avec
`bufferedReader.readLine()` :

```java
    try {
        FileReader fr = new FileReader("hello");
        
        /* BufferedReader s'occupe de lire les caractères
           un à la fois et d'en faire une String pour nous */
        BufferedReader reader = new BufferedReader(fr);

        String s = reader.readLine();
        System.out.println(s);

        reader.close();
    } catch (IOException ex) {
        System.out.println("Erreur à l'ouverture du fichier");
    }
```

# Fichiers en Java

Ou toutes les lignes en répétant `readLine()` jusqu'à trouver `null` :

```java
    try {
        FileReader fr = new FileReader("hello");
        BufferedReader reader = new BufferedReader(fr);

        String s;
        while ((s = reader.readLine()) != null) {
            System.out.println(s);
        }

        reader.close();
    } catch (IOException ex) {
        System.out.println("Erreur à l'ouverture du fichier");
    }
```

# Fichiers en Java

De la même façon, on peut écrire un caractère à la fois dans un
fichier avec un `FileWriter` :

```java
    try {
        FileWriter fw = new FileWriter("lettres.txt");

        for (char c = 'a'; c < 'z'; c += 2) {
            fw.write(c);
        }

        fw.close();
    } catch (IOException ex) {
        System.out.println("Erreur à l'écriture du fichier");
    }
```

`=>` Écrit : "acegikmoqsuwy" dans le fichier lettres.txt

# Fichiers en Java

Ou plus simplement avec un `BufferedWriter` :

```java
    try {
        FileWriter fw = new FileWriter("lettres.txt");
        BufferedWriter writer = new BufferedWriter(fw);

        String s = "ABC";
        writer.append(s);

        writer.close();
    } catch (IOException ex) {
        System.out.println("Erreur à l'écriture du fichier");
    }
```

`=>` Écrit : "ABC" dans le fichier lettres.txt

# Bug commun

Si on essaie d'exécuter ce code :

```java
    try {
        FileWriter fw = new FileWriter("lettres.txt");
        BufferedWriter writer = new BufferedWriter(fw);

        String s = "Bonjour !";

        writer.append(s);

    } catch (IOException ex) {
        System.out.println("Erreur à l'écriture du fichier");
    }
```

On se retrouve avec un fichier `lettres.txt` vide... Pourquoi ?

# Bug commun

- Écrire des bytes dans un fichier est une opération (relativement)
  coûteuse
- `BufferedWriter` utilise un tampon en mémoire vive (un `buffer`)
  pour accumuler des bytes à écrire, plutôt que de les écrire
  directement sur le disque dur à chaque "write"

&nbsp;

- De temps en temps, le tampon est vidé et les bytes accumulés sont
  écrits d'un coup dans le fichier
- Si le programme se termine trop vite, le `BufferedWriter` peut
  encore avoir des bytes accumulés dans son buffer qui ne seront pas
  écrits dans le fichier


# Fermeture des fichiers

- Très important de **fermer le fichier** une fois qu'on a terminé de
  lire/écrire
    - Ouvrir un fichier demande d'allouer des ressources au niveau du
      système d'exploitation
    - Important de redonner les ressources aussitôt qu'on a terminé
    - Également, un `BufferedWriter` pourrait ne pas terminer d'écrire
      les bytes temporairement stockés dans son tampon mémoire si
      jamais on termine le programme avant d'avoir fermé le fichier
      proprement

```java
    // Ouverture du fichier
    BufferedWriter writer = new BufferedWriter(new FileWriter("..."));

    // Écritures dans le fichier tant qu'on en a besoin
    // ...

    // Fermeture du fichier
    writer.close(); // Très important, dès qu'on a terminé !
```

# Stream

- On peut généraliser le concept de fichier à une *séquence quelconque
  de bytes qui arrivent dans un certain ordre*
    - Des données transmises sur un réseau
    - Les caractères entrés au clavier par un utilisateur
    - L'output d'un programme
    - ...

&nbsp;

- C'est le concept de **Stream**

# Stream

## Hiérarchie

```java
java.lang.Object
    -> java.io.Reader (implements Closeable, Readable)
        -> java.io.InputStreamReader
            -> java.io.FileReader

    -> java.io.Writer (implements Appendable, Closeable, ...)
        -> java.io.OutputStreamWriter
            -> java.io.FileWriter
```

# Stream

- L'objet `Scanner` permet de lire des informations depuis un *stream*
  quelconque :

```java
Scanner scan = new Scanner(System.in);
```

- `System.in` correspond à un *stream* : l'*entrée standard* de notre
  programme, qui sera (par défaut) le texte entré sur la ligne de
  commande
- On peut cependant utiliser un `Scanner` sur n'importe quel stream :

```java
Scanner scan = new Scanner(new FileInputStream("numbers.txt"));
```

# `Scanner` pour lire un fichier

## Fichier scores.txt

Exemple : High scores pour un jeu de Pac-Man

```python
310490 MasterWhooper
288425 JimmyW
18825 JJWhoopbraham
4225 jimmythekid
1225 thejaydawg123
725 aaaaaaaaaaaa
...
```

# `Scanner` pour lire un fichier

```java
try {

    Scanner scan = new Scanner(new File("high-scores.txt"));

    while (scan.hasNext()) {

        // Lire le score (nombre entier)
        int score = scan.nextInt();
        // Lire le reste de la ligne : espace + nom du high score
        String nom = scan.nextLine().substring(1);

        System.out.println(nom + " a eu " + score + " points");
    }

} catch (FileNotFoundException ex) {
    System.out.println("Erreur à l'ouverture du fichier");
}
```

# `Scanner` pour lire un fichier

## Résultat

```java
MasterWhooper a eu 310490 points
JimmyW a eu 288425 points
JJWhoopbraham a eu 18825 points
jimmythekid a eu 4225 points
thejaydawg123 a eu 1225 points
aaaaaaaaaaaa a eu 725 points
```

&nbsp;


# Format textuel vs format binaire

- Stocker des données en binaire ou en caractères ?
    - `12345` (int) vs `"12345"` (String)
- `Scanner` lit un format textuel : interprète une suite de `char`
  comme des caractères textuels
    - `"12345"` `=>` 5 chars `=>` 5 x 16 bits `=>` 10 bytes pour
      stocker un petit nombre
- On pourrait vouloir gagner de l'espace en utilisant les bytes plus
  efficacement
    - `(int) 12345` se stocke 32 bits, donc seulement 4 bytes
    - `(short) 12345` se stocke 16 bits `=>` seulement 2 bytes

# Format textuel vs format binaire

- On peut utiliser un stream de type `DataOutputStream` avec les
  méthodes `writeInt(n)`, `writeByte(n)`, `writeShort(n)`,
  `writeDouble(n)` ... pour écrire des données brutes, dans leur
  format binaire natif

&nbsp;

- Similairement, un `DataInputStream` permet de lire des données
  brutes sur un stream avec `readInt()`, `readByte()`, `readDouble()`, ...

# Format textuel vs format binaire

```java
try {
    FileOutputStream fileOutputStream =
        new FileOutputStream("puissances.txt");

    DataOutputStream output = new DataOutputStream(fileOutputStream);

    for (int i = 0; i < 30; i++) {
        // Écrit les puissances de 2 dans un fichier
        output.writeInt(1 << i);
    }

    output.close();
} catch (IOException ex) {
    System.out.println("Erreur à l'ouverture du fichier");
}
```

# Format textuel vs format binaire

```bash
$ od --endian=big -x -d puissances.txt

000000  0000  0001  0000  0002  0000  0004  0000  0008
           0     1     0     2     0     4     0     8
000020  0000  0010  0000  0020  0000  0040  0000  0080
           0    16     0    32     0    64     0   128
000040  0000  0100  0000  0200  0000  0400  0000  0800
           0   256     0   512     0  1024     0  2048
000060  0000  1000  0000  2000  0000  4000  0000  8000
           0  4096     0  8192     0 16384     0 32768
000100  0001  0000  0002  0000  0004  0000  0008  0000
           1     0     2     0     4     0     8     0
000120  0010  0000  0020  0000  0040  0000  0080  0000
          16     0    32     0    64     0   128     0
000140  0100  0000  0200  0000  0400  0000  0800  0000
         256     0   512     0  1024     0  2048     0
000160  1000  0000  2000  0000
        4096     0  8192     0

```

# Idée : écrire un objet dans un fichier

- On pourrait prendre une instance d'une classe et sauvegarder toutes
  ses propriétés dans un fichier

```java
class Rectangle {
    private int largeur, hauteur;

    // ...
}
```

```java
Rectangle r = new Rectangle(640, 480);
r.sauvegarder("rectangle.dat");

// ...

Rectangle r = Rectangle.charger("rectangle.dat");
System.out.println(r.getLargeur());
// => Afficherait 640
```

# Idée : écrire un objet dans un fichier

```java
public class Rectangle {
    // ...
    public void sauvegarder(String filename) {
        try {
            FileOutputStream fileOutputStream =
                new FileOutputStream(filename);
            DataOutputStream output =
                new DataOutputStream(fileOutputStream);

            output.writeInt(largeur); // ***
            output.writeInt(hauteur); // ***

            output.close();
        } catch (IOException ex) {
            System.out.println("Erreur à l'ouverture du fichier");
        }
    }
}
```

# Idée : écrire un objet dans un fichier

```java
public static Rectangle charger(String filename) throws IOException {

    FileInputStream fileInputStream = new FileInputStream(filename);
    DataInputStream input = new DataInputStream(fileInputStream);

    // Lecture des attributs dans le même ordre
    int largeur = input.readInt(); // ***
    int hauteur = input.readInt(); // ***

    input.close();

    return new Rectangle(largeur, hauteur);
}
```

# Idée : écrire un objet dans un fichier

```java
    Rectangle r = new Rectangle(640, 480);
    r.sauvegarder("rectangle.dat");

    /* Le fichier rectangle.dat contient :
       ^@^@^B\200^@^@^@^A\340 */

    // ...
    // plus loin, dans une autre fonction :
    try {
        Rectangle r = Rectangle.charger("etudiant.dat");

        System.out.println(r.getLargeur());
    } catch (IOException ex) {
        System.out.println("Erreur à la lecture du rectangle");
    }
```

# Sérialisation et Désérialisation

- Prendre une structure complexe (objet, tableau, ...), et la
  transformer en une suite de bytes pour la noter dans un fichier,
  l'envoyer sur le réseau ou autre est ce qu'on appelle de la
  **sérialisation**

&nbsp;

- Le processus inverse, interpréter une suite de bytes comme étant une
  instance particulière d'une structure complexe s'appelle la
  **désérialisation**

# Sérialisation en Java

Java met à disposition un mécanisme pour sérialiser un objet :

1. `implements Serializable` (aucune méthode à implémenter)

```java

public class Etudiant implements Serializable {

    private String prenom, nom;
    private int matricule;

    public Etudiant(String prenom, String nom, int matricule) {
        this.prenom = prenom;
        this.nom = nom;
        this.matricule = matricule;
    }
}
```

# Sérialisation en Java


2. Utiliser un `ObjectOutputStream` pour écrire l'objet sur un stream
   dans son format sérialisé :

```java
    Etudiant e = new Etudiant("Jimmy", "Whooper", 1239572);

    try {
        FileOutputStream fileOs =
            new FileOutputStream("etudiant.dat")

        ObjectOutputStream os = new ObjectOutputStream(fileOs);

        os.writeObject(e);

        os.close();
    } catch (IOException ex) {
        System.out.println("Erreur à l'écriture");
    }
```

# Sérialisation en Java

`etudiant.dat` contient alors quelque chose du genre de :

```java
??^@^Esr^@^NEtudiant??^P^B^@^CI^@	matriculeL^@^Cnomt^@^RL
java/lang/String;L^@^Fprenomq^@~^@^Axp^@^R?^Tt^@^GWhoopert^@^EJimmy
```

# Désérialisation en Java

On peut récupérer l'instance d'`Etudiant` via :

```java
try {
    FileInputStream fileIs = new FileInputStream("etudiant.dat");
    ObjectInputStream is = new ObjectInputStream(fileIs);

    Etudiant e = (Etudiant) is.readObject();

    System.out.println(e.nomComplet());
    // => "Jimmy Whooper"

} catch(ClassNotFoundException ex) {
    System.out.println("La classe lue n'existe pas dans le programme");
} catch (IOException ex) {
    ex.printStackTrace();
    System.out.println("Erreur à la lecture du fichier");
}
```


# Sérialisation

- Attention ! Ça brise l'encapsulation...
    - N'importe qui peut maintenant modifier les bytes qui composent
      l'objet sérialisé et reconstruire une version de notre objet
      dont l'état n'est pas cohérent
- Également dangereux pour la maintenabilité : incompatibilités
  possibles entre les versions des objets sérialisés
    - Si on veut pouvoir désérialiser un objet, l'implantation de la
      classe doit rester **intacte** (pas de nouveaux attributs, pas
      d'attributs manquants)

# Lecture/Écriture non séquentielle

- Dans certains contextes, on peut vouloir lire et écrire des données
  de façon non séquentielle
    - Exemple : la liste de mots d'un dictionnaire
    - On veut faire une recherche dichotomique pour trouver si un mot
      existe ou non, et non tout lire depuis le début à chaque fois

# Lecture/Écriture non séquentielle

Pour un petit fichier, on aurait toujours l'option de :

1. Charger le contenu en mémoire (ex.: toutes les lignes dans un
`ArrayList<String>` ou une autre structure de données)
2. Faire les lectures/modifications/insertions de lignes...
3. Sauvegarder le contenu de l'`ArrayList<>` dans le fichier

# Lecture/Écriture non séquentielle

- Pour des très gros fichiers, ça peut ne pas être une option viable
    - ~60 000 mots d'un dictionnaire
    - Une liste d'informations sur 250,000 utilisateurs
    - ...
- On risque de faire exploser la mémoire en chargeant tout ça d'un
  coup !

```python
# Fichier dictionnaire.txt : contient tous les mots de l'anglais
a
about
after
all
also
an
any
...
zoomba
```

# `RandomAccessFile`

Un `RandomAccessFile` permet de lire un fichier de façon non
séquentielle

&nbsp;

On crée un `RandomAccessFile` en spécifiant le *mode* d'accès au
fichier dont on a besoin :

```java
/* Si on veut seulement lire le fichier,
   sans le modifier : "r" => read-only */
RandomAccessFile dict = new RandomAccessFile("dict.txt", "r");

/* Si on veut lire + écrire dans le fichier,
   "rw" => read-write */
RandomAccessFile dict = new RandomAccessFile("dict.txt", "rw");
```



# `RandomAccessFile`

- Un `RandomAccessFile` permet de lire différents types de données
  dans un fichier tout en gardant un *curseur* sur la position dans le
  fichier
    - `readInt()` : lit 32 bits dans un `int`
    - `readLong()` : lit 64 bits comme un `long`
    - `readDouble()`, `readChar()`, `readBoolean()`, ...
    - `readLine()` : lit la prochaine ligne du fichier (de la position
      actuelle jusqu'au prochain `\n`)

&nbsp;

- Chaque lecture/écriture de données fait avancer le curseur
- On peut connaître la position du curseur (en `long`) avec
  `getFilePointer()`
- On peut déplacer le curseur vers l'avant ou vers l'arrière avec la
  méthode `seek(long n)`
    - Où `n` est la position dans le fichier (0 = début du fichier)

# `RandomAccessFile`

```java
try {
    RandomAccessFile mots = 
        new RandomAccessFile("dictionnaire.txt", "r");

    mots.seek(8); // Avance au 8ème byte du fichier

    System.out.println(mots.readLine());
    // => "after"

} catch (IOException ex) {
    ex.printStackTrace();
}
```


# `RandomAccessFile`

Comment connaître la position à laquelle `seek`er pour retrouver un enregistrement ?

&nbsp;

- La seule chose qu'on peut faire est de `seek`er à un index arbitraire
dans le fichier...
    - Comment trouver le $Nième$ mot de notre dictionnaire ?
    - Comment trouver le $Nième$ enregistrement

# 1. Aligner les entrées

- Solution simple : donner une taille fixe à chaque entrée du fichier
  pour aligner les mots
- Index du $N^{ième}$ bloc = $N *$ taille d'un bloc

[columns]

[column=0.55]

- Limité dans la quantité d'informations qu'on peut stocker par bloc
  selon une taille maximal choisie d'avance

&nbsp;

- Possiblement beaucoup d'espace gaspillé en bytes d'alignement

[column=0.45]

```python
# Fichier dictionnaire.txt où
# chaque mot est stocké avec
# une taille fixe (max 16 char)
# . = byte nul 0x0
a...............about.......
....after...........all.....
........also............an..
............any.............
as.............. [etc]
```

[/columns]

# 2. Structure d'index

- Autre solution : construire un **index**
- Garder un deuxième fichier qui contient la liste du début de chaque
  entrée (où chaque adresse est stockée avec une taille fixe, ex: 4 bytes)

[columns]

[column=0.5]

```python
# Fichier dictionnaire.txt où
# les mots sont stockés sans
# contrainte de taille
aaboutafterallalsoananyas [etc]



```

[column=0.5]

```python
# Fichier dictionnaire.dat qui
# contient les index de chaque
# mot sur 32 bits = 4 bytes
# (affiché en hexadécimal ici)
0000 0001 0006 000B 000E 0012
0014 0017   [etc]
```

[/columns]

# 2. Structure d'index

- On peut imaginer des structures d'index plus complexes...

- Ex.: pour un fichier qui contient des `Etudiant` sérialisés

```java
      ??^@^Esr^@^NEtudiant??^P^B^@^CI^@matriculeL^@^Cno
      mt^@^RLjava/lang/String;L^@^Fprenomq^@~^@^Axp^@^R
      ?^Tt^@^GWhoopert^@^EJimmy??^@^Esr^@^NEtudiant??^P
      ^B^@^CI^@matriculeL^@^Cnomt^@^RLjava/lang/String;
      L^@^Fprenomq^@~^@^Axp^@^R?^Tt^@^GWhoopert^@^EXavi
      er [etc]
```

- Rien ne nous empêche de construire plusieurs index, ordonnés selon
  ce qui nous intéresse !
    - Trouver un étudiant rapidement par son prénom ?
    - Trouver un étudiant rapidement par son nom de famille ?
    - Trouver un étudiant rapidement par son matricule ?
    - Ordonner les étudiants par ordre alphabétique Nom + prénom ?
      (Ex.: "J. Whooper" avant "X. Whooper")


<!-- - Stdin / Stdout -->

<!-- En Java: System.in, System.out -->

<!-- - FiltrerNombres.java -->

<!-- - SommeNombres.java -->

<!-- - nombres-aleatoires.js -->

<!-- - cowsay -->


<!-- - Fichiers spéciaux -->
<!--   - /dev/null -->
<!--   - /dev/full -->
<!--   - /dev/urandom -->
