---
title: Algorithmie et tris
author: Par Nicolas Hurtubise$\newline$\footnotesize{Avec quelques bouts sur l'algorithmie tirés des slides de Pierre McKenzie}
header-includes:
    - \usepackage{textcomp}
date: IFT1025 - Programmation 2
---

# Trier des données

\center

- Pourquoi trier ?
- Comment trier ?


# Tri par sélection (Selection sort)

\center

*Explication du Tri par sélection en classe*

&nbsp;

https://en.wikipedia.org/wiki/Selection_sort

```
3 8 1 4 2 9 5 6
```

# Tri par insertion (Insertion sort)

\center

*Explication du Tri par insertion en classe*

&nbsp;

https://en.wikipedia.org/wiki/Insertion_sort

```
3 8 1 4 2 9 5 6
```


# Tri à bulles (Bubble sort)

\center

*Explication du Tri à bulles en classe*

&nbsp;

https://en.wikipedia.org/wiki/Bubble_sort

```
3 2 8 1 4 2 9 5
```

# Est-ce qu'on peut faire autrement ?

Notre plus gros problème est qu'en pire cas, ces tris doivent comparer
chaque élément avec tous les $n$ autres...

&nbsp;

- Si on a un ensemble de 128 éléments, en pire cas, on a 128
  comparaisons à faire par élément

- Si on a plutôt un ensemble de 64 éléments, en pire cas, on a
  seulement 64 comparaisons à faire par élément...

- Observation : si on a deux listes déjà triées, les fusionner en une
  seule liste triée peut se faire en parcourant les listes une seule
  fois


# Est-ce qu'on peut faire autrement ?

## Idée : diviser pour régner

<!-- http://www.bowdoin.edu/~ltoma/teaching/cs231/spring14/Lectures/6-moresorting/sortLB.pdf -->

Trier 128 éléments :

- Équivalent à trier 2 ensembles de 64 éléments + fusionner les
  deux ensembles

\pause

Trier 64 éléments :

- Équivalent à trier 2 ensembles de 32 éléments + fusionner les
  deux ensembles

\pause

Trier 32 éléments + fusionner...

Trier 16 éléments + fusionner...

...

Trier 2 éléments + fusionner...

Trier 1 élément : trivial

&nbsp;

# Tri par fusion (Merge sort)


\center

*Explication du Tri par fusion en classe*

&nbsp;

https://en.wikipedia.org/wiki/Merge_sort

```
3 2 8 1 4 2 9 5
```

# Tri rapide (Quicksort)

\center

*Explication du Tri rapide en classe*

&nbsp;

https://en.wikipedia.org/wiki/Quicksort

&nbsp;

Explication interactive ici : http://me.dt.in.th/page/Quicksort/

```
3 2 8 1 4 2 9 5
```

1. Avec des nouveaux tableaux
2. *In situ*

# Tri par monceau (Heapsort)

\center

*Explication du Tri par monceau en classe*

&nbsp;

https://en.wikipedia.org/wiki/Heapsort

```
3 2 8 1 4 2 9 5
```

1. Avec un monceau explicite
2. *In situ*


# Performance d'un algorithme

- Les algorithmes ne sont pas tous également efficaces
- Comment comparer les différents algos entre eux ?

# Performance d'un algorithme

Une solution simple est le *Benchmark* (l'analyse expérimentale)

1. Implémenter l'algorithme en Java ou dans un autre langage
2. Tester l'algo sur des données de tailles différentes 
    - ex.: trier des listes de 50, 100, 150, 200, ... éléments
3. Mesurer la différence de temps entre le début de l'exécution et la
   fin
4. Regarder graphiquement les résultats

# Performance d'un algorithme

```java
public class QuickSortBenchmark {
    public static void quicksort(...) { ... }

    public static long benchmark(int[] tab) {
        long timeBefore = System.nanoTime();

        quicksort(tab, 0, tab.length - 1);

        long timeAfter = System.nanoTime();
        return timeAfter - timeBefore;
    }

    public static void main(String[] args) {
        for (int i = 1; i < 10000; i++) {
            int[] tab = randomIntegers(i);
            System.out.println(i + " " + benchmark(tab));
        }
    }
}
```

# Performance d'un algorithme

\center

![Temps d'exécution : Tri par sélection vs Tri rapide](img/sort-benchmark.png)&nbsp;

# Performance d'un algorithme

Limitations :

- On doit implémenter l'algorithme au complet pour le tester
    - On veut possiblement connaître la complexité en temps d’un
      algorithme avant de l’implémenter, question de sauver du temps
      et de l'argent...
- Les résultats trouvés ne sont pas représentatifs de toutes les entrées
    - Limité par les tests qu'on a fait
- Pour comparer 2 algorithmes différents pour le même problème, on
  doit utiliser exactement le même environnement (hardware, software)

# Analyse théorique d'un algorithme

Une autre option est de compter le nombre d'opérations de l'algorithme
en fonction de la taille de son entrée

&nbsp;

C'est ce qu'on appelle *l'analyse théorique* :

- Se fait à partir du pseudo-code de l’algorithme et non de
  l'implémentation
- Caractérise le temps d’exécution comme une fonction de $n$, la
  taille de l'entrée
- Prend en considération toutes les entrées
- Indépendant de l'environnement utilisé (hardware, software)

# Analyse théorique d'un algorithme

Pseudo-code pour un algorithme quelconque :

```javascript
function truc(int n) {
    print "Compteurs :" // une opération

    for i=1...n {       // répéter n fois
    
        for j=1...10 {       // répéter 10 fois
        
            print(j)               // une opération

        }
    }
}
// Nombre total d'opérations : 1 + n * 10 * 1 = 10n + 1 opérations
```

- Élément clé : les boucles
    - Répéter { ... } X fois `=>` multiplication par X

# Analyse théorique d'un algorithme

Combien d'opérations pour exécuter une tâche ?

&nbsp;

`=>` Dépend de ce qu'on définit comme étant une *opération*

- Addition/soustraction
- Accès dans un tableau
- Comparaison de deux nombres
- ...

Exemples :

- Calculer la somme des nombres de 1 jusqu'à $n$
    - requiert $n$ opérations
- Compter $n$ fois jusqu'à $n$
    - requiert $n^2$ opérations


# Analyse théorique d'un algorithme

Ce qui nous intéresse est la façon dont l'algo performe quand il y a
*de plus en plus* de données à traiter...

&nbsp;

Si $n$ est deux fois plus gros, combien d'opérations de plus ça prend
?

- 2 fois plus de temps ?
    - Linéraire : $n$ éléments `=>` $n$ opérations
- 4 fois plus de temps ?
    - Carré : $n$ éléments `=>` $n^2$ opérations

&nbsp;

On s'intéresse à l'**ordre de grandeur** plutôt qu'au nombre exact
d'opérations

# Analyse théorique d'un algorithme

```java
long somme = 0;
for(int i=0; i<n; i++) {
    somme += i;
}
```

- Somme des nombres 1 à $n$
    - $n$ opérations

- Somme des nombres 1 à $2n$
    - $2n$ opérations
    - Si $n$ double, le nombre d'opérations double
    - Croissance : linéaire

- Somme des nombres 1 à $n + 10$
    - $n + 10$ opérations
    - Si $n$ double, le nombre d'opérations double, à une petite constante près (le $+ 10$)
    - Donc la croissance est également linéaire


# Notation Big-O

On peut utiliser la *notation Big-O* pour comparer les algorithmes :

$$O(f(n))$$

&nbsp;

La notation *Big-O* décrit une *borne supérieure asymptotique* pour le
temps d'exécution d'un algorithme.

&nbsp;

On peut donc l'utiliser pour indiquer l'ordre de grandeur du **pire
cas** d'un algorithme.

&nbsp;

On peut également s'intéresser à d'autres facteurs, (ex.: le temps
d'exécution moyen, quantité de mémoire utilisée par l'algo, etc.)

# Notation Big-O

Pour un $n$ assez grand, compter jusqu'à `An + B` (où A et B sont des
constantes) se fait en temps $O(n)$

&nbsp;

- Si $n=10000$, compter jusqu'à $2n + 10$ va prendre $20010$ opérations
- Si $n=20000$, compter jusqu'à $2n + 10$ va prendre $40010$ opérations

&nbsp;

- Donc, si n double, le temps requis va devenir $\approx x 2$
- Croissance linéaire : $O(n)$

<!-- TODO graphique linéaire -->

# Notation Big-O

*Intuitivement* : on oublie généralement les constantes dans l'analyse
de l'ordre

&nbsp;

Un algo qui demande
$2\textcolor{blue}n^{\textcolor{blue}2}\textcolor{black} + 1$
opérations `=>` $O(n^2)$


# Temps d'exécution du Tri par sélection

*Analyse du pire cas d'un Tri par sélection en classe*


```
3 8 1 4 2 9 5 6
```


# Temps d'exécution du Tri par insertion

*Analyse du pire cas d'un Tri par insertion en classe*

```
9 8 6 5 4 3 2 1
```

# Temps d'exécution du tri à bulles

*Analyse du pire cas d'un Tri à bulles en classe*

```
9 8 6 5 4 3 2 1
```


# Temps d'exécution du tri par fusion

- Diviser pour régner :

```java
   [      sort(8)        ]
   => [   sort(4)   ] x2
     => [ sort(2) ] x2
       => sort(1) x2
```

- Pour trier 8 nombres, on répète 3 fois :
    - Partitionner les n nombres en deux moitiés
    - Trier n/2 nombres
- Pourquoi 3 fois ?
    - $3 = log_2(8)$
    - Vient du fait qu'on découpe en deux à chaque fois

# Temps d'exécution du Tri par fusion

*Analyse du pire cas d'un Tri par fusion en classe*

```
9 8 6 5 4 3 2 1
```


# Temps d'exécution du Quicksort

- Également de type *diviser pour régner*...
    - `=>` On tente de couper en deux le tableau à chaque étape
    - Idéalement, on le fait
- Mais pas le même pire cas que le Tri par fusion
    - Qu'est-ce qui se passe si une des deux "moitiés" est vide ?

&nbsp;

*Analyse du pire cas d'un Tri rapide en classe*

```
9 8 6 5 4 3 2 1
```

# Temps d'exécution du Heapsort

- Construire un monceau : $O(n log(n))$
    - Insérer tous les éléments : $n$ opérations
    - *Percoler* à chaque fois : $O(log(n))$ opérations
- Obtenir le tableau trié : $O(n log(n))$
    - Enlever le *min* du monceau : $O(log(n))$
    - Répéter $n$ fois, jusqu'à avoir un tableau trié
- `=>` $O(n log(n))$

# Performance pour trier n éléments

- Jusqu'ici, le pire cas le moins pire qu'on a vu est de l'ordre de $O(n
  log(n))$
- Est-ce qu'on peut faire mieux que ça ?

# Est-ce qu'on peut faire mieux ?

Trier la séquence suivante alphabétiquement :

```
ACCAABBCAAABACABACCABCCABCABCABCBCBBCACCBBBACABBAACB
```

&nbsp;

`=>` On sait que la séquence ne contient que des `A`, `B` et `C`

# Tri par comptage

\center

*Explication sommaire du principe du Tri par comptage en classe*

&nbsp;

https://en.wikipedia.org/wiki/Counting_sort

# Tri par paquets

Autre cas :

Un comptable doit trier des factures de 2003 par date


# Tri par paquets

\center

*Explication sommaire du principe du Tri par paquets en classe*

&nbsp;

https://en.wikipedia.org/wiki/Bucket_sort

# Les tris en pratique

- Notation Big-O : asymptotique, on ignore les constantes,
  puisqu'elles ont moins d'importance quand $n$ devient très grand
    - $(n + 100) / n \approx 1$ si n est très grand
    - Le $+ 100$ peut être négligé
- En pratique... $n$ n'est pas forcément très grand, attention aux
  constantes !

# Les tris en pratique

\center

![](img/practical-sorts.png)&nbsp;

&nbsp;

$10n + 570$ *comparé à* $n^2$

# Les tris en pratique

Considérations pratiques :

- Le Tri par insertion fonctionne très rapidement sur des petits tableaux
  ou sur des tableaux presque triés, alors que le quicksort ou le
  mergesort vont avoir de l'*overhead*
    - Optimisation au Quicksort : Sedgesort (Robert Sedgewick)

# Tri adaptatif

- Rien ne nous empêche d'*adapter* l'algorithme de tri qu'on utilise
  aux *données* précises qu'on a

```java
1 2 3 _5__4_ 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
```

Pour cette séquence, comment va se comporter...

- Un Tri par sélection ?
- Un QuickSort (avec pivot toujours à gauche) ?
- Un MergeSort ?
- Un Tri par insertion ?


# Conclusion

Les tris sont une très bonne introduction à l'algorithmie

- Il existe des tonnes de façons d'accomplir une tâche donnée
- Certaines façons sont plus efficaces que d'autres (d'un ordre de
  magnitude complètement différent)

&nbsp;

Mais en pratique, on préfère utiliser les algorithmes de tri déjà
disponibles plutôt que de recoder un algo de zéro à chaque fois

# Conclusion

En Java : utiliser `Arrays.sort(tableau)`

- Tri adaptatif qui peut se comporter comme d'autres tris (choisi
  selon le tableau à trier) :
    * Tri par insertion
    * Tri par fusion
    * Une variante du QuickSort (le Dual-Pivot Quicksort)
    * *Timsort* (un algorithme de tri adaptatif avancé)

L'algorithme s'ajuste selon ce qui est le plus efficace pour un
tableau donné

# Compléments

Chaîne Youtube qui représente quelques algorithmes de tri sous forme
de danses folkloriques :

https://www.youtube.com/user/AlgoRythmics/videos

# D'autres algorithmes de tris plus ou moins pertinents...

Autres algorithmes inefficaces (trouvez l'ordre *Big-O*) :

- Permutation sort
    - Générer et vérifier toutes les permutations du tableau jusqu'à
      trouver celle qui est triée
- Bogosort
    - Répéter [mélanger le tableau au hasard] tant que le tableau
      n'est pas trié
- Sleepsort
- StackSort
    - Implémentation ici : https://gkoberger.github.io/stacksort/

