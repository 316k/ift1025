---
title: Héritage & Polymorphisme - Exercices
author: Par Nicolas Hurtubise
header-includes:
    - \usepackage{textcomp}
date: IFT1025 - Programmation 2
---


# Références en mémoire

```java
public static void main(String[] args) {
    int[][] t = {{1,3,9},{2,6,4}};
    int[] t1 = t[0];
    int[] t2 = {1,3,9};

    System.out.println(t[0] == t[0]);
    System.out.println(t1 == t2);
    System.out.println(t[0] == t1);
    System.out.println(t[1] == t2);
}
```

1. true, true, true, true
2. false, false, false, true
3. true, false, false, true
4. true, false, true, false

# Cast

```java
public class Universitaire {}
public class Professeur extends Universitaire {}
public class Etudiant extends Universitaire {}
public class EtudiantGradue extends Etudiant {}

Etudiant e = new Etudiant();         // Valide ?
Universitaire u = new Professeur();  // Valide ?
Professeur p = new EtudiantGradue(); // Valide ?
EtudiantGradue eg = new Etudiant();  // Valide ?
```

1. Valide, Valide, Invalide, Valide
4. Valide, Valide, Invalide, Invalide
2. Valide, Invalide, Invalide, Valide
3. Valide, Invalide, Valide, Invalide


# Type de l'instance vs Type de la référence

```java
public class Universitaire {}
public class Professeur extends Universitaire {}
public class Etudiant extends Universitaire {}
public class EtudiantGradue extends Etudiant {}

Etudiant e = new EtudiantGradue(); // ?
```

[columns]

[column=0.7]

Type de l'instance :

- a: Etudiant
- b: EtudiantGradue
- c: Universitaire

Type de la référence :

- d: Etudiant
- e: EtudiantGradue
- f: Universitaire

[column=0.3]

1. a, f
2. a, e
3. b, d
4. c, e

[/columns]

# Cast

```java
public class Universitaire {}
public class Professeur extends Universitaire {}
public class Etudiant extends Universitaire {}
public class EtudiantGradue extends Etudiant {}

Universitaire u = new Universitaire();
Etudiant e = new Etudiant();
Professeur p = new Professeur();
EtudiantGradue eg = new EtudiantGradue();

Universitaire a = (Universitaire) p; // ?
```

1. Upcast, cast nécessaire
2. Downcast, cast nécessaire
3. Upcast, cast facultatif
4. Downcast, cast facultatif

# Cast

```java
public class Universitaire {}
public class Professeur extends Universitaire {}
public class Etudiant extends Universitaire {}
public class EtudiantGradue extends Etudiant {}

Universitaire u = new Universitaire();
Etudiant e = new Etudiant();
Professeur p = new Professeur();
EtudiantGradue eg = new EtudiantGradue();

Universitaire a = (Universitaire) p;
Professeur b = (Professeur) a; // ?
```

1. Upcast, cast nécessaire
2. Downcast, cast nécessaire
3. Upcast, cast facultatif
4. Downcast, cast facultatif

# Cast

```java
public class Universitaire {}
public class Professeur extends Universitaire {}
public class Etudiant extends Universitaire {}
public class EtudiantGradue extends Etudiant {}

Universitaire u = new Universitaire();
Etudiant e = new Etudiant();
Professeur p = new Professeur();
EtudiantGradue eg = new EtudiantGradue();

Universitaire a = (EtudiantGradue) eg; // ?
```

1. Cast valide et facultatif
2. Cast valide et nécessaire
3. Cast invalide

# Cast

```java
public class Universitaire { public String nomComplet() { ... } }
public class Professeur extends Universitaire {}
public class Etudiant extends Universitaire {}
public class EtudiantGradue extends Etudiant {}

Universitaire u = new Universitaire();

Object a = u; // Valide ?

System.out.println(a.toString()); // Valide ?
System.out.println(a.nomComplet()); // Valide ?
System.out.println(a == u); // true/false ?
```

1. Valide, Valide, Valide, true
2. Valide, Invalide, Valide, false
3. Valide, Valide, Invalide, true
4. Invalide, Invalide, Valide, false


# Cast

```java
Integer i = new Integer(10);     // Valide ?
Integer j = 20;                  // Valide ?
System.out.println((String) i);  // Valide ?
System.out.println((String) 10); // Valide ?
```

1. Valide, Valide, Valide, Valide
2. Valide, Invalide, Valide, Invalide
3. Invalide, Invalide, Invalide, Invalide
4. Valide, Valide, Invalide, Invalide

# instanceof


```java
Universitaire u = new Universitaire();
Etudiant e = new Etudiant();
Professeur p = new Professeur();
EtudiantGradue eg = new EtudiantGradue();

Etudiant a = eg;
Object b = u;

System.out.println(a instanceof Universitaire);  // ?
System.out.println(a instanceof EtudiantGradue); // ?
System.out.println(a instanceof Etudiant);       // ?
System.out.println(b instanceof Object);         // ?
System.out.println(b instanceof Professeur);     // ?
```

1. true, false, true, false, true
2. true, true, true, true, false
3. true, false, true, true, false
4. true, false, true, false, false


# Overload (surcharge) / Override (redéfinition)

[columns]

[column=0.6]

```java
public class Parent {
    public String foo(int a) {
        return "" + a;
    }
}
public class Enfant extends Parent {
    // Overload ou Override ?
    public String foo(int b) {
        return "[" + b + "]";
    }
}

Parent p = new Enfant();
System.out.println(p.foo(10));
// => Affiche ?
Enfant e = (Enfant) p;
System.out.println(e.foo(10));
// => Affiche ?
```

[column=0.4]

1. Surcharge, 10, [10]
2. Surcharge, [10], [10]
3. Redéfinition, [10], [10]
4. Redéfinition, [10], 10

[/columns]


# Overload (surcharge) / Override (redéfinition)

[columns]

[column=0.6]

```java
public class Parent {
    public void foo(Object a) {
        System.out.println("Object");
    }
}
public class Enfant extends Parent {
    public void foo(int b) {
        System.out.println("int");
    }
    public void foo(String c) {
        System.out.println("String");
    }
}
Enfant e = new Enfant();
Parent p = new Parent();

e.foo(e); e.foo(10);
p.foo("Test"); p.foo(10);
```

[column=0.4]

1. Object, int, Object, Object
2. Object, Object, Object, Object
3. Invalide, int, Object, Invalide
4. Object, int, String, Object

[/columns]

# Overload (surcharge) / Override (redéfinition)

[columns]

[column=0.6]

```java
public class Parent {
    public void foo(Object a) {
        System.out.println("Object");
    }
}
public class Enfant extends Parent {
    public void foo(int b) {
        System.out.println("int");
    }
    public void foo(String c) {
        System.out.println("String");
    }
}
Parent e = new Enfant();
Parent p = new Parent();

e.foo(e); e.foo(10);
p.foo("Test"); p.foo(10);
```

[column=0.4]

1. Object, int, Object, Object
2. Object, Object, Object, Object
3. Invalide, int, Object, Invalide
4. Object, int, String, Object

[/columns]


# Overload (surcharge) / Override (redéfinition)

[columns]

[column=0.6]

```java
public class Parent {
    public void foo(Parent a) {
        System.out.println("A");
    }
}
public class Enfant extends Parent {
    public void foo(Parent b) {
        System.out.println("B");
    }
    public void foo(Enfant c) {
        System.out.println("C");
    }
}
Enfant e = new Enfant();
Parent p = new Parent();

e.foo(e); e.foo(p);
p.foo(e); p.foo(p);
```

[column=0.4]

1. C, B, C, B
2. C, B, A, A
3. A, B, C, A
4. A, A, A, A

[/columns]


# Overload (surcharge) / Override (redéfinition)

[columns]

[column=0.6]

```java
public class Parent {
    public void foo(Parent a) {
        System.out.println("A");
    }
}
public class Enfant extends Parent {
    public void foo(Parent b) {
        System.out.println("B");
    }
    public void foo(Enfant c) {
        System.out.println("C");
    }
}
Parent e = new Enfant();
Parent p = new Parent();

e.foo(e); e.foo(p);
p.foo(e); p.foo(p);
```

[column=0.4]

1. C, B, C, B
2. C, B, A, A
3. A, B, C, A
4. A, A, A, A

[/columns]
