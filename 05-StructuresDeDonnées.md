---
title: Structures de Données
author: Par Nicolas Hurtubise \newline \newline \footnotesize Inspiré et dérivé des notes de Pascal Vincent, elles-mêmes reprises et adaptées de celles de Jian Yun Nie et Miklos Csuros
header-includes:
    - \usepackage{textcomp}
date: IFT1025 - Programmation 2
---

# Au programme...

- Structures de données ?
- Type abstrait vs Implémentation concrète
- Listes
- Autres structures de données
- Itérateurs
- Types génériques
- Dictionnaires

# Structures de données ?

- Un autre cours complet y est dédié : \newline IFT2015 - Structures de
  données

&nbsp;

- En IFT1025 : Introduction informelle
    - Non théorique, beaucoup moins détaillé
    - Focus sur les aspects pratiques en programmation Java
    - Excellent pour pratiquer des notions clés :
        - Abstraction, interfaces, polymorphisme, références,
          récursivité, ...

# Structures de données ?

- À quoi servent les structures de données ?

\pause

- À structurer des données !

\pause

- Plus spécifiquement : *contenir* et *organiser* des informations
    - Un ensemble de nombres : liste de nombres non ordonnée
    - Un dictionnaire papier : liste de mots en ordre
      croissant
    - Une liste d'associations : liste de clés `=>` valeurs

# Structures de données ?

- Exemple de structure de données que vous connaissez : les tableaux
  de Java

```java
double[] nombres = new double[100];
String[] mots = new String[200];
Object[] choses = new Object[1000];
```

- Autre exemple : les classes Java
    - La classe `Etudiant` permet de stocker un prénom, un nom, un
      matricule, ...

# Tableaux

Les tableaux en Java ont quelques limitations...

- On doit déterminer leur taille à l'avance
- On ne peut pas directement les agrandir
    - Solution : créer manuellement un nouveau tableau plus grand et
      recopier tous les éléments à conserver
- Un tableau n'est pas un *Objet* Java
    - Possède seulement l'attribut `length`
    - Pas de méthodes (non-`static`) pour ajouter/insérer/supprimer
    - On ne peut pas le sous-classer

# Types abstraits vs Implémentations concrètes

Il y a deux concepts importants à distinguer :

- Le *type abstrait* d'une structure de données
- Son *implémentation* concrète

## Type **abstrait**

<!-- Modèle qui définit des opérations possibles à faire avec des
données (ex.: obtenir le ième élément) -->

- Typiquement représentée par une `interface`
- Définit les opérations qu'on peut faire avec les données \newline &nbsp;
- Un même type abstrait peut avoir plusieurs implémentations concrètes
  très différentes
- `=>` Comportement du point de vue de *quelqu'un qui utilise la
  structure de données*

# Types abstraits vs Implémentations concrètes

Il y a deux concepts importants à distinguer :

- Le *type abstrait* d'une structure de données
- Son *implémentation* concrète

## Implémentation **concrète**

- Typiquement une classe qui `implements` une interface
- Comment les données sont organisées pour permettre de faire les
  opérations voulues
- Quels algorithmes spécifiques sont utilisés pour manipuler les
  données
- `=>` Comportement du point de vue de *quelqu'un qui code la classe*

# Types abstraits

Quelques exemples de types abstraits :

- Liste (ou *séquence*, ou *vecteur*)
    - Opérations : ajouter/supprimer/accéder à un élément
    - Les éléments arrivent dans un certain ordre et sont indexés par
      leur position dans la liste (nombre entier) \pause
- Pile (`Stack`) et File d'attente (`Queue`)
    - Liste dont on n'accède directement qu'au premier/dernier élément
      (*Last In First Out* ou *First In First Out*) \pause
- Ensemble (`Set`)
    - Similaire à liste, mais sans doublon, et sans notion de
      position/index \pause
- Tableau associatif, ou dictionnaire (`Map`) arbitraire
    - Les éléments (valeurs) sont indexés par une clé d'un type (ex:
      `String`) plutôt qu'un index entier

# Structures de données concrètes

Structures de données concrètes : correspond à différents types
d'organisation des éléments en mémoire et aux algorithmes de
manipulation associés

&nbsp;

- Tableau (`array`)
- Liste chaînée (`linked list`)
- Arbre (`tree`) binaire ou n-aire avec noeuds chaînés
- Table de hachage (hash table)
- Arbre binaire de recherche
- ...

# Types abstraits : **Liste**

Opérations supportées :

- Connaître le nombre d'éléments
- Obtenir l'élément à la position `i`
- Modifier l'élément à la position `i`
- Insérer un élément avant l'élément à la position `i` (ou au début/à
  la fin)
- Supprimer l'élément à la position `i`

Une *liste* est flexible : pas besoin de fixer le nombre total ou
maximal d'éléments d'avance

# Types abstraits : **Liste**

On peut représenter le type abstrait `Liste` par une `interface`, qui
définit ce qu'on peut faire avec une liste sans spécifier
l'implémentation (le code)

&nbsp;

Pour une liste de nombres entiers :

```java
public interface Liste {

    public int size(); // Nombre total d'éléments
    public int get(int i); // Accès à l'élément #i
    public void set(int i, int valeur); // Modifier #i
    public void add(int valeur); // Ajouter à la fin
    public void add(int i, int valeur); // Insérer avant #i
    public int remove(int i); // Supprimer #i
    ...
}
```

# Implémentation concrète : *Tableau*

[columns]

[column=0.5]

Une implémentation possible pour le type *Liste* serait d'utiliser un
tableau pour stocker les éléments

&nbsp;

Un tableau (`array`) permet de stocker une séquence de données
contiguë en mémoire

[column=0.5]

```java
int[] nombres = {10, 20, 30, 40, 50};
```

\center

![](img/gv/05.memarray.png)&nbsp;

[/columns]

# Implémentation concrète : *Tableau*

- L'avantage d'avoir des cases contiguës en mémoire est au niveau de
  la performance

<!-- Image de la mémoire -->

- L'ordinateur peut rapidement calculer l'adresse de la case mémoire
  où se trouve le $i^{ème}$ élément
    - `adresse = debut du tableau + i * taille d`'`une case`
- Garantis l'accès en *temps constant*, ou $O(1)$, peu importe le
  numéro d'index `i` qui nous intéresse

# Implémentation concrète : *Tableau*

<!-- Et si on veut ajouter un élément à la fin du tableau ? -->

Une **Liste** doit permettre d'ajouter un élément via la méthode
`.add(...)`

&nbsp;

&nbsp;

[columns]

[column=0.5]

Puisque les éléments *doivent* être contigus, la case qui suit le
tableau *doit* être libre, ce qui est rarement le cas dans nos
programmes...

&nbsp;

&nbsp;

&nbsp;

[column=0.5]

![](img/gv/05.memarrayblocked.png){width=73%}&nbsp;

[/columns]

# Implémentation concrète : *Tableau*

[columns]

[column=0.5]

Solution : allouer un nouveau tableau, ailleurs dans la mémoire, là où
il y a assez de place pour tout mettre

[column=0.5]

![](img/gv/05.memarraycopied.png)&nbsp;

[/columns]

# Tableau dynamique

On peut se coder une classe qui implémente l'interface *Liste* en
utilisant un tableau derrière

```java
// Taille initiale de 10 cases (index : 0 à 9)
TableauDynamique tab = new TableauDynamique(10);

for(int i=0; i<10; i++) {
    tab.set(i, i * 10);
}

tab.get(5) // => 50

tab.add(12345); // Ajoute un élément

tab.size() // => 11

tab.get(10) // => 12345
```

# Tableau dynamique

```java
public class TableauDynamique implements Liste {

    private int[] array;

    public TableauDynamique(int nbElements) {
        array = new int[nbElements];
    }

    public int get(int idx) {
        return array[idx];
    }

    public void set(int idx, int value) {
        array[idx] = value;
    }

    // ...
```

# Tableau dynamique

```java

    /**
     * Ajoute une valeur à la fin du tableau
     * (comme tab.push() en JavaScript)
     */
    public void add(int value) {
        int[] newArray = new int[array.length + 1];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i];
        }
        newArray[array.length] = value;

        array = newArray;
    }

```

# Tableau dynamique

```java
    /**
     * Enlève et retourne la idx-ème valeur
     */
    public int remove(int idx) {
        int[] newArray = new int[array.length - 1];
        int removed = array[idx];

        for (int i = 0; i < idx; i++) {
            newArray[i] = array[i]; // Copie avant idx
        }

        for (int i = idx + 1; i < array.length; i++) {
            newArray[i - 1] = array[i]; // Copie après idx
        }

        array = newArray;

        return removed;
    }
```

# Tableau dynamique

```java
    /**
     * Retourne le nombre d'éléments dans le tableau
     */
    public int size() {
        return array.length;
    }

    @Override
    public String toString() {
        // Affiche l'objet comme si c'était un tableau
        return Arrays.toString(array);
    }
```


# Tableau dynamique

```java
    // Test du tableau dynamique
    public static void main(String[] args) {
        TableauDynamique tab = new TableauDynamique(5);

        for (int i = 0; i < tab.size(); i++)
            tab.set(i, i * 10);

        System.out.println(tab);
        // => [0, 10, 20, 30, 40]

        tab.add(50);

        System.out.println(tab.size()); // => 6
        System.out.println(tab.remove(1)); // => 10
        System.out.println(tab.remove(1)); // => 20

        System.out.println(tab);
        // => [0, 30, 40, 50]
    }
}
```

# Tableau dynamique

Constatations :

- L'orienté objet est particulièrement pratique ici !

&nbsp;

- Une fois la classe `TableauDynamique` écrite, on peut l'utiliser
  dans tous nos programmes, pas besoin de réécrire une procédure pour
  redimensionner un tableau à chaque fois

# Tableau dynamique

Analysons la performance de méthode `add(int valeur)` :

- Si on a 10 éléments, ajouter un élément de plus demande de copier
  les 10 anciens éléments dans un nouveau tableau
- Si on a 10000 éléments, on doit copier 10000 éléments
- ...
- Pour une liste de $N$ éléments, insérer un nouvel élément demande
  $N$ copies avant d'ajouter le prochain

Même chose pour la méthode `remove(int idx)`

# Tableau dynamique

Est-ce qu'on peut faire mieux ?

- Problème : la création d'un nouveau tableau + copie des éléments est
  coûteuse

\pause

- Solution : pré-allouer plus d'espace que nécessaire
    - Ex.: resizer le tableau avec 1.5 fois la taille précédente,
      plutôt qu'augmenter à coup de `+1` case
    - Gaspille possiblement de la mémoire
    - `add`/`remove` à la fin deviennent $O(1)$ la majorité du temps
    - Quand même besoin de redimensionner le tableau de temps en
      temps, donc $O(n)$ à l'occasion...

\pause

- Autre solution : données **non-contiguës** en mémoire


# Liste chaînée

Idée : données **non-contiguës** en mémoire

Un `Noeud` qui contient un nombre entier peut être défini avec :

```java
public class Noeud {

  public int valeur;
  public Noeud prochain;

  public Noeud(int valeur, Noeud prochain) {
    this.valeur = valeur;
    this.prochain = prochain;
  }
}
```

# Liste chaînée

Idée : données **non-contiguës** en mémoire

Un `Noeud` contient une valeur et une *référence* vers le `Noeud`
suivant

[columns]

[column=0.5]

```java
Noeud n = new Noeud(10, null);











```

[column=0.5]

\center

![](img/gv/05.memnode.png)&nbsp;

[/columns]

# Liste chaînée

Idée : données **non-contiguës** en mémoire

Un `Noeud` contient une valeur et une *référence* vers le `Noeud`
suivant

[columns]

[column=0.5]

```java
Noeud n = new Noeud(10, null);

Noeud n2 = new Noeud(20, null);









```

[column=0.5]

\center

![](img/gv/05.memnode2.png)&nbsp;

[/columns]

# Liste chaînée

Idée : données **non-contiguës** en mémoire

Un `Noeud` contient une valeur et une *référence* vers le `Noeud`
suivant

[columns]

[column=0.5]

```java
Noeud n = new Noeud(10, null);

Noeud n2 = new Noeud(20, null);

n.prochain = n2;







```

[column=0.5]

\center

![](img/gv/05.memnode3.png)&nbsp;

[/columns]

# Liste chaînée

Idée : données **non-contiguës** en mémoire

Un `Noeud` contient une valeur et une *référence* vers le `Noeud`
suivant

[columns]

[column=0.5]

```java
Noeud n = new Noeud(10, null);

Noeud n2 = new Noeud(20, null);

Noeud n3 = new Noeud(30, null);

n.prochain = n2;

n2.prochain = n3;



```

[column=0.5]

\center

![](img/gv/05.memnode4.png)&nbsp;

[/columns]


# Liste chaînée

[columns]

[column=0.5]

- Pour retrouver la $N^{ième}$ valeur à partir du premier `Noeud`, on
  peut simplement suivre la chaîne de `Noeud`s

&nbsp;

- Un nouveau `Noeud` peut être placé n'importe où en mémoire, les
  données n'ont pas à être contiguës

[column=0.5]

![](img/gv/05.memarraynodes.png)&nbsp;

[/columns]

# Liste chaînée

[columns]

[column=0.5]

- Pour retrouver la $N^{ième}$ valeur à partir du premier `Noeud`, on
  peut simplement suivre la chaîne de `Noeud`s

&nbsp;

- Un nouveau `Noeud` peut être placé n'importe où en mémoire, les
  données n'ont pas à être contiguës

[column=0.5]

![](img/gv/05.memarraynodes2.png)&nbsp;

[/columns]

# Liste chaînée

On peut utiliser cette structure pour implanter l'interface *Liste*

- Une **liste chaînée** contient le premier `Noeud` de la chaîne

```java
ListeChainee liste = new ListeChainee();

// On commence avec une liste vide




```

&nbsp;

\center

![](img/gv/05.linkedlist.empty.png){width=40%}&nbsp;

# Liste chaînée

On peut utiliser cette structure pour implanter l'interface *Liste*

- Une **liste chaînée** contient le premier `Noeud` de la chaîne

```java
ListeChainee liste = new ListeChainee();

liste.setPremier(new Noeud(1,
                  new Noeud(2,
                    new Noeud(3,
                      new Noeud(4, null)))));
```

\center

![](img/gv/05.linkedlist.example.png){width=70%}&nbsp;

# Liste chaînée

- On peut traverser la liste en suivant la chaîne de références

```java
public class ListeChainee implements Liste {

    // Lorsqu'on crée une nouvelle liste, premier=null par défaut
    private Noeud premier;

    /**
     * Affiche tous les éléments de la liste
     */
    public void print() {
        Noeud n = this.premier;

        while(n != null) {
            System.out.println(n.valeur);
            n = n.prochain;
        }
    }
```

# Liste chaînée

```java
                Noeud n = this.premier;

                while(n != null) {
                    System.out.println(n.valeur);
                    n = n.prochain;
                }
```

\center

![](img/gv/05.linkedlist.0.png){width=90%}&nbsp;

# Liste chaînée

```java
                Noeud n = this.premier;

                while(n != null) {
                    System.out.println(n.valeur);
                    n = n.prochain;
                }
```

\center

![](img/gv/05.linkedlist.1.png){width=90%}&nbsp;

# Liste chaînée

```java
                Noeud n = this.premier;

                while(n != null) {
                    System.out.println(n.valeur);
                    n = n.prochain;
                }
```

\center

![](img/gv/05.linkedlist.2.png){width=90%}&nbsp;

# Liste chaînée

```java
                Noeud n = this.premier;

                while(n != null) {
                    System.out.println(n.valeur);
                    n = n.prochain;
                }
```

\center

![](img/gv/05.linkedlist.3.png){width=90%}&nbsp;

# Liste chaînée

```java
                Noeud n = this.premier;

                while(n != null) {
                    System.out.println(n.valeur);
                    n = n.prochain;
                }
```

\center

![](img/gv/05.linkedlist.4.png){width=90%}&nbsp;

# Liste chaînée

```java
                Noeud n = this.premier;

                while(n != null) {
                    System.out.println(n.valeur);
                    n = n.prochain;
                }
```

\center

![](img/gv/05.linkedlist.5.png){width=90%}&nbsp;

# Liste chaînée

- Trouver le $i^{ème}$ élément demande de suivre $i$ références

```java
    /**
     * Méthode pour retrouver le idx-ème noeud de la chaîne
     * 
     * Notez : la méthode est private, car on ne veut pas permettre
     * aux utilisateurs de la classe d'avoir un accès aux noeuds
     * directement (principe d'encapsulation)
     */
    private Noeud getNoeud(int idx) {
        Noeud n = this.premier;

        for(int i=0; i<idx; i++) {
            n = n.prochain;
        }

        return n;
    }
```

# Liste chaînée

```java
    /**
     * Accès au idx-ème élément
     */
    public int get(int idx) {
        return getNoeud(idx).valeur;
    }

    /**
     * Modifie le idx-ème élément
     */
    public void set(int idx, int valeur) {
        getNoeud(idx).valeur = valeur;
    }
```

# Liste chaînée

- Pour déterminer la longueur de la liste, il suffit de parcourir
  toute la liste en comptant le nombre de `Noeud`s visités

\center

![](img/gv/05.linkedlist.size.0.png){width=80%}&nbsp;

&nbsp;

&nbsp;

# Liste chaînée

- Pour déterminer la longueur de la liste, il suffit de parcourir
  toute la liste en comptant le nombre de `Noeud`s visités

\center

![](img/gv/05.linkedlist.size.1.png){width=80%}&nbsp;

&nbsp;

&nbsp;

# Liste chaînée

- Pour déterminer la longueur de la liste, il suffit de parcourir
  toute la liste en comptant le nombre de `Noeud`s visités

\center

![](img/gv/05.linkedlist.size.2.png){width=80%}&nbsp;

&nbsp;

&nbsp;

# Liste chaînée

- Pour déterminer la longueur de la liste, il suffit de parcourir
  toute la liste en comptant le nombre de `Noeud`s visités

\center

![](img/gv/05.linkedlist.size.3.png){width=80%}&nbsp;

&nbsp;

&nbsp;

# Liste chaînée

- Pour déterminer la longueur de la liste, il suffit de parcourir
  toute la liste en comptant le nombre de `Noeud`s visités

\center

![](img/gv/05.linkedlist.size.4.png){width=80%}&nbsp;

&nbsp;

&nbsp;

# Liste chaînée

- Pour déterminer la longueur de la liste, il suffit de parcourir
  toute la liste en comptant le nombre de `Noeud`s visités

\center

![](img/gv/05.linkedlist.size.5.png){width=80%}&nbsp;

&nbsp;

&nbsp;


# Liste chaînée

- Pour déterminer la longueur de la liste, il suffit de parcourir
  toute la liste en comptant le nombre de `Noeud`s visités

```java
    public int size() {

        int size = 0;
        Noeud n = this.premier;

        while(n != null) {
            size++;
            n = n.prochain;
        }

        return size;
    }
```

# Liste chaînée

- Insérer un nouveau `Noeud` au début

```java
liste.premier = new Noeud(777, liste.premier);
```

\center

![](img/gv/05.linkedlist.addFirst.0.png){width=80%}&nbsp;

# Liste chaînée

- Insérer un nouveau `Noeud` au début

```java
liste.premier = new Noeud(777, liste.premier);
```

\center

![](img/gv/05.linkedlist.addFirst.1.png){width=80%}&nbsp;

# Liste chaînée

- Insérer un nouveau `Noeud` au début

```java
liste.premier = new Noeud(777, liste.premier);
```

\center

![](img/gv/05.linkedlist.addFirst.2.png){width=80%}&nbsp;

# Liste chaînée

- Ajouter un nouveau `Noeud` à la fin

```java
liste.add(777);
```

\center

![](img/gv/05.linkedlist.addLast.0.png){width=80%}&nbsp;

# Liste chaînée

- Ajouter un nouveau `Noeud` à la fin

```java
liste.add(777);
```

\center

![](img/gv/05.linkedlist.addLast.1.png){width=80%}&nbsp;

# Liste chaînée

- Ajouter un nouveau `Noeud` à la fin

```java
liste.add(777);
```

\center

![](img/gv/05.linkedlist.addLast.2.png){width=80%}&nbsp;

# Liste chaînée

- Ajouter un nouveau `Noeud` à la fin

```java
liste.add(777);
```

\center

![](img/gv/05.linkedlist.addLast.3.png){width=80%}&nbsp;

# Liste chaînée

- Ajouter un nouveau `Noeud` à la fin

```java
liste.add(777);
```

\center

![](img/gv/05.linkedlist.addLast.4.png){width=80%}&nbsp;

# Liste chaînée

- Ajouter un nouveau `Noeud` à la fin

```java
liste.add(777);
```

\center

![](img/gv/05.linkedlist.addLast.5.png){width=80%}&nbsp;

# Liste chaînée

- Ajouter un nouveau `Noeud` à la fin

```java
liste.add(777);
```

\center

![](img/gv/05.linkedlist.addLast.6.png){width=80%}&nbsp;


# Liste chaînée

- Ajouter un nouveau `Noeud` à la fin

```java
    public void add(int valeur) {
        if(this.premier == null) {
            // Cas 1 : rien dans la liste
            this.premier = new Noeud(valeur, null);
        } else {
            // Cas 2 : trouver le noeud final
            Noeud fin = this.premier;
            Noeud n = this.premier.prochain;

            while(n != null) {
                fin = n;
                n = n.prochain;
            }

            // Ajoute un noeud après la fin de la liste
            fin.prochain = new Noeud(valeur, null);
        }
    }
```

# Liste chaînée

- Ajouter un nouveau `Noeud` à une position arbitraire

```java
    public void add(int i, int valeur) {
        if(i == 0) {
            // Cas 1 : ajouter au début
            this.premier = new Noeud(valeur, this.premier);
        } else {
            // Cas 2 : ajouter avant le ième noeud
            Noeud nouveau = new Noeud(valeur, null);
            // Trouve le noeud juste avant celui qu'on insère
            Noeud precedent = getNoeud(i - 1);

            // Le noeud inséré réfère au suivant
            nouveau.prochain = precedent.prochain;

            /* Met à jour la référence du noeud juste
               avant celui qu'on insère */
            precedent.prochain = nouveau;
        }
    }
```

# Liste chaînée

- Supprimer le $i^{ème}$ `Noeud`

```java
liste.remove(2);
```

\center

![](img/gv/05.linkedlist.remove.0.png){width=80%}&nbsp;

# Liste chaînée

- Supprimer le $i^{ème}$ `Noeud`

```java
liste.remove(2);
```

\center

![](img/gv/05.linkedlist.remove.1.png){width=80%}&nbsp;

# Liste chaînée

- Supprimer le $i^{ème}$ `Noeud`

```java
liste.remove(2);
```

\center

![](img/gv/05.linkedlist.remove.2.png){width=80%}&nbsp;

# Liste chaînée

- Supprimer le $i^{ème}$ `Noeud`

```java
liste.remove(2);
```

\center

![](img/gv/05.linkedlist.remove.3.png){width=80%}&nbsp;

# Liste chaînée

- Supprimer le $i^{ème}$ `Noeud`

```java
liste.remove(2);
```

\center

![](img/gv/05.linkedlist.remove.4.png){width=80%}&nbsp;

# Liste chaînée

- Supprimer le $i^{ème}$ `Noeud`

```java
liste.remove(2);
```

\center

![](img/gv/05.linkedlist.remove.5.png){width=80%}&nbsp;

# Liste chaînée

- Supprimer le $i^{ème}$ `Noeud`

```java
    public int remove(int i) {
        int retour;

        if(i == 0) {
            // Cas 1 : retirer le premier élément
            retour = this.premier.valeur;
            this.premier = this.premier.prochain;
        } else {
            // Cas 2 : retirer le ième élément
            // ...





```

# Liste chaînée

- Supprimer le $i^{ème}$ `Noeud`

```java
        } else {
            // Cas 2 : retirer le ième élément
            Noeud precedent = getNoeud(i - 1);

            // On va retourner la valeur du Noeud i
            retour = precedent.prochain.valeur;

            // On connecte le noeud i-1 au noeud i+1
            Noeud suivant = precedent.prochain.prochain;
            precedent.prochain = suivant;
        }

        return retour;
    }
```

# Liste : Tableau vs Liste chaînée

Comparaison des performances :

|                                | `TableauDynamique`\footnote{Version naïve qui resize de +1 à chaque ajout/suppression d'élément} | `ListeChainee`      |
|--------------------------------|--------------------------------------------------------------------------------------------------|---------------------|
| Accès au $i^{ème}$ élément     | $O(1)$ - instantané                                                                              | $O(i)$ opérations   |
| Accès au dernier élément       | $O(1)$ - instantané                                                                              | $O(n)$ opérations   |
| Insérer/supprimer au début     | $O(n)$ opérations                                                                                | $O(1)$ - instantané |
| Insérer/supprimer en $i^{ème}$ | $O(n)$ opérations                                                                                | $O(i)$ opérations   |
| Insérer/supprimer à la fin     | $O(n)$ opérations                                                                                | $O(n)$ opérations   |
| Trouver la taille              | $O(1)$ - instantané                                                                              | $O(n)$ opérations   |

# Pourquoi avoir différentes implantations ?

## Question

Quoi utiliser entre `TableauDynamique` et `ListeChainee` ?

&nbsp;

\pause

Ça dépend du contexte : varie selon le cas d'utilisation

- Beaucoup d'insertions/accès au début de la liste ?

    \pause

    `=> ListeChainee`

\pause

- Beaucoup d'accès à la $i^{ème}$ position dans la liste ?

    \pause

    `=> TableauDynamique`

# En Java

Avant de vous mettre à inclure `TableauDynamique` et `ListeChainee`
dans tous vos programmes...

&nbsp;

Java vient avec ces types prédéfinis dans la librairie standard, en
plus complet/flexible

- `List` : interface pour une liste
    - `ArrayList` : implantation d'un tableau dynamique
    - `LinkedList` : implantation d'une liste chaînée


# En Java

*Notez* : ça fait quelques façons différentes d'obtenir la longueur de
quelque chose...

|            | **String** | **Tableau** | **List<E>** \footnotesize (`ArrayList` ou `LinkedList`) |
|------------+------------+-------------+---------------------------------------------------------|
| *Exemple*  | `"abc"`    | `int[]`     | `new ArrayList<Integer>()`                              |
| *Longueur* | `length()` | `length`    | `size()`                                                |
|            | Méthode    | Attribut    | Méthode                                                 |


# Autres structures de données

![Hiérarchie des `Collection`s de Java (jaune=types abstraits, bleu=implémentations concrètes)](img/datastructures-concrete.png)

- En jaune : types abstraits (interfaces)
- En bleu : structures concrètes

# `LinkedList`

La classe `LinkedList` telle qu'implantée dans la librairie standard
de Java est un peu plus efficace que notre classe `ListeChainee`

&nbsp;

Certaines opérations fréquentes sont relativement coûteuses dans notre
version :

- Calculer la longueur : $O(n)$
- Ajouter à la fin : $O(n)$
- Enlever à la fin : $O(n)$

Est-ce qu'on peut faire mieux ?

# Amélioration : Compter le nombre d'éléments à mesure

Plutôt que de calculer la longueur de la liste explicitement, on
pourrait ajouter un attribut dans `ListeChainee` pour compter le
nombre d'éléments :

```java
public class ListeChainee implements Liste {

    private Noeud premier;
    private int size;

    public void add(int valeur) {
        // Ajouter un noeud à la fin
        // ...

        this.size++;
    }
```

# Amélioration : Compter le nombre d'éléments à mesure

```java
    public int remove(int i) {
        this.size--;

        // ... retirer et retourner la valeur à i
    }

    public int size() {
        return this.size;
    }
}
```

&nbsp;

\center

Calculer la longueur de la liste : $O(n) \rightarrow O(1)$

# Amélioration : Référence vers le dernier élément

Une autre idée serait de toujours garder une référence vers le dernier
`Noeud` de la liste :

```java
public class ListeChainee implements Liste {

    private Noeud premier;
    private Noeud dernier;

    private int size;
    
    // ...
}
```

# Amélioration : Référence vers le dernier élément

\center

![](img/gv/05.linkedlist.lastref.png){width=80%}&nbsp;

- Ajouter un cas spécial dans `get()/set()` si c'est le dernier
  élément pour accélérer l'exécution
- Ajuster `add(...)`, `remove(...)` pour toujours garder la référence
  vers le dernier `Noeud`

&nbsp;

Ajouter à la fin, accès au dernier élément : $O(n) \rightarrow O(1)$

# Amélioration : Référence vers le dernier élément

Pour *supprimer* efficacement le dernier `Noeud` : garder une
référence vers le dernier `Noeud` n'est pas suffisant...

&nbsp;

&nbsp;

&nbsp; \newline &nbsp;

\center

![](img/gv/05.linkedlist.lastref.rem.0.png){width=80%}&nbsp;

# Amélioration : Référence vers le dernier élément

Supprimer efficacement le dernier `Noeud` : garder une référence vers
le dernier `Noeud` n'est pas suffisant

&nbsp;

Mettre à jour le `prochain` de l'avant-dernier demande quand même de
retraverser toute la liste pour trouver quel `Noeud` est
l'avant-dernier...

\center

![](img/gv/05.linkedlist.lastref.rem.1.png){width=80%}&nbsp;

# Amélioration : Liste doublement chaînée

Solution : *liste doublement chaînée*

- Chaque `Noeud` a désormais deux références :
    - `prochain`
    - `precedent`
- Demande plus de code pour gérer les références

```java
public class Noeud {

    public int valeur;
    public Noeud prochain;
    public Noeud precedent;

}
```

# Amélioration : Liste doublement chaînée

Ajouter un élément dans une liste doublement chaînée :

\center

![](img/gv/05.linkedlist.doublylinked.0.png)&nbsp;

&nbsp;

\center

&nbsp;

&nbsp; \newline &nbsp;

# Amélioration : Liste doublement chaînée

Ajouter un élément dans une liste doublement chaînée :

\center

![](img/gv/05.linkedlist.doublylinked.1.png)&nbsp;

&nbsp;

\center

&nbsp;

&nbsp; \newline &nbsp;

# Amélioration : Liste doublement chaînée

Ajouter un élément dans une liste doublement chaînée :

\center

![](img/gv/05.linkedlist.doublylinked.2.png)&nbsp;

&nbsp;

\center

&nbsp;

&nbsp; \newline &nbsp;

# Amélioration : Liste doublement chaînée

Ajouter un élément dans une liste doublement chaînée :

\center

![](img/gv/05.linkedlist.doublylinked.3.png)&nbsp;

&nbsp;

\center

&nbsp;

&nbsp; \newline &nbsp;

# Amélioration : Liste doublement chaînée

Ajouter un élément dans une liste doublement chaînée :

\center

![](img/gv/05.linkedlist.doublylinked.4.png)&nbsp;

&nbsp;

\center

\pause

Enlever à la fin : $O(n) \rightarrow O(1)$ \pause

Bonus : on peut maintenant parcourir la liste à l'envers efficacement !

# Liste : Tableau vs Liste chaînée (améliorée)

Comparaison des performances avec améliorations :

|                                | `TableauDynamique`\footnote{Version qui garde de l'espace supplémentaire au cas où} | `ListeChainee`                |
|--------------------------------|-------------------------------------------------------------------------------------|-------------------------------|
| Accès au $i^{ème}$ élément     | $O(1)$ - instantané                                                                 | $O(min(i, n - i))$ opérations |
| Accès au dernier élément       | $O(1)$ - instantané                                                                 | $O(1)$ - instantané           |
| Insérer/supprimer au début     | $O(n)$ opérations                                                                   | $O(1)$ - instantané           |
| Insérer/supprimer en $i^{ème}$ | $O(n)$ opérations                                                                   | $O(i)$ opérations             |
| Insérer/supprimer à la fin     | $O(1)$ ou $O(n)$*                                                                   | $O(1)$ - instantané           |
| Trouver la taille              | $O(1)$ - instantané                                                                 | $O(1)$ - instantané           |

\footnotesize *Selon s'il y a assez d'espace disponible

# Pile (Stack)

Une `Pile` est conceptuellement comme une pile de vaisselle : on a
seulement accès au dessus

&nbsp;

*LIFO : Last In, First Out*

[columns]

[column=0.5]

On peut :

- Regarder le dessus de la pile
- Ajouter sur le dessus
- Retirer du dessus et récupérer la valeur
- Vérifier si la pile est vide

[column=0.5]

\center

![C'est une mauvaise idée de commencer par prendre l'assiette du dessous...](img/plates.png){width=60%}

[/columns]

# Pile (Stack)

![Exemple d'utilisation d'une pile](img/stack.png)

- `peek()` : Regarder le dessus de la pile
- `push(int x)` : Ajouter `x` sur le dessus
- `pop()` : Retirer du dessus et récupérer la valeur
- `isEmpty()` : Vérifier si la pile est vide

# Pile (Stack)

## Question

Une `Pile` est elle un type abstrait ou une structure de données
concrète ?

\pause

&nbsp;

Un type abstrait : on définit la façon dont on peut utiliser une Pile,
il y a différentes façons de coder une classe qui respecte cette
spécification

&nbsp;

\pause

*Notez tout de même*: la classe `Stack` des `Collection`s de Java est
une *implémentation concrète* du concept de Pile

# Pile (Stack)

## Question

Comment en faire l'implémentation ?

- Avec des noeuds chaînés ?
- Avec un tableau ?
- Autre idée ?

# File (Queue)

Une `File` est conceptuellement l'"inverse" d'une `Pile` : premier
arrivé, premier servi

&nbsp;

*FIFO : First In First Out*

\center

![](img/fwat.jpg){width=80%}&nbsp;

# File (Queue)

![Exemple d'utilisation d'une file](img/queue.png)


- `enqueue(x)` : Mettre dans la file (enfiler)
- `dequeue()` : Enlever de la file (défiler)
- `isEmpty()` : Vérifier si la file est vide

# File (Queue)

## Question

Comment en faire l'implémentation ?

- Idées ?

&nbsp;

\pause

*Notez* : en Java, l'interface `Deque` (pour double-ended queue) est
implantée par la classe `LinkedList` (donc avec une Liste doublement
chaînée qui a un accès au premier noeud et au dernier noeud)

# Arbre

[columns]

[column=0.6]

Un arbre est une structure de données hiérarchique

- Le premier noeud est la racine
- Chaque noeud peut être suivi de $0, 1, 2, ..., N$ noeuds
- Chaque noeud réfère à ses *enfants* et a exactement 1 noeud
  *parent* (sauf pour la racine, qui n'en a pas)
- Un noeud qui n'a pas d'enfant est appelé une *feuille*

[column=0.4]

\center

![](img/gv/05.arbre.png)&nbsp;

[/columns]

# Arbre

![Exemple d'utilisation d'un arbre comme structure de données : le système de fichiers Unix](img/gv/08.fs.png)

# Arbre binaire

[columns]

[column=0.4]

Un *arbre binaire* est un cas particulier d'un arbre où chaque noeud a
au plus 2 enfants

[column=0.6]

![](img/gv/05.arbrebin.png)&nbsp;

[/columns]

# Arbre binaire de recherche

On peut organiser les noeuds selon leur valeur :

- Gauche : valeurs `<=` valeur du parent
- Droite : valeurs `>` valeur du parent

\center

![](img/gv/05.arbrebinrech.png){width=60%}&nbsp;

# Arbre binaire de recherche

```java
public class Noeud {
    private int valeur;
    private Noeud gauche;
    private Noeud droite;

    // ...
}

// Arbre Binaire de Recherche
public class ABR {
    private Noeud racine;

    // Méthodes pour chercher/ajouter/supprimer/...
}
```

# Arbre binaire de recherche

- Recherche d'une valeur $x$ :

[columns]

[column=0.55]

- Commencer avec `noeud =` *racine de l'arbre*
- Comparer $x$ avec `noeud`
    - Si `noeud == null`, \newline $x$ n'est pas dans l'arbre
    - Si $x$ `== noeud`, \newline valeur trouvée
    - Si $x <$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      gauche*
    - Si $x >$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      droite*

[column=0.7]

\center

![](img/gv/05.arbrebinfind.1.png){width=80%}&nbsp;

[/columns]

# Arbre binaire de recherche

- Recherche d'une valeur $x$ :

[columns]

[column=0.55]

- Commencer avec `noeud =` *racine de l'arbre*
- Comparer $x$ avec `noeud`
    - Si `noeud == null`, \newline $x$ n'est pas dans l'arbre
    - Si $x$ `== noeud`, \newline valeur trouvée
    - Si $x <$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      gauche*
    - Si $x >$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      droite*

[column=0.7]

\center

![](img/gv/05.arbrebinfind.2.png){width=80%}&nbsp;

[/columns]

# Arbre binaire de recherche

- Recherche d'une valeur $x$ :

[columns]

[column=0.55]

- Commencer avec `noeud =` *racine de l'arbre*
- Comparer $x$ avec `noeud`
    - Si `noeud == null`, \newline $x$ n'est pas dans l'arbre
    - Si $x$ `== noeud`, \newline valeur trouvée
    - Si $x <$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      gauche*
    - Si $x >$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      droite*

[column=0.7]

\center

![](img/gv/05.arbrebinfind.3.png){width=80%}&nbsp;

[/columns]

# Arbre binaire de recherche

- Recherche d'une valeur $x$ :

[columns]

[column=0.55]

- Commencer avec `noeud =` *racine de l'arbre*
- Comparer $x$ avec `noeud`
    - Si `noeud == null`, \newline $x$ n'est pas dans l'arbre
    - Si $x$ `== noeud`, \newline valeur trouvée
    - Si $x <$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      gauche*
    - Si $x >$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      droite*

[column=0.7]

\center

![](img/gv/05.arbrebinfind.4.png){width=80%}&nbsp;

[/columns]

# Arbre binaire de recherche

- Recherche d'une valeur $x$ :

[columns]

[column=0.55]

- Commencer avec `noeud =` *racine de l'arbre*
- Comparer $x$ avec `noeud`
    - Si `noeud == null`, \newline $x$ n'est pas dans l'arbre
    - Si $x$ `== noeud`, \newline valeur trouvée
    - Si $x <$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      gauche*
    - Si $x >$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      droite*

[column=0.7]

\center

![](img/gv/05.arbrebinnotfound.1.png){width=80%}&nbsp;

[/columns]

# Arbre binaire de recherche

- Recherche d'une valeur $x$ :

[columns]

[column=0.55]

- Commencer avec `noeud =` *racine de l'arbre*
- Comparer $x$ avec `noeud`
    - Si `noeud == null`, \newline $x$ n'est pas dans l'arbre
    - Si $x$ `== noeud`, \newline valeur trouvée
    - Si $x <$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      gauche*
    - Si $x >$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      droite*

[column=0.7]

\center

![](img/gv/05.arbrebinnotfound.2.png){width=80%}&nbsp;

[/columns]

# Arbre binaire de recherche

- Recherche d'une valeur $x$ :

[columns]

[column=0.55]

- Commencer avec `noeud =` *racine de l'arbre*
- Comparer $x$ avec `noeud`
    - Si `noeud == null`, \newline $x$ n'est pas dans l'arbre
    - Si $x$ `== noeud`, \newline valeur trouvée
    - Si $x <$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      gauche*
    - Si $x >$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      droite*

[column=0.7]

\center

![](img/gv/05.arbrebinnotfound.3.png){width=80%}&nbsp;

[/columns]


# Arbre binaire de recherche

- Recherche d'une valeur $x$ :

[columns]

[column=0.55]

- Commencer avec `noeud =` *racine de l'arbre*
- Comparer $x$ avec `noeud`
    - Si `noeud == null`, \newline $x$ n'est pas dans l'arbre
    - Si $x$ `== noeud`, \newline valeur trouvée
    - Si $x <$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      gauche*
    - Si $x >$ `noeud`, \newline recommencer avec \newline `noeud =` *enfant
      droite*

[column=0.7]

\center

![](img/gv/05.arbrebinnotfound.4.png){width=80%}&nbsp;

[/columns]

# Arbre binaire de recherche

Quelle est la performance de rechercher un valeur ?

- Dans une liste chaînée ? \pause $O(n)$
    - Besoin de traverser tous les noeuds \pause
- Dans un tableau non-trié ? \pause $O(n)$ \pause
- Dans un tableau trié ? \pause $O(log(n))$
    - Par recherche dichotomique \pause
- Dans une liste chaînée triée ? \pause $O(n)$
    - On ne peut pas faire de recherche dichotomique ici, on ne peut
      pas sauter à un noeud arbitraire de la liste sans commencer par
      traverser tous les autres ! \pause
- Dans un arbre binaire de recherche ? \pause $O(\text{profondeur de l'arbre})$
    - $O(log(n))$ si l'arbre est balancé
    - $O(n)$ en pire cas, si l'arbre est complètement débalancé

# Arbre binaire de recherche

![Arbre binaire balancé : O(log(n)) opérations pour rechercher une valeur](img/gv/05.arbrebin.balanced.png)

# Arbre binaire de recherche

![Arbre binaire non-balancé : O(n) opérations (en pire cas) pour rechercher une valeur](img/gv/05.arbrebin.unbalanced.png)

# Arbre binaire de recherche

- Insérer une valeur $x$ :

[columns]

[column=0.55]

- Similaire à la recherche

- On descend dans l'arbre (en allant à gauche/droite selon la valeur
  du noeud actuel) jusqu'à trouver un endroit disponible où insérer le
  nouveau noeud $x$

[column=0.7]

\center

![](img/gv/05.arbrebininsert.0.png){width=80%}&nbsp;

[/columns]


# Arbre binaire de recherche

- Insérer une valeur $x$ :

[columns]

[column=0.55]

- Similaire à la recherche

- On descend dans l'arbre (en allant à gauche/droite selon la valeur
  du noeud actuel) jusqu'à trouver un endroit disponible où insérer le
  nouveau noeud $x$

[column=0.7]

\center

![](img/gv/05.arbrebininsert.1.png){width=80%}&nbsp;

[/columns]


# Arbre binaire de recherche

- Insérer une valeur $x$ :

[columns]

[column=0.55]

- Similaire à la recherche

- On descend dans l'arbre (en allant à gauche/droite selon la valeur
  du noeud actuel) jusqu'à trouver un endroit disponible où insérer le
  nouveau noeud $x$

[column=0.7]

\center

![](img/gv/05.arbrebininsert.2.png){width=80%}&nbsp;

[/columns]


# Arbre binaire de recherche

- Insérer une valeur $x$ :

[columns]

[column=0.55]

- Similaire à la recherche

- On descend dans l'arbre (en allant à gauche/droite selon la valeur
  du noeud actuel) jusqu'à trouver un endroit disponible où insérer le
  nouveau noeud $x$

[column=0.7]

\center

![](img/gv/05.arbrebininsert.3.png){width=80%}&nbsp;

[/columns]


# Arbre binaire de recherche

- Insérer une valeur $x$ :

[columns]

[column=0.55]

- Similaire à la recherche

- On descend dans l'arbre (en allant à gauche/droite selon la valeur
  du noeud actuel) jusqu'à trouver un endroit disponible où insérer le
  nouveau noeud $x$

[column=0.7]

\center

![](img/gv/05.arbrebininsert.4.png){width=80%}&nbsp;

[/columns]

# Arbre binaire de recherche

- Supprimer un noeud :

[columns]

[column=0.55]

- Similaire à la recherche (on doit commencer par trouver le noeud
  qu'on souhaite retirer)

- On doit cependant *réorganiser l'arbre* lorsqu'on supprime un noeud

[column=0.7]

\center

![](img/gv/05.arbrebindel.1.png){width=80%}&nbsp;

[/columns]

# Arbre binaire de recherche

- Supprimer un noeud :

[columns]

[column=0.55]

\center

**Cas 1**

&nbsp;

- 0 enfant
- Rien de spécial à faire

&nbsp;

&nbsp;

&nbsp;

&nbsp;

[column=0.7]

\center

![](img/gv/05.arbrebindel.3.png){width=80%}&nbsp;

[/columns]

# Arbre binaire de recherche

- Supprimer un noeud :

[columns]

[column=0.55]

\center

**Cas 2**

&nbsp;

- 1 enfant
- Remonter l'enfant à la place du noeud supprimé

&nbsp;

&nbsp;

&nbsp;

[column=0.7]

\center

![](img/gv/05.arbrebindel.2.png){width=80%}&nbsp;

[/columns]


# Arbre binaire de recherche

- Supprimer un noeud :

[columns]

[column=0.55]

\center

**Cas 3**

&nbsp;

- 2 enfants
- Remplacer le noeud au choix par un des deux :
    * Le plus petit à droite
    * Le plus grand à gauche
- Gérer l'enfant de ce noeud au besoin

[column=0.7]

\center

![](img/gv/05.arbrebindel.0.png){width=80%}&nbsp;

[/columns]

# Arbre binaire de recherche

## Question

&nbsp;

Un arbre binaire de recherche est une structure de données abstraite
ou concrète ?

&nbsp;

# Exercice

Combien d'opérations sont exécutées dans le code suivant ?

```java
Liste liste = new TableauDynamique(100); // pré-allouer 100 éléments

liste.add(1);
liste.add(2);
liste.add(3);
liste.add(4);
// ...
liste.add(100);

for(int i=0; i<liste.size(); i++) {
    System.out.println(liste.get(i));
}
```

# Exercice

Combien d'opérations sont exécutées dans le code suivant ?

```java
Liste liste = new ListeChainee();

liste.add(1);
liste.add(2);
liste.add(3);
liste.add(4);
// ...
liste.add(100);

for(int i=0; i<liste.size(); i++) {
    System.out.println(liste.get(i));
}
```

# Itérateurs

Le code précédent pose problème...

&nbsp;

Atteindre le $i^{ème}$ élément d'une liste chaînée via la méthode
`liste.get(i)` demande de suivre $i$ références.

- `=> liste.get(1) =>` 1 référence à suivre
- `=> liste.get(2) =>` 2 références à suivre
- `=> liste.get(3) =>` 3 références à suivre
- ...
- `=> liste.get(100) =>` 100 références à suivre

&nbsp;

$$1 + 2 + 3 + ... + (n-1) + n = n(n - 1)/2 \approx n^2 \text{ !}$$

# Itérateurs

Il nous faut une façon efficace de parcourir une liste !

&nbsp;

- Solution : garder une référence vers l'élément actuel
- Concrètement : utiliser un `Iterator`
- Façon uniforme de parcourir les différentes structures de données
    - Sert à "traverser tous les éléments"
    - S'applique à toutes les structures de données vues à date, mêmes
      celles qui ne stockent pas les données de façon ordonnées (ex.:
      un Ensemble)

# Itérateurs

Un itérateur a les méthodes suivantes (définies dans une `interface
Iterator`) :

- `iterator.hasNext()` : `boolean` qui indique s'il reste un élément à
  visiter
- `iterator.next()` : permet de visiter le prochain élément

&nbsp;

Les classes qui implantent l'interface `Liste` peuvent fournir une
méthode pour obtenir un objet de type `Iterator` qui parcourt tous les
éléments

# Itérateurs

On peut coder différentes classes qui permettent de traverser les
différentes structures de données concrètes :

```java
public class ListeChaineeIterator implements Iterator {
    public boolean hasNext() {
        /* Indique si on est arrivé à la fin
           de la chaîne ou non */
    }
    
    public int next() {
       /* Retourne la valeur du noeud actuel et
          avance au prochain noeud */
    }
}
```

# Itérateurs

On peut coder différentes classes qui permettent de traverser les
différentes structures de données concrètes :

```java
public class TableauDynamiqueIterator implements Iterator {
    public boolean hasNext() {
        /* Indique si l'indice >= à la taille
           du tableau */
    }
    
    public int next() {
       /* Retourne la valeur du tab[i] avance au
          prochain index avec i++ */
    }
}
```


# Itérateurs

```java
TableauDynamique liste = new TableauDynamique(); // ... liste.add(...)
Iterator it = liste.iterator(); // -> TableauDynamiqueIterator

while(it.hasNext()) {
    int valeur = it.next();
    System.out.println(valeur);
}
```

\center

![](img/gv/05.iterator-arraylist.0.png){width=72%}&nbsp;

# Itérateurs

```java
TableauDynamique liste = new TableauDynamique(); // ... liste.add(...)
Iterator it = liste.iterator(); // -> TableauDynamiqueIterator

while(it.hasNext()) {
    int valeur = it.next();
    System.out.println(valeur);
}
```

\center

![](img/gv/05.iterator-arraylist.1.png){width=72%}&nbsp;

# Itérateurs

```java
TableauDynamique liste = new TableauDynamique(); // ... liste.add(...)
Iterator it = liste.iterator(); // -> TableauDynamiqueIterator

while(it.hasNext()) {
    int valeur = it.next();
    System.out.println(valeur);
}
```

\center

![](img/gv/05.iterator-arraylist.2.png){width=72%}&nbsp;

# Itérateurs

```java
TableauDynamique liste = new TableauDynamique(); // ... liste.add(...)
Iterator it = liste.iterator(); // -> TableauDynamiqueIterator

while(it.hasNext()) {
    int valeur = it.next();
    System.out.println(valeur);
}
```

\center

![](img/gv/05.iterator-arraylist.3.png){width=72%}&nbsp;

# Itérateurs

```java
TableauDynamique liste = new TableauDynamique(); // ... liste.add(...)
Iterator it = liste.iterator(); // -> TableauDynamiqueIterator

while(it.hasNext()) {
    int valeur = it.next();
    System.out.println(valeur);
}
```

\center

![](img/gv/05.iterator-arraylist.4.png){width=72%}&nbsp;

# Itérateurs

```java
TableauDynamique liste = new TableauDynamique(); // ... liste.add(...)
Iterator it = liste.iterator(); // -> TableauDynamiqueIterator

while(it.hasNext()) {
    int valeur = it.next();
    System.out.println(valeur);
}
```

\center

![](img/gv/05.iterator-arraylist.5.png){width=72%}&nbsp;

# Itérateurs

```java
Liste liste = new ListeChainee(); // ... liste.add(...)
Iterator it = liste.iterator(); // -> ListeChaineeIterator

while(it.hasNext()) {
    int valeur = it.next();
    System.out.println(valeur);
}
```

\center

![](img/gv/05.iterator.0.png){width=72%}&nbsp;

# Itérateurs

```java
Liste liste = new ListeChainee(); // ... liste.add(...)
Iterator it = liste.iterator(); // -> ListeChaineeIterator

while(it.hasNext()) {
    int valeur = it.next();
    System.out.println(valeur);
}
```

\center

![](img/gv/05.iterator.1.png){width=72%}&nbsp;

# Itérateurs

```java
Liste liste = new ListeChainee(); // ... liste.add(...)
Iterator it = liste.iterator(); // -> ListeChaineeIterator

while(it.hasNext()) {
    int valeur = it.next();
    System.out.println(valeur);
}
```

\center

![](img/gv/05.iterator.2.png){width=72%}&nbsp;

# Itérateurs

```java
Liste liste = new ListeChainee(); // ... liste.add(...)
Iterator it = liste.iterator(); // -> ListeChaineeIterator

while(it.hasNext()) {
    int valeur = it.next();
    System.out.println(valeur);
}
```

\center

![](img/gv/05.iterator.3.png){width=72%}&nbsp;

# Itérateurs

```java
Liste liste = new ListeChainee(); // ... liste.add(...)
Iterator it = liste.iterator(); // -> ListeChaineeIterator

while(it.hasNext()) {
    int valeur = it.next();
    System.out.println(valeur);
}
```

\center

![](img/gv/05.iterator.4.png){width=72%}&nbsp;

# Itérateurs

```java
Liste liste = new ListeChainee(); // ... liste.add(...)
Iterator it = liste.iterator(); // -> ListeChaineeIterator

while(it.hasNext()) {
    int valeur = it.next();
    System.out.println(valeur);
}
```

\center

![](img/gv/05.iterator.5.png){width=72%}&nbsp;

# Itérateurs

- Java vient déjà avec une interface `Iterator` dans sa
bibliothèque standard, qui a les méthodes `hasNext()` et `next()`
- Variante de la boucle `for` pour simplifier l'utilisation d'un
  `Iterator` :

```java
// Boucle for sur une classe qui implémente Iterator
for(int val : liste) {
    System.out.println(val);
}

// Équivalent à :
while(it.hasNext()) {
    int val = it.next();
    System.out.println(val);
}
```

# Généralisation

Quoi faire si on veut une `ListeChainee` qui contient autre chose que
des `int` ? \pause

&nbsp;

- Dupliquer les classes `ListeChainee` et `Noeud`, puis remplacer les
  `int valeur` par des `String`, `double`, `Etudiant`, ... selon ce
  qu'on veut

\pause

- Faire une seule copie des classes, où la `valeur` de chaque `Noeud`
  est stockée sous forme d'`Object`, puis `(caster)` partout

\pause

- Utiliser les **types paramétrés de Java**

# Les Generics : Types Paramétrés

Solution : utiliser des *types paramétrés*

- Permettent de définir et d'utiliser un type (une
classe) paramétré par un autre type
    -  Ex: une liste chaînée de `String`, un tableau dynamique
       d'`Etudiant`s, ...
- Pas besoin de réécrire une nouvelle classe pour chaque cas
- Pas besoin de faire des casts depuis `Object`

# Les Generics : Types Paramétrés

- Syntaxe d'utilisation : le paramètre de type est spécifié entre

`< ... >`

```java
// Créer une liste chaînée de type String
ListeChainee<String> liste = new ListeChainee<String>();

liste.add("Bonjour !");

// Ok !
String s = liste.get(0);
```

- Pour l'utilisation, on peut considérer `ListeChainee<String>`,
`ListeChainee<Object>`, `ListeChainee<Polygone>`, ... comme des
classes ordinaires, avec un nom particulier, comme si quelqu'un avait
réécrit chaque cas

# Les Generics : Types Paramétrés

```java
String s = liste.get(0);
```

- La méthode `get(int idx)` de `ListeChainee<String>` retournera une
référence de type `String`

&nbsp;

- *Notez* : Les classes `ListeChainee<...>` n'ont pas de lien
  entre elles
    - `ListeChainee<String>` n'est pas une sous-classe de
      `ListeChainee<Object>`
    - `ListeChainee<Etudiant>` n'est pas une sous-classe de
      `ListeChainee<Universitaire>`
    - Pas de hiérarchie, ce sont des classes distinctes

# Définition d'un type générique

- Plutôt que de réécrire chaque cas individuellement, les generics
  permettent de définir une "recette" générique

```java
public class Conteneur<E> {
    private E contenu;

    public Conteneur(E element) {
        this.contenu = element;
    }

    public E getContenu() {
        return this.contenu;
    }

    public void setContenu(E element) {
        this.contenu = element;
    }
}
```

# Définition d'un type générique


- Ici `E` est le **paramètre formel de type** et sera remplacé partout
  par le type spécifié au moment de la déclaration d'un type de `Conteneur<...>`
  particulier.


```java
Etudiant jimmy = new Etudiant("Jimmy", "Whooper")
Conteneur<Etudiant> c = new Conteneur<Etudiant>(jimmy);
```

Conceptuellement, comme si on déclarait automatiquement :

[columns]

[column=0.48]

```java
public class Conteneur<E> {
  private E contenu;

  public Conteneur(E element) {
    this.contenu = element;
  }
  // ...
}
```

[column=0.6]

```java
public class Conteneur {
  private Etudiant contenu;

  public Conteneur(Etudiant element) {
    this.contenu = element;
  }
  // ...
}
```

[/columns]

# Nom du paramètre de type

- On aurait pu utiliser n'importe quel nom à la place de E mais la
  librairie de Java adopte une certaine convention de nommage :
    - E - Element (utilisé notamment dans les `Collection`s)
    - K - Key
    - N - Number
    - T - Type
    - V - Value
    - S, U, V, etc - 2ème type, 3ème, 4ème, ...

# Plusieurs types paramétrés

- On peut avoir plusieurs paramètres de type
    - Séparés par des virgules dans les `<>`

```java
public class Paire<A, B> {
    private A x;
    private B y;

    public Paire(A x, B y) {
        this.x = x;
        this.y = y;
    }

    public A getX() {
        return x;
    }
    ...
```

# Plusieurs types paramétrés


```java
    ...
    public void setX(A x) {
        this.x = x;
    }

    public B getY() {
        return y;
    }

    public void setY(B y) {
        this.y = y;
    }
}
```

# Plusieurs types paramétrés


Utilisation :

```java
Paire<Etudiant, String> paire;

Etudiant jimmy = new Etudiant("Jimmy", "Whooper");

paire = new Paire<Etudiant, String>(jimmy, "Charismatique");
```

# `ArrayList<E>` et `LinkedList<E>`

En réalité, les listes de Java ne sont pas limitées à un seul type,
elles utilisent les `generics` :

```java
LinkedList<Etudiant> inscriptions = new LinkedList<Etudiant>();
inscriptions.add(new Etudiant("Jimmy", "Whooper"));
inscriptions.add(new Etudiant("Johnny", "Whiper"));

for(Etudiant e : inscriptions) {
    System.out.println(e.nomComplet() + " est inscrit !");
}

/* Affiche :
Jimmy Whooper est inscrit !
Johnny Whiper est inscrit !
*/
```

# `ArrayList<E>` et `LinkedList<E>`

```java
/* Notez : pour utiliser des ArrayList de types primitifs,
   on *doit* utiliser les classes wrapper de Java */
ArrayList<Integer> listeNombres = new ArrayList<Integer>();
listeNombres.add(new Integer(10));

// Auto-boxé en Integer (voir chapitre 4)
listeNombres.add(55);


// INCORRECT, int n'est pas une classe, c'est un type primitif
ArrayList<int> listeNombres = new ArrayList<int>();
```

# Opérateur *diamond* `<>`

Note : lors de la déclaration, on peut utiliser l'*"opérateur
diamond"*, soit `<>`, pour simplifier l'écriture du code :


```java
// Façon longue :
ArrayList<Etudiant> tab = new ArrayList<Etudiant>();

// Équivalent :
ArrayList<Etudiant> tab = new ArrayList<>();


/* INCORRECT, le type de tab doit être défini correctement,
   seul le "new ..." peut être résolu automatiquement */
ArrayList<> tab = new ArrayList<Etudiant>();
```

# Quelques notes sur les Generics

On peut définir des interfaces génériques exactement de la même façon
qu'on définit des classes génériques :

```java
public interface MonInterface<E> {

    public E truc(E argument);

}
```

&nbsp;

Les interfaces `List<E>` et `Iterator<E>` de la librairie standard en
Java sont d'ailleurs des interfaces génériques !

# Quelques notes sur les Generics


On peut utiliser des types arbitrairement imbriqués...

```java


Paire<Paire<String, Paire<String, Paire<String, String>>>> p = ...;


```

... Mais un grand nombre d'imbrications de types est généralement
signe que vous devriez plutôt définir des classes spécialisées pour
régler votre problème


# Limitations des générics de Java : *type erasure*

Les types sont vérifiés lors de la compilation mais l'information
n'est pas retenue dans le code : le *paramètre de type* `E` n'est pas
disponible lors de l'exécution.

&nbsp;

Entre autres :

- `E` ne peut pas être instancié
    - `new E()` impossible
    - `E[] tab = new E[10];` impossible
- On ne peut pas vérifier si `x instanceof E`


# `Map`

- Un `Map` (ou **Dictionnaire**, ou **Tableau associatif**) est une
  structure de données qui permet d'associer des clés et des valeurs
  de types arbitraires :

```
Integer => Integer
------------------
1       => 500
10      => 3267152
22      => 273661
...
```

# `Map`

- Un `Map` (ou **Dictionnaire**, ou **Tableau associatif**) est une
  structure de données qui permet d'associer des clés et des valeurs
  de types arbitraires :

```
String     => String
--------------------------------
"bonjour"  => "hello"
"enchanté" => "nice to meet you"
"welcome"  => "bienvenue"
...
```

# `Map`

- Un `Map` (ou **Dictionnaire**, ou **Tableau associatif**) est une
  structure de données qui permet d'associer des clés et des valeurs
  de types arbitraires :

```
Etudiant => String
-----------------------
jimmy    => "courageux"
johnny   => "espiègle"
xavier   => "courtois"
...
```

# `Map`

- `Map` est un type abstrait
- Plusieurs implantations concrètes (qu'on ne verra pas en détail)
    - `TreeMap<K, V>`
    - `HashMap<K, V>`
- `TreeMap` : recherche basée sur un arbre binaire de recherche
- `HashMap` : structure avancée
    - Étudiée dans le cours *IFT2035 - Structures de données*

# `Map`

```java
Etudiant jimmy = ..., johnny = ..., xavier = ...;

HashMap<Etudiant, String> notes = new HashMap<>();

notes.put(jimmy, "courageux");
notes.put(johnny, "espiègle");
notes.put(xavier, "courtois");

System.out.println(notes.get(johnny));
// => espiègle
```

`=>` Structure de données très utile dans la vraie vie !

Cherchez sur internet pour trouver toute la documentation

# `Map`

- Notez que les enregistrements de *JavaScript* sont en quelque sorte
  des `Map<String, Object>`...

```javascript
var map = {}; // Nouveau dictionnaire

map.bonjour = 10;

// équivalent à :
map["bonjour"] = 10;

map["test"] = [1,2,3];
console.log("" + map.test);
// => 1,2,3
```
