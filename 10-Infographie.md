---
title: Infographie 2D & Animation
author: Par Nicolas Hurtubise
date: IFT1025 - Programmation 2
---

# Dessin de composantes

<!-- https://gamedevelopment.tutsplus.com/tutorials/introduction-to-javafx-for-game-development--cms-23835 -->

- On a vu comment dessiner des objets graphiques sur l'écran avec
  JavaFX
- À la base, la seule chose que votre ordinateur sait faire, c'est
  donner une certaine couleur aux pixels de votre écran...

&nbsp;

- "Dessiner une composante" est donc un concept d'assez haut niveau
  que JavaFX abstrait
    - Bouton = rectangle de pixels d'une certaine couleur
    - Plus foncé sur les bordures
    - Dégradé vertical
    - Plus pâle si la souris passe par-dessus
    - ...

![](img/fx-plain-btn.png)

# Pixel

- Vient de *Pic*ture *El*ement
- Plus petit élément contrôllable de l'écran
- Chaque pixel a une couleur
- On peut décomposer n'importe quelle une couleur affichable par
  l'écran en une combinaison de couleurs primaires
    - Ex.: $Couleur = (I_{Rouge}, I_{Vert}, I_{Bleu})$
    - 3 *canaux* de 8 bits chacun
    - 8 bits `=>` chaque canal peut avoir une valeur de 0 à 255
      (inclus)

## Exemples

```
Noir  = (0, 0, 0)
Blanc = (255, 255, 255)
Rouge = (255, 0, 0)
Vert  = (0, 255, 0)
Jaune = (255, 255, 0)
```

# Pixel

![Vue de près d'un écran d'ordinateur standard - Mrmariokartguy https://commons.wikimedia.org/wiki/File:Light_pixel-beam.jpg](img/Light_pixel-beam.jpg){width=70%}

# La composante `Canvas`

L'objet `Canvas` en JavaFX donne une surface sur laquelle dessiner des
choses arbitraires

```java
    public void start(Stage primaryStage) throws Exception {
        int w = 640;
        int h = 480;

        Pane root = new Pane();
        Scene scene = new Scene(root, w, h);
        Canvas canvas = new Canvas(w, h);

        root.getChildren().add(canvas);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
```

# La composante `Canvas`

- $(x, y) = (0,0)$ en haut à gauche
- $x$ croissant vers la droite
- $y$ croissant vers le bas

\center

![](img/fx-canvas.png){width=70%}&nbsp;

# Dessiner des primitives

On peut récupérer le *contexte graphique* d'un canvas avec la méthode
`.getGraphicsContext2D()` et l'utiliser pour dessiner des formes :

```java
GraphicsContext context = canvas.getGraphicsContext2D();

context.fillRect(250, 250, 130, 90);
```

\center

![](img/fx-canvas1.png){width=50%}&nbsp;

# Dessiner des formes primitives

`GraphicsContext` gère l'état du canvas :

- Quelle couleur est utilisée pour remplir les formes et dessiner les
  lignes/contours
- Quelle police utiliser
- Quels effets appliquer (ombres, flous, ...)

&nbsp;

Quelques méthodes de dessin sont disponibles :

- `fillRect(double x, double y, double w, double h)`
    - Pour dessiner un rectangle de taille $w$ par $h$ dont le coin
      haut-gauche est au pixel $(x, y)$
- `fillOval(double x, double y, double w, double h)`
    - Pour dessiner un oval de taille $w$ par $h$ qui commence à $(x,
      y)$

# Dessiner des formes primitives

- `fillPolygon(double[] xPoints, double[] yPoints, int nPoints)`
    - Pour dessiner un polygone quelconque avec $nPoints$ sommets
      positionnés aux coordonnées contenues dans les tableaux
      `xPoints` et `yPoints`
- `fillRoundRect(double x, double y, double w, double h, double
  arcWidth, double arcHeight)`
    - Équivalent à fillRect, mais avec des coins arrondis

# Dessiner des formes primitives

- `strokeLine(double x1, double y1, double x2, double y2)`
    - Dessine une ligne du point `x1`, `y1` au point `x2`, `y2`
- `strokeRect`, `strokeOval`, `strokePolygon`, ...
    - pour dessiner seulement les contours des formes plutôt que de
      les remplir d'une couleur

# Dessiner du texte et des images

En plus des formes et contours, on a des méthodes pour dessiner des
objets un peu plus complexes

- `fillText(String text, double x, double y)`
    - Permet de dessiner du texte sur le canvas

- `drawImage(Image img, double x, double y)`
    - Permet de dessiner une image sur le canvas

# Exemple

```java
int w = 640, h = 480;

Pane root = new Pane();
Scene scene = new Scene(root, w, h);
Canvas canvas = new Canvas(w, h);
root.getChildren().add(canvas);

GraphicsContext context = canvas.getGraphicsContext2D();

context.fillRect(200, 120, 70, 30);
context.fillOval(130, 235, 60, 50);
context.fillPolygon(
        new double[]{60, 70, 50, 27, 49},
        new double[]{42, 85, 90, 120, 60},
        5);
context.fillRoundRect(250, 250, 40, 90, 20, 30);
context.strokeLine(150, 55, 300, 250);
context.fillText("\"twado", 20, 180);
context.drawImage(new Image("/queen.png"), 350, 140);
```

# Exemple

![Résultat](img/fx-canvas2.png)

# Paramètres du `context`

Pour modifier la couleur de remplissage, la couleur des traits, la
police de caractères utilisée, la taille des lignes et d'autres
paramètres, on peut utiliser des mutateurs comme :

- `setFill(Paint p)` : change la couleur de remplissage
- `setStroke(Paint p)` : change la couleur de lignes/contours
- `setFont(Font f)` : change la police d'écriture
- ...

# Paramètres du `context`

Un objet de type `Paint` (classe abstraite) pourrait par exemple être
une instance de la classe `Color` (sous-classe de `Paint`) :

```java
/* Couleur définie au format RGBa, où les quantité de chaque couleur
   est donnée entre 0 et 1

   a = canal alpha = opacité de la couleur
   0 veut dire 100% transparent
   1 veut dire 100% opaque */
new Color(0, 0.5, 0.9, 1)

Color.rgb(125, 88, 222) // Couleur définie au format RGB
Color.RED               // Couleur pré-définie
Color.BLUE              // Couleur pré-définie

// Notez que beaucoup de couleurs sont pré-définies...
Color.MISTYROSE, Color.BLANCHEDALMOND, Color.MEDIUMAQUAMARINE

Color.web("#AA00FF")    // Couleur RGB au format hexadécimal
```

# Exemple

```java
context.setFill(Color.rgb(255, 125, 0));
context.fillRect(200, 120, 70, 30);
context.fillOval(130, 235, 60, 50);
context.strokeOval(130, 235, 60, 50);
context.fillPolygon(
    new double[]{60, 70, 50, 27, 49},
    new double[]{42, 85, 90, 120, 60},
    5);
context.setFill(Color.rgb(125, 88, 222));
context.fillRoundRect(250, 250, 40, 90, 20, 30);

context.setStroke(Color.RED);
context.setLineWidth(5);
context.strokeLine(150, 55, 300, 250);
context.setFont(Font.font("Purisa", 28));
context.fillText("\"twado", 20, 180);

context.drawImage(new Image("/queen.png"), 350, 140);
```

# Exemple

![Résultat](img/fx-canvas3.png)

# `Paint`

- La classe `Paint` est également classe-mère de d'autres classes, par
  exemple, `LinearGradient`, qui permet de définir des dégradés :

```java
Paint grad = new LinearGradient(
    0, 0, 100, 0, false, CycleMethod.REPEAT,
    new Stop(0, Color.BLUE), new Stop(100, Color.RED)
);

context.setFill(grad);
```

\center


![](img/fx-linear-gradient.png){width=40%}&nbsp;


# Animation

Animation = Séquence d'images affichée rapidement

[columns]

[column=0.25]

\center

![](img/animation/ball1.png)&nbsp;

*t=0*

[column=0.25]

\center

![](img/animation/ball2.png)&nbsp;

*t=1*

[column=0.25]

\center

![](img/animation/ball3.png)&nbsp;

*t=2*

[column=0.25]

\center

![](img/animation/ball4.png)&nbsp;

*t=3*

[/columns]

&nbsp;

Pour créer une animation sur un `Canvas`, on peut simplement
redessiner très rapidement une forme en la déplaçant un peu à chaque
fois

# Animation

- JavaFX offre la classe abstraite `AnimationTimer`
- On peut la sous-classer et redéfinir la méthode `handle`, qui sera
automatiquement appelée plusieurs fois par seconde :

```java
AnimationTimer timer = new AnimationTimer() { // Classe anonyme
    @Override
    public void handle(long now) {
        System.out.println("Fonction appelée: " + now);
    }
};
timer.start(); // *** Important : démarrer le timer ***

/* Affiche dans la console:
Fonction appelée: 341755129561759
Fonction appelée: 341755143545649
Fonction appelée: 341755159955423
Fonction appelée: 341755176366685
Fonction appelée: 341755193515747
... */
```

# Animation

- La fonction `public void handle(long now)` est appelée
  automatiquement à chaque *frame*
    - i.e. chaque fois que JavaFX fait un rendu de l'affichage de la
      fenêtre sur l'écran
- Elle reçoit en paramètre `now` le temps actuel en nanosecondes
    - 1 nanoseconde = `1e-9` ($10^{-9}$) seconde
    - Équivalent à appeler `System.nanoTime()` \newline (à quelques
      nanosecondes près)

# Animation

Cette classe sert principalement à créer des animations, par exemple
en faisant évoluer un dessin dans un `Canvas`

```java
context.setFill(Color.RED);

AnimationTimer timer = new AnimationTimer() { // Classe anonyme
    private double x = 0, y = 120;

    @Override
    public void handle(long now) {
        x += 1; // Décale l'objet à droite d'un pixel

        // Efface tout le canvas et redessine le carré
        context.clearRect(0, 0, w, h);
        context.fillRect(x, y, 40, 40);
    }
};
timer.start(); // *** Important : démarrer le timer
```

# Animation

![](img/animation/fx-carre-rouge-1.png){width=33%}
![](img/animation/fx-carre-rouge-2.png){width=33%}
![](img/animation/fx-carre-rouge-3.png){width=33%}

\center

*Voir : programme JavaFXAnimation*

# Animation basée sur le temps

Si on affiche la différence de temps entre chaque appel de la
fonction, on se rend compte que ce n'est pas constant...

```java
AnimationTimer timer = new AnimationTimer() {
    private long lastTime = System.nanoTime();
    @Override
    public void handle(long now) {
        System.out.println(now - lastTime);
        lastTime = now;
    }
};
timer.start();
/* Affiche :
...
16799412
16688279
21089672
17069223
16519262 ...
```

# Animation basée sur le temps

- La fréquence exacte à laquelle JavaFX fait le rendu de la fenêtre
  dépend de beaucoup de choses...

- Si l'ordinateur qu'on utilise est lent ou si plusieurs programmes
  s'exécutent en même temps que le nôtre, JavaFX ne fera pas un rendu
  aussi souvent que dans des conditions normales
- `=>` Notre carré rouge se déplacerait plus lentement sur l'écran !

# Animation basée sur le temps

Pour avoir une vitesse constante d'un ordinateur à l'autre, on doit
plutôt se baser sur le temps au moment du rendu.

&nbsp;

Plutôt que d'incrémenter x d'une valeur arbitraire, on va utiliser le
paramètre `long now` pour :

1. Calculer le temps qui s'est écoulé depuis le dernier appel de la
   fonction `handle` ($\Delta t$, delta-temps)
2. Calculer le déplacement en fonction de la vitesse en pixels/s qu'on
   souhaite avoir

# Animation basée sur le temps

La vitesse est définie comme la dérivée de la position par rapport au
temps :

$$\vec{v} = \frac{d\vec{x}}{dt}$$

Pour un intervalle de temps $\Delta t$, on a donc la variation de
position :

$$\Delta \vec{x} = \Delta t * \vec{v}$$

&nbsp;

&nbsp;

On pourrait utiliser quelque chose comme :

&nbsp;

$v_x$ = 90 pixels/s = 90 * 1e-9 pixels/nanoseconde
$v_y$ = 0, pour un déplacement horizontal seulement


# Animation basée sur le temps

```java
AnimationTimer timer = new AnimationTimer() {
    private long lastTime = 0;
    private double x = 0, y = 150;

    @Override
    public void handle(long now) {

        // Temps en sec = 10^(-9) * temps en nanosec
        double deltaTime = (now - lastTime) * 1e-9;
        x += deltaTime * 90; // 90 pixels/s

        context.clearRect(0, 0, w, h);
        context.fillRect(x, y, 40, 40);
        lastTime = now;
    }
};
```

\pause

Ce qui serait le code correct à un détail près : `lastTime = 0` lors
du premier appel de `handle`, donc `deltaTime` est gigantesque...

# Animation basée sur le temps

```java
AnimationTimer timer = new AnimationTimer() {
    private long lastTime = 0;
    private double x = 0, y = 150;

    @Override
    public void handle(long now) {
        // Saute le premier appel pour éviter un "jump" du carré
        if (lastTime == 0) {
            lastTime = now;
            return;
        }
        double deltaTime = (now - lastTime) * 1e-9;
        x += deltaTime * 90; // 90 pixels/seconde

        context.clearRect(0, 0, w, h);
        context.fillRect(x, y, 40, 40);
        lastTime = now;
    }
};
```

# Animation basée sur le temps

```java
AnimationTimer timer = new AnimationTimer() {
    private long lastTime = 0;
    private double x = 0, y = 150;

    // Autre façon de faire :
    @Override
    public void start() {
        lastTime = System.nanoTime();
        super.start(); // Commence les appels de handle(...)
    }

    @Override
    public void handle(long now) {
        double deltaTime = (now - lastTime) * 1e-9;

        // ...

    }
};
```

# Animation d'images

On peut appliquer ce principe à une suite d'images :

&nbsp;

[columns]

[column=1]

\center

![](img/mario/1.png)![](img/mario/2.png)![](img/mario/3.png)![](img/mario/4.png)

&nbsp;

\footnotesize

*Source : Super Mario Bros - NES - Nintendo*

[/columns]

&nbsp;

&nbsp;

- On peut charger une image par *frame* de notre animation et faire un
`drawImage` avec la bonne image à chaque appel de `handle`
- Cycle d'images `=>` modulo $N_{images}$

# Animation d'images

- Le *frame rate* de notre animation correspond à combien d'images
  sont affichées par seconde
- On peut calculer la frame à afficher à un temps $t$ donné en utilisant
  :

$$idx = \lfloor t * framerate \rfloor \text{ modulo } N_{images}$$

&nbsp;

<!-- PROGRAMME : Mario -->
*Voir : programme Mario*

# Animation d'images

Au début du programme :

```java
/*
 * 16 fps = 16 changements d'image par seconde
 * (ou 16*10^-9 images par nanoseconde)
 */
double frameRate = 16 * 1e-9;

// Charge les images de l’animation dans un tableau
Image[] frames = new Image[]{
    new Image("/1.png"),
    new Image("/2.png"),
    new Image("/3.png")
};
```

# Animation d'images

Dans `handle()` :

```java
    /* Calcul du deltaTime par rapport au
       début de l'animation */
    double deltaTime = now - startTime;
    int frame = (int) (deltaTime * frameRate);

    Image img = frames[frame % frames.length];
    context.clearRect(50, 50, img.getWidth(), img.getHeight());
    context.drawImage(img, 50, 50);
```

# Exemple : balle qui rebondit

On va essayer d'étoffer un peu l'exemple du carré qui se déplace pour
avoir quelque chose d'un peu plus réaliste : une balle qui rebondit.

&nbsp;

Le déplacement reste :

$$\Delta \vec{x} = \Delta t * \vec{v}$$

&nbsp;

Mais quand la balle touche un côté de l'écran, on va inverser la
direction dans laquelle elle se déplace pour simuler un rebond :

$$v_x = -v_x$$


# Exemple : balle qui rebondit

```java
AnimationTimer timer = new AnimationTimer() {
    // Garde trace du temps entre chaque appel
    private long lastTime = 0;

    // Position, vitesse et taille de la balle avec (x, y) au centre
    private double x = WIDTH / 2, y = HEIGHT / 2;
    private double vx = 200, vy = 190;
    private double w = 40, h = 40;

    @Override
    public void handle(long now) {
        if (lastTime == 0) {
            lastTime = now;
            return;
        }

        // Calcul du temps écoulé (en secondes)
        double deltaTime = (now - lastTime) * 1e-9;
```

# Exemple : balle qui rebondit

```java
        x += deltaTime * vx;
        y += deltaTime * vy;

        if (x + w / 2 > WIDTH || x - w / 2 < 0) {
            vx *= -1; // Inverse la vitesse lors d'un rebond
        }

        if (y + h / 2 > HEIGHT || y - h / 2 < 0) {
            vy *= -1; // Même chose en y
        }

        context.clearRect(0, 0, WIDTH, HEIGHT);
        context.fillOval(x - w / 2, y - h / 2, w, h);

        lastTime = now;
    }
};
timer.start();
```

# Exemple : balle qui rebondit

Pour avoir un rebond un peu plus réaliste, on peut amortir un peu la
vitesse à chaque rebond :

```java
        if (x + w / 2 > WIDTH || x - w / 2 < 0) {
            // Inverse et réduit la vitesse lors d'un rebond
            vx *= -0.9;
        }

        if (y + h / 2 > HEIGHT || y - h / 2 < 0) {
            vy *= -0.9;
        }
```

De cette façon, la vitesse va graduellement diminuer à chaque rebond.


# Exemple : balle qui rebondit

![](img/animation/phyisque/1.png){width=33%}
![](img/animation/phyisque/2.png){width=33%}
![](img/animation/phyisque/3.png){width=33%}

\center

*Résultat affiché*

<!-- PROGRAMME : JavaFXBall -->
*Voir : programme JavaFXBall*

# Balle qui rebondit : Glitch...

De temps en temps, il semble y avoir un petit glitch dans le
programme... La balle peut rester coincée sur un côté de l'écran :

&nbsp;

![](img/animation/phyisque/glitch1.png){width=25%}
![](img/animation/phyisque/glitch2.png){width=25%}
![](img/animation/phyisque/glitch3.png){width=25%}
![](img/animation/phyisque/glitch4.png){width=25%}

&nbsp;

Qu'est-ce qui se passe ?

# Balle qui rebondit : Glitch...

Le *frame rate* de notre programme n'est pas constant...

Qu'est-ce qui se passe si $\Delta t$ est plus grand que d'habitude au
moment où la balle traverse le mur ?

\center

![](img/animation/phyisque/glitch-explain1.png)&nbsp;

# Balle qui rebondit : Glitch...

Le *frame rate* de notre programme n'est pas constant...

Qu'est-ce qui se passe si $\Delta t$ est plus grand que d'habitude au
moment où la balle traverse le mur ?

\center

![](img/animation/phyisque/glitch-explain2.png)&nbsp;

# Balle qui rebondit : Glitch...

Le *frame rate* de notre programme n'est pas constant...

Qu'est-ce qui se passe si $\Delta t$ est plus grand que d'habitude au
moment où la balle traverse le mur ?

\center

![](img/animation/phyisque/glitch-explain3.png)&nbsp;

# Balle qui rebondit : Glitch...

Le *frame rate* de notre programme n'est pas constant...

Qu'est-ce qui se passe si $\Delta t$ est plus grand que d'habitude au
moment où la balle traverse le mur ?

\center

![](img/animation/phyisque/glitch-explain4.png)&nbsp;


# Balle qui rebondit : Glitch...

La balle a le temps de traverser le mur, mais n'a plus une vitesse
assez grande (ou on n'a pas un $\Delta t$ assez grand) pour lui
permettre de revenir de l'autre côté.

&nbsp;

Puisque notre condition est basée sur le fait d'être de l'autre côté
du mur ou non, on détecte une nouvelle condition qui inverse la
vitesse à chaque frame :

```java
if (x + w / 2 > WIDTH || x - w / 2 < 0) {
    vx *= -0.9;
}
```

&nbsp;

Deux solutions équivalentes possibles :

1. Ramener la balle dans la fenêtre lorsqu'on voit qu'il y a une
   collision avec un mur
2. Déplacer la balle seulement s'il n'y a pas eu de collision

# Balle qui rebondit (corrigée)

Déplacer la balle seulement lorsqu'il n'y aura pas de collision :

```java
double newX = x + deltaTime * vx;
double newY = y + deltaTime * vy;

if (newX + w / 2 > WIDTH || newX - w / 2 < 0) {
    vx *= -0.9;
} else {
    x = newX;
}

if (newY + h / 2 > HEIGHT || newY - h / 2 < 0) {
    vy *= -0.9;
} else {
    y = newY;
}
context.clearRect(0, 0, WIDTH, HEIGHT);
context.fillOval(x - w / 2, y - h / 2, w, h);
```

<!-- PROGRAMME : JavaFXBall2 -->
*Voir : programme JavaFXBall2*


# Balle qui rebondit : un peu plus réaliste

On peut ajouter une autre composante à la physique de notre balle pour
rendre le tout plus réaliste : la gravité.

&nbsp;

Plutôt que d'avoir une balle qui part dans tous les sens, on aurait
une balle qui est attirée vers le sol et qui finit par se stabiliser
par terre.

- La gravité est simplement une accélération vers le bas
- L'accélération est la dérivée de la vitesse, de la même façon que la
  vitesse est la dérivée de la position :

$$\Delta \vec{v} = \Delta t * \vec{a}$$
$$\Delta \vec{x} = \Delta t * \vec{v}$$

# Balle qui rebondit : un peu plus réaliste

```java
AnimationTimer timer = new AnimationTimer() {
    private long lastTime = 0;
    private double x = WIDTH / 2, y = HEIGHT / 2;
    private double vx = 200, vy = 190;
    private double w = 40, h = 40;

    // Accélération constante vers le bas (200px/s^2)
    private double ax = 0, ay = 200;

    @Override
    public void handle(long now) {
        if (lastTime == 0) {
            lastTime = now;
            return;
        }
```

# Balle qui rebondit : un peu plus réaliste

```java
        // ...

        double deltaTime = (now - lastTime) * 1e-9;

        // Mise à jour de la vitesse
        vx += deltaTime * ax;
        vy += deltaTime * ay;

        // Calcul de la nouvelle position
        double newX = x + deltaTime * vx;
        double newY = y + deltaTime * vy;

        // ...
```

# Balle qui rebondit : un peu plus réaliste

```java
        // Collisions (rien de différent)
        if (newX + w / 2 > WIDTH || newX - w / 2 < 0) {
            vx *= -0.9;
        } else {
            x = newX;
        }

        if (newY + h / 2 > HEIGHT || newY - h / 2 < 0) {
            vy *= -0.9;
        } else {
            y = newY;
        }

        context.clearRect(0, 0, WIDTH, HEIGHT);
        context.fillOval(x - w / 2, y - h / 2, w, h);

        lastTime = now;
    }
};
```

# Exemple : balle qui rebondit

![](img/animation/phyisque/rebound4.png){width=25%}
![](img/animation/phyisque/rebound3.png){width=25%}
![](img/animation/phyisque/rebound2.png){width=25%}
![](img/animation/phyisque/rebound1.png){width=25%}

\center

*Résultat affiché*

<!-- PROGRAMME : JavaFXBall3 -->
*Voir : programme JavaFXBall3*


# Nettoyer le code

- Ça commence à faire beaucoup de code dans l'`AnimationTimer`...
- Qu'est-ce qu'on fait si on veut deux balles plutôt qu'une seule ?

&nbsp;

\pause

`=>` On peut se faire un objet `Balle` !

# Nettoyer le code

```java
// Fichier Balle.java
public class Balle {

    private double x, y;
    private double w, h;

    private double vx = 200, vy = 190;

    // Gravité = accélération vers le bas
    private double ax = 0, ay = 200;

    public Balle(double x, double y, double w, double h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    // + Getters/Setters...
```

# Nettoyer le code


```java
    /**
     * Met à jour la position et la vitesse de la balle
     *
     * @param dt Temps écoulé depuis le dernier update() en secondes
     */
    public void update(double dt) {

        // Mise à jour de la vitesse
        vx += dt * ax;
        vy += dt * ay;

        // Calcul de la nouvelle position
        double newX = x + dt * vx;
        double newY = y + dt * vy;
```

# Nettoyer le code

```java
        // (suite de la méthode update)

        if (newX + w / 2 > JavaFXBall.WIDTH || newX - w / 2 < 0) {
            vx *= -0.9;
        } else {
            x = newX;
        }

        if (newY + h / 2 > JavaFXBall.HEIGHT || newY - h / 2 < 0) {
            vy *= -0.9;
        } else {
            y = newY;
        }
    }
```

# Nettoyer le code

Notre `AnimationTimer` peut maintenant laisser l'objet `Balle`
s'auto-gérer et se contenter de l'afficher :

```java
// Dans la fonction start()

Balle balle = new Balle(WIDTH / 2, HEIGHT / 2, 40, 40);

AnimationTimer timer = new AnimationTimer() {
    private long lastTime = 0;

    @Override
    public void handle(long now) {
        if (lastTime == 0) {
            lastTime = now;
            return;
        }
```

# Nettoyer le code

```java
        double deltaTime = (now - lastTime) * 1e-9;

        balle.update(deltaTime);

        context.clearRect(0, 0, WIDTH, HEIGHT);
        context.fillOval(
                balle.getX() - balle.getW() / 2,
                balle.getY() - balle.getH() / 2,
                balle.getW(),
                balle.getH());

        lastTime = now;
    }
};
timer.start();
```

<!-- PROGRAMME : JavaFXBall4 -->

*Voir : programme JavaFXBall4*


# Deux balles plutôt qu'une

Ça devient maintenant beaucoup plus facile d'avoir plusieurs balles
qui rebondissent sur l'écran !

```java
Balle b1 = new Balle(WIDTH / 2, HEIGHT / 2, 40, 40);
Balle b2 = new Balle(WIDTH / 2, HEIGHT / 2, 40, 40);

AnimationTimer timer = new AnimationTimer() {
    private long lastTime = 0;

    @Override
    public void handle(long now) {
        if (lastTime == 0) {
            lastTime = now;
            return;
        }
```

# Deux balles plutôt qu'une

```java
    double deltaTime = (now - lastTime) * 1e-9;

    b1.update(deltaTime);
    b2.update(deltaTime);

    context.clearRect(0, 0, WIDTH, HEIGHT);
    context.fillOval(
            b1.getX() - b1.getW() / 2,
            b1.getY() - b1.getH() / 2,
            b1.getW(),
            b1.getH());
    context.fillOval(
            b2.getX() - b2.getW() / 2,
            b2.getY() - b2.getH() / 2,
            b2.getW(),
            b2.getH());
    lastTime = now;
}
```

# Deux balles plutôt qu'une

![](img/animation/phyisque/double1.png){width=33%}
![](img/animation/phyisque/double2.png){width=33%}
![](img/animation/phyisque/double3.png){width=33%}

&nbsp;

\center

*Résultat affiché*

*Voir : programme JavaFXBall5*

# Deux balles plutôt qu'une

On pourrait bien sûr généraliser ça à $N$ balles avec un tableau :

```java
// Initialisation :
Balle[] balles = new Balle[5];

for(int i=0; i<5; i++)
    balles[i] = new Balle(i * WIDTH / 5, HEIGHT / 2, 40, 40);

AnimationTimer timer = new AnimationTimer() {
    // (...)
    // handle() ...
        double deltaTime = (now - lastTime) * 1e-9;

        context.clearRect(0, 0, WIDTH, HEIGHT);

        // Update + dessiner chaque balle
        for(Balle b : balles) {
            b.update(deltaTime);
            context.fillOval(...);
        }
```

# Interactions entre les balles

- Présentement, les balles rebondissent sur les murs mais ne se
  frappent jamais

- On pourrait ajouter des collisions entre les balles pour rendre ça
  plus intéressant

&nbsp;

## Questions

- Comment savoir s'il y a une collision entre deux balles ?
- Comment trouver la vitesse de chaque balle après collision ?

# Collision entre deux balles

Il y a collision si la distance entre les deux centres est plus petite
que la somme des rayons, donc si :

$$dist(\vec{x}_1, \vec{x}_2) < r_1 + r_2$$

\center

![](img/animation/phyisque/intersection-cercles1.png){width=65%}&nbsp;

# Collision entre deux balles

Pour la formule de la distance entre deux points 2D, on a simplement :

$$dist(\vec{x}_1, \vec{x}_2) < r_1 + r_2$$

$$\sqrt{(x_1 - x_2)^2 + (y_1 - y_2)^2} < r_1 + r_2$$

&nbsp;

Ou encore (équivalent et un peu plus efficace) :

$$(x_1 - x_2)^2 + (y_1 - y_2)^2 < (r_1 + r_2)^2$$


# Collision entre deux balles

On va d'abord ré-exprimer la taille d'un objet `Balle` en fonction
d'un rayon, plutôt qu'en largeur/longueur et ajouter une méthode
`intersects(Balle other)` qui nous indique si la balle intersecte une
autre balle ou non :

```java
public class Balle {

    private double x, y;
    private double r;
    private double vx = 200, vy = 190;
    private double ax = 0, ay = 200;

    public Balle(double x, double y, double r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }
```

# Collision entre deux balles

```java
    // Ajustement de la méthode update() pour utiliser le rayon :
    // ...
    // if (newX + r > JavaFXBall.WIDTH || newX - r < 0) {
    // ...

    /**
     * Indique s'il y a intersection entre les deux balles
     *
     * @param other autre balle
     * @return true s'il y a intersection
     */
    public boolean intersects(Balle other) {
        double dx = this.x - other.x;
        double dy = this.y - other.y;
        double dCarre = dx * dx + dy * dy;

        return dCarre < (this.r + other.r) * (this.r + other.r);
    }
```

# Interactions entre les balles

Comment trouver la vitesse de chaque balle après collision ?

&nbsp;

Lorsque les balles ont la même masse, on peut simplement inverser les
vitesses avant et après la collision :

$$\vec{v_1}' = \vec{v_2}$$
$$\vec{v_2}' = \vec{v_1}$$

# Interactions entre les balles

Dans Balle.java, on peut ajouter :

```java
   /**
     * Interchange les vitesses s'il y a collision
     * entre les deux balles
     * @param other l'autre balle
     */
    public void testCollision(Balle other) {
        if (this.intersects(other)) {
            double vx = this.vx;
            double vy = this.vy;

            this.vx = other.vx;
            this.vy = other.vy;

            other.vx = vx;
            other.vy = vy;
        }
    }
```

# Interactions entre les balles

On peut alors modifier le handle() pour considérer les collisions :

```java
        double deltaTime = (now - lastTime) * 1e-9;

        b1.update(deltaTime);
        b2.update(deltaTime);

        b1.testCollision(b2);

        context.clearRect(0, 0, WIDTH, HEIGHT);
        context.fillOval(...)

        ...
```

# Interactions entre les balles

![](img/animation/phyisque/collision1.png){width=33%}
![](img/animation/phyisque/collision2.png){width=33%}
![](img/animation/phyisque/collision3.png){width=33%}

&nbsp;

\center

*Résultat affiché*


# *Collisions Wars 2 : Le Glitch Contre-Attaque*

- Dans certains cas (particulièrement lorsque les vitesses sont
  élevées), on peut retrouver le glitch de tout à l'heure
- Les deux balles restent collées ensemble pour les mêmes raisons que
ce qui se passait avec le mur

\center

![](img/animation/phyisque/glitch-explain2.png){width=40%}&nbsp;


# *Collisions Wars 2 : Le Glitch Contre-Attaque*

Lorsqu'on détecte qu'il y a une collision, on peut forcer les deux
balles à s'éloigner l'une de l'autre pour éviter ce problème :

\center

![](img/animation/phyisque/intersection-cercles2.png){width=80%}&nbsp;


# *Collisions Wars 2 : Le Glitch Contre-Attaque*

```java
/**
 * Interchange les vitesses et déplace les balles
 * lorsqu'il y a collision
 *
 * @param other l'autre balle
 */
public void testCollision(Balle other) {
    if (this.intersects(other)) {
        double vx = this.vx;
        double vy = this.vy;

        this.vx = other.vx;
        this.vy = other.vy;

        other.vx = vx;
        other.vy = vy;
```

# *Collisions Wars 2 : Le Glitch Contre-Attaque*

```java
        // Calcule la quantité qui overlap en X et Y
        // Déplace les deux de ces quantités/2
        double dx = other.x - this.x;
        double dy = other.y - this.y;
        double d2 = dx * dx + dy * dy;
        double d = Math.sqrt(d2);

        // Overlap en pixels
        double overlap = d - (this.r + other.r);

        // Direction dans laquelle se déplacer (+1 ou -1)
        double directionX = dx / d;
        double directionY = dy / d;
```

# *Collisions Wars 2 : Le Glitch Contre-Attaque*

```java
        // Déplacement de la moitié de la distance d'overlap
        double deplacementX = directionX * overlap / 2;
        double deplacementY = directionY * overlap / 2;

        /* Déplacement de la moitié de la distance d'overlap
           pour chaque balle (déplacement inverse pour
           l'autre balle) */
        this.x += deplacementX;
        this.y += deplacementY;
        other.x -= deplacementX;
        other.y -= deplacementY;
    }
}
```


# *Collisions Wars 3 : Le Retour du Glitch*

Pas mal... Mais une balle peut maintenant se retrouver à l'extérieur
de l'écran à cause d'une collision avec l'autre, ce qui nous ramène à
notre premier glitch !

&nbsp;

![](img/animation/phyisque/retour-du-glitch1.png){width=33%}
![](img/animation/phyisque/retour-du-glitch2.png){width=33%}
![](img/animation/phyisque/retour-du-glitch3.png){width=33%}

&nbsp;

\center

*Résultat affiché*

# *Collisions Wars 3 : Le Retour du Glitch*

On peut régler le problème pour forcer les coordonnées des balles à
toujours être dans la fenêtre à la fin d'un `update()`

&nbsp;

Une balle va temporairement pouvoir se retrouver à l'extérieur de
l'écran après une collision avec une autre balle, mais va être ramenée à l'intérieur dès le prochain appel à `update()`

# *Collisions Wars 3 : Le Retour du Glitch*

La fonction `update()` devient :

```java
/**
 * Met à jour la position et la vitesse de la balle
 *
 * @param dt Temps écoulé depuis le dernier update() en secondes
 */
public void update(double dt) {

    vx += dt * ax;
    vy += dt * ay;

    x += dt * vx;
    y += dt * vy;

```

# *Collisions Wars 3 : Le Retour du Glitch*

```java
    if (x + r > JavaFXBall.WIDTH || x - r < 0) {
        vx *= -0.9;
    }
    if (y + r > JavaFXBall.HEIGHT || y - r < 0) {
        vy *= -0.9;
    }

    // Force x et y à être dans les bornes
    x = Math.min(x, JavaFXBall.WIDTH - r);
    x = Math.max(x, r);

    y = Math.min(y, JavaFXBall.HEIGHT - r);
    y = Math.max(y, r);
}
```

<!-- PROGRAMME : JavaFXBall6 -->

*Voir : programme JavaFXBall6*


# Utiliser $N$ balles

Comme dans l'exemple de tantôt, on peut généraliser l'idée à $N$
balles avec un tableau de `Balle`s :

```java
Balle[] balles = new Balle[30];

for (int i = 0; i < balles.length; i++) {
    balles[i] = new Balle(
        i * WIDTH / balles.length,
        Math.random() * HEIGHT,
        10);
}
AnimationTimer timer = new AnimationTimer() {
    private long lastTime = 0;

    @Override
    public void handle(long now) {
        if (lastTime == 0) {
            lastTime = now;
            return;
        }
```

# Utiliser $N$ balles

```java
        double deltaTime = (now - lastTime) * 1e-9;
        context.clearRect(0, 0, WIDTH, HEIGHT);

        for (int i = 0; i < balles.length; i++) {
            Balle b = balles[i];
            b.update(deltaTime);

            // Collision de la balle avec toutes les balles suivantes
            for (int j = i + 1; j < balles.length; j++) {
                b.testCollision(balles[j]);
            }
            context.fillOval(
                    b.getX() - b.getW() / 2,
                    b.getY() - b.getH() / 2,
                    b.getW(),
                    b.getH());
        }
        lastTime = now;
    }
};
```

# Utiliser $N$ balles

![](img/animation/phyisque/multiballes1.png){width=25%}
![](img/animation/phyisque/multiballes2.png){width=25%}
![](img/animation/phyisque/multiballes3.png){width=25%}
![](img/animation/phyisque/multiballes4.png){width=25%}

&nbsp;

\center

*Résultats pour N=3, 5, 30 et 100 balles*

*Voir : programme JavaFXBall7*
<!-- PROGRAMME : JavaFXBall7 -->

# Utiliser $N$ balles

On peut arriver à des choses assez intéressantes en combinant un grand
nombre de petites balles avec une accélération qui suit la souris...

![Résultat lorsque la gravité est définie par la position de la souris](img/animation/phyisque/gravity.png){width=50%}

*Voir : programme JavaFXBall8*
<!-- PROGRAMME : JavaFXBall8 -->


# Utiliser $N$ balles

Et si on teste avec un très grand nombre de balles ?

![Résultat à 6000 balles... Pas excellent, ça lag](img/animation/phyisque/multiballes5.png)


# Utiliser $N$ balles

- L'ordre de l'algo de vérification de collisions est $O(n^2)$...
    - Si $N$ devient trop grand, ça va se mettre à être trop long pour
      garder un affichage fluide
- Optimisations possibles
    - Structure de données spatiale pour éviter de tester des
      collisions entre des objets très éloignés (ex.: grille)

# Ajouter des carrés

Et si on voulait ajouter des carrés ?

- Même update de la position $(x, y)$ et de la vitesse
- Pas le même affichage (`fillRect` au lieu de `fillOval`)
- Pas le même test pour les collisions
    - Cercle $\leftrightarrow$ Cercle
    - Cercle $\leftrightarrow$ Carre
    - Carre $\leftrightarrow$ Carre
- Pas le même déplacement minimal pour résoudre une collision

&nbsp;

On peut se faire une hiérarchie d'objets pour représenter ça !

- `abstract class Entity`
    - `class Balle extends Entity`
    - `class Carre extends Entity`

# `Entity`

Tout ce qui est commun à toutes les formes qu'on voudrait ajouter à
l'animation peut se retrouver dans la classe abstraite `Entity` :

```java
public abstract class Entity {
    protected double x, y;
    protected double vx, vy;
    protected double ax = 0, ay = 200;

    public Entity(double x, double y) {
        this.x = x;
        this.y = y;
        vx = Math.random() * 200;
        vy = Math.random() * 200;
    }

    public double getX() { return x; }
    public double getY() { return y; }
    public abstract double getW();
    public abstract double getH();
```

# `Entity`

```java
    public abstract void draw(GraphicsContext context);

    /**
     * Met à jour la position et la vitesse de la balle
     *
     * @param dt Temps écoulé depuis le dernier update() en secondes
     */
    public void update(double dt) {

        vx += dt * ax;
        vy += dt * ay;
        x += dt * vx;
        y += dt * vy;
```

# `Entity`

```java
        if (x + getW() / 2 > JavaFXBall.WIDTH || x - getW() / 2 < 0) {
            vx *= -0.9;
        }
        if (y + getH() / 2 > JavaFXBall.HEIGHT || y - getH() / 2 < 0) {
            vy *= -0.9;
        }

        x = Math.min(x, JavaFXBall.WIDTH - getW() / 2);
        x = Math.max(x, getW() / 2);

        y = Math.min(y, JavaFXBall.HEIGHT - getH() / 2);
        y = Math.max(y, getH() / 2);
    }
}
```

# `Entity`

```java
    public void testCollision(Entity other) {
        if (this.intersects(other)) {
            // Échange les vitesses des deux objets
            double vx = this.vx;
            double vy = this.vy;

            this.vx = other.vx;
            this.vy = other.vy;

            other.vx = vx;
            other.vy = vy;

            pushOut(other);
        }
    }
```

# `Entity`

L'intersection va dépendre des entités en jeu :

```java
    public abstract boolean intersects(Balle other);
    public abstract boolean intersects(Carre other);
```

&nbsp;

On va redéfinir dans chaque sous-classe :

1. `balle.intersects(Balle other)`
2. `balle.intersects(Carre other)`

et

3. `carre.intersects(Carre c)`
4. `carre.intersects(Balle other)` (même que 2.)

# `Entity`

L'intersection va dépendre des entités en jeu :

```java
    public abstract boolean intersects(Balle other);
    public abstract boolean intersects(Carre other);
```

## Question

Quelle méthode est appelée si on fait ça?

```java
Entity carre = new Carre(...);
Entity balle = new Balle(...);

carre.intersects(balle); // ?


```

# `Entity`

Question piège... Il n'y a aucune signature qui correspond à 

```java
entity.intersects(Entity other)
```

&nbsp;

On doit donc ajouter une méthode supplémentaire :

```java

    public boolean intersects(Entity other) {
        if(other instanceof Balle) {
            return this.intersects((Balle) other);
        } else {
            return this.intersects((Carre) other);
        }
    }
```

# `Entity`

On va avoir le même problème pour `pushOut()`

```java
    public abstract void pushOut(Balle other);
    public abstract void pushOut(Carre other);

    public void pushOut(Entity other) {
        if(other instanceof Balle) {
            this.pushOut((Balle) other);
        } else {
            this.pushOut((Carre) other);
        }
    }
```

# `Balle`

On peut maintenant réduire un peu le code dans `Balle` et ajouter les
traitements spécifiques aux interactions `Balle` $\leftrightarrow$
`Carre`

```java
public class Balle extends Entity {

    private double r;

    public Balle(double x, double y, double r) {
        super(x, y);
        this.r = r;
    }

    @Override
    public double getW() {
        return 2 * r;
    }
    @Override
    public double getH() {
        return 2 * r;
    }
```

# `Balle`

```java
    /**
     * Indique s'il y a intersection entre deux balles
     *
     * @param other L'autre balle
     * @return true s'il y a intersection
     */
    public boolean intersects(Balle other) {
        double dx = this.x - other.x;
        double dy = this.y - other.y;
        double d2 = dx * dx + dy * dy;

        return d2 < (this.r + other.r) * (this.r + other.r);
    }
```

# `Balle`

```java
    /**
     * @see https://yal.cc/rectangle-circle-intersection-test/
     */
    public boolean intersects(Carre other) {
        /**
         * Trouve le point (x, y) à l'intérieur du carré le plus
         * proche du centre du cercle et vérifie s'il se trouve
         * dans le rayon du cercle
         */
        double deltaX = x - Math.max(
                other.getX() - other.getW() / 2,
                Math.min(x, other.getX() + other.getW() / 2));
        double deltaY = y - Math.max(
                other.getY() - other.getH() / 2,
                Math.min(y, other.getY() + other.getW() / 2));

        return deltaX * deltaX + deltaY * deltaY < r * r;
    }
```

# `Balle`

```java
    public void pushOut(Balle other) {
        // (Même méthode que tout à l'heure)
    }

    public void pushOut(Carre other) {
        /* (Similaire à intersects(Carre) et à pushOut(Balle),
           voir le code source de cet exemple */
    }
```

# `Balle`

```java
    @Override
    public void draw(GraphicsContext context) {
        context.fillOval(
                this.x - this.r,
                this.y - this.r,
                2 * this.r,
                2 * this.r);
    }
}
```

# `Carre`

```java
public class Carre extends Entity {

    private double largeur;

    public Carre(double x, double y, double largeur) {
        super(x, y);
        this.largeur = largeur;
    }

    @Override
    public double getW() {
        return largeur;
    }

    @Override
    public double getH() {
        return largeur;
    }
```

# `Carre`

```java
    /**
     * Indique si les deux carrés s'intersectent. Deux carrés ne sont
     * *pas* en intersection si : 1) S'il y en a un qui est au-dessus
     * de l'autre 2) S'il y en a un à gauche de l'autre Sinon, les
     * deux overlappent
     *
     * @see https://stackoverflow.com/a/32088787
     * @param other L'autre carré
     * @return true si les carrés s'intersectent
     */
    public boolean intersects(Carre other) {
        return !(
           // Un des carrés est à gauche de l'autre
           x + largeur / 2 < other.x - other.largeur / 2
           || other.x + other.largeur / 2 < this.x - this.largeur / 2
           // Un des carrés est en haut de l'autre
           || y + largeur / 2 < other.y - other.largeur / 2
           || other.y + other.largeur / 2 < this.y - largeur / 2);
    }
```

# `Carre`

```java
    @Override
    public boolean intersects(Balle other) {
        // Complètement équivalent
        return other.intersects(this);
    }

    @Override
    public void pushOut(Balle other) {
        other.pushOut(this);
    }
```


# `Carre`

```java
    public void pushOut(Carre other) {
        double dx = x - other.x;
        double dy = y - other.y;
        double distX = (largeur + other.largeur) / 2 - Math.abs(dx);
        double distY = (largeur + other.largeur) / 2 - Math.abs(dy);

        if (Math.abs(distX) < Math.abs(distY)) {
            distX *= (x > other.x ? -1 : 1);
            distY = 0;
        } else {
            distY *= (y > other.y ? -1 : 1);
            distX = 0;
        }
        this.x -= distX / 2;
        this.y -= distY / 2;
        other.x += distX / 2;
        other.y += distY / 2;
    }
}
```

# `Carre`

```java
    @Override
    public void draw(GraphicsContext context) {
        context.fillRect(this.x - this.largeur / 2,
                this.y - this.largeur / 2,
                this.largeur, this.largeur);
    }
```

# Overview

- `Entity`
    - protected double x, y;
    - protected double vx, vy;
    - protected double ax, ay;
    - public void testCollision(Entity other) { ... }
    - public void update(double dt) { ... } \pause
    - public *abstract* boolean intersects(Balle other);
    - public *abstract* boolean intersects(Carre other);
    - public boolean intersects(Entity other); \pause
    - public *abstract* void pushOut(Balle other);
    - public *abstract* void pushOut(Carre other);
    - public void pushOut(Entity other); \pause
    - public *abstract* void draw(GraphicsContext context);

# Overview

- `Carre` et `Balle`
    - Redéfinissent intersects(Carre other), intersects(Balle other)
    - Redéfinissent pushOut(Carre other), pushOut(Balle other)
    - Redéfinissent draw(GraphicsContext context)

# Modifications à `start()`

On peut finalement ajuster `start()` pour utiliser un tableau
d'`Entity` :

```java
Entity[] entities = new Entity[10];

for (int i = 0; i < entities.length/2; i++) {
    entities[i] = new Balle(
        i * WIDTH / entities.length, // x
        Math.random() * HEIGHT,      // y
        15);                         // rayon
}

for (int i = entities.length/2; i < entities.length; i++) {
    entities[i] = new Carre(
        i * WIDTH / entities.length, // x
        Math.random() * HEIGHT,      // y
        30);                         // largeur
}
```


# Faire "sauter" les objets

On pourrait rajouter un dernier petit bout de code dans `start()` pour
rendre le tout plus intéressant :

```java
scene.setOnKeyPressed((value) -> {
    if (value.getCode() == KeyCode.SPACE) {
        for (Entity e : entities) {
            e.setVelocityY(-300);
        }
    }
});
```

*Voir : programme JavaFXBall9*
<!-- PROGRAMME : JavaFXBall9 -->

# Changer les couleurs

On peut s'amuser à changer les couleurs des balles en ajoutant une
propriété `color` à chaque entité :

```java
public abstract class Entity {
    protected double x, y;
    protected double vx, vy;
    protected double ax = 0, ay = 200;
    protected Color color;

    public Entity(double x, double y) {
        // (...)
        color =
            new Color(Math.random(), Math.random(), Math.random(), 1);
    }
    public void draw(GraphicsContext context) {
        context.setFill(color);
        /* Les classes qui héritent devront appeler
           super.draw(context); pour que ça fasse effet */
    }
```

# Changer les couleurs

![Résultat haut en couleurs](img/animation/phyisque/colors1.png){width=60%}

# Changer les couleurs

- La représentation *Rouge-Vert-Bleu* est la représentation de
  couleurs la plus proche de ce qu'un ordinateur moyen supporte
- Ce n'est pas la seule façon de représenter les couleurs

![Vue du programme `gcolor2`, qui propose une interface pour choisir des couleurs](img/color-wheel.png)

# Changer les couleurs

- Un exemple de représentation alternative est la représentation *HSB*
  (Hue-Saturation-Brightness)

\center

![](img/color-wheel-notes.png)&nbsp;

# Animer les couleurs

Plutôt que de garder une couleur fixe, on peut garder une valeur de
*teinte* et calculer une couleur animée sur la roue *HSB* :

```java
protected double hue, colorSpeed = 40;

public void update(double dt) {
    // (...)

    // Notez : la teinte est spécifiée en degrés, [0, 360[
    hue = (hue + dt * colorSpeed) % 360;
}

public void draw(GraphicsContext context) {
    context.setFill(Color.hsb(hue, 0.8, 1));
}
```

# Animer les couleurs

![Résultat haut en couleurs](img/animation/phyisque/color-wheel1.png){width=33%}
![Résultat haut en couleurs](img/animation/phyisque/color-wheel2.png){width=33%}
![Résultat haut en couleurs](img/animation/phyisque/color-wheel3.png){width=33%}

&nbsp;

\center

*Résultat psychédélique*

<!-- PROGRAMME : JavaFXBall10 -->
*Voir : programme JavaFXBall10*

# Plateformes!

Un élément commun dans plusieurs jeux vidéos est la présence de
plateformes.

- On peut les traverser de bas en haut
- On ne peut pas les traverser de haut en bas

![Plateforme dans Super Mario World](img/plateforme.png){width=40%}

# Plateformes!

Une stratégie simple pour faire de nos Entités des plateformes est de
détecter une collision...

&nbsp;

- Seulement lorsque le personnage tombe \newline `=>` seulement si
  $v_y$ va vers le bas

&nbsp;

- Seulement entre les "pieds" du personnage et les plateformes
  \newline `=>` pas besoin de tester de collisions entre les
  plateformes, ni de tester pour une collision entre le haut du corps
  du personnage et la plateforme


# Plateformes!

De plus, les plateformes ne bougent pas :

- Lorsqu'il y a une collision, plutôt que de résoudre la collision en
  déplaçant les deux entités (plateforme *et* personnage), on déplace
  seulement le personnage

&nbsp;

Le personnage ne peut pas sauter depuis les airs :

- On doit être en train de toucher le sol pour pouvoir sauter

# Plateformes!

![Résultat](img/javafx-plateforme.png){width=75%}

\center

*Voir : programme SuperJumper*

# MVC?

On a parlé au dernier chapitre d'organiser le code dans une structure
*Modèle-Vue-Contrôleur*...

&nbsp;

&nbsp;

Ce n'est pas exactement ce qu'on fait ici

- Détection de collisions dans le `AnimationTimer`?
- Contrôleur?

&nbsp;

Comment pourrait-on adapter le code ici?

# MVC?

- Modèle :
    - Objets du jeu (Personnage, plateformes, items, ...) \pause
    - Logique du jeu (ex.: moteur de physique, `update()`, détection
      de collision, mais aussi le score, est-ce que la partie est
      terminée, etc...) \pause

- Contrôleur :
    - Tout ce qui est nécessaire pour éviter que la vue gère le modèle
      elle-même \pause

- Vue :
    - Fenêtre simple avec un canvas
    - Avertir le contrôleur lorsqu'il y a des événements

\pause

*Voir : programme SuperJumperMVC*

# MVC?

- Modèle :
    - Objets du jeu (Personnage, plateformes, items, ...)
    - Logique du jeu (ex.: moteur de physique, `update()`, détection
      de collision, mais aussi le score, est-ce que la partie est
      terminée, etc...)

- Contrôleur :
    - Tout ce qui est nécessaire pour éviter que la vue gère le modèle
      elle-même

- Vue :
    - Fenêtre simple avec un canvas
    - Avertir le contrôleur lorsqu'il y a des événements
    - **Méthode draw() dans les objets?**

<!-- # Exemple un peu plus complexe : Clic Le Clown -->

<!-- - Un classique du monde vidéoludique\footnote{Voir le \textit{3615 Usul} pour -->
<!--   plus d'informations} -->

<!-- &nbsp; -->

<!-- Backstory : -->

<!-- > Des super-zombies maxistes envahissent un parc d'attraction pour le -->
<!-- > convertir en parc autogéré. Le parc est bien sûr défendu par des -->
<!-- > robots-mutants, chiens de garde du capitalisme. -->
<!-- > -->
<!-- > &nbsp; -->
<!-- > -->
<!-- > Vous incarnez un des zombies dans ce parc, seul face à un soldat de -->
<!-- > l'armée de clowns résistants. Votre mission : cliquer sur le clown. -->
<!-- >  -->
<!-- > &nbsp; -->
<!-- > -->
<!-- > Le clown s'appelle Clic. Clic le Clown. -->

<!-- # Clic Le Clown -->

<!-- ![Notre Clone de Clic Le Clown](img/animation/clicleclown.png) -->

<!-- # 1. Dessiner le Clown -->

<!-- On peut réutiliser le code qu'on vient de faire et et créer une -->
<!-- sous-classe "`Clown`" de la classe `Entity`. -->

<!-- &nbsp; -->

<!-- On peut dessiner le Clown à partir des primitives `fillOval`, -->
<!-- `fillRect`, ..., en utilisant des proportions relatives à la -->
<!-- largeur/hauteur du clown : -->

<!-- ```java -->
<!-- @Override -->
<!-- public void draw(GraphicsContext context) { -->
<!--     double headWidth = 0.75 * w; -->
<!--     double headHeight = 0.7 * h; -->

<!--     double hatWidth = headWidth * 0.8; -->
<!--     double hatHeight = h * 0.4; -->

<!--     double headX = this.getX(); -->
<!--     double headY = this.getY() + 0.15 * h; -->
<!-- ``` -->

<!-- # 1. Dessiner le Clown -->

<!-- ```java -->
<!--     // Cheveux -->
<!--     double hairWidth = 0.45 * w; -->
<!--     double hairHeight = 0.6 * h; -->
<!--     context.setFill(Color.web("#D40000")); -->
<!--     context.fillOval( -->
<!--         headX - headWidth * 0.35 - hairWidth / 2, -->
<!--         headY - headHeight / 2, -->
<!--         hairWidth, hairHeight); -->
<!--     context.fillOval( -->
<!--         headX + headWidth * 0.35 - hairWidth / 2, -->
<!--         headY - headHeight / 2, -->
<!--         hairWidth, hairHeight); -->
<!--     // Tête -->
<!--     context.setFill(Color.web("#D2D2D2")); -->
<!--     context.fillOval( -->
<!--         headX - headWidth / 2, -->
<!--         headY - headHeight / 2, -->
<!--         headWidth, headHeight); -->
<!-- ``` -->

<!-- # 1. Dessiner le Clown -->

<!-- ```java -->
<!--     // Chapeau -->
<!--     context.setFill(Color.web("#008000")); -->
<!--     context.fillPolygon( -->
<!--             new double[]{ -->
<!--                 this.getX() - hatWidth / 2, -->
<!--                 this.getX() + hatWidth / 2, -->
<!--                 this.getX() -->
<!--             }, -->
<!--             new double[]{ -->
<!--                 this.getY() - h / 2 + hatHeight, -->
<!--                 this.getY() - h / 2 + hatHeight, -->
<!--                 this.getY() - h / 2 -->
<!--             }, 3); -->

<!--     // Yeux, Nez, Bouche, etc -->
<!-- ``` -->

<!-- <\!-- PROGRAMME : ClicLeClown1 -\-> -->
<!-- *Voir : programme ClicLeClown1* -->

<!-- # 2. Détecter le clic sur le Clown -->

<!-- On a un bel objet `Clown` qui sait se dessiner lui-même, tout ce qui -->
<!-- manque, c'est : -->

<!-- 1. Ajouter un événement lorsqu'on clique quelque part sur le `Canvas` -->
<!-- 2. Ajouter une méthode dans `Clown` pour détecter si un point $(x, y)$ -->
<!--    est à l'intérieur du clown -->

<!-- \center -->

<!-- ![](img/animation/clicleclown-intersection.png){width=50%}&nbsp; -->

<!-- # 2. Détecter le clic sur le Clown -->

<!-- - À la place de tout dessiner manuellement dans `draw`, on pourrait -->
<!-- plutôt garder une liste de primitives qui composent le Clown -->

<!-- - `List<Entity>` -->
<!--     - `Oval extends Entity` -->
<!--     - `Triangle extends Entity` -->
<!--     - `Rectangle extends Entity` -->

<!-- - Chaque `Entity` définirait une méthode `contains(Point2D p)` qui -->
<!-- indiquerait si un point est à l'intérieur de l'entité ou non -->
<!-- - `Clown` définirait cette méthode en passant à travers sa liste -->
<!--   d'entités : si une des entités contient le point, alors le clown -->
<!--   contient le point -->

<!-- # 2. Détecter le clic sur le Clown -->

<!-- ```java -->
<!-- public class Clown extends Entity { -->

<!--     private double w, h; -->

<!--     private ArrayList<Entity> entities = new ArrayList<>(); -->

<!--     public Clown(double x, double y, double w, double h) { -->
<!--         super(x, y); -->
<!--         this.w = w; -->
<!--         this.h = h; -->

<!--         double headWidth = 0.75 * w; -->
<!--         double headHeight = 0.7 * h; -->

<!--         double hatWidth = headWidth * 0.8; -->
<!--         double hatHeight = h * 0.4; -->

<!--         double headX = this.getX(); -->
<!--         double headY = this.getY() + 0.15 * h; -->
<!-- ``` -->

<!-- # 2. Détecter le clic sur le Clown -->

<!-- ```java -->
<!--         // Cheveux -->
<!--         double hairWidth = 0.45 * w; -->
<!--         double hairHeight = 0.6 * h; -->
<!--         double hairRadius = hairHeight / 2; -->
<!--         Oval hairLeft = new Oval(headX - headWidth * 0.35, headY,  -->
<!--             hairHeight / 2, (hairWidth / 2) / hairRadius); -->
<!--         hairLeft.setColor(Color.web("#D40000")); -->
<!--         Oval hairRight = new Oval(headX + headWidth * 0.35, headY, -->
<!--             hairHeight / 2, (hairWidth / 2) / hairRadius); -->
<!--         hairRight.setColor(Color.web("#D40000")); -->
<!--         entities.add(hairLeft); -->
<!--         entities.add(hairRight); -->
<!--         // Tête -->
<!--         double teteRayon = headHeight / 2; -->
<!--         double teteScaleX = (headWidth / 2) / teteRayon; -->
<!--         Oval tete = new Oval(headX, headY, teteRayon, teteScaleX); -->
<!--         tete.setColor(Color.web("#D2D2D2")); -->
<!--         entities.add(tete); -->
<!--         // Etc... -->
<!-- ``` -->

<!-- # 2. Détecter le clic sur le Clown -->

<!-- ```java -->
<!--     @Override -->
<!--     public void draw(GraphicsContext context) { -->
<!--         // Dessiner toutes les primitives dans l'ordre -->
<!--         for (Entity e : entities) { -->
<!--             e.draw(context); -->
<!--         } -->
<!--     } -->

<!--     @Override -->
<!--     public boolean contains(Point2D p) { -->
<!--         /* Si une des primitives contient p, -->
<!--            alors le clown contient p */ -->
<!--         for(Entity e : entities) -->
<!--             if(e.contains(p)) -->
<!--                 return true; -->

<!--         return false; -->
<!--     } -->
<!-- ``` -->

<!-- # 2. Détecter le clic sur le Clown -->

<!-- On peut facilement coder la classe `Rectangle`, le dessin demande un -->
<!-- `fillRect`, la méthode `contains(Point2D)` est facile à trouver : -->

<!-- ```java -->
<!--     @Override -->
<!--     public void draw(GraphicsContext context) { -->
<!--         super.draw(context); -->

<!--         context.fillRect(this.x - this.w / 2, -->
<!--                 this.y - this.h / 2, -->
<!--                 this.w, this.h); -->
<!--     } -->
<!--     @Override -->
<!--     public boolean contains(Point2D p) { -->
<!--         return p.getX() < x + w / 2 -->
<!--                 && p.getX() > x - w / 2 -->
<!--                 && p.getY() < y + h / 2 -->
<!--                 && p.getY() > y - h / 2; -->
<!--     } -->
<!-- ``` -->


<!-- # 2. Détecter le clic sur le Clown -->

<!-- Un `Oval` va être défini comme un cercle ayant un certain rayon plus -->
<!-- un facteur d'étirement/contraction $s_x$ sur l'axe horizontal : -->

<!-- \center -->

<!-- ![](img/animation/oval1.png)&nbsp; -->


<!-- # 2. Détecter le clic sur le Clown -->

<!-- Un `Oval` va être défini comme un cercle ayant un certain rayon plus -->
<!-- un facteur d'étirement/contraction $s_x$ sur l'axe horizontal : -->

<!-- \center -->

<!-- ![](img/animation/oval2.png)&nbsp; -->


<!-- # 2. Détecter le clic sur le Clown -->

<!-- ```java -->
<!--     public Oval(double x, double y, double r, double sx) { -->
<!--         super(x, y); -->
<!--         this.r = r; -->
<!--         this.sx = sx; -->
<!--     } -->
<!--     @Override -->
<!--     public void draw(GraphicsContext context) { -->
<!--         super.draw(context); -->
<!--         context.fillOval( -->
<!--                 this.x - this.sx * this.r, -->
<!--                 this.y - this.r, -->
<!--                 2 * this.sx * this.r, -->
<!--                 2 * this.r); -->
<!--     } -->
<!--     @Override -->
<!--     public boolean contains(Point2D p) { -->
<!--         return (p.getX() - x) / sx * (p.getX() - x) / sx -->
<!--                 + (p.getY() - y) * (p.getY() - y) < this.r * this.r; -->
<!--     } -->
<!-- ``` -->

<!-- # 2. Détecter le clic sur le Clown -->

<!-- Pour un triangle, on peut calculer les coins à partir de la -->
<!-- largeur/hauteur et utiliser `fillPolygon` : -->

<!-- \center -->

<!-- ![](img/animation/triangle.png)&nbsp; -->

<!-- # 2. Détecter le clic sur le Clown -->

<!-- ```java -->
<!-- public class Triangle extends Entity { -->
<!--     private double[] xPoints, yPoints; -->
<!--     private double w, h; -->

<!--     public Triangle(double x, double y, double w, double h) { -->
<!--         super(x, y); -->
<!--         this.w = w; -->
<!--         this.h = h; -->
<!--         xPoints = new double[3]; -->
<!--         yPoints = new double[3]; -->
<!--         xPoints[0] = x - w / 2; -->
<!--         yPoints[0] = y + h / 2; -->
<!--         xPoints[1] = x; -->
<!--         yPoints[1] = y - h / 2; -->
<!--         xPoints[2] = x + w / 2; -->
<!--         yPoints[2] = y + h / 2; -->
<!--     } -->
<!-- ``` -->

<!-- # 2. Détecter le clic sur le Clown -->

<!-- Le clic devient alors trivial à détecter depuis la méthode `start()` : -->

<!-- ```java -->
<!-- canvas.setOnMouseClicked((event) -> { -->
<!--     Point2D p = new Point2D(event.getX(), event.getY()); -->

<!--     if (clicLeClown.contains(p)) { -->
<!--         /* La couleur du background change lors -->
<!--            d'un clic sur le clown */ -->
<!--         scene.setFill(Color.hsb(Math.random() * 360, 0.8, 0.8)); -->
<!--     } -->
<!-- }); -->
<!-- ``` -->

<!-- # 2. Détecter le clic sur le Clown -->

<!-- ![Résultat quand on clique sur Clic le Clown](img/animation/clicleclown-win.png){width=70%} -->

<!-- <\!-- PROGRAMME : ClicLeClown2 -\-> -->
<!-- *Voir : programme ClicLeClown2* -->

<!-- # Faire bouger le clown -->

<!-- - Le jeu est un peu facile... On pourrait augmenter le niveau de -->
<!-- difficulté en déplaçant le clown sur l'écran -->

<!-- &nbsp; -->

<!-- - On a déjà le code pour mettre à jour la vitesse avec `vx` et `vy` + -->
<!--   des rebonds sur les côtés de l'écran, on peut réutiliser ça -->
<!--   directement -->

<!-- &nbsp; -->

<!-- - On va garder la méthode `update()` dans `Entity` + donner une -->
<!--   vitesse de départ à notre Clown -->

<!-- # Faire bouger le clown -->

<!-- Simplement : -->

<!-- ```java -->
<!--     public Clown(double x, double y, double w, double h) { -->
<!--         super(x, y); -->
<!--         this.w = w; -->
<!--         this.h = h; -->

<!--         double speed = 200; -->
<!--         double angle = Math.random() * 2 * Math.PI; -->

<!--         vx = Math.cos(angle) * speed; -->
<!--         vy = Math.sin(angle) * speed; -->

<!--         // Définition des primitives -->
<!--         // ... -->
<!-- ``` -->

<!-- # Faire bouger le clown -->

<!-- Hmmm... -->

<!-- ![Le clown n'a pas l'air de se déplacer...](img/animation/clicleclown.png){width=70%} -->


<!-- # Faire bouger le clown -->

<!-- - La position $(x, y)$ des primitives est seulement définie dans le -->
<!--   constructeur... -->
<!-- - Il faut mettre à jour les $(x, y)$ des `entities` lorsqu'on met à -->
<!--   jour les coordonnées du clown -->


<!-- # Faire bouger le clown -->

<!-- - On pourrait redéfinir `update(double dt)` pour mettre également à -->
<!--   jour les positions des primitives qui composent le clown : -->

<!-- ```java -->
<!--     @Override -->
<!--     public void update(double dt) { -->
<!--         double lastX = x, lastY = y; -->
<!--         x += dt * vx; -->
<!--         y += dt * vy; -->

<!--        // ... Rebonds ... -->

<!--         double deltaX = lastX - x, deltaY = lastY - y; -->
<!--         for (Entity e : entities) { -->
<!--             e.setX(e.getX() + deltaX); -->
<!--             e.setY(e.getY() + deltaY); -->
<!--         } -->
<!--     } -->
<!-- ``` -->

<!-- # Faire bouger le clown -->

<!-- Hmmm... -->

<!-- ![Le clown perd son chapeau...](img/animation/clicleclown-losthat.png){width=70%} -->

<!-- # Faire bouger le clown -->

<!-- Les coordonnées des points sont calculées dans le constructeur de -->
<!-- `Triangle`... On doit modifier ça : -->

<!-- ```java -->
<!--     public double[] getXPoints() { -->
<!--         double[] xPoints = new double[3]; -->
<!--         xPoints[0] = x - w / 2; -->
<!--         xPoints[1] = x; -->
<!--         xPoints[2] = x + w / 2; -->

<!--         return xPoints; -->
<!--     } -->
<!--     public double[] getYPoints() { -->
<!--         double[] yPoints = new double[3]; -->
<!--         yPoints[0] = y + h / 2; -->
<!--         yPoints[1] = y - h / 2; -->
<!--         yPoints[2] = y + h / 2; -->

<!--         return yPoints; -->
<!--     } -->
<!-- ``` -->

<!-- # Faire bouger le clown -->

<!-- ```java -->
<!--     @Override -->
<!--     public void draw(GraphicsContext context) { -->
<!--         super.draw(context); -->

<!--         double[] xPoints = getXPoints(); -->
<!--         double[] yPoints = getYPoints(); -->
<!--         context.fillPolygon(xPoints, yPoints, 3); -->
<!--     } -->
<!-- ``` -->

<!-- # Faire bouger le clown -->

<!-- ![Finalement !](img/animation/clicleclown-win-move.png){width=70%} -->

<!-- <\!-- PROGRAMME : ClicLeClown3 -\-> -->
<!-- *Voir : programme ClicLeClown3* -->

<!-- # Rotation du clown -->

<!-- - On pourrait rendre le jeu encore plus joli en faisant tourner le -->
<!--   Clown sur lui-même à mesure qu'il se déplace -->

<!-- ![Le clown pourrait tourner sur lui-même](img/animation/clicleclown-rotate.png){width=50%} -->

<!-- - Il y a une méthode `context.rotate()`, on peut peut-être utiliser ça ? -->

<!-- # Rotation du clown -->

<!-- On pourrait ajouter une vitesse angulaire en degrés/seconde : -->

<!-- ```java -->
<!-- public class Clown extends Entity { -->

<!--     private double w, h; -->
<!--     // Vitesse angulaire (degrés/seconde) -->
<!--     private double vAngulaire = 10, angle = 0; -->

<!--     // Dans public void update(double dt), on ajoute : -->
<!--         ... -->
<!--         angle += dt * vAngulaire; -->
<!--         ... -->
<!-- ``` -->

<!-- # Rotation du clown -->

<!-- On peut tester dans la méthode `draw()` un appel à context.rotate() -->
<!-- pour voir si ça fait tourner notre clown : -->

<!-- ```java -->
<!--     @Override -->
<!--     public void draw(GraphicsContext context) { -->
<!--         context.rotate(angle); -->
<!--         for (Entity e : entities) { -->
<!--             e.draw(context); -->
<!--         } -->
<!--         // On "défait" la rotation, pour éviter d'accumuler les angles -->
<!--         context.rotate(-angle); -->
<!--     } -->
<!-- ``` -->

<!-- # Rotation du clown -->

<!-- Hmmm... -->

<!-- ![Ça tourne... Mais Clic finit par sortir de l'écran et aller se promener ailleurs](img/animation/clicleclown-rotation2.png){width=60%} -->


<!-- <\!-- PROGRAMME : ClicLeClown4 -\-> -->
<!-- *Voir : programme ClicLeClown4* -->

<!-- # Rotation du clown -->

<!-- Ce n'est visiblement pas aussi simple que ça... -->

<!-- - Tester des choses au hasard ne marche pas très bien ici -->
<!-- - Prenons un peu de temps pour comprendre comment ça marche avant de -->
<!--   continuer... -->

<!-- <\!-- TODO: modifier tout ça -\-> -->

<!-- <\!-- # Transformations -\-> -->

<!-- <\!-- Les transformations que `GraphicsContext` proposent de faire se font à -\-> -->
<!-- <\!-- l'aide de notre ami l'**Algèbre linéaire** -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- <\!-- On peut représenter toutes sortes de transformations géométriques par -\-> -->
<!-- <\!-- des opérations sur des vecteurs et matrices -\-> -->

<!-- <\!-- - Translation -\-> -->
<!-- <\!-- - Rotation -\-> -->
<!-- <\!-- - Scaling -\-> -->
<!-- <\!-- - Cisaillement -\-> -->
<!-- <\!-- - Réflexion -\-> -->

<!-- <\!-- # Transformations -\-> -->

<!-- <\!-- ## Rappel sur les matrices -\-> -->

<!-- <\!-- \begin{equation*} -\-> -->
<!-- <\!--   p' = T.p = -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     a & b \\ -\-> -->
<!-- <\!--     c & d -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     x \\ -\-> -->
<!-- <\!--     y -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!--   = -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     ax + by \\ -\-> -->
<!-- <\!--     cx + dy -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!-- \end{equation*} -\-> -->

<!-- <\!-- \center -\-> -->

<!-- <\!-- 2x2 . 2x1 `=>` 2x1 -\-> -->

<!-- <\!-- # Translation -\-> -->

<!-- <\!-- \center -\-> -->

<!-- <\!-- ![](img/infographie/translation.png)&nbsp; -\-> -->

<!-- <\!-- # Scaling (changement d'échelle) -\-> -->

<!-- <\!-- \center -\-> -->

<!-- <\!-- ![](img/infographie/scaling.png)&nbsp; -\-> -->

<!-- <\!-- # Rotation -\-> -->

<!-- <\!-- \center -\-> -->

<!-- <\!-- ![](img/infographie/rotation.png)&nbsp; -\-> -->

<!-- <\!-- # Cisaillement -\-> -->

<!-- <\!-- \center -\-> -->

<!-- <\!-- ![](img/infographie/shear.png)&nbsp; -\-> -->

<!-- <\!-- # Réflexion -\-> -->

<!-- <\!-- \center -\-> -->

<!-- <\!-- ![](img/infographie/reflexion.png)&nbsp; -\-> -->

<!-- <\!-- # Optimisation -\-> -->

<!-- <\!-- Associativité : -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- <\!-- Pour un point $p = (x, y)$ et des matrices de -\-> -->
<!-- <\!-- rotation/échelle/cisaillement/réflexion $A$ et $B$, on a : -\-> -->

<!-- <\!-- $$A.(B.p) = (A.B).p$$ -\-> -->

<!-- <\!-- Où $.$ représente le produit matriciel -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- <\!-- Plusieurs multiplications de matrices peuvent donc se combiner en une -\-> -->
<!-- <\!-- seule ! -\-> -->

<!-- <\!-- # Optimisation -\-> -->

<!-- <\!-- Scaling suivi de Rotation : -\-> -->

<!-- <\!-- $$p' = M_{Rotation}.M_{Scaling}.p$$ -\-> -->

<!-- <\!-- Forme d'une matrice de rotation : -\-> -->

<!-- <\!-- \begin{equation*} -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     cos(\theta) & -sin(\theta) \\ -\-> -->
<!-- <\!--     sin(\theta) & cos(\theta) -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!-- \end{equation*} -\-> -->

<!-- <\!-- Forme d'une matrice de scaling : -\-> -->

<!-- <\!-- \begin{equation*} -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     s & 0 \\ -\-> -->
<!-- <\!--     0 & s -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!-- \end{equation*} -\-> -->

<!-- <\!-- # Optimisation -\-> -->

<!-- <\!-- On peut combiner les deux en une seule matrice pour accélérer le -\-> -->
<!-- <\!-- calcul ! -\-> -->

<!-- <\!-- \begin{equation*} -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     cos(\theta) & -sin(\theta) \\ -\-> -->
<!-- <\!--     sin(\theta) & cos(\theta) -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     s_x & 0 \\ -\-> -->
<!-- <\!--     0 & s_y -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!--   = -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     s_x*cos(\theta) & -s_y*sin(\theta) \\ -\-> -->
<!-- <\!--     s_x*sin(\theta) & s_y * cos(\theta) -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!-- \end{equation*} -\-> -->

<!-- <\!-- Un scaling de $\times 2$ en $x$ et $y$ suivi d'une rotation de $\pi/6$ -\-> -->
<!-- <\!-- *radians* s'exprime avec : -\-> -->

<!-- <\!-- \begin{equation*} -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     2 * cos(\pi/6) & -2 * sin(\pi/6) \\ -\-> -->
<!-- <\!--     2 * sin(\pi/6) &  2 * cos(\pi/6) -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!--   = -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     1.7321 & -1 \\ -\-> -->
<!-- <\!--     1      & 1.7321 -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!-- \end{equation*} -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- <\!-- (Notez : les transformations appliquées sur le point sont effectuées -\-> -->
<!-- <\!-- de **droite à gauche**, les matrices ont l'air "dans l'ordre inverse") -\-> -->

<!-- <\!-- # Optimisation -\-> -->

<!-- <\!-- - Toutes les matrices se multiplient, sauf celle de la translation, -\-> -->
<!-- <\!--   qui demande une addition... -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- <\!-- $$[Scaling(2), Rotation(\pi)], Translation(2,2), [Scaling(3), Shearing_x(2)]$$ -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- <\!-- On peut cependant utiliser un *trick* : ajouter une dimension... -\-> -->

<!-- <\!-- # Coordonnées homogènes -\-> -->

<!-- <\!-- Un point 2D en coordonnées cartésiennes $(x, y)$ peut être représenté -\-> -->
<!-- <\!-- en coordonnées homogènes par : -\-> -->

<!-- <\!-- $$(x, y) = (x, y, 1)$$ -\-> -->

<!-- <\!-- Simplement en ajoutant une troisième coordonnée égale à 1. -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- <\!-- Dans cet espace, les vecteurs proportionnels représentent le même point : -\-> -->

<!-- <\!-- $$(x, y, w) = (2x, 2y, 2w) = (x/w, y/w, 1)$$ -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- <\!-- Pour retrouver des coordonnées cartésiennes, on peut simplement -\-> -->
<!-- <\!-- diviser les coordonnées par la dernière coordonnée : -\-> -->

<!-- <\!-- $$(x, y, w) = (x/w, y/w)$$ -\-> -->


<!-- <\!-- # Coordonnées homogènes -\-> -->

<!-- <\!-- Pour fonctionner avec des vecteurs à 3 dimensions, les matrices de -\-> -->
<!-- <\!-- transformations deviennent des matrices 3x3 : -\-> -->

<!-- <\!-- [columns] -\-> -->

<!-- <\!-- [column=0.5] -\-> -->

<!-- <\!-- \center -\-> -->

<!-- <\!-- **Matrice de rotation** -\-> -->

<!-- <\!-- \begin{equation*} -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     cos(\theta) & -sin(\theta) & 0 \\ -\-> -->
<!-- <\!--     sin(\theta) & cos(\theta) & 0 \\ -\-> -->
<!-- <\!--     0 & 0 & 1 -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!-- \end{equation*} -\-> -->

<!-- <\!-- [column=0.5] -\-> -->

<!-- <\!-- \center -\-> -->

<!-- <\!-- **Matrice de scaling** -\-> -->

<!-- <\!-- \begin{equation*} -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     s_x & 0 & 0 \\ -\-> -->
<!-- <\!--     0 & s_y & 0 \\ -\-> -->
<!-- <\!--     0 & 0 & 1 -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!-- \end{equation*} -\-> -->

<!-- <\!-- [/columns] -\-> -->

<!-- <\!-- # Coordonnées homogènes -\-> -->

<!-- <\!-- En utilisant cette représentation, on peut exprimer une translation -\-> -->
<!-- <\!-- comme une multiplication matricielle : -\-> -->

<!-- <\!-- \center -\-> -->

<!-- <\!-- **Matrice de translation** -\-> -->

<!-- <\!-- \begin{equation*} -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     1 & 0 & T_x \\ -\-> -->
<!-- <\!--     0 & 1 & T_y \\ -\-> -->
<!-- <\!--     0 & 0 & 1 -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!-- \end{equation*} -\-> -->

<!-- <\!-- # Coordonnées homogènes -\-> -->

<!-- <\!-- Exemple : translation du point $(4, 6)$ de +5 en $x$, +7 en $y$ -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- <\!-- On a notre matrice : -\-> -->

<!-- <\!-- \begin{equation*} -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     1 & 0 & T_x \\ -\-> -->
<!-- <\!--     0 & 1 & T_y \\ -\-> -->
<!-- <\!--     0 & 0 & 1 -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!--   = -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     1 & 0 & 5 \\ -\-> -->
<!-- <\!--     0 & 1 & 7 \\ -\-> -->
<!-- <\!--     0 & 0 & 1 -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!-- \end{equation*} -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- <\!-- Ce qui donne : -\-> -->

<!-- <\!-- \begin{equation*} -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     1 & 0 & 5 \\ -\-> -->
<!-- <\!--     0 & 1 & 7 \\ -\-> -->
<!-- <\!--     0 & 0 & 1 -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     4 \\ -\-> -->
<!-- <\!--     6 \\ -\-> -->
<!-- <\!--     1 \\ -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!--   = -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     1*4 + 0*6 + 1 * 5 \\ -\-> -->
<!-- <\!--     0*4 + 1*6 + 1 * 7 \\ -\-> -->
<!-- <\!--     0*4 + 0*6 + 1 * 1 -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!--   = -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     4 + 5 \\ -\-> -->
<!-- <\!--     6 + 7 \\ -\-> -->
<!-- <\!--     1 -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!--   = -\-> -->
<!-- <\!--   \begin{bmatrix} -\-> -->
<!-- <\!--     9 \\ -\-> -->
<!-- <\!--     13 \\ -\-> -->
<!-- <\!--     1 -\-> -->
<!-- <\!--   \end{bmatrix} -\-> -->
<!-- <\!-- \end{equation*} -\-> -->

<!-- <\!-- On a effectivement : $(4, 6) + (5, 7) = (9, 13)$ -\-> -->

<!-- <\!-- # Définir des primitives -\-> -->

<!-- <\!-- Dessiner un carré transformé peut facilement se faire : -\-> -->

<!-- <\!-- 1. En appliquant la matrice de transformation sur chaque coin -\-> -->
<!-- <\!-- 2. En reliant les lignes entre les points transformés -\-> -->


<!-- <\!-- # Définir des primitives -\-> -->

<!-- <\!-- ## Transformer plusieurs points d'un coup -\-> -->

<!-- <\!-- \begin{equation*} -\-> -->
<!-- <\!--   \begin{split} -\-> -->
<!-- <\!--     \label{} -\-> -->
<!-- <\!--     P' &= T.P \\ -\-> -->
<!-- <\!--     &= -\-> -->
<!-- <\!--     \begin{bmatrix} -\-> -->
<!-- <\!--       a & b & c \\ -\-> -->
<!-- <\!--       d & e & f \\ -\-> -->
<!-- <\!--       0 & 0 & 1 -\-> -->
<!-- <\!--     \end{bmatrix} -\-> -->
<!-- <\!--     \begin{bmatrix} -\-> -->
<!-- <\!--       x_1 & x_2 & \hdots & x_n \\ -\-> -->
<!-- <\!--       y_1 & y_2 & \hdots & y_n \\ -\-> -->
<!-- <\!--       1 & 1 & \hdots & 1 -\-> -->
<!-- <\!--     \end{bmatrix} \\ -\-> -->
<!-- <\!--     &= -\-> -->
<!-- <\!--     \begin{bmatrix} -\-> -->
<!-- <\!--       (ax_1 + by_1 + c) & (ax_2 + by_2 + c) & \hdots & (ax_n + by_n + c) \\ -\-> -->
<!-- <\!--       (dx_1 + ey_1 + f) & (dx_2 + ey_2 + f) & \hdots & (dx_n + ey_n + f) \\ -\-> -->
<!-- <\!--       1 & 1 & \hdots & 1 -\-> -->
<!-- <\!--     \end{bmatrix} -\-> -->
<!-- <\!--   \end{split} -\-> -->
<!-- <\!-- \end{equation*} -\-> -->

<!-- <\!-- \center -\-> -->

<!-- <\!-- 3x3 . 3xn `=>` 3xn -\-> -->

<!-- <\!-- # Définir des primitives -\-> -->

<!-- <\!-- On peut par exemple partir du carré unitaire : -\-> -->

<!-- <\!-- - 4 Coins -\-> -->
<!-- <\!--     - $(0, 0)$ -\-> -->
<!-- <\!--     - $(1, 0)$ -\-> -->
<!-- <\!--     - $(0, 1)$ -\-> -->
<!-- <\!--     - $(1, 1)$ -\-> -->

<!-- <\!-- Rotation de $\pi/2$ autour du point $(0, 0)$ : -\-> -->

<!-- <\!-- \begin{equation*} -\-> -->
<!-- <\!--     \begin{bmatrix} -\-> -->
<!-- <\!--       cos(\pi/2) & -sin(\pi/2) & 0 \\ -\-> -->
<!-- <\!--       sin(\pi/2) & cos(\pi/2)  & 0 \\ -\-> -->
<!-- <\!--       0 & 0 & 1 -\-> -->
<!-- <\!--     \end{bmatrix} -\-> -->
<!-- <\!--     \begin{bmatrix} -\-> -->
<!-- <\!--       0 & 0 & 1 & 1 \\ -\-> -->
<!-- <\!--       0 & 1 & 1 & 0 \\ -\-> -->
<!-- <\!--       1 & 1 & 1 & 1 -\-> -->
<!-- <\!--     \end{bmatrix} -\-> -->
<!-- <\!-- \end{equation*} -\-> -->

<!-- <\!-- # Définir des primitives -\-> -->

<!-- <\!-- On arrive à : -\-> -->

<!-- <\!-- \begin{equation*} -\-> -->
<!-- <\!--     \begin{bmatrix} -\-> -->
<!-- <\!--       0 & -1 & 0 \\ -\-> -->
<!-- <\!--       1 &  0 & 0 \\ -\-> -->
<!-- <\!--       0 &  0 & 1 -\-> -->
<!-- <\!--     \end{bmatrix} -\-> -->
<!-- <\!--     \begin{bmatrix} -\-> -->
<!-- <\!--       0 & 0 & 1 & 1 \\ -\-> -->
<!-- <\!--       0 & 1 & 1 & 0 \\ -\-> -->
<!-- <\!--       1 & 1 & 1 & 1 -\-> -->
<!-- <\!--     \end{bmatrix} -\-> -->
<!-- <\!--     = -\-> -->
<!-- <\!--     \begin{bmatrix} -\-> -->
<!-- <\!--       0 & -1 & -1 & 0 \\ -\-> -->
<!-- <\!--       0 &  0 &  1 & 1 \\ -\-> -->
<!-- <\!--       1 &  1 &  1 & 1 -\-> -->
<!-- <\!--     \end{bmatrix} -\-> -->
<!-- <\!-- \end{equation*} -\-> -->

<!-- <\!-- [columns] -\-> -->

<!-- <\!-- [column=0.5] -\-> -->

<!-- <\!-- \center -\-> -->

<!-- <\!-- *Avant transformation* -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- <\!-- ![](img/transform-square-1.png){width=60%}&nbsp; -\-> -->

<!-- <\!-- [column=0.5] -\-> -->

<!-- <\!-- \center -\-> -->

<!-- <\!-- *Après transformation* -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- <\!-- ![](img/transform-square-2.png){width=60%}&nbsp; -\-> -->

<!-- <\!-- [/columns] -\-> -->

<!-- # Transformer les points *vs* Transformer le monde -->

<!-- Pour transformer des formes, on a une autre option : appliquer la -->
<!-- transformation inverse à la surface de dessin -->

<!-- &nbsp; -->

<!-- ![](img/transform0.png){width=50%} -->
<!-- ![](img/transform1.png){width=50%} -->


<!-- # Transformer les points *vs* Transformer le monde -->

<!-- \center -->

<!-- ![](img/transform2.png)&nbsp; -->

<!-- &nbsp; -->

<!-- *Rotation du carré de $\frac{\pi}{12}$* -->

<!-- # Transformer les points *vs* Transformer le monde -->

<!-- \center -->

<!-- ![](img/transform3.png)&nbsp; -->

<!-- &nbsp; -->

<!-- *Rotation du monde de $-\frac{\pi}{12}$ + dessin du carré original* -->


<!-- # `Canvas` -->

<!-- <\!-- En pratique, JavaFX abstrait pour nous toute cette manipulation de -\-> -->
<!-- <\!-- matrices. -\-> -->

<!-- <\!-- &nbsp; -\-> -->

<!-- On peut **transformer le canvas** à partir de méthodes de son -->
<!-- `GraphicsContext` : -->

<!-- - `context.translate(x, y)` -->
<!--     - Ajoute une translation au canvas -->
<!-- - `context.rotate(angle)` -->
<!--     - Ajoute une rotation (angle en degrés) -->
<!-- - `context.scale(sx, sy)` -->
<!--     - Ajoute un facteur d'échelle au canvas -->

<!-- <\!-- TODO : exemples de chaque -\-> -->

<!-- # `Canvas` -->

<!-- <\!-- TODO : reshaper les matrices -\-> -->

<!-- On peut également spécifier manuellement la matrice de transformation -->
<!-- qu'on souhaite utiliser. -->

<!-- Pour une matrice : -->

<!-- \begin{equation*} -->
<!--   \begin{bmatrix} -->
<!--     a & b & c \\ -->
<!--     d & e & f \\ -->
<!--     0 & 0 & 1 -->
<!--   \end{bmatrix} -->
<!-- \end{equation*} -->

<!-- - `context.transform(a, d, b, e, c, f)` -->
<!--     - Multiplie la transformation actuelle par la matrice spécifiée -->
<!-- - `context.setTransform(a, d, b, e, c, f)` -->
<!--     - Remplace la transformation actuelle par la matrice spécifiée -->

<!-- <\!-- PROGRAMME : CanvasTransform -\-> -->

<!-- *Voir : programme CanvasTransform* -->

<!-- # `Canvas` -->

<!-- ```java -->
<!-- // Si on part d'un canvas sans transformation, faire : -->

<!-- context.scale(0.5, 0.5); -->
<!-- // Rotation de PI/6 (exprimée en degrés) -->
<!-- context.rotate(Math.PI / 6 * 360/(Math.PI * 2)); -->

<!-- // est équivalent à faire : -->

<!-- context.setTransform(0.433013, 0.249999, -0.249999, 0.433013, 0, 0); -->
<!-- ``` -->

<!-- # `Canvas` -->

<!-- - Il n'y a pas de méthode définie pour remettre la transformation à -->
<!--   zéro -->
<!-- - On peut cependant spécifier explicitement qu'on souhaite utiliser la -->
<!--   *matrice identité* comme transformation : -->

<!-- ```java -->
<!-- context.setTransform(1, 0, 0, 1, 0, 0); -->

<!-- /* Ou équivalent : -->
<!--    Le constructeur par défaut de Affine() -->
<!--    crée une matrice identité */ -->

<!-- context.setTransform(new Affine()); -->
<!-- ``` -->

<!-- # Clic le Clown -->

<!-- Pour faire une rotation autour du centre, on doit donc : -->

<!-- 1. Faire une translation du canvas de façon à apporter le $(0, 0)$ à -->
<!--    l'endroit où on voudrait avoir le centre de l'objet -->
<!-- 2. Faire faire au canvas la rotation inverse de celle voulue pour -->
<!--    l'objet -->
<!-- 3. Dessiner l'objet avec son centre sur $(0, 0)$ -->
<!-- 4. Faire faire au canvas la rotation inverse de la précédente -->
<!-- 5. Remettre le centre à sa place (translation inverse) -->

<!-- &nbsp; -->

<!-- Même principe pour le scaling -->

<!-- # Clic le Clown : transformations -->

<!-- Pour faire tourner Clic le Clown sur lui-même, on peut donc utiliser : -->

<!-- ```java -->
<!--     @Override -->
<!--     public void draw(GraphicsContext context) { -->
<!--         context.translate(x, y); -->
<!--         context.rotate(-angle); -->

<!--         for(Entity e : entities){ -->
<!--             e.draw(context); -->
<!--         } -->

<!--         // On défait la transformation -->
<!--         context.rotate(angle); -->
<!--         context.translate(-x, -y); -->
<!--     } -->
<!-- ``` -->

<!-- # Clic le Clown : transformations -->

<!-- - Puisqu'on applique une transformation à tout le canvas avant de -->
<!--   dessiner les primitives `Oval`, `Rectangle`, ..., le clown est -->
<!--   magiquement **toujours centré à $(0, 0)$** quand vient le temps de -->
<!--   dessiner ses composantes -->
<!-- - Les positions $(x, y)$ des primitives doivent donc être exprimées -->
<!--   par rapport à $(0, 0)$ directement, par exemple, le bout de code : -->

<!-- ```java -->
<!-- Triangle chapeau = new Triangle( -->
<!--     x, y - h / 2 + hatHeight / 2, -->
<!--     hatWidth, hatHeight); -->
<!-- ``` -->

<!-- Doit devenir : -->

<!-- ```java -->
<!-- Triangle chapeau = new Triangle( -->
<!--     0, 0 - h / 2 + hatHeight / 2, -->
<!--     hatWidth, hatHeight); -->
<!-- ``` -->

<!-- # Clic le Clown : transformations -->

<!-- On peut également retirer le bout de code suivant de la fonction -->
<!-- `update(double dt)` : -->

<!-- ```java -->
<!--     double deltaX = lastX - x, deltaY = lastY - y; -->
<!--     for (Entity e : entities) { -->
<!--         e.setX(e.getX() + deltaX); -->
<!--         e.setY(e.getY() + deltaY); -->
<!--     } -->
<!-- ``` -->

<!-- Les "positions" $(x, y)$ des primitives ne changent plus : seule la -->
<!-- position du Clown est modifiée, et ça s'applique à tout le canvas au -->
<!-- moment de dessiner. -->

<!-- # Clic le Clown : transformations -->

<!-- ![Tout ça pour ça](img/animation/clicleclown-rotation3.png){width=70%} -->

<!-- <\!-- PROGRAMME : ClicLeClown5 -\-> -->
<!-- *Voir : programme ClicLeClown5* -->

<!-- # Clic le Clown : transformations -->

<!-- Quelques petits problèmes... -->

<!-- - La détection de clics ne marche plus -->
<!--     - Tous les objets pensent qu'ils sont en $(0, 0)$ ! -->
<!-- - La collision avec le mur se fait encore comme si le Clown n'avait -->
<!--   subit aucune rotation -->


<!-- # Clic à l'intérieur ou à l'extérieur du clown ? -->

<!-- Pour la détection de clics, on a deux choix : -->

<!-- 1. Tenter de trouver les équations qui correspondent aux `Oval`, `Triangle`, `Rectangle` transformés -->

<!-- \centerline{\textbf{ou}} -->

<!-- 2. Appliquer la *transformation inverse* au point sur lequel on a cliqué -->


<!-- # Clic à l'intérieur ou à l'extérieur du clown ? -->

<!-- - La deuxième solution est définitivement plus rapide à appliquer -->

<!-- - On ajoute une propriété qui correspond à l'inverse de la matrice de -->
<!--   transformation : -->

<!-- ```java -->
<!--     private Affine inverseTransform; -->
<!-- ``` -->

<!-- - À chaque appel de `draw()`, le `context` crée une matrice de -->
<!--   transformation pour nous permettre de dessiner le clown avec les -->
<!--   bonnes rotations/scaling/... -->

<!-- - On peut en profiter pour demander l'inverse -->

<!-- # Clic à l'intérieur ou à l'extérieur du clown ? -->

<!-- ```java -->
<!--     @Override -->
<!--     public void draw(GraphicsContext context) { -->
<!--         context.translate(x, y); -->
<!--         context.rotate(-angle); -->
<!--         try { -->
<!--             // Stocke la transformation inverse -->
<!--             inverseTransform = context.getTransform().createInverse(); -->
<!--         } catch (NonInvertibleTransformException ex) { -->
<!--             /* On pourrait avoir une erreur si jamais -->
<!--                la transformation actuelle n'est pas inversible -->
<!--                (ex.: scaling de 0 en X) */ -->
<!--             ex.printStackTrace(); -->
<!--         } -->
<!--         for (Entity e : entities) { -->
<!--             e.draw(context); -->
<!--         } -->
<!--         context.rotate(angle); -->
<!--         context.translate(-x, -y); -->
<!--     } -->
<!-- ``` -->

<!-- # Clic à l'intérieur ou à l'extérieur du clown ? -->

<!-- - C'est très loin d'être efficace... Mais on peut alors très -->
<!--   gracieusement implémenter la détection de clic : -->

<!-- ```java -->
<!--     @Override -->
<!--     public boolean contains(Point2D p) { -->
<!--         Point2D pTransformed = inverseTransform.transform(p); -->

<!--         for (Entity e : entities) { -->
<!--             if (e.contains(pTransformed)) { -->
<!--                 return true; -->
<!--             } -->
<!--         } -->

<!--         return false; -->
<!--     } -->
<!-- ``` -->

<!-- - Pour un jeu comme Clic Le Clown, ça passe -->

<!-- # Clic à l'intérieur ou à l'extérieur du clown ? -->

<!-- ![Résultat affiché](img/animation/clicleclown-rotated-win.png){width=70%} -->

<!-- *Voir : programme ClicLeClown6* -->


<!-- # Clic à l'intérieur ou à l'extérieur du clown ? -->

<!-- - Comment performe notre algorithme de détection de clics si on se -->
<!--   retrouve avec des squelettes très complexes ou beaucoup éléments ? -->

<!-- - On passe à travers l'entièreté de notre squelette (chapeau, yeux, -->
<!--   nez, pupilles, ...) pour vérifier si le point est à l'intérieur de -->
<!--   notre forme ou non -->

<!-- \center -->

<!-- ![](img/animation/clicleclown-clic-far-away.png){width=50%}&nbsp; -->

<!-- # Corriger le rebond -->

<!-- - En ce moment, la détection de collisions assume que le Clown est -->
<!--   englobé par un rectangle $w \times h$ dont les axes sont alignés avec l'écran... -->
<!-- - C'était vrai avant les rotations, ce n'est plus vrai ! -->
<!-- - On pourrait cependant retrouver un rectangle englobant pour le clown -->
<!--   transformé sans trop de difficultés, en considérant les points -->
<!--   $min_x, max_x, min_y, max_y$ (après transformation) sur tout -->
<!--   l'ensemble d'entités qui composent le clown -->

<!-- # Corriger le rebond -->

<!-- ![Boîte englobante pour le clown transformé : on pourrait plutôt utiliser cette boîte pour les rebonds avec les côtés de l'écran](img/animation/clicleclown-aabb.png) -->

<!-- # Observations -->

<!-- Il faut admettre que notre classe `Entity` est rendue bizarre... -->

<!-- - On a des attributs position $(x, y)$ qui sont à 0 pour la majorité -->
<!--   des éléments -->
<!-- - On a des vitesses qui sont à 0 pour la majorité des éléments -->
<!-- - On a l'entité `Clown` qui est une `Entity` un peu spéciale, -->
<!--   puisqu'elle est uniquement composée d'autres `Entity` -->

<!-- &nbsp; -->

<!-- - Est-ce qu'on pourrait généraliser un peu mieux notre code ? -->

<!-- # Ajouter un corps à la tête de Clown -->

<!-- Quoi faire si on veut arriver à ce résultat-là ? -->

<!-- ![Clown avec un corps de bonhomme allumette -- un peu creepy](img/animation/clicleclown-body.png) -->


<!-- # Hiérarchie d'`Entity` -->

<!-- - Plutôt que d'avoir un `ArrayList<Entity>` dans `Clown`, on peut -->
<!--   généraliser l'idée et donner à *chaque* `Entity` zéro ou plusieurs -->
<!--   enfants -->

<!-- ![Arbre pour définir un clown complet](img/gv/10.hierarchie.png)&nbsp; -->

<!-- # Hiérarchie d'`Entity` -->

<!-- - Les entités graphiques complexes seraient définies par un *arbre* de -->
<!--   primitives + transformations -->
<!-- - La transformation d'un noeud s'applique également à tous ses enfants -->
<!-- - *Les transformations s'appliquent en cascade grâce à des -->
<!--   multiplications de matrices faites récursivement* -->

<!-- # Hiérarchie d'`Entity` -->

<!-- \center -->

<!-- ![Hiérarchie d'`Entity` + transformations](img/gv/10.transform.png)&nbsp; -->

<!-- # Hiérarchie d'`Entity` -->

<!-- Chaque `Entity` est définie avec une **transformation par rapport à -->
<!-- son parent** plutôt qu'avec une position $(x, y)$ absolue -->

<!-- ![Clown (`Entity` à la racine de l'arbre) défini avec une translation par rapport au canvas](img/animation/clown-hierarchie/1.png){width=35%} -->

<!-- # Hiérarchie d'`Entity` -->

<!-- Chaque `Entity` est définie avec une **transformation par rapport à -->
<!-- son parent** plutôt qu'avec une position $(x, y)$ absolue -->

<!-- ![Tête (enfant de `Clown`) défini avec une translation par rapport à son parent `Clown`](img/animation/clown-hierarchie/2.png){width=35%} -->


<!-- # Hiérarchie d'`Entity` -->

<!-- Chaque `Entity` est définie avec une **transformation par rapport à -->
<!-- son parent** plutôt qu'avec une position $(x, y)$ absolue -->

<!-- ![Les Cheveux sont définis par rapport à la Tête (la hiérarchie donne `Clown` -> `Tête` -> \{Cheveux à gauche, Cheveux à droite\})](img/animation/clown-hierarchie/3.png){width=35%} -->

<!-- # Hiérarchie d'`Entity` -->

<!-- Chaque `Entity` est définie avec une **transformation par rapport à -->
<!-- son parent** plutôt qu'avec une position $(x, y)$ absolue -->

<!-- ![Les Cheveux sont définis par rapport à la Tête (la hiérarchie donne `Clown` -> `Tête` -> \{Cheveux à gauche, Cheveux à droite\})](img/animation/clown-hierarchie/4.png){width=35%} -->


<!-- # Hiérarchie d'`Entity` -->

<!-- - Comment implémenter ça ? -->
<!-- - La définition par défaut de la méthode `draw(GraphicsContext -->
<!--   context)` devient -->
<!--     1. Appliquer les transformations -->
<!--     2. Dessiner tous les enfants -->
<!--     3. Inverser les transformations -->
<!-- - Seules les entités primitives redéfinissent `draw()` -->
<!--     - Les primitives correspondent aux feuilles de l'arbre : `Oval`, -->
<!--       `Rectangle`, ... -->


<!-- # Hiérarchie d'`Entity` -->

<!-- ```java -->
<!--     public void draw(GraphicsContext context) { -->
<!--         context.translate(x, y); -->
<!--         context.rotate(-angle); -->
<!--         context.scale(sx, sy); -->
<!--         // autres transformations au besoin -->

<!--         // ... Stocke la transformation inverse au besoin... -->

<!--         for (Entity e : entities) { -->
<!--             e.draw(context); -->
<!--         } -->

<!--         // Défaire la transformation -->
<!--         // ... -->
<!--         context.scale(1/sx, 1/sy); -->
<!--         context.rotate(angle); -->
<!--         context.translate(-x, -y); -->
<!--     } -->
<!-- ``` -->

<!-- # Hiérarchie d'`Entity` -->

<!-- - On peut simplifier l'étape de transformations inverses en -->
<!--   "sauvegardant" la matrice de transformation du canvas avant -->
<!--   d'appliquer les transformations propres à chaque noeud de l'arbre -->

<!-- # Hiérarchie d'`Entity` -->

<!-- - Le canvas a un `Stack` de transformations intégré qu'on peut -->
<!--   utiliser : -->

<!-- ```java -->
<!--     public void draw(GraphicsContext context) { -->
<!--         context.save(); -->

<!--         context.translate(x, y); -->
<!--         context.rotate(-angle); -->
<!--         context.scale(sx, sy); -->
<!--         // autres transformations au besoin -->

<!--         // ... Stocke la transformation inverse au besoin... -->

<!--         for (Entity e : entities) { -->
<!--             e.draw(context); -->
<!--         } -->
<!--         // Défaire la transformation -->
<!--         context.restore(); -->
<!--     } -->
<!-- ``` -->

<!-- # Hiérarchies -->

<!-- - On définit donc un arbre de composantes graphiques qui ont chacune -->
<!--   leur propre transformation -->
<!-- - Les transformations s'appliquent en cascade -->
<!-- - ... -->
<!-- - *Un arbre de composantes graphiques* -->

<!-- # Hiérarchies -->

<!-- \center -->

<!-- ![](img/javafx-layers/fancy2.png)&nbsp; -->

<!-- # Hiérarchies -->

<!-- On peut justement appliquer des transformations à chaque composante de -->
<!-- l'arbre de composantes de JavaFX... -->

<!-- \center -->

<!-- ![](img/animation/fx-transform0.png){width=40%}&nbsp; -->

<!-- # Hiérarchies -->

<!-- ```java -->
<!-- root.setScaleY(1.8); -->
<!-- root.setScaleY(1.5); -->
<!-- ``` -->

<!-- \center -->

<!-- ![](img/animation/fx-transform1.png){width=40%}&nbsp; -->


<!-- # Hiérarchies -->

<!-- ```java -->
<!-- root.setScaleX(1.5); root.setScaleY(0.8); -->
<!-- sad.setScaleX(-1); grid.setRotate(60); -->
<!-- ``` -->

<!-- \center -->

<!-- ![](img/animation/fx-transform2.png){width=40%}&nbsp; -->

<!-- <\!-- # Animation 3D -\-> -->

<!-- <\!-- On peut généraliser les transformations 2D qu'on a vues à des -\-> -->
<!-- <\!-- transformations 3D -\-> -->

<!-- <\!-- - Comme nos écrans ne sont pas en 3D (pour l'instant...), on doit -\-> -->
<!-- <\!--   cependant ajouter une étape de **projection** des coordonnées -\-> -->

<!-- <\!-- <\\!-- TODO image d'une projection 2D -\\-> -\-> -->

<!-- <\!-- <\\!-- TODO image d'une projection 3D -\\-> -\-> -->

<!-- <\!-- # Animation 3D -\-> -->

<!-- <\!-- - Matrice de projection orthographique -\-> -->
<!-- <\!-- - Perspective -\-> -->

<!-- <\!-- # Revenons une dernière fois sur la couleur... -\-> -->

<!-- <\!-- On peut considérer l'espace de couleurs Rouge-Vert-Bleu (*RGB* en -\-> -->
<!-- <\!-- anglais) comme un espace vectoriel 3D. -\-> -->

<!-- <\!-- ![https://commons.wikimedia.org/wiki/File:RGB_color_solid_cube.png](img/800px-RGB_color_solid_cube.png){width=65%} -\-> -->

<!-- <\!-- Une couleur en *RGB* est un point 3D dans cet espace -\-> -->

<!-- <\!-- # YIQ -\-> -->

<!-- <\!-- Matrice de changement de base : -\-> -->

<!-- <\!-- https://en.wikipedia.org/wiki/YIQ -\-> -->

<!-- <\!-- Rotation 3D en YIQ => Rotation de la couleur ! -\-> -->

<!-- <\!-- https://beesbuzz.biz/code/hsv_color_transforms.php -\-> -->

<!-- <\!-- Cube RGB vs Cube YIQ => -\-> -->

<!-- <\!-- # HSB -\-> -->

<!-- <\!-- HSB est un espace un peu différent et correspond plutôt à un cylindre -\-> -->
<!-- <\!-- dans l'espace : -\-> -->

<!-- <\!-- https://en.wikipedia.org/wiki/HSL_and_HSV#Basic_principle -\-> -->


<!-- <\!-- # Porter le jeu à succès Clic Le Clown sur Android -\-> -->

<!-- <\!-- - MVC ! -\-> -->
<!-- <\!-- - Séparer la logique de l'affichage pour un maximum -\-> -->

<!-- <\!-- # Porter le jeu à succès Clic Le Clown sur Android -\-> -->

<!-- <\!-- Séparer la méthode `draw` du reste -\-> -->

<!-- <\!-- Pourquoi ? -\-> -->
