---
title: De JavaScript à Java
author: Nicolas Hurtubise ~ DIRO \newline Dérivé de documents préparés par Pascal Vincent
header-includes:
    - \usepackage{textcomp}
    - \usepackage{listings}
date: IFT1025 - Programmation 2
---

# Au programme...

- Java vs JavaScript
- Programme Java de base
- Types
- Tableaux
- Strings
- Entrées/sorties d'un programme
- Modèle mémoire

# Java $\approx$ JavaScript ?

* Tous deux arrivés en 1995
* Noms similaires
* Similarités superficielles
    - Syntaxes de base similaires, inspirées du C
    - Librairies standards similaires, ex.: `Math.sqrt(x)`
* Langages très différents

# Java $\approx$ JavaScript ?

- Toute la syntaxe de base reste la même : `if`, `while`, `for`, `switch`, `/* commentaires */`, ...
- Déclaration des *variables* et *fonctions* change
- Paradigmes différents : structure et organisation des programmes change

# **JavaScript** vs Java

## JavaScript

* Langage de *script*, *interprété* à l'origine\footnote{Les
  implémentations les plus populaires actuellement sont des JIT (Just
  In Time Compiler)}
* Typage *dynamique*
* Orienté objet par prototypes
* Langage conçu en 10 jours chez Netscape pour utilisation sur le web


# **JavaScript** vs Java

## JavaScript

- Langage "secondaire"
    - Ajout de contenu dynamique dans les pages web
    - *Dynamic HTML*, ou *DHTML*
- Plusieurs approximations
- A évolué et est maintenant un langage utilisé hors des navigateurs
    - Node.js, programmation côté serveur et ligne de commande
    - Electron, applications de bureau (ex.: Atom, Slack, Discord...)

# JavaScript vs **Java**

## Java

- Langage compilé (en bytecode)
- Orienté objet par classes
- Typage *statique*
- Langage plus rigide, ne tolère pas d'erreurs
- Conçu chez Sun Microsystems pour être *"general purpose"*
- Utilisé un peu partout
    - Applications de bureau
    - [Systèmes embarqués](https://docs.oracle.com/javase/8/embedded/develop-apps-platforms/overview.htm)
    - *Applets* sur le web
    - Serveurs web
    - Applications mobiles sur *Android*

# JavaScript vs Java

![Utilisation typique de JavaScript à la fin des années 90, https://babbage.cs.qc.cuny.edu/IEEE-754.old/Decimal.html](img/old-js-page.png)

# JavaScript vs Java

![Une des premières applications "d'envergure" de JavaScript, http://digital-archaeology.org/wp-content/uploads/2016/07/googlemaps.png](img/googlemaps.png){ width=75% }

# JavaScript vs Java

![Applet Java tournant sur Netscape, http://www.webbasedprogramming.com/Java-Developers-Reference/f13-6.gif](img/applet.gif){ width=75% }

# Java

![IntelliJ IDEA, programme écrit en Java](img/IntelliJ_IDEA_14.1.3.png)

# Java

![Application Android QKSMS](img/qksms.png)

# Hello World

Programme qui affiche "Hello, World !" à l'écran


## JavaScript

```javascript
// codeBoot
print("Hello, World !");

// Node/Browser
console.log("Hello, World !");
```

# Hello World

Programme qui affiche "Hello, World !" à l'écran

## Java

```java
// Fichier: Hello.java
public class Hello {
   public static void main(String[] args) {
      // Votre code ici
      System.out.println("Hello, World !");
   }
}
```

- `main`: la fonction principale de votre programme, celle qui est
  appelée au début du programme
- `public static void` ? `String[] args` ?
- `System.out.println`: fonction équivalente à "print" dans `codeBoot` ou à `console.log` dans `node.js`

# Exécution

## Javascript

- Lancer dans codeBoot
- `node hello.js`
- `<script src="hello.js"></script>`

## Java

Compilation nécessaire avant d'exécuter :

```bash
javac Hello.java # crée le fichier "Hello.class"
java Hello # Exécute le code dans Hello.class
```

# Compilation

- Plutôt que d'exécuter directement le code Java, on le transforme
  d'abord en *bytecode*
- *Bytecode* == un langage intermédiaire entre le java (langage de
  programmation) et le binaire (langage machine)
- Accélère l'exécution : on fait une partie du travail une seule fois
  plutôt qu'à chaque exécution

```
                   Fichier source
                         |
                         | javac
                         |
                     Bytecode
                         |
                         | java
                         |
                Exécution du programme
```

# Compilation

```java
// Fichier HelloWorld.java
public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello, world !");
    }
}
```

# Compilation

```
0000000 ca fe ba be 00 00 00 34 00 1d 0a 00 06 00 0f 09
0000020 00 10 00 11 08 00 12 0a 00 13 00 14 07 00 15 07
0000040 00 16 01 00 06 3c 69 6e 69 74 3e 01 00 03 28 29
0000060 56 01 00 04 43 6f 64 65 01 00 0f 4c 69 6e 65 4e
0000100 75 6d 62 65 72 54 61 62 6c 65 01 00 04 6d 61 69
0000120 6e 01 00 16 28 5b 4c 6a 61 76 61 2f 6c 61 6e 67
0000140 2f 53 74 72 69 6e 67 3b 29 56 01 00 0a 53 6f 75
0000160 72 63 65 46 69 6c 65 01 00 0a 48 65 6c 6c 6f 2e
0000200 6a 61 76 61 0c 00 07 00 08 07 00 17 0c 00 18 00
0000220 19 01 00 0e 48 65 6c 6c 6f 2c 20 77 6f 72 6c 64
0000240 20 21 07 00 1a 0c 00 1b 00 1c 01 00 05 48 65 6c
0000260 6c 6f 01 00 10 6a 61 76 61 2f 6c 61 6e 67 2f 4f
0000300 62 6a 65 63 74 01 00 10 6a 61 76 61 2f 6c 61 6e
0000320 67 2f 53 79 73 74 65 6d 01 00 03 6f 75 74 01 00
...
```

# Langage statiquement typé

- En JS, une variable peut prendre n'importe quelle valeur

## JS

```javascript
// On déclare une nouvelle variable avec *var*
var a = "xyz";
a = true;
a = 123;
```

# Langage statiquement typé

- Java : une variable doit *toujours contenir le même type de donnée*
- Spécifié à la déclaration

## Java

```java
// On déclare une variable en précisant son type
// int == integer == nombre entier
int a = 0;

// Invalide, a peut seulement contenir un entier
a = "Bonjour !";

String b = "abc";

// Pas valide non plus...
b = true;
```

# Types

## Pourquoi ?

- JavaScript stocke tous les nombres comme des nombres flottants sur
  **64 bits**
    - On pourrait avoir besoin de plus de précision, ou au contraire,
      de beaucoup moins de précision
- Si on connaît d'avance quel type de donnée une variable contient, on
  peut s'en servir pour :
    - Optimiser le code
    - Utiliser moins d'espace en mémoire
    - Prévenir des erreurs

# Types

## Exemple

```javascript
// Affiche NaN car le calcul est invalide...
print(Math.max("bonjour", 123));
```

# Types

## En Java

```java
public static void main(String[] args) {
    // Cause une erreur à la compilation, avant même
    // d'avoir à tester le programme
    System.out.println(Math.max("bonjour", 123));
}
```

# Types de variables

Il y a quelques **types primitifs** :


- int : integer, nombre entier 32 bits
- float : nombre à virgule flottante sur 32 bits
- long : "long" nombre entier, sur 64 bits
- double : nombre à virgule flottante *double précision*, sur 64 bits
    - Seul type pour les nombres en JS !
- boolean : valeur de vérité true/false
- char : caractère textuel (un seul)
- byte : nombre entier 8 bits
- short : "short int", nombre entier 16 bits

# Types de variables

Par défaut, les nombres entiers littéraux sont des `int` et les
nombres décimaux littéraux sont des `double`

```java
int age = 25;
double nombreDecimal = 123456789.0;


// Suffixe L pour spécifier un "long int"
long grandNombreEntier = 12345678910L;

// Suffixe f pour spécifier un float 32 bits
float nombreFlottant32bits = 15.3f;

boolean isCoffeeCold = false; // ou true
char caractere = 'a'; // Notez les *guillemets simples ici*
```

# Types de variables

**Important** : le résultat des opérations dépend du type des variables

&nbsp;

```Java
double a = 3.0;
double b = 2.0;

System.out.println(a + b); // Affiche 5.0 (double)



System.out.println(a / b); // Affiche 1.5 (double)
```


# Types de variables

**Important** : le résultat des opérations dépend du type des variables

&nbsp;

```Java
int a = 3;
int b = 2;

System.out.println(a + b); // Affiche 5 (int)

/* Un calcul effectué sur deux int aura
   pour résultat un int, attention aux divisions */
System.out.println(a / b); // Affiche 1 ! (int)
```

# Types de variables

Solution au problème : on peut forcer à changer une valeur de type
avec un **cast**

```java
int a = 3;
int b = 2;

// (double) a => 3.0
// (double) b => 2.0
System.out.println((double) a / (double) b);
// Affiche 1.5


```

# Types de variables

- À noter : les variables sont automatiquement considérées comme leur
  équivalent "plus large" au besoin :

    `byte` $\rightarrow$ `short` $\rightarrow$ `int` $\rightarrow$
    `long` $\rightarrow$ `float` $\rightarrow$ `double`

```java
int a = 3;

/* Convertit automatiquement a en double
   pour faire le calcul */
System.out.println(a / 2.0); // Affiche 1.5
```

# Types de variables


Pour forcer un type "plus spécifique", on doit utiliser un `(cast)`

```java
double c = 3.0;
double d = 2.0;

/* Affiche 1.5, car a est un double,
   donc le calcul se fait en doubles */
System.out.println(c / 2);

/* Affiche 1, car les deux opérandes
   de la division sont des entiers */
System.out.println((int) c / (int) d);
```

# Types de variables

Notez : un cast de `float` en `int` tronque la partie fractionnaire (ça n'arrondit pas)

```java
System.out.println((int) 4.6); // Affiche 4
System.out.println((int) -4.6); // Affiche -4
```


# Résumé sur les types

- Toute variable a un type fixe défini à la déclaration
- Les types primitifs les plus communs sont :

| Type        | Exemple de valeur littérale   |
|-------------+-------------------------------|
| byte        | (byte) 99                     |
| short       | (short) 1024                  |
| **int**     | 65535                         |
| long        | 100000000000L                 |
| float       | 3.5f                          |
| **double**  | 123.456, 1.23456e2, 123456e-3 |
| **char**    | 'a', '?', '.', ...            |
| **boolean** | true ou false                 |


# Résumé sur les types

- La conversion de types numériques se fait implicitement au besoin,
  *tant que ça ne risque pas de faire perdre de la précision au
  programme*
- Convertir un nombre entier plus petit vers un nombre entier plus grand
    - Ex.: `byte` $\rightarrow$ `int`, `int` $\rightarrow$ `long`, ...
- Convertir un nombre flottant plus petit en un nombre flottant plus grand
    - Ex.: `float` $\rightarrow$ `double`
- Convertir un nombre entier en un nombre flottant
    - Ex.: `int` $\rightarrow$ `float`, `int` $\rightarrow$ `double`,
      `long` $\rightarrow$ `float`, ...

```java
int a = 10;
float b = a; // OK: conversion implicite int->float
float c = 100; // OK: int -> float
```

# Résumé sur les types

- Quand il y a un risque de perdre de la précision, on **doit** caster explicitement la valeur
- C'est une façon de dire au compilateur :

> Ne t'inquiète pas, je sais ce que je fais
>
> Si on perd de la précision, c'est mon problème

```java
int x = 2500;
short y = (short) x;

// INCORRECT: car 1.5 représente un double : précision perdue
float z = 1.5;

float z = 1.5f; // OK: littéral float
```

Lire le document *Complément : Conversions entre types de base (Pascal
Vincent)* pour plus d'exemples et de précisions

# Fonctions

- En Java, toute instruction doit se trouver dans une fonction
- La première fonction appelée est la fonction `main`, qui est déclarée avec :

```java
public static void main(String[] args)
```

- `public static` : pour l'instant, utilisez ça tel quel sans demander
  ce que ça mange en hiver, ça sera expliqué plus tard

# Fonctions

- Les valeurs passées en paramètres à une fonction et la valeur qu'une
  fonction retourne sont également typées

- Lorsqu'on déclare une fonction, on doit donc inclure des
  informations sur les types (paramètres et valeur de retour)

## Java

```java
public static int carre(int x) {
    return x * x;
}
```

# Fonctions

Les fonctions qui ne retournent pas de valeur doivent avoir pour type
de retour `void`

## Java

```java
public static void direBonjour() {
    System.out.println("Bonjour !");
}
```


# Fonctions

<!-- https://docs.oracle.com/javase/tutorial/java/javaOO/methods.html -->

- La **signature d'une fonction** est définie en Java comme étant son
  nom + le type de ses arguments

## Java

```java
public static int carre(int x) {
    return x * x;
}
```

## Signature

```
carre(int)
```

# Note sur la portée des variables

En JavaScript, les variables (non globales) déclarées avec `var` ont
une portée limitée à la fonction dans laquelle elles ont été déclarées

## JavaScript

```javascript
function nombreHarmonique(n) {
    var somme = 0;

    for(var i=1; i<=n; i++) {
        var terme = 1/i;
        somme += terme;
    }

    /* Affiche la dernière valeur de i
       et la dernière valeur de terme */
    print(i + "," + terme);

    return somme;
}
```

# Note sur la portée des variables

En Java, cette portée est toujours limitée au *bloc* dans lequel les
variables ont été déclarées

## Java

```java
public static double nombreHarmonique(int n) {
    double somme = 0;

    for(int i=1; i<=n; i++) {
        double terme = 1.0/i;
        somme += terme;
    }

    System.out.println(i); // Invalide !
    System.out.println(terme); // Invalide !

    return somme; // OK
}
```


# Tableaux

- En Java, les tableaux, comme les variables, ne peuvent contenir
  qu'un seul type de donnée

- Si on veut un tableau contenant des entiers, on doit déclarer la
  variable reliée comme étant de type "tableau de int" : `int[]`

# Tableaux

- Les tableaux en Java ont une **taille fixe**

- On initialise un tableau en précisant sa taille :

```java
int[] a = new int[3]; // Nouveau tableau vide de taille 3
a[0] = 1;
a[1] = 2;
a[2] = 3;

/* Alternativement, on peut initialiser directement avec
   un tableau littéral
   Java comprend qu'on veut un tableau de taille 5 */
int[] b = {1,2,3,4,5};
```

# Tableaux

On peut connaître le nombre d'éléments dans un tableau en utilisant
`tableau.length` :

```java
int[] tab = {10,20,30};
System.out.println(tab.length); // Affiche 3
```

# Tableaux

- Comme en JavaScript, un tableau 2D est un tableau dont tous les
  éléments sont de type "tableau 1D"

```java
int[][] a = new int[2][5];

// a[0] est de type "int[]" (tableau de int)

/* Tableaux 2D littéraux : Java comprend
   qu'on veut un tableau de taille 2 dans lequel
   chaque élément est un tableau de int de taille 3 */
int[][] b = {{1,2,3},{4,5,6}};

int[][] c = {{1},{2,3},{4,5,6}};
```


# Tableaux

- Comme en JavaScript, la comparaison de deux tableaux via l'opérateur
  `==` ne donne pas le résultat voulu, puisqu'on se retrouve à
  comparer des **références mémoire**

- Contrairement à JS, Java propose une fonction pour tester si deux
  tableaux contiennent les mêmes valeurs :

```java
Arrays.equals(tableau1, tableau2);
```

- **Attention** : ça ne teste que la première dimension. On peut
  cependant utiliser `Arrays.deepEquals(tableau1, tableau2)` pour
  tester récursivement l'égalité de tableaux multidimentionnels


# Tableaux

## Attention !

Les tableaux en Java ont une taille fixe, il **n'**existe **pas** de
fonctions `push` et `pop` comme en JS pour ajouter/retirer des
éléments


# Tableaux

Pour ajouter un élément à la fin d'un tableau de taille N, on doit donc :

- Créer un *nouveau tableau* de taille N + 1
- Copier les N premiers éléments du tableau original dedans
- Ajouter un élément de plus

# Tableaux

```java
public static void main(String[] args) {
    int[] t = {10, 20, 30}; // Tableau de 3 cases

    // Création d'un nouveau tableau de 4 cases
    int[] temp = new int[t.length + 1];

    // Copie des éléments existants
    for(int i=0; i<t.length; i++)
        temp[i] = t[i];

    // Ajout d'un élément à la fin
    temp[t.length] = 40;

    // t référence maintenant le nouveau tableau à 4 cases
    t = temp;
}
```

# Tableaux

Même principe si on souhaite retirer un élément du tableau :

- Créer un *nouveau tableau* de taille N - 1
- Copier les N - 1 éléments à conserver dans le nouveau tableau


# Tableaux

Pas très pratique...

&nbsp;

Solutions :

- Connaître d'avance la taille de nos tableaux
- Profiter de l'orienté objet pour définir des structures plus
  complexes... On y reviendra plus tard

# `String`

## Rappel

Un ordinateur ne peut que stocker que des nombres. Stocker du texte demande donc de :

1. Convertir chaque caractère en chiffre (ex.: via la table ASCII)
2. Stocker une suite de chiffres pour former un texte complet


# `String`

- "Chaîne de caractères" == Tableau de `char`

&nbsp;

## Java

```java
char[] cours = {'I', 'F', 'T', '1', '0', '2', '5'};
```

# `String`

- Java propose le type spécial `String` pour manipuler des chaînes de
  caractères

- On peut utiliser les doubles guillemets pour créer une `String`
  littérale :

```java
String nom = "Jimmy Whooper";
String chansonFavorite = "Ces Gens Qui Dansent";
```

# Opérateur + : concaténation

Comme en JavaScript, l'opérateur `+` sert autant à l'addition de
nombres qu'à la concaténation de `String`s

```java
String phrase = "Bonjour mon ami.";

System.out.println(phrase + " Comment vas-tu ?");
// => "Bonjour mon ami. Comment vas-tu ?"

System.out.println("10" + "20");
// => "1020", car on concatène des String
```

# Opérateur + : concaténation

- Du moment qu'au moins un des opérandes du `+` est de type `String`,
  l'autre est converti en `String` au besoin et l'opération effectuée
  est une concaténation (mise bout-à-bout)
- La valeur résultante est également de type `String`

```java
System.out.println(25 + "10"); // affiche "2510"

int b = 123;
System.out.println("a" + b); // affiche "a123"
```

# Longueur d'une `String`

- On utilise la méthode `.length()` qui retourne le nombre de
caractères d'une `String` sous la forme d'un int

- Notez qu'un espace compte aussi pour 1 caractère

```java
"Allo".length() // vaut 4

String phrase = "Bonjour mon ami.";

phrase.length() // vaut 16

(" 25"+10).length() // vaut 5
```


# Extraire un caractère à une position donnée

- Une `String` est un type qui représente une chaîne de caractère,
c.a.d. une séquence de caractères (`char`)

- Les caractères ont une position (ou index) numérotée à partir de 0

Exemple: `"Nom"` est une chaîne formée de 3 caractères (donc de longueur
3) :

    à la position 0 c'est 'N'
    à la position 1 c'est 'o'
    à la position 2 c'est 'm'

# Extraire un caractère à une position donnée

- Pour obtenir à partir d'une `String` s, le caractère (char) à une
position (ou index) donnée i il suffit d'appeler s.charAt(i)

```java
"Allo".charAt(0) // vaut 'A', de type char
// (et non pas "A" qui serait de type String)

String n = "123";
n.charAt(n.length()-1) // vaut '3' (et non pas "3" ni 3)
n.charAt(3) // donnera lieu à une erreur à l'exécution
(n + 0).charAt(3) // vaut '0' (et non pas "0", ni 0)
```

# Extraire un caractère à une position donnée

Attention : on ne peut pas ainsi modifier un caractère dans une
`String` :

```java
n.charAt(1) = 'Z'; // donnera une erreur à la compilation
```

# Extraire une sous-chaîne

- Pour extraire une partie d'une `String` s on fait appel à la méthode
`s.substring(debut, fin)`

- Le résultat de cette expression est une sous-chaîne de s :

    * Commençant au caractère à la position `debut`
    * S'arrêtant juste avant le caractère à la position `fin`
    * La longueur de la sous-chaîne résultante est donc `fin - debut`
    * Ce résultat est de type `String`
    * Il s'agit d'une nouvelle `String`

# Extraire une sous-chaîne

Exemples :

```java
"Bonjour".substring(1,6) // vaut "onjou" (pas "Bonjour" ni "onjour")

String salut = "Allo";
int pos = 0;

salut.substring(pos, pos) // vaut "" (et non pas "A")
salut.substring(pos, pos+1) // vaut "A" (et non pas 'A')
salut.substring(pos, pos+2) // vaut "Al"
salut.substring(0, salut.length()-1) // vaut "All" (pas "Allo")
salut.substring(salut.length()-1, salut.length()) // vaut "o"
```

# Extraire une sous-chaîne

- Pour obtenir une `String` où le caractère à la positions i a été
  remplacé par '`X`' on peut écrire par exemple:

```java
String salut = "Allo";

int i = 1;

String salut2 = salut.substring(0,i) +
     'X' + salut.substring(i+1,salut.length());
// salut2 aura alors la valeur "AXlo".
```

# Transformation majuscule / minuscule

- La méthode `.toUpperCase()` retourne une version où toutes les
  minuscules ont été transformées en majuscules

```java
"BonJour!".toUpperCase() // vaut "BONJOUR!"
```

- La méthode `.toLowerCase()` retourne une version où toutes les
  majuscules ont été transformées en minuscules

```java
"BonJour!".toLowerCase() vaut "bonjour!"
```


# Comparer si deux `String` ont la même valeur

- *Attention !* Une string n'est pas un type primitif
- C'est un type spécial
- Contrairement à en JavaScript, on **ne** peut **pas** comparer deux
  `String`s avec `==` :

## Java

```java
String a = "gazoline";
String b = "gaz";

System.out.println(a == (b + "oline")); // Affiche false
```

Puisqu'il ne s'agit pas d'un type primitif, on compare des *références
mémoire*, similairement aux tableaux


# Comparer si deux `String` ont la même valeur

- Cette approche est un piège commun lorsqu'on commence à programmer
  en Java, mais est cohérente avec ce qui se passe réellement en
  mémoire

&nbsp;

- Pour comparer deux strings, on doit utiliser la méthode
  `.equals()` :

```java
String a = "gazoline";
String b = "gazoline";

System.out.println(a.equals(b));
// => true

System.out.println(("gaz" + "oline").equals("gazoline"));
// => true
```

# Comparer si deux `String` ont la même valeur

- Notez : lors de la comparaison, la casse est importante :

&nbsp;

```java
System.out.println("abc".equals("ABC"));
// => false

System.out.println("aBc".equals("abc"));
// => false
```

# Comparer si deux `String` ont la même valeur

- Si les différences majuscules/minuscules ne nous importent pas, on peut comparer avec `.equalsIgnoreCase()`


```java
System.out.println("abc".equalsIgnoreCase("ABC"));
// => true

System.out.println("aBc".equalsIgnoreCase("abc"));
// => true
```

# Comparer deux `String` par leur ordre lexicographique

- Aka, savoir laquelle est avant l'autre dans le dicitonnaire

&nbsp;

- Deux chaînes de caractères peuvent être identiques ou différentes
- Si elles sont différentes, on peut aussi dire qu'une est
  "supérieure" ou "inférieure" à l'autre selon qu'on la placerait
  après ou avant dans un dictionnaire
- Cette relation d'ordre définie entre les `String` s'appelle l'*ordre
  lexicographique*
- Puisque les `String`s ne sont pas des types primitifs, on **ne peut
  pas** utiliser les opérateurs de comparaison `==`, ni `>` ou `<`
  pour les comparer

# Comparer deux `String` par leur ordre lexicographique

- On vient de voir la méthode .equals() qui évalue si deux `String` sont
  identiques
- La méthode `.compareTo()` permet en plus de savoir laquelle précède
  l'autre dans l'ordre lexicographique

# Comparer deux `String` par leur ordre lexicographique

- `str1.compareTo(str2)` retourne un `int` dont il suffit de
  considérer le signe pour savoir quelle `String` précède l'atre dans
  le dicitonnaire.

```java
"ABC".compareTo("ABC")
// vaut 0 car les deux String sont identiques.

"ABC".compareTo("ABZ")
// donne un entier négatif car "ABC" viendrait avant "ABZ"
// dans un dictionnaire ("ABC" est considéré plus petit que "ABZ")

"ABZ".compareTo("ABC")
// donne un entier positif car "ABZ" viendrait après "ABC"
// dans un dictionnaire ("ABZ" est considéré plus grand que "ABC")

"ABZ".compareTo("ABCDEFGH")
// donne un entier positif car"ABZ" est considéré plus
// grand que "ABCDEFGH" dans l'ordre lexicographique
```

# Comparer deux `String` par leur ordre lexicographique

- La comparaison "lexicographique" se fait via la valeur numérique du
  code Unicode qui représente les caractères
- Les chiffres sont avant les lettres majuscules qui sont avant les
  lettres minuscules

```bash
                 30 40 50 60 70 80 90 100 110 120
                ---------------------------------
               0:    (  2  <  F  P  Z  d   n   x
               1:    )  3  =  G  Q  [  e   o   y
               2:    *  4  >  H  R  \  f   p   z
               3: !  +  5  ?  I  S  ]  g   q   {
               4: "  ,  6  @  J  T  ^  h   r   |
               5: #  -  7  A  K  U  _  i   s   }
               6: $  .  8  B  L  V  `  j   t   ~
               7: %  /  9  C  M  W  a  k   u  DEL
               8: &  0  :  D  N  X  b  l   v
               9: '  1  ;  E  O  Y  c  m   w
```

# Comparer deux `String` par leur ordre lexicographique

- Donc "123".compareTo("ABC") donne un entier **négatif**
    - Car les chiffres viennent avant les lettres majuscules
- Donc "ABc".compareTo("ABZ") donnera un entier **positif**
    - Car les lettres minuscules viennent *après* les lettres
      majuscules dans la table Unicode
    - Donc 'c' `>` 'Z' (même si ça serait l'inverse dans le dictionnaire)

&nbsp;

- Si on veut ignorer les différences majuscule/minuscule, on peut
utiliser la méthode `.compareToIgnoreCase()`, ce qui revient au même
que de tout mettre en minuscules et ensuite appeler `.compareTo()`


# Une chaîne en contient-elle une autre, et si oui, où ?

Exemple: "Bonjour Monsieur!" contient "jour" à la position 3

On peut pour obtenir ce résultat, en utilisant `.indexOf()` :

```java
"Bonjour Monsieur!".indexOf("jour") // vaut 3
```

# Une chaîne en contient-elle une autre, et si oui, où ?

- Comme en JavaScript, si la chaîne ne contient pas la sous-chaîne
recherchée, indexOf retourne -1

```java
"Bonjour Monsieur!".indexOf("jours") // vaut -1
```

# Une chaîne en contient-elle une autre, et si oui, où ?

- On peut optionnellement, comme second paramètre explicite à
  `.indexOf()`, indiquer la position à partir de laquelle chercher

```java
"Bonjour Monsieur!".indexOf("on", 0) // vaut 1
// Remarquez qu'il s'agit de la position du premier "on" trouvé

"Bonjour Monsieur!".indexOf("on", 5) // vaut 9
// Le premier "on" trouvé à partir de la position 5

"Bonjour Monsieur!".indexOf("on", 10) // vaut -1
// "on" introuvable passé la position 10
```


# Une chaîne en contient-elle une autre, et si oui, où ?

- Note : on peut aussi utiliser un char à la place d'une `String` pour
ce qui est recherché (mais l'entité sur laquelle on fait la recherche
doit être une `String`, pas un char)

```java
"Bonjour".indexOf('j') // équivalent à "Bonjour".indexOf("j")
```


# Conversions entre `String` et types primitifs

- Rappel: Pour convertir entre des types primitifs de Java on peut
  souvent utiliser un cast

```java
(int) 3.25 // vaut 3 et ce résultat est de type int
```

- *Mais*... les `String` ne sont pas des types simples de Java, on ne
  peut pas utiliser de cast pour convertir une `String` en `int` ou un
  `double` en sa représentation `String`


```java
(int) "3.25" // INCORRECT
```


# Conversions entre `String` et types primitifs

- Pour convertir une valeur d'un type primitif (`char`, `int`, `long`,
  `double`, `float`, `boolean`, ...) en `String`, deux possibilités :

```java
10 + "" // vaut "10"

128.7 + "" // vaut "128.7" et est de type String

String.valueOf(10) // vaut "10"

boolean a = true;
a + "" // vaut "true"
String.valueOf(a) // vaut "true"
```

# Conversions entre `String` et types primitifs

- Pour convertir une `String` représentant un nombre en un type
  primitif de Java, on peut utiliser des fonctions prédéfinies :

```java
Integer.parseInt("1" + "2") // vaut 12 et est de type int
Double.parseDouble("-3e-1") // vaut -0.3 et est de type double

Integer.parseInt("bonjour") // lance une erreur à l'exécution

/* Notez, des méthodes similaires existent pour
   les autres types primitifs float, long, ... */
```

# Différences sur la taille Array vs `String`

Notez bien la différence pour trouver la taille :

- Array: `tab.length`
- String: `str.length()`


# Entrées & Sorties

## Affichage à l'écran

- System.out.println
- System.out.print (sans saut de ligne)

## Interaction avec l'utilisateur

- `args` : arguments de la ligne de commande
- `Scanner` : lecture de données interactive

# Entrées & Sorties

## Arguments passés via la ligne de commande

```java
public class Hello {
   public static void main(String[] args) {
      // Votre code ici
      System.out.println("Hello, " + args[0] + " !");
   }
}
```

- `args`: tableau de `String`s passé en paramètre lorsqu'on lance le
  programme via la ligne de commande


```
$ javac Hello.java
$ java Hello monargument
Hello, monargument !
```

# Entrées & Sorties

## Arguments passés via la ligne de commande

```java
public class Max {

    public static void main(String[] args) {
        int max = -1;

        for(int i=0; i<args.length; i++) {
            max = Math.max(max, Integer.parseInt(args[i]));
        }

        System.out.println("Maximum=" + max);
    }
}
/* Exécution :
$ javac Max.java
$ java Max 1 6 3 1 5 3
Maximum=6
*/
```


# Entrées & Sorties

## Lecture de données interactive

- `Scanner` : permet de lire interactivement des données sur la ligne
  de commande

```java
public static void main(String[] args) {
    java.util.Scanner scanner =
        new java.util.Scanner(System.in);

    int number = 0;

    while(number >= 0) {
        number = scanner.nextInt();
        System.out.println("Nombre entré : " + number);
    }
}
```


# Entrées & Sorties

## Lecture de données interactive

- On peut importer `java.util.Scanner` pour pouvoir écrire directement
  `Scanner` dans le code

```java
import java.util.Scanner;

public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    int number = 0;

    while(number >= 0) {
        number = scanner.nextInt();
        System.out.println("Nombre entré : " + number);
    }
}
```


# Entrées & Sorties

- `Scanner` permet de lire plusieurs types de données depuis la console :
    * `nextInt()` pour lire un `int`
    * `nextDouble()` pour lire un `double`
    * `nextByte()`, `nextFloat()`, ...
    * `nextLine()` une `String` allant jusqu'à la fin de la ligne de
      texte (donc jusqu'au prochain `\n`)


# Modèle mémoire

La mémoire d'un programme est conceptuellement séparée en deux parties :

- La *pile* (en anglais, *stack*)
- Le *tas* (en anglais, *heap*)

Les variables définies dans les fonctions appelées sont allouées sur la pile,
tandis que les données plus complexes sont allouées dans le tas

# Modèle mémoire

[columns]

[column=0.75]

```java

public static void main(String[] args) {
    int x = 10;
    // -> ici <-
    System.out.println(add(x, 20));

}

public static int add(int a, int b) {
    int somme = a + b;

    return somme;
}


```

[column=0.25]

\center{\textbf{Stack}}

![](img/gv/01.mem01.png)&nbsp;

[/columns]

# Modèle mémoire

[columns]

[column=0.75]

```java

public static void main(String[] args) {
    int x = 10;

    System.out.println(add(x, 20));

}

public static int add(int a, int b) {
    int somme = a + b;
    // -> ici <-
    return somme;
}


```

[column=0.25]

\center{\textbf{Stack}}

![](img/gv/01.mem02.png)&nbsp;

[/columns]

# Modèle mémoire

[columns]

[column=0.75]

```java

public static void main(String[] args) {
    int x = 10;

    System.out.println(add(x, 20));
    // -> ici <-
}

public static int add(int a, int b) {
    int somme = a + b;

    return somme;
}


```

[column=0.25]

\center{\textbf{Stack}}

![](img/gv/01.mem01.png)&nbsp;

[/columns]

# Modèle mémoire


[columns]

[column=0.75]

```java
public static void main(String[] args) {
    int[] t = {1,2,3};

    System.out.println(moyenne(t));
}

public static float moyenne(int[] tab) {

    float somme = 0;
    // -> ici <-

    for (int i = 0; i < tab.length; i++) {
        somme += tab[i];
    }

    return somme / tab.length;
}
```

[column=0.25]

\center{\textbf{Stack}}

![](img/gv/01.mem03.png)&nbsp;

[/columns]


# Modèle mémoire

[columns]

[column=0.6]

```java
public static void main(String[] args) {
    int[] t = {1,2,3};
    // -> ici <-
    System.out.println(moyenne(t));
}

...











```

[column=0.4]

\center{\textbf{Stack - Heap}}

![](img/gv/01.mem04.png)&nbsp;

[/columns]

# Modèle mémoire

[columns]

[column=0.6]

```java
public static void main(String[] args) {
    int[] t = {1,2,3};

    System.out.println(moyenne(t));
}

public static float moyenne(int[] tab) {

    float somme = 0;
    // -> ici <-

    for (int i = 0; i < tab.length; i++) {
        somme += tab[i];
    }

    return somme / tab.length;
}
```

[column=0.4]

\center{\textbf{Stack - Heap}}

![](img/gv/01.mem05.png)&nbsp;

[/columns]



# Modèle mémoire

[columns]

[column=0.6]

- Le diagramme précédent n'est pas tout à fait juste...
- Comme une `String` n'est pas un type primitif, une case qui contient
  un `String` est en fait une *référence* vers un autre bout de la
  mémoire qui contient les informations sur la `String` (le tableau de
  `char` sous-jacent, la longueur, ...)

[column=0.4]

\center{\textbf{Stack - Heap}}

![](img/gv/01.mem06.png)&nbsp;

[/columns]


# Modèle mémoire

[columns]

[column=0.6]

```java
public static void main(String[] args) {
    int[][] tab2D = new int[2][2];
    tab2D[0][0] = 10;
    tab2D[0][1] = 20;

    tab2D[1][0] = 30;
    tab2D[1][1] = 40;

    System.out.println(moyenne(tab2D[0]));
    // => 15.0

    System.out.println(moyenne(tab2D[1]));
    // => 35.0
}

public static float moyenne(int[] tab) {
   ...
}
```

[column=0.4]

\center{\textbf{Stack - Heap}}

![](img/gv/01.mem07.png)&nbsp;

[/columns]


# Modèle mémoire

En résumé :

- Les cases mémoire ne peuvent contenir que des *types primitifs*
  (`int`, `char`, `double`, ...) ou des *références* vers d'autres
  endroits en mémoire
- Quand on appelle une fonction, les valeurs passées en paramètres
  sont *copiées* dans d'autres cases mémoire
- Dans le cas de types complexes, c'est la *référence* qui est copiée

# Pourquoi passer de JavaScript à Java ?

Java peut sembler plus lourd à première vue :

- Besoin de compiler les programmes avant de les exécuter
- Typage statique des variables
- Tableaux de taille fixe
- `public static void main(String[] args)` ...


# Pourquoi passer de JavaScript à Java ?

1. Avantages au niveau de la performance :

    - Une fois qu'un programme est *compilé*, on accélère son temps
      d'exécution
    - Les *Arrays* de JS ont un coût au niveau de la performance et de
      la mémoire utilisée
        - Allouent plus de mémoire que nécessaire au cas où +
          redimentionnement automatique lorsque nécessaire

# Pourquoi passer de JavaScript à Java ?

2. Avantages au niveau de la gestion d'erreurs :

    - Java peut attraper certaines erreurs lors de la compilation
      plutôt que lors de l'exécution
    - Le système de types refuse certaines choses incohérentes plutôt
      que d'inventer une réponse moyennement cohérente

```java
print(Math.max("bonjour", 123)); // NaN en JS
print([5] == 5); // true en JS
print("" == false); // true en JS

System.out.println(Math.max("bonjour", 123));
    // => error: no suitable method found for max(String,int)
System.out.println(new int[]{5} == 5);
// => error: incomparable types: int[] and int
System.out.println("" == false);
// => error: incomparable types: String and boolean
```


# Pourquoi passer de JavaScript à Java ?

3. Quelques autres avantages

    - Cas d'école pour la programmation orientée objet
    - Multithreading
    - Utilisation sur Android
    - ...


# Pourquoi passer de JavaScript à Java ?

**Notez**: Apprendre Java != Abandonner JavaScript

- Pas les mêmes contextes d'utilisation
- Certaines implantations de JavaScript peuvent surpasser la
  performance de Java dans certains contextes
