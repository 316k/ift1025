---
title: Multithreading
author: Par Nicolas Hurtubise$\newline$\footnotesize{En partie basé sur les notes de Jian-Yun Nie et sur https://docs.oracle.com/javase/tutorial/essential/concurrency/}
header-includes:
    - \usepackage{textcomp}
date: IFT1025 - Programmation 2
---

# Au programme...

<!-- # Ref -->

<!-- http://tutorials.jenkov.com/java-concurrency/java-memory-model.html -->
<!-- https://docs.oracle.com/javase/tutorial/essential/concurrency/interfere.html -->

- Threads multiples
- Ordre d'exécution
- Interférence entre les threads
- Synchroniser les threads
- Threads vs JavaFX

# Problème

Prenons un problème simple :

&nbsp;

Étant donné une grande séquence de nombres aléatoires $\{x_i\}$, on
veut calculer la somme du sinus de tous ces nombres :

$$\Sigma_{i=1}^N sin(x_i) \text{ ~~~ } = \text{ ~~~ } sin(x_1) + sin(x_1) + ... + sin(x_N)$$

# Problème

Assez facile à coder :

```java
        double[] numbers = ...

        double total = 0;

        for(int i=0; i<numbers.length; i++) {
            total += Math.sin(numbers[i]);
        }
```

# Problème

On peut écrire un petit programme qui évalue combien de temps ce code
va prendre en moyenne :

```java
    public static double test(double[] numbers) {

        long before = System.nanoTime();

        double total = 0;

        for(int i=0; i<numbers.length; i++) {
            total += Math.sin(numbers[i]);
        }

        System.out.println("Total=" + total);

        return (System.nanoTime() - before) * 1e-9;
    }
```

*Voir : programme SommeSinus*

# Problème

Si on évalue cette fonction une dizaine de fois sur une même machine
et qu'on regarde le temps min/max et moyen, on peut arriver à un
résultat du style de :

```java
        Total=1013.8657457133398
        Plus rapide:  0.777610793s
        Plus long:  0.8544868240000001s
        Temps moyen: 0.8009486491999999s
```

\pause

Environ ~0.8 seconde... Est-ce qu'on peut aller plus vite ?

# Accélérer l'exécution du code

Si on tente d'exécuter le programme avec un tableau contenant la
moitié des nombres seulement, on obtient une performance $\approx 2
\times$ plus rapide...

```java
        Total=...
        Plus rapide:  0.395482054s
        Plus long:  0.421997722s
        Temps moyen: 0.40452870510000005s
```

Et si on exécutait le code sur plusieurs ordinateurs en même temps ?

# Accélérer l'exécution du code

![Exécution du programme sur des ordinateurs différents en même temps](img/happy-multithread.png){width=75%}

Temps total : $\approx 0.405$ secondes seulement!

# Accélérer l'exécution du code

- Les ordinateurs modernes ont généralement plusieurs processeurs
- `=>` On devrait pouvoir exécuter des tâches indépendantes en parallèle

[columns]

[column=0.5]

\center

**Processeur 1**

- Trouver la somme des sinus des nombres de la *première moitié* du
  tableau

[column=0.5]

\center

**Processeur 2**

- Trouver la somme des sinus des nombres de la *seconde moitié* du
  tableau

[/columns]

&nbsp;

&nbsp;

- Une fois que les deux tâches sont terminées, il ne reste qu'à
  additionner les deux résultats
- On devrait alors arriver à notre réponse deux fois plus vite !

# `Thread`

Java a été conçu pour ce genre de scénarios

&nbsp;

- En Java, la classe `Thread` représente un fil d'exécution
  d'instructions séquentielles
- Par défaut, un premier `Thread` est créé lorsqu'on lance le programme
- On peut en créer d'autres au besoin

# `Thread`

Un programme est minimalement composé d'un Thread

\center

![Grosse image pixelisée](img/threads1.png){width=70%}

# `Thread`

On peut créer plus de threads au besoin.

&nbsp;

Si possible (selon le nombre et la disponibilité des processeurs),
plusieurs threads peuvent s'exécuter en parallèle pour accélérer le
temps de calcul :

\center

![Deuxième grosse image pixelisée](img/threads2.png){width=60%}&nbsp;


# `Runnable`

- L'interface `Runnable` permet de définir un bout de code à
  exécuter :

```java
@FunctionalInterface
public interface Runnable {
    public void run();
}
```

- On peut créer un nouveau thread en lui passant un instance d'un
  `Runnable`, qui définit le code à exécuter :

```java
Runnable r = new ClasseQuiImplementeRunnable();
Thread t = new Thread(r);
t.start(); // Démarre le thread
```

# `Runnable`

Alternativement, on peut profiter du fait que `Runnable` est une
`@FunctionalInterface` et utiliser la syntaxe `() -> { ... }` :

```java
Thread t = new Thread(() -> {
    // Code à exécuter ici
});

t.start();
```

# Exemple

Reprenons le problème de la somme des sinus des $N$ nombres, en
utilisant deux threads plutôt qu'un seul :

```java
    public static double test(double[] numbers) {

        long before = System.nanoTime();

        // On démarre un autre thread pour la première moitié
        // du tableau
        Thread autreThread = new Thread(() -> {
            double total = 0;
            for(int i=0; i<numbers.length / 2; i++) {
                total += Math.sin(numbers[i]);
            }

            System.out.println("Sous-total 1 = " + total);
            grandTotal += total; // Ajoute au grand total
        });
        autreThread.start();
```

# Exemple

Reprenons le problème de la somme des sinus des $N$ nombres, en
utilisant deux threads plutôt qu'un seul :


```java

        // La deuxième moitié est calculée dans ce thread-ci
        double total2 = 0;
        for(int i=numbers.length / 2; i<numbers.length; i++) {
            total2 += Math.sin(numbers[i]);
        }

        System.out.println("Sous-total 2 = " + total2);
        grandTotal += total2; // Ajoute au grand total

        System.out.println("Grand total = " + grandTotal);

        return (System.nanoTime() - before) * 1e-9;
    }





```

# Exemple

Résultat si on exécute le code :

```java
        Sous-total 1 = 2321.3495562058024
        Sous-total 2 = -1307.483810492505
        Grand total = 1013.8657457132974
        Temps requis: 0.5461563140000001
```

On est passés de ~0.8s de calcul à ~0.55s, pas si mal!

# Question

Si on réexécute le code, quelques fois, on peut arriver à un résultat
différent, qui ressemble plutôt à :

```java
        Sous-total 2 = -1307.483810492505
        Grand total = -1307.483810492505
        Temps requis: 0.526445118
        Sous-total 1 = 2321.3495562058024
```

&nbsp;

Pourquoi ?

# Ordre d'exécution

L'exécution des threads est imprévisible...

[columns]

[column=0.55]

- Le petit diagramme semble montrer des instructions alignées, mais en
  réalité, l'ordre dépend de beaucoup de facteurs

&nbsp;

- Comme on va le voir, cela a le potentiel de causer beaucoup de maux
  de tête...

[column=0.4]

\center

![*Mensonge !*](img/threads2.png)

[/columns]

# Ordre d'exécution

Un programme simple utilisant deux threads :

[columns]

[column=0.5]

```java
Runnable hello = () -> {
    System.out.print("H");
    System.out.print("e");
    System.out.print("l");
    System.out.print("l");
    System.out.print("o\n");
};
```

[column=0.5]

```java
Runnable world = () -> {
    System.out.print("W");
    System.out.print("o");
    System.out.print("r");
    System.out.print("l");
    System.out.print("d\n");
};
```

[/columns]

```java
public static void main(String[] args) {
    // ... Définition des Runnable hello et world
    Thread t1 = new Thread(hello);
    Thread t2 = new Thread(world);

    t1.start();
    t2.start();
}
```

# Ordre d'exécution

Aura un résultat imprévisible qui change d'une exécution à l'autre :

```java
$ java TestThreads
HWeollrlo
d
$ java TestThreads
HWeollo
rld
$ java TestThreads
Hello
World
$ java TestThreads
HWorld
ello
$ java TestThreads
World
Hello
...
```

*Voir : programme TestThreads*

# Attendre la fin d'un thread

On peut avoir besoin d'exécuter certaines instructions
séquentiellement.

&nbsp;

La méthode :

```java
thread.join()
```

permet de mettre en pause le thread actuel jusqu'à ce que ce `thread`
ait terminé son exécution.

<!-- # Attendre la fin d'un thread -->

<!-- On peut au besoin spécifier un nombre de millisecondes maximal à -->
<!-- attendre en paramètre avec : -->

<!-- ```java -->
<!-- /* Attend que le thread termine -->
<!--    Si le thread n'a pas terminé après 1000ms, on -->
<!--    continue l'exécution du programme quand même */ -->
<!-- uneInstanceDeThread.join(1000); -->
<!-- ``` -->

# Attendre la fin d'un thread

Supposons qu'on souhaite afficher "HelloWorld" correctement après que
les deux threads pour `hello` et `world` précédents se soient
terminés :

```java
Thread t1 = new Thread(hello), t2 = new Thread(world);
t1.start();
t2.start();
try {
    t1.join(); // Thread principal en pause - attend la fin de t1
    t2.join(); // Thread principal en pause - attend la fin de t2

    System.out.println("HelloWorld");
} catch (InterruptedException ie) {
    System.out.println("Interruption");
}
/* Affiche :
HWeollo       (ou autre variation, selon l'exécution des threads)
rld
HelloWorld    (toujours correctement) */
```

<!-- # Mettre en pause un thread un certain nombre de temps -->

<!-- Pour mettre en pause un thread pendant un certain temps, on peut -->
<!-- utiliser la fonction : -->

<!-- ```java -->
<!-- Thread.sleep(4000); -->
<!-- ``` -->

<!-- Qui demande au Thread actuel de s'arrêter pour une certaine durée -->
<!-- (exprimée en millisecondes). -->

<!-- # Mettre en pause un thread un certain nombre de temps -->

<!-- **Attention :** le nombre de millisecondes demandé ne sera pas -->
<!-- forcément parfaitement respecté. -->

<!-- &nbsp; -->

<!-- C'est le système d'exploitation qui est en charge de réveiller le -->
<!-- thread éventuellement, mais le système d'exploitation a possiblement -->
<!-- beaucoup d'autres choses à gérer en même temps. -->

<!-- # Interruptions -->

<!-- Les threads qui dorment (soit par un `Thread.sleep(n)`, soit à cause -->
<!-- d'un `t.join()`) peuvent être réveillés par d'autres threads avec leur -->
<!-- fonction `uneInstanceDeThread.interrupt()` -->

<!-- &nbsp; -->

<!-- Lorsqu'un thread est réveillé, une `InterruptedException` est -->
<!-- lancée. À cause du *Catch or Specify Requirement* de Java, on se -->
<!-- retrouve donc généralement à utiliser un `try...catch` autour des -->
<!-- `.sleep()` et `.join()`. -->

# Interférence entre les threads

Qu'est-ce qui pourrait se passer si on a une variable partagée par
deux Threads ?

```java
class Counter {
    private int c = 0;

    public void increment() {
        int temp = c;
        temp += 1;
        c = temp;
    }
    public void decrement() {
        int temp = c;
        temp -= 1;
        c = temp;
    }
    public int value() {
        return c;
    }
}
```

# Interférence entre les threads

```java
public static void main(String[] args) {
    Compteur c = new Compteur();

    Runnable incrementer = () -> {
        for (int i = 0; i < 10; i++) {
            c.increment();
        }
    };

    Runnable decrementer = () -> {
        for (int i = 0; i < 10; i++) {
            c.decrement();
        }
    };
```

# Interférence entre les threads

```java
    Thread t1 = new Thread(incrementer);
    Thread t2 = new Thread(decrementer);

    t1.start();
    t2.start();

    try {
        t1.join();
        t2.join();

        System.out.println("Valeur finale : " + c.valeur());
    } catch (InterruptedException e) {
        System.out.println("Interrpution");
    }
}
```

# Interférence entre les threads

Si on teste ce code plusieurs fois, on obtient quelque chose comme :

```java
Valeur finale : 0
Valeur finale : 0
Valeur finale : 0
Valeur finale : 0
Valeur finale : 0
Valeur finale : 0
Valeur finale : 0
Valeur finale : 0
Valeur finale : 2
Valeur finale : 0
Valeur finale : 0
Valeur finale : 0
Valeur finale : 0
Valeur finale : 2
Valeur finale : -1
Valeur finale : 0
Valeur finale : 0
Valeur finale : 0
```

# Interférence entre les threads

Les threads s'exécutent dans un ordre aléatoire...

[columns]

[column=0.5]

\center

*Thread 1*

```java
    public void increment() {
        int temp = c; // A.
        temp += 1;    // B.
        c = temp;     // C.
    }
```

*Thread 2*

```java
    public void decrement() {
        int temp = c; // 1.
        temp -= 1;    // 2.
        c = temp;     // 3.
    }
```

[column=0.5]

\center

![](img/gv/11.compteur.0.png)&nbsp;

[/columns]

# Interférence entre les threads

Les threads s'exécutent dans un ordre aléatoire...

[columns]

[column=0.5]

\center

*Thread 1*

```java
    public void increment() {
        int temp = c; // A.
        temp += 1;    // B.
        c = temp;     // C.
    }
```

*Thread 2*

```java
    public void decrement() {
        int temp = c; // 1.
        temp -= 1;    // 2.
        c = temp;     // 3.
    }
```

[column=0.5]

\center

![](img/gv/11.compteur.1.png)&nbsp;

[/columns]

# Interférence entre les threads

Les threads s'exécutent dans un ordre aléatoire...

[columns]

[column=0.5]

\center

*Thread 1*

```java
    public void increment() {
        int temp = c; // A.
        temp += 1;    // B.
        c = temp;     // C.
    }
```

*Thread 2*

```java
    public void decrement() {
        int temp = c; // 1.
        temp -= 1;    // 2.
        c = temp;     // 3.
    }
```

[column=0.5]

\center

![](img/gv/11.compteur.2.png)&nbsp;

[/columns]

# Interférence entre les threads

Les threads s'exécutent dans un ordre aléatoire...

[columns]

[column=0.5]

\center

*Thread 1*

```java
    public void increment() {
        int temp = c; // A.
        temp += 1;    // B.
        c = temp;     // C.
    }
```

*Thread 2*

```java
    public void decrement() {
        int temp = c; // 1.
        temp -= 1;    // 2.
        c = temp;     // 3.
    }
```

[column=0.5]

\center

![](img/gv/11.compteur.3.png)&nbsp;

[/columns]

# Interférence entre les threads

Les threads s'exécutent dans un ordre aléatoire...

[columns]

[column=0.5]

\center

*Thread 1*

```java
    public void increment() {
        int temp = c; // A.
        temp += 1;    // B.
        c = temp;     // C.
    }
```

*Thread 2*

```java
    public void decrement() {
        int temp = c; // 1.
        temp -= 1;    // 2.
        c = temp;     // 3.
    }
```

[column=0.5]

\center

![](img/gv/11.compteur.4.png)&nbsp;

[/columns]

# Interférence entre les threads

Les threads s'exécutent dans un ordre aléatoire...

[columns]

[column=0.5]

\center

*Thread 1*

```java
    public void increment() {
        int temp = c; // A.
        temp += 1;    // B.
        c = temp;     // C.
    }
```

*Thread 2*

```java
    public void decrement() {
        int temp = c; // 1.
        temp -= 1;    // 2.
        c = temp;     // 3.
    }
```

[column=0.5]

\center

![](img/gv/11.compteur.5.png)&nbsp;

[/columns]

# Interférence entre les threads

Les threads s'exécutent dans un ordre aléatoire...

[columns]

[column=0.5]

\center

*Thread 1*

```java
    public void increment() {
        int temp = c; // A.
        temp += 1;    // B.
        c = temp;     // C.
    }
```

*Thread 2*

```java
    public void decrement() {
        int temp = c; // 1.
        temp -= 1;    // 2.
        c = temp;     // 3.
    }
```

[column=0.5]

\center

![](img/gv/11.compteur.6.png)&nbsp;

[/columns]

# Interférence entre les threads

Et si on utilisait plutôt ça ?

[columns]

[column=0.5]

\center

*Thread 1*

```java
    public void increment() {
        c++;
    }
```

[column=0.5]

\center

*Thread 2*

```java
    public void decrement() {
        c--;
    }
```

[/columns]

&nbsp;

&nbsp;

&nbsp;

Est-ce qu'on aurait le même problème ?

# Interférence entre les threads

```java
Valeur finale : -1
Valeur finale : 0
Valeur finale : 0
Valeur finale : 0
Valeur finale : 0
Valeur finale : 3
Valeur finale : -1
Valeur finale : 0
Valeur finale : 2
Valeur finale : 0
Valeur finale : -2
Valeur finale : 0
Valeur finale : 0
Valeur finale : 0
Valeur finale : 0
```

Réponse : ça ne règle pas notre bug...

# Question

Qu'est-ce qui risque de se produire si deux threads essaient d'ajouter
des éléments à une liste chaînée ?

```java
LinkedList<Integer> list = new LinkedList<>();

Runnable incrementer = () -> {
    for (int i = 0; i < 100; i++) {
        list.add(i);
    }
};
Runnable decrementer = () -> {
    for (int i = 0; i < 100; i++) {
        list.add(i);
    }
};

// Création + .start() sur 2 threads
```


# Solution : bloc `synchronized`

Pour garder une cohérence dans les données, on a besoin d'un mécanisme
pour indiquer que certaines opérations doivent **absolument se faire
exécuter par un seul thread à la fois**.

&nbsp;

&nbsp;

Java propose le bloc `synchronized(...) { }` pour spécifier qu'un bout
de code ne peut être exécuté que par un thread à la fois :

```java
    public void increment() {
        synchronized(...) {
            /* Un seul thread à la fois ici */
            c++;
        }
    }
```

# Solution : bloc `synchronized`

Chaque `Object` possède un `lock` qui peut être utilisé pour
verrouiller une section du code et faire en sorte qu'elle ne soit
exécutable que par le thread qui le possède.

```java
    private final Object lockCompteur = new Object();

    public void increment() {
        synchronized(lockCompteur) {
            /* Un seul thread à la fois ici, car il faut
               acquérir le lock de l'objet lockCompteur
               avant d'entrer dans cette zone */
            c++;
        }
    }
```

Si deux threads tentent d'exécuter cette section en même temps, un
seul des deux obtient le lock et l'autre doit attendre que le premier
termine.

# Solution : bloc `synchronized`

On pourrait réécrire notre classe `Compteur` en spécifiant que les
bout du code qui utilisent la variable `c` doivent être exécutés par
un seul thread à la fois.

```java
public class Compteur {
    private final Object lockCompteur = new Object();
    private int c = 0;

    public void increment() {
        synchronized(lockCompteur) {
            c++;
        }
    }
    public void decrement() {
        synchronized(lockCompteur) {
            c--;
        }
    }
    // (Même chose pour public void valeur())
```

# Solution : bloc `synchronized`

Si on exécute le code, on obtient :

```java
Thread t1 = new Thread(incrementer);
Thread t2 = new Thread(decrementer);

t1.start();
t2.start();

try {
    t1.join();
    t2.join();

    System.out.println(c.valeur());
} catch (InterruptedException e) {
    System.out.println("Interrpution");
}

// Affiche systématiquement : 0
```

# Solution : bloc `synchronized`

Cela n'est bien sûr pas gratuit... Si nos threads doivent s'attendre
les uns les autres avant de toucher aux données, on perd possiblement
l'accélération qu'on avait grâce au parallélisme :

```java
long beforeTime = System.nanoTime();

t1.start(); // incrémente 10000000 fois le compteur
t2.start(); // décrémente 10000000 fois le compteur

try {
    t1.join();
    t2.join();

    System.out.println(c.valeur());
    // Affiche le temps passé à exécuter les threads
    System.out.println((System.nanoTime() - beforeTime) * 1e-9);
} catch(...) { ... }
```

# Solution : bloc `synchronized`

Sans `synchronized` :

```java
    System.out.println(c.valeur());
    // => -9478 (certaines opérations sont perdues...)

    System.out.println((System.nanoTime() - beforeTime) * 1e-9);
    // => 0.017287111 (très rapide)
```

Avec `synchronized` :

```java
    System.out.println(c.valeur());
    // => 0 (tout se passe bien)

    System.out.println((System.nanoTime() - beforeTime) * 1e-9);
    // => 1.767603946 (plutôt lent...)
```

# Solution : bloc `synchronized`

Version *non multithreadée*, si les deux boucles sont exécutées dans
le `main()` directement :

```java
    System.out.println(c.valeur());
    // => 0

    System.out.println((System.nanoTime() - beforeTime) * 1e-9);
    // => 0.455638106 (plus vite que des threads synchronisés!)
```

&nbsp;

Non seulement les threads doivent s'attendre mutuellement avant de
pouvoir toucher au compteur, mais toute la gestion de l'exclusivité
d'exécution génère de l'*overhead* supplémentaire!


# Plusieurs Locks

Supposons qu'on a un objet à deux compteurs qu'on souhaite utiliser
dans plusieurs threads en même temps :

```java
public class CompteurDouble {
    private int c1 = 0;
    private int c2 = 0;

    public void increment1() {
        c1++;
    }

    public void increment2() {
        c2++;
    }

    public int total() {
        return c1 + c2;
    }
}
```

# Plusieurs Locks

On peut utiliser des locks différents pour permettre à `c1` d'être
modifié en même temps que `c2`, sans pour autant permettre à `c1`
d'être modifié en double.

```java
public class CompteurDouble {
    private final Object lockC1 = new Object();
    private final Object lockC2 = new Object();
    private int c1 = 0, c2 = 0;

    public void increment1() {
        synchronized (lockC1) {
            c1++;
        }
    }
    public void increment2() {
        synchronized (lockC2) {
            c2++;
        }
    }
```

# Plusieurs Locks

La méthode `total()`, qui utilise les deux, aura donc besoin
d'acquérir les deux locks pour s'exécuter correctement.

```java
    public int total() {
        synchronized (lockC1) {
            synchronized (lockC2) {
                return c1 + c2;
            }
        }
    }
}
```

&nbsp;

&nbsp;

Ce qui ouvre la porte à un nouveau problème...


# Deadlock

Lorsqu'on a possiblement besoin de plus d'un lock à la fois, on peut
se retrouver face à une situation problématique :

&nbsp;

[columns]

[column=0.33]

\center

**Thread 1**

&nbsp;

Possède A

Attend pour avoir B

[column=0.33]

\center

**Thread 2**

&nbsp;

Possède B

Attend pour avoir C

[column=0.33]

\center

**Thread 3**

&nbsp;

Possède C

Attend pour avoir A

[/columns]

&nbsp;

&nbsp;

&nbsp;

Les threads peuvent s'interbloquer...

# Deadlock

Problème classique : dîner des philosophes

[columns]

[column=0.6]

- 5 philosophes, qui peuvent soit :
    1. Penser
    2. Manger des spaghettis
- Les philosophes ne se parlent pas
- Pour manger des spaghettis, il faut posséder deux fourchettes

`=>` La stratégie naïve mène à une attente infinie dans plusieurs cas!

&nbsp;

*Voir : programme Philosophes*

[column=0.4]

\center

![](img/philo.png){width=100%}&nbsp;

[/columns]

&nbsp;

&nbsp;

\scriptsize

https://upload.wikimedia.org/wikipedia/commonqs/thumb/7/7b/An_illustration_of_the_dining_philosophers_problem.png/800px-An_illustration_of_the_dining_philosophers_problem.png

<!-- # Wait / Notify -->

<!-- # Famine -->

<!-- # Question -->

<!-- Si on reprend le problème de la somme des sinus, qu'est-ce qu'on -->
<!-- pourrait faire pour finir avec une seule valeur finale qui est -->
<!-- toujours correcte? -->

<!-- ```java -->
<!-- /* -->
<!-- Threads #1 -- Product : ... -->
<!-- Threads #2 -- Product : ... -->
<!-- Temps requis : 0.43654182100000005s */ -->
<!-- ``` -->

# Threads *vs* JavaFX

- JavaFX utilise plusieurs threads pour gérer l'affichage, les
  événements, etc. mais n'expose qu'un seul thread aux programmeurs :
  le *Thread d'application*
- Tous les `handlers` d'événements, d'animation timer, ... y sont
  exécutés
- Dès qu'on souhaite modifier l'interface graphique, on **doit**
  passer par ce thread[^1]

&nbsp;

`=>` Implication : si on se met à faire des longues opérations dans un
handler d'événement, on se retrouve à bloquer l'interface...

[^1]: Ceci évite à JavaFX d'avoir à utiliser des `synchronized`
    partout, ce qui ralentirait l'exécution

# Threads *vs* JavaFX

Supposez que vous voulez télécharger et afficher une image lorsqu'on
clique sur un bouton :

```java
btn.setOnAction((event) -> {
    Image img = /* (télécharger l'image et la retourner) */

    // Si la connexion est lente, cette ligne peut prendre
    // beaucoup de temps à finir de s'exécuter...

    viewer.setImage(img);
});
```

L'interface va geler et arrêter de répondre aux événements tant que
l'image n'est pas téléchargée au complet...

# `Platform.runLater`

Une solution très simple à ce problème est de 1 :

1. Lancer un Thread séparé qui exécute la tâche longue à exécuter
2. Utiliser `Platform.runLater(uneInstanceDeRunnable)` pour exécuter
   du code du sur thread d'application de JavaFX plus tard, à la fin
   de l'exécution du thread

# `Platform.runLater`

```java
btn.setOnAction((event) -> {
    Thread downloadAndShowImage = new Thread(() -> {
        Image img = /* (télécharger l'image et la retourner) */

        Platform.runLater(() -> {
            viewer.setImage(img);
        });
    });

    downloadAndShowImage.start();
});
```

# `Platform.runLater`

![Exemple de programme qui exécute un calcul sur le thread d'application vs dans un autre thread](img/javafx-threads.png){width=70%}

\center

*Voir : programme JavaFXBlockingUI*

<!-- # Loi d'Amdahl -->

<!-- Les opérations qui ne peuvent pas être parallélisées sont -->
<!-- déterminantes dans le facteur d'accélération qu'on obtient grâce au -->
<!-- parallélisme. -->

<!-- &nbsp; -->

<!-- Pour un nombre de processeurs $N$ et une proportion $S$ de code qui -->
<!-- doit être exécuté séquentiellement (non-parallèle) : -->

<!-- $$acceleration \leq \frac{1}{S + \frac{1 - S}{N}}$$ -->

<!-- # Loi d'Amdahl -->

<!-- Pour un code qui se parallélise à 75%, on a S = 25% -->

<!-- ```python -->
<!-- # Accélération avec 4 processeurs : -->
<!-- >>> 1/(0.25 + (1 - 0.25)/4) -->
<!--     2.2857142857142856 -->

<!-- # Avec 10 processeurs : -->
<!-- >>> 1/(0.25 + (1 - 0.25)/10) -->
<!--     3.0769230769230766 -->

<!-- # Avec 10000 processeurs : -->
<!-- >>> 1/(0.25 + (1 - 0.25)/10000) -->
<!--     3.9988003598920323 -->

<!-- # Sur un réseau de 100000000 ordinateurs : -->
<!-- >>> 1/(0.25 + (1 - 0.25)/100000000) -->
<!--     3.9999998800000034 -->
<!-- ``` -->

# Conclusion

- Utiliser plusieurs threads dans un même programme peut accélérer
  l'exécution du code... \pause

- Mais ça peut aussi causer beaucoup de maux de tête si on ne fait pas
  attention!
    - Exécution imprévisible
    - Mauvaise gestion de la synchronisation \newline `->`
      *Deadlocks*, perte de temps...
    - Encore de plus de problèmes qui ne sont pas mentionnés dans ce
      chapitre (ex.: *Famines*, *Livelocks*)
    - Sujet abordé en IFT2245 (Systèmes d'exploitation) \pause
- Dans certaines situations (en particulier avec les interfaces
  graphiques), on doit absolument utiliser des threads dans certains
  contextes

<!-- # Le parallélisme en pratique... -->

<!-- - Certains algorithmes se parallélisent très facilement et très bien -->
<!--     - Exemple : algorithmes de rendu graphique (cartes graphiques `=` -->
<!--       plein de co-processeurs optimisés pour ça) -->
<!--     - Intelligence artificielle (Apprentissage machine, grosses -->
<!--       multiplications de matrices) -->

<!-- &nbsp; -->

<!-- - D'autres se parallélisent plus difficilement et demandent de se -->
<!--   creuser la tête pour éviter les problèmes de synchronisation de -->
<!--   données -->

<!-- # Le parallélisme en pratique... -->

<!-- Beaucoup de considérations pratiques : -->

<!-- - Données synchronisées => overhead -->
<!-- - Difficile à débugger -->
<!--     - Pas déterministe ! -->
<!-- - Overhead pour démarrer les threads -->
<!-- - Délai si on doit attendre qu'un thread termine avant de commencer -->
<!--   une autre tâche -->
<!-- - Beaucoup de paramètres à considérer... -->
<!--     - Nombre de processeurs de l'ordinateur -->
<!--     - Cache interne des processeurs... -->
<!--     - Bottleneck possiblement sur l'I/O plutôt que sur le problème -->
<!--       \newline Lire un ficher, communication réseau `=>` plus long que -->
<!--       les calculs qu'on fait habituellement ! -->

<!-- # Question -->

<!-- Si on reprend la classe `CompteurDouble` de tout à l'heure en -->
<!-- utilisant deux threads, un qui appelle `increment1()` 100000000 fois, -->
<!-- un autre qui appelle `increment2()` 100000000 fois -->

<!-- Laquelle des deux versions est la plus rapide ? -->

<!-- [columns] -->

<!-- [column=0.4] -->

<!-- ```java -->
<!-- public void increment1() { -->
<!--     synchronized (lockC1) { -->
<!--         c1++; -->
<!--     } -->
<!-- } -->
<!-- public void increment2() { -->
<!--     synchronized (lockC2) { -->
<!--         c2++; -->
<!--     } -->
<!-- } -->
<!-- ``` -->

<!-- [column=0.6] -->

<!-- ```java -->
<!-- public synchronized void increment1() { -->
<!--     c1++; -->
<!-- } -->
<!-- public synchronized void increment2() { -->
<!--     c2++; -->
<!-- } -->
<!-- ``` -->

<!-- [/columns] -->


<!-- # Réponse -->

<!-- - Bien que la théorie nous laisse croire le contraire, la version à un -->
<!--   seul lock fonctionne sensiblement plus rapidement que celle à deux -->
<!--   locks sur ma machine... -->

<!-- - Beaucoup de détails techniques sont à considérer avant de pouvoir -->
<!--   affirmer qu'une version performe mieux qu'une autre, en particulier -->
<!--   avec des langages comme Java qui offrent une grosse abstraction des -->
<!--   mécanismes de synchronisation de threads -->
<!--     - Beaucoup de choses peuvent se produire sous le capot -->
<!--     - Optimisations dans le langage -->
<!--     - Implantation exacte de `synchronized` -->
<!--     - ... -->

<!-- # Réponse -->

<!-- Notez : si on ralentit l'exécution des méthodes `incrementX()` en -->
<!-- ajoutant du code lent à exécuter : -->

<!-- ```java -->
<!-- c1++; -->

<!-- // Code lent : -->
<!-- for (int i = 0; i < 1000; i++) { -->
<!--     Math.pow(c1, (double) i); -->
<!-- } -->
<!-- ``` -->

<!-- La version à deux locks distincts fonctionne sensiblement plus -->
<!-- rapidement que celle utilisant un lock sur tout l'objet (méthodes -->
<!-- `synchronized`) -->


<!-- # Le parallélisme en pratique... -->

<!-- Avant de commencer à paralléliser votre programme pour essayer de -->
<!-- l'accélérer, réfléchissez-y soigneusement... -->

<!-- - Ce n'est pas trivial à faire -->
<!-- - Vous pourriez vous retrouver dans un cas où votre programme -->
<!--   parallélisé est plus lent que votre programme original -->
<!-- - Si possible, considérez *changer l'algorithme* plutôt que d'essayer -->
<!--   de le paralléliser -->
<!--     - Exemple : utiliser un Quicksort au lieu d'un Tri par sélection -->
<!--       plutôt que de paralléliser votre tri par sélection -->
<!-- - Dans tous les cas, **Mesurez** les performances et vérifiez quelle -->
<!--   partie de l'algorithme est réellement un bottleneck -->
<!--     - Assurez-vous que votre parallélisation en vaut la peine -->


<!--

- Java qui ordonnance
- Regarder les threads dans NetBeans
- Thread.yield
- await/notify, stopper un thread, shit
    - AnnoyingRunnable (systemoutprint de la shit jusqu'à ce qu'on tape PAUSE/STOP)
    - Jouer de la musique en console
- Task & Executor
- Exemple du serveur qui attends des connexions et qui sert tout le monde
- extends Thread vs new Thread(runnable)

Stuff à Pascal Vincent

-->


<!-- # Interruptions -->

<!-- # Mettre un thread en pause -->

# Annexe : *Pitfall de `start()` vs `run()`*

La classe `Thread` implémente `Runnable` (après tout, on peut *runner*
un thread...)

&nbsp;

Lancer directement un appel à `thread.run()` est possible mais ne crée
pas de nouveau thread : ça va seulement exécuter le code donné, dans
le thread actuel.

```java
Thread t1 = new Thread(incrementer);
Thread t2 = new Thread(decrementer);

/* Exécute le code du runnable sans partir de
   nouveau thread... */
t1.run();

/* ... donc on attend que l'appel de fonction
   termine avant de passer à la suite du code */
t2.run();
```
