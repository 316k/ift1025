---
title: Plan de cours
author: Nicolas Hurtubise, François Major ~ DIRO
header-includes:
    - \usepackage{textcomp}
date: IFT1025 - Programmation 2
---

# Organisation du cours

- 3h de théorie + 2h de pratique par semaine

## Théorie

- François Major
- Nicolas Hurtubise

## Labos pratique

- Olivier Mailhot
- Ilan Elbaz
- Justin Michaud


# Objectifs du cours

- Description officielle

> Concepts avancés : classes, objets, héritage, interfaces,
> réutilisation, événements. Introduction aux structures de données et
> algorithmes : listes, arbres binaires, fichiers, recherche et
> tri. Notions d'analyse numérique : précision.


# Objectifs du cours

* Maîtriser les techniques de *Programmation Orientée Objet*
* Maîtriser le langage *Java*
* Maîtriser les bases des structures de données et algorithmes
* Arriver à créer des programmes pour des tâches simples à moyennement
  complexes

# Objectifs du cours

## Programmation 1

&nbsp;

Bases de la programmation et bonnes pratiques

&nbsp;

\pause

## Programmation 2

&nbsp;

Plus gros focus sur *structurer le code*

`=>` Arriver à gérer plus de complexité

&nbsp;

# Livres de références (non-obligatoires)

- Big Java : copie locale disponible au DIRO [http://www.iro.umontreal.ca/~pift1025/bigjava/](http://www.iro.umontreal.ca/~pift1025/bigjava/)

&nbsp;

- Think Java : disponible librement sur Internet : [http://greenteapress.com/thinkjava6/thinkjava.pdf](http://greenteapress.com/thinkjava6/thinkjava.pdf)

# Autres livres intéressants

- Head First `(.*)`
    - Série de livres plus rigolos, moins formels\footnote{Lire: moins
      endormants} que la majorité des livres, focusés sur capturer
      l'attention des lecteurs pour aider à retenir l'information
      importante
    - *Head First Java* (bases du langage)
    - *Head First Object-Oriented Analysis and Design* (design de
      programmes)

# Autres livres intéressants

- Y. Daniel Liang, Introduction to JAVA Programming (Comprehensive
  Version) 8e edition, Prentice Hall, 2011. 
    - Dense. Beaucoup d'exemples intéressants.
- J. Bloch, N. Gafter, Java Puzzlers , Addison-Wesley, 2005.
    - Livre avancé, "puzzles" basés sur des cas limites du langage
- J. Gosling, B. Joy, G.Steele, G. Bracha, The Java Language
  Specification, 3rd ed., Addison-Wesley, 2005.
    - Spécification formelle de Java
- J. Bloch, Effective Java, 2nd Ed. Addison-Wesley, 2008.
    - Livre avancé, focus sur les bonnes pratiques en Java


# Évaluation

- TP1 (15%)
- Intra (25%)
- TP2 (15%)
- Final (30%)
- Exercices notés (15%)

&nbsp;

Les exercices notés ($\approx$ 5 ou 6) sont **à faire
individuellement**

&nbsp;

Les travaux pratiques (TP1 & TP2) se feront **en équipes de deux**

<!-- # Exercices -->

<!-- | # | Exercice                                 | % | Notes          | -->
<!-- |---+------------------------------------------+---+----------------| -->
<!-- | 1 | De JavaScript à Java (tableaux, strings) | 2 | auto           | -->
<!-- | 2 | Matrice & Vecteur                        | 3 | auto           | -->
<!-- | 3 | Design Orienté Objet pour le TP1         | 1 | peers          | -->
<!-- | 4 | Polynômes (par listes chaînées)          | 3 | auto           | -->
<!-- | 5 | GUI pour le TP1                          | 5 | manuel         | -->
<!-- | 6 | Multithreading (dans un programme GUI)   | 1 | manuel, rapide | -->


# Évaluation

**Attention**, les notes comportent un seuil : il faut obtenir un
minimum de 50% sur la moyenne pondérée de l'intra et du final pour que
les notes des TPs et exercices soient comptées.

# Sujets abordés \small (sujet à changements pendant la session)

1. Intro, JavaScript vs Java          <!-- N -->
2. Classes et Objets                  <!-- F -->
3. Héritage, Hiérarchies et Surcharge <!-- F -->
4. Polymorphisme et Interfaces        <!-- F -->
5. Algorithmie (récursion, tris, ...) <!-- F -->
6. Structures de données              <!-- F -->
7. **Intra (2h) 26 février**
8. Generics, Exceptions               <!-- N -->
9. Fichiers                           <!-- N -->
10. Programmation événementielle & Interfaces graphiques <!-- N -->
11. Infographie 2D : Animation                           <!-- N -->
12. Multithreading\*                                     <!-- N -->
13. Précision numérique\*, Regex\*                       <!-- N (à voir) -->
14. **Final (3h) 22 avril**

\small

\* Sujets abordés selon le temps disponible


<!--

Janvier
 9-10. Intro
 16-17. Classes & Objets
       (Limite pour l'annulation: 01/23/2018)
 23-24. x Héritage, Hiérarchies et Surcharge
 30-31. 1h : Interfaces + exemples, 2h : Génériques, Exceptions, Packages

Février
 6-7. Mini-préface Temps d'exécution (Big-O), Structures de données (Slides Pascal Vincent + HashMap au besoin pour fill)
 13-14. Algorithmie (récursion, tris)
 20-21. Intra
 27-28. Regex, Fichiers
    -> sawk Shitty-AWK

Mars
 6-7. Relâche (5 au 9 mars)
 13-14. Programmation événementielle & Interfaces graphiques
      (Abandon: 03/16/2018)
 20-21. Infographie 2D (Animations, Hiérarchies, transformations)
 27-28. Multithreading

Avril
 3-4. Précision numérique
 10-11. (Even more précision numérique pour le kek)
 17-18. Exam final
-->

# Éthique

**Aucun plagiat ne sera toléré.**

## Règlement sur le Plagiat

(Extrait du règlement disciplinaire sur le plagiat ou la fraude de l'UdeM)


> Constitue un plagiat:
> 
> 1. Faire exécuter son travail par un autre
> 2. Utiliser, sans le mentionner, le travail d'autrui
> 3. Échanger des informations lors d'un examen
> 4. Falsifier de documents
>
> Le plagiat est passible de sanctions allant jusqu'à l'exclusion du programme.

Consulter http://integrite.umontreal.ca/
