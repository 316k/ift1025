---
title: Regex # & *Stream-oriented Programming*
author: >
    Par Nicolas Hurtubise
    \newline
    \newline
    \footnotesize
    Basé en partie sur https://openclassrooms.com/courses/concevez-votre-site-web-avec-php-et-mysql/les-expressions-regulieres-partie-1-2
    \newline ([CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/))
header-includes:
    - \usepackage{textcomp}
date: IFT1025 - Programmation 2
---

# Question

Est-ce qu'une personne saine d'esprit écrirait des choses comme:

```java
((https?|ftp):\/\/(w{3}\.)?)?[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.([a-z]+)
```

&nbsp;

Dans son code ?

&nbsp;

\pause

**Bien sûr que oui !**

# Regex : *Regular Expression*

- *RegEx* = *Ex*pressions *Rég*ulières

&nbsp;

- On les retrouve dans beaucoup de langages de programmation et outils
  de programmeurs en général
    - Éditeurs de textes, bases de données, ...
- On peut penser aux Regex comme à une sorte de **rechercher &
  remplacer** très poussé


# Regex : *Regular Expression*

- Les regex viennent à l'origine de la théorie des des langages
  réguliers, formalisés par Stephen Cole Kleene dans les années 1950
    - **Stephen Cole Kleene** : étudiant d'Alonzo Church
    - **Alonzo Church** : superviseur d'Alan Turing
    - **Alan Turing** : si le nom ne vous dit rien, googlez-le
- L'idée a cependant été étendue, au point où certains systèmes de
  regex peuvent reconnaître plus que seulement des langages réguliers
- On ne s'intéressera pas à la théorie des langages réguliers
  ici\footnote{Une partie du cours d'Introduction à l'Informatique
  Théorique s'y intéresse}, on va surtout se concentrer sur les
  applications pratiques des expressions régulières

# Regex : à quoi ça sert ?

Les regex ont des tonnes de cas d'utilisation, par exemple :

- Vérifier automatiquement si une adresse e-mail entrée a une forme
  valide (comme `jimmy@whooper.org`)
- Modifier une date que vous avez au format français (05/08/1985) pour
  la réécrire en AAAA-MM-JJ (1985-08-05)
- Remplacer automatiquement toutes les adresses http://... par des
  liens cliquables sur votre site web

# `string.matches(pattern)`

La façon la plus simple d'utiliser les expressions régulières en Java
est via la fonction `matches()` sur les `String` :

&nbsp;

```java
String str = "Bonjour";
String pattern = "Bonjour";

if(str.matches(pattern)) {
    System.out.println("Ça match !");
} else {
    System.out.println("Pas de match...");
}

// => Affiche : Ça match !
```

&nbsp;

`=>` la chaîne `"Bonjour"` *match* le *pattern* `"Bonjour"`

# `string.matches(...)`

Quelle différence avec `.equals()` ?

&nbsp;

La méthode `matches()` utilise en réalité *tout un mini-langage
spécialisé* pour décider si une chaîne en match une autre

&nbsp;

Pour la méthode `matches()`, cette chaîne de caractères :

```java
((https?|ftp):\/\/(w{3}\.)?)?[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.([a-z]+)
```

A un sens très précis.

&nbsp;

\footnotesize

(On va s'y rendre tranquillement, une chose à la fois)

# Recherches simples

- Le pattern le plus simple qu'on peut utiliser est un mot lui-même
  (sensible à la casse)
- `str.matches(...)` retourne `true` seulement si la chaîne est
  *entièrement* matchée par le pattern

[columns]

[column=0.6]

```java
String s1 = "Bonjour";
String s2 = "Bonsoir";

String regex = "Bonjour";
System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => false
```

[column=0.4]

**`Bonjour`**

- \colorbox{green}{Bonjour}
- \colorbox{red}{Bonsoir}
- \colorbox{red}{Bon sang}
- \colorbox{red}{Bonne nuit}
- \colorbox{red}{bonjour}
- \colorbox{red}{Bonjour à vous !}

[/columns]


# Le symbole *OU* (`|`)

On peut combiner des regex avec le symbole `|` : match l'un ou l'autre
des patterns fournis

[columns]

[column=0.6]

```java
String s1 = "Bonjour";
String s2 = "Bonsoir";
String s3 = "Bonne nuit";

String regex = "Bonjour|Bonsoir";

System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => false
```

[column=0.4]

**`Bonjour|Bonsoir`**

- \colorbox{green}{Bonjour}
- \colorbox{green}{Bonsoir}
- \colorbox{red}{Bonne nuit}
- \colorbox{red}{bonsoir}
- \colorbox{red}{BonjourBonsoir}
- \colorbox{red}{Bonjour|Bonsoir}
- \colorbox{red}{Banane}
- \colorbox{red}{Bonsoir2}

[/columns]

# Le symbole *OU* (`|`)

On peut utiliser les parenthèses pour appliquer un OU sur certaines
parties du pattern seulement :


[columns]

[column=0.6]

```java
String s1 = "Bonjour";
String s2 = "Bonsoir";
String s3 = "Bonbon";

String regex = "Bon(jour|soir|bon)";

System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => false
```

[column=0.4]

**`Bon(jour|soir|bon)`**

- \colorbox{green}{Bonjour}
- \colorbox{green}{Bonsoir}
- \colorbox{green}{Bonbon}
- \colorbox{red}{Bonne nuit}
- \colorbox{red}{bonsoir}
- \colorbox{red}{Bon jour}
- \colorbox{red}{BonjourBonsoir}

[/columns]


# Wildcard : `.`

Le `.` a une signification spéciale : il peut passer pour n'importe
quel caractère

[columns]

[column=0.6]

```java
String s1 = "Bonjour";
String s2 = "Bonsoir";
String s3 = "Bonjojr";

String regex = "Bon.o.r";
System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => true
```

[column=0.4]

**`Bon.o.r`**

- \colorbox{green}{Bonjour}
- \colorbox{green}{Bonsoir}
- \colorbox{green}{Bonsosr}
- \colorbox{green}{BonAoAr}
- \colorbox{green}{Bon!o\#r}
- \colorbox{red}{Bonne nuit}
- \colorbox{red}{bonsoir}
- \colorbox{red}{Banane}
- \colorbox{red}{Bonsoir2}

[/columns]

# Échapper

Quoi faire si on veut littéralement un point ou une barre dans notre
regex ?

&nbsp;

On peut les échapper avec un *backslash* : `\.` et `\|`

```java
String s1 = "perdu.com";
String s2 = "perdu.org";
String s3 = "umontreal.ca";

/* Pattern : .....\.com
   Notez : \ a besoin d'être échappé dans une String en Java... */
String regex = ".....\\.com";

System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => false
System.out.println(s3.matches(regex));
// => false
```

# Les classes de caractères

Le `.` permet de matcher différents caractères avec une même regex,
mais on peut vouloir matcher *certains* caractères seulement, par
exemple : o ou a, mais rien d'autre

- \colorbox{green}{gr\textbf{\underline{o}}s}
- \colorbox{red}{gr\textbf{\underline{a}}s}
- \colorbox{red}{gr\textbf{\underline{i}}s}
- \colorbox{red}{gr\textbf{\underline{\#}}s}

# Les classes de caractères

Les crochets `[]` permettent de définir les caractères autorisés

[columns]

[column=0.6]

```java
String s1 = "gros";
String s2 = "gras";
String s3 = "gris";

String regex = "gr[ao]s";
System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => false
```

[column=0.4]

**`gr[ao]s`**

- \colorbox{green}{gras}
- \colorbox{green}{gros}
- \colorbox{red}{gris}
- \colorbox{red}{grand}
- \colorbox{red}{grps}
- \colorbox{red}{gr\#s}
- \colorbox{red}{gr8s}

[/columns]

# Les classes de caractères

Si on veut matcher n'importe quel mot qui match `gr`+voyelle+`s`, on
peut utiliser :

```java
gr[aeiouy]s
```

- \colorbox{green}{gris}
- \colorbox{green}{gros}
- \colorbox{green}{gras}
- \colorbox{green}{grus}
- \colorbox{green}{grys}
- \colorbox{green}{gres}
- \colorbox{red}{grand}
- \colorbox{red}{grps}
- \colorbox{red}{gr\#s}
- \colorbox{red}{gr8s}

# Les classes de caractères

Et si on veut n'importe quelle lettre de l'alphabet ?

```java
gr[abcdefghijklmnopqrstuvwxyz]s
```

Ça commence à être un peu plus long...

# Les classes de caractères

Et pour autoriser des lettres et des chiffres ?

```java
gr[abcdefghijklmnopqrstuvwxyz0123456789]s
```

&nbsp;

Des lettres majuscules/minuscules et des chiffres ?

```java
gr[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789]s
```

Si on voulait que les trois derniers caractères soient des lettres et
des chiffres ?...

# Les intervalles de classe

On peut utiliser la syntaxe `[a-z]` pour définir un intervalle de
caractères acceptés

[columns]

[column=0.6]

```java
String s1 = "gros";
String s2 = "grxs";
String s3 = "gr#s";

String regex = "gr[a-z]s";
System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => false
```

[column=0.4]

**`gr[a-z]s`**

- \colorbox{green}{gris}
- \colorbox{green}{gros}
- \colorbox{green}{grxs}
- \colorbox{red}{grAs}
- \colorbox{red}{gr7s}
- \colorbox{red}{gr\#s}
- \colorbox{red}{gr,s}

[/columns]

# Les intervalles de classe

On peut combiner plusieurs intervalles, ou encore des intervalles et
des caractères :


[columns]

[column=0.6]

```java
String s1 = "gros";
String s2 = "grxs";
String s3 = "gr#s";

String regex = "gr[a-z0-9]s";
System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => false
```

[column=0.4]

**`gr[a-z0-9]s`**

- \colorbox{green}{gris}
- \colorbox{green}{gros}
- \colorbox{green}{grxs}
- \colorbox{green}{gr2s}
- \colorbox{green}{gr6s}
- \colorbox{red}{grAs}
- \colorbox{red}{gr\#s}
- \colorbox{red}{gr,s}

[/columns]


# Les intervalles de classe

On peut combiner plusieurs intervalles, ou encore des intervalles et
des caractères :


[columns]

[column=0.6]

```java
String s1 = "gr0s";
String s2 = "gr!s";
String s3 = "gr#s";

String regex = "gr[a-z0-9!]s";
System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => false
```

[column=0.4]

**`gr[a-z0-9!]s`**

- \colorbox{green}{gris}
- \colorbox{green}{gros}
- \colorbox{green}{grxs}
- \colorbox{green}{gr2s}
- \colorbox{green}{gr6s}
- \colorbox{green}{gr!s}
- \colorbox{red}{grAs}
- \colorbox{red}{gr\#s}

[/columns]

# Les intervalles de classe

Quoi faire si on veut autoriser le caractère `-` dans l'intervalle ?

&nbsp;

On peut le mettre au début ou la fin, par exemple : `[a-z-]` ou
`[-a-z]`

[columns]

[column=0.6]

```java
String s1 = "gr0s";
String s2 = "gr!s";
String s3 = "gr-s";

String regex = "gr[a-z0-9!-]s";
System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => true
```

[column=0.4]

**`gr[a-z0-9!-]s`**

- \colorbox{green}{gris}
- \colorbox{green}{gros}
- \colorbox{green}{grxs}
- \colorbox{green}{gr2s}
- \colorbox{green}{gr6s}
- \colorbox{green}{gr!s}
- \colorbox{green}{gr-s}
- \colorbox{red}{grAs}
- \colorbox{red}{gr\#s}

[/columns]


# Inverser l'intervalle

Et si on veut plutôt autoriser tous les caractères sauf quelques uns ?

&nbsp;

On peut utiliser `^` au début de la classe pour inverser le contenu
`[^...]`

[columns]

[column=0.6]

```java
String s1 = "gr0s";
String s2 = "gr!s";
String s3 = "gr-s";

String regex = "gr[^a-z0-9!-]s";
System.out.println(s1.matches(regex));
// => false
System.out.println(s2.matches(regex));
// => false
System.out.println(s3.matches(regex));
// => false
```

[column=0.4]

**`gr[^a-z0-9!-]s`**

- \colorbox{red}{gris}
- \colorbox{red}{gros}
- \colorbox{red}{grxs}
- \colorbox{red}{gr2s}
- \colorbox{red}{gr6s}
- \colorbox{red}{gr!s}
- \colorbox{red}{gr-s}
- \colorbox{green}{grAs}
- \colorbox{green}{gr\#s}

[/columns]


# Les intervalles de classe

## Question

Est-ce que l'intervalle `[A-z]` devrait être valide ?

&nbsp;

(de A majuscule à z minuscule)

# Les intervalles de classe

Les intervalles se basent sur les valeurs numériques des caractères.
`A-z` va matcher quoi que ce soit qui a une valeur `>= A` et `<= z`.

[columns]

[column=0.53]

```java
            Table ASCII
 30 40 50 60 70 80 90 100 110 120
 ---------------------------------
0:    (  2  <  F  P  Z  d   n   x
1:    )  3  =  G  Q  [  e   o   y
2:    *  4  >  H  R  \  f   p   z
3: !  +  5  ?  I  S  ]  g   q   {
4: "  ,  6  @  J  T  ^  h   r   |
5: #  -  7  A  K  U  _  i   s   }
6: $  .  8  B  L  V  `  j   t   ~
7: %  /  9  C  M  W  a  k   u  DEL
8: &  0  :  D  N  X  b  l   v
9: '  1  ;  E  O  Y  c  m   w
```

[column=0.47]

- A à Z
- a à z
- Quelques symboles entre Z et a

[/columns]


# Exercices

1. Écrire une regex qui match uniquement les noms de Jimmy Whooper et
   de son frère :

- Jimmy Whooper
- Xavier Whooper

&nbsp;

Utilisez http://scriptular.com/ pour tester vos réponses

# Exercices


2. Écrire une regex qui match les noms de toute la famille de Jimmy :

- Xavier Whooper
- Eleanor Whooper
- Sarah Whooper
- Jean-Claude Whooper
- Jean-Philippe Whooper
- Marc Whooper

# Exercices


3. Écrire une regex qui match toutes les variantes orthographiques du
   mot "ambigu" au féminin :

- ambiguë [^1]
- ambigüe [^2]
- ambigue [^3]
- ambigüë [^4]

[^1]: orthographe traditionnelle
[^2]: orthographe rectifiée en 1990
[^3]: orthographe incorrecte
[^4]: personne n'écrit ça, mais bon, pour l'exercice...

# Exercices


4. Écrire une regex qui match les nombres hexadécimaux à deux chiffres :

- 0xAA
- 0xB9
- 0x10
- 0xFF
- 0xad
- 0xb5


# Les quantificateurs

Les concepts vus à date sont assez puissants, mais on est encore
limités...

&nbsp;

Comment est-ce qu'on peut valider si une adresse email est valide ?

- jimmy@whooper.org
- bob@e3b.org
- nicolas.hurtubise@umontreal.ca

On ne connaît pas d'avance le nombre exact de caractères que les
adresses valides vont avoir

&nbsp;

On va avoir besoin des **quantificateurs**, qui permettent de dire
combien de fois peuvent se répéter un caractère ou une suite de
caractères.

# Les quantificateurs

Les quantificateurs de base sont les suivants :

- `?` : 0 ou 1 fois
- `+` : 1 ou plusieurs fois
- `*` : 0, 1 ou plusieurs fois

# Quantificateur `?`

- `?` : 0 ou 1 fois

[columns]

[column=0.6]

```java
String s1 = "banane";
String s2 = "bananes";
String s3 = "bananessss";

String regex = "bananes?";
System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => false
```

[column=0.4]

**`bananes?`**

- \colorbox{green}{banane}
- \colorbox{green}{bananes}
- \colorbox{red}{bananesss}
- \colorbox{red}{baienane}
- \colorbox{red}{bananebanane}

[/columns]

# Quantificateur `?`

On peut l'appliquer à des groupes parenthésés ou encore à des classes
de caractères et intervalles

[columns]

[column=0.6]

```java
String s1 = "0xCAFE";
String s2 = "CAFE";
String s3 = "0x0x0xCAFE";

String regex = "(0x)?CAFE";
System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => false
```

[column=0.4]

**`(0x)?CAFE`**

- \colorbox{green}{0xCAFE}
- \colorbox{green}{CAFE}
- \colorbox{red}{0x0x0xCAFE}
- \colorbox{red}{cafe}
- \colorbox{red}{0xCafé}
- \colorbox{red}{Café}

[/columns]


# Quantificateur `+`

- `+` : 1 ou plusieurs fois

[columns]

[column=0.6]

```java
String s1 = "lol";
String s2 = "loooooool";
String s3 = "loooooooooooooool";

String regex = "lo+l";
System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => true
```

[column=0.4]

**`lo+l`**

- \colorbox{green}{lol}
- \colorbox{green}{loooooool}
- \colorbox{green}{loooooooooooooool}
- \colorbox{red}{ll}
- \colorbox{red}{llloooollll}
- \colorbox{red}{LoL}

[/columns]


# Quantificateur `+`

On peut bien sûr appliquer `+` à des groupes parenthésés et des
classes de caractères :

[columns]

[column=0.6]

```java
String s1 = "ha!";
String s2 = "hahaha!";
String s3 = "aaahhah!haahhah!";

String regex = "([ha]+!)+";
System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => true
```

[column=0.4]

**`([ha]+!)+`**

- \colorbox{green}{ha!}
- \colorbox{green}{ha!ha!ha!ha!}
- \colorbox{green}{a!}
- \colorbox{green}{h!}
- \colorbox{green}{haahhahha!}
- \colorbox{green}{hhhhhh!}
- \colorbox{green}{ahaahah!ahaah!}
- \colorbox{red}{!}
- \colorbox{red}{ah!!}
- \colorbox{red}{ha!ha!ha}
- \colorbox{red}{!ah}

[/columns]

<!-- TODO : l'exemple est bizarre -->

# Quantificateur `*`

- `*` : 0, 1 ou plusieurs fois

[columns]

[column=0.6]

```java
String s1 = "B";
String s2 = "Ba";
String s3 = "Baaaaaaaaaaaaaaaaaaaaa";

String regex = "Ba*";
System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => true
```

[column=0.4]

**`Ba*`**

- \colorbox{green}{B}
- \colorbox{green}{Ba}
- \colorbox{green}{Baaaaaaa}
- \colorbox{green}{Baaaaaaaaaaaaaaa}
- ...

[/columns]


# Accolades `{}`

On peut vouloir un nombre de répétitions plus précis que 0, 1,
plusieurs. Les accolades `{ ... }` permettent de définir des
quantificateurs arbitraires :

- `{n}` : seulement un nombre = exactement ce nombre de répétitions
    - `w{5}` est équivalent à `wwwww`
    - `(na){8} Batman !` est équivalent à `nananananananana Batman !`
- `{m,n}` : au minimum `m` fois, au maximum `n` fois
    - `a{4,9}` match 4 à 9 `a` d'affilée
    - `wa{2,5}t` match `waat`, `waaat`, `waaaat` et `waaaaat`

# Accolades `{}`

- `{m,}` : au minimum `m` fois, pas de maximum
    - `a{3,}` est équivalent à `aaaa*`
- `{,n}` : au maximum `n` fois, mas de minimum
    - `wa{,3}t` match `wt`, `wat`, `waat`, `waaat`


# Exercices

1. Écrire une regex qui permet de matcher uniquement :

- `Bonjour`
- `Banjo`


# Exercices

2. Écrire une regex qui permet de matcher les couleurs représentées
   hexadécimal (#RRGGBB ou #RGB) :

- \#000
- \#123
- \#AFF
- \#f0f
- \#FF00FF
- \#44FF88

# Exercices

3. Écrire une regex qui match `twado` avec 1 ou plusieurs `a` et `o`,
   répété un nombre illimité de fois :

- twado
- twaaaaaadoooooo
- twadoooooooooooo
- twadotwado
- twaaaaaadotwadooooooo
- *(chaîne vide)*
- twadotwadotwadotwado

# Exercices

4. Écrire une regex qui match 1 ou plusieurs `twado`s séparés par des
   virgules

- \colorbox{green}{twado}
- \colorbox{green}{twado, twado}
- \colorbox{green}{twado, twado, twado, twado}
- \colorbox{red}{twado, twado, twado, twado, }
- \colorbox{red}{twado, }

# Exercices

5. Écrire une regex qui match les nombres entiers (sans zéros de trop
   à gauche) :

- \colorbox{green}{10}
- \colorbox{green}{2136636772}
- \colorbox{green}{-55}
- \colorbox{green}{+333333}
- \colorbox{red}{001324}
- \colorbox{red}{-027}
- \colorbox{red}{-}
- \colorbox{red}{+}

# Exercices

6. Qu'est-ce que la regex montrée au début essaie de matcher ?

&nbsp;

```java
((https?|ftp):\/\/(w{3}\.)?)?[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.([a-z]+)
```


# Les classes abrégées

|      | Signification                                                                              |
|------+--------------------------------------------------------------------------------------------|
| `\d` | Indique un chiffre. \newline Ça revient exactement à `[0-9]`                               |
| `\D` | Indique ce qui n'est PAS un chiffre. Ça revient à `[^0-9]`                                 |
| `\w` | Indique un caractère alphanumérique ou un underscore.  Cela correspond à `[a-zA-Z0-9_]`    |
| `\W` | Indique ce qui n'est PAS un mot. \newline Si vous avez suivi, ça revient à `[^a-zA-Z0-9_]` |
| `\s` | Indique un espace blanc (`\t \n \r`)                                                       |
| `\S` | Indique ce qui n'est PAS un espace blanc (`\t \n \r`)                                      |

Notez qu'on doit échapper les `\` en java, ce qui donne des regex du
genre de :

```java
String regex = "\\w+";
```

# Les classes abrégées

[columns]

[column=0.6]

```java
String s1 = "555-2222";
String s2 = "343-6111";
String s3 = "123-4567";

String regex = "\\d{3}-\\d{4}";
System.out.println(s1.matches(regex));
// => true
System.out.println(s2.matches(regex));
// => true
System.out.println(s3.matches(regex));
// => true
```

[column=0.4]

**`\d{3}-\d{4}`**

- \colorbox{green}{555-2222}
- \colorbox{green}{343-6111}
- \colorbox{green}{123-4567}
- \colorbox{red}{1234-567}
- \colorbox{red}{1234567}

[/columns]

# Exercice

Qu'est-ce que la regex suivante permet de matcher ?

```java
[+-]?(\d+(\.\d+)?|\.\d+)([eE][+-]?\d+)?
```


# `replaceFirst()` et `replaceAll()`

En plus de faire de la validation, on peut utiliser les regex pour
remplacer des bouts de strings par d'autres avec les fonctions
`str.replaceFirst(pattern)` et `str.replaceAll(pattern)`

```java
String phrase =
    "Ça faisait longtemps qu'on n'avait pas parlé de Jimmy Whooper";

System.out.println(phrase.replaceFirst("a", "e"));
// Çe faisait longtemps qu'on n'avait pas parlé de Jimmy Whooper


System.out.println(phrase.replaceAll("a", "e"));
// Çe feiseit longtemps qu'on n'eveit pes perlé de Jimmy Whooper
```

# `replaceFirst()` et `replaceAll()`

On peut se servir de ça pour nettoyer le texte entré par les
utilisateurs :

```java
String phrase =
  "Un  texte   \n\n\nécrit      n'importe\tcommment  !";

System.out.println(phrase.replaceAll("\\s+", " "));
// Un texte écrit n'importe commment !
```

# Matcher au début ou à la fin seulement

Dans le contexte où le match s'applique seulement sur une partie de la
`String`, on peut s'intéresser à matcher seulement des choses en
début/fin de texte

- `^` en début de pattern indique que le pattern doit être trouvé au
  tout début de la chaîne de caractères
    - `^jour` est trouvé dans `journée` mais pas dans `bonjour`
- `$` en fin de pattern indique que le pattern doit être trouvé à la
  fin de la chaîne
    - `jour$` est trouvé dans `bonjour` mais pas dans `journée`
- On peut combiner les deux : `^pattern$`
    - `^jour$` est trouvé dans `jour`, pas dans `bonjour` ni `journée`

# Matcher au début ou à la fin seulement


```java
String phrase = "bon, qui veut des bons bonbons ? C'est toujours bon";

System.out.println(phrase.replaceAll("bon", "***"));
// ***, qui veut des ***s ******s ? C'est toujours ***

System.out.println(phrase.replaceAll("^bon", "***"));
// ***, qui veut des bons bonbons ? C'est toujours bon

System.out.println(phrase.replaceAll("bon$", "***"));
// bon, qui veut des bons bonbons ? C'est toujours ***
```


# Groupes de capture

Dans le contexte du remplacement, les parenthèses ont une autre
utilité : permettre de "capturer" des sous-parties pour les réutiliser
dans le remplacement.

&nbsp;

C'est ce qu'on appelle des groupes de capture : on peut les référencer
dans le pattern de remplacement avec `$1`, `$2`, `$3`, ...

```java
String phrase = "Je suis Jimmy Whooper";
System.out.println(phrase.replaceAll("Je suis (.+)", "Il est $1"));
// Il est Jimmy Whooper

String phrase2 = "Je serai disponible les 10/01/2011 et 21/12/2012";
System.out.println(
    phrase2.replaceAll(
        "(\\d\\d)/(\\d\\d)/(\\d{4})",
        "$3-$2-$1"));
// Je serai disponible les 2011-01-10 et 2012-12-21
```

# Groupes de capture

Dans tous les cas, `$0` correspond à l'entièreté du match :

```java
String phrase = "Je suis Jimmy Whooper";
System.out.println(
    phrase.replaceAll(
        "Je suis (.+)",
        "Il est $1 (déduit depuis : '$0')"));
// Il est Jimmy Whooper (déduit depuis : 'Je suis Jimmy Whooper')

String phrase2 = "Ceci est un test";
System.out.println(phrase2.replaceAll("^.*$", "$0 $0"));
// Ceci est un test Ceci est un test
```

# Exercice : regex complète

1. Écrivez une expression régulière qui permet de valider un numéro de
   téléphone montréalais (avec indicatif 514 ou 438) :

- 514 222-4449
- (514\) 222-4449
- 438-223-4928
- 1-438-172-2849
- (514\) 343-6111 #1918

&nbsp;

2. Utilisez-la pour extraire l'indicatif régional (seulement lorsqu'il
   s'agit d'un numéro valide)

# Les Regex dans la vraie vie

- Souvent utile, on peut exprimer de façon concise des choses qui
  demanderaient d'écrire des programmes complets
- Il y a différentes "saveurs" de regex, mais la syntaxe de base reste
  sensiblement similaire
    - Ex.: la syntaxe en Java et en JavaScript est essentiellement la
      même
- Difficile à débugger... C'est généralement du code *write-only*
    - Quand ça ne marche pas comme ça devrait, c'est souvent plus
      facile de recommencer de zéro que d'essayer de trouver quel
      caractère pose problème dans le tas
- C'est facile de faire des approximations et de se retrouver avec une
  regex qui ne couvre pas tous les cas réels possibles
    - Tests unitaires!

# Regex pour valider une adresse email

Écrire une regex qui valide des adresses email

- jimmy@whooper.org
- bob@e3b.org
- nicolas.hurtubise@umontreal.ca
- the.jay.dawg_73@whooper.org

# Regex pour valider une adresse email

En réalité, il y a beaucoup, beaucoup plus de cas à considérer...

https://www.ietf.org/rfc/rfc0822.txt?number=822

- admin@localhost est une adresse email valide
- admin@127.0.0.1 est une adresse email valide
- 2.2.4@127.0.0.1 est valide
- 22.@127.0.0.1 n'est pas valide

&nbsp;

La regex qui valide correctement toutes les adresses email est
légèrement plus compliquée...

# Regex pour valider une adresse email

```java
(?:(?:\r\n)?[ \t])*(?:(?:(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:
\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?
:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()
<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:
\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t
])*))*@(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?
:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\]
(?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \
000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([
^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*))*|(?:[^()<>@,;:\\".\[\] \000
-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\
"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*)*\<(?:(?:\r\n)?
[ \t])*(?:@(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|
\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \
t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?
```

\footnotesize

... (ça continue pendant des pages et des pages) ...

Rassurez-vous : ce n'est pas écrit à la main, c'est plutôt généré à
partir de la grammaire officielle. [Voir la source](http://www.ex-parrot.com/pdw/Mail-RFC822-Address.html)

<!-- # Awk -->

<!-- https://web.archive.org/web/20160310181446/http://www.eecs.harvard.edu/cs152/lectures/CS152-Lecture_14-Kernighan.pdf -->
