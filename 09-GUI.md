---
title: Les Interfaces Graphiques (GUI)\footnote{Sous-titre - comment finalement faire des programmes qui pourraient servir au commun des mortels}
author: Par Nicolas Hurtubise <!-- $\newline$\footnotesize{...} -->
header-includes:
    - \usepackage{textcomp}
date: IFT1025 - Programmation 2
---

# Au programme...

- JavaFX
- Scène sous forme d'arbre
- Éléments graphiques & Événements
- Layouts
- MVC : Modèle-Vue-Contrôleur

# Interface graphique (GUI)

\centerline{\textit{GUI : Graphical User Interface}}

&nbsp;

## Pourquoi?

- *Présenter* des données organisées arbitrairement
- *Interactions* plus intuitives, guidées par les utilisateurs

&nbsp;

# Interface graphique (GUI)

Les programmes en ligne de commande vont typiquement s'exécuter de façon linéaire.

&nbsp;

&nbsp;

Une interface graphique est intrinsèquement **événementielle**

&nbsp;

- L'utilisateur peut cliquer n'importe où n'importe quand
    - On ne connaît pas d'avance la séquence d'événements qui va se
      produire à l'exécution
- Le programme doit *réagir* aux actions de l'utilisateur

<!-- # *Swing* -->

<!-- La libraire d'interfaces graphiques *Swing* est traditionnelle en Java -->
<!-- (et dans le cours de Programmation 2) -->

<!-- - Librairie standard pour les GUI en Java depuis ~1997 (fonctionne -->
<!--   partout où Java est supporté, Windows, Mac, GNU/Linux, ...) -->
<!-- - Vieux, lourd, inconsistant... Pas le meilleur design de librairie -->
<!-- - De plus en plus délaissé par les entreprises pour des solutions web -->
<!--   (`JavaScript` et `HTML/CSS`, `Electron` pour des logiciels -->
<!--   *desktop*) -->
<!-- - Pas officiellement *déprécié*, encore par défaut dans Java pour la -->
<!--   compatibilité, mais l'utilisation pour des nouvelles applications -->
<!--   est découragée -->
<!-- - Globalement désagréable à utiliser -->

# Exemple d'interface graphique en Java avec *Swing*

![Programme graphique réalisé avec Swing](img/swing.png)

# JavaFX

Bien que la librairie *Swing* soit encore enseignée dans certains
cours de Java, on va plutôt se concentrer sur JavaFX

- *Swing* : librairie standard pour les GUIs en Java depuis ~1997
    - Vieux, lourd, inconsistant... Pas le meilleur design de librairie
    - Par encore officiellement *déprécié*, mais l'utilisation pour
      des nouvelles applications est découragée

- *JavaFX* se veut le successeur de *Swing* pour les programmes
  graphiques
    - Première distribution en 2008
    - API plus consistant que celui de Swing, design plus "*moderne*",
      similaire à celui d'Android

    <!-- - S'intègre dans des applications *Swing* au besoin -->
<!-- - Plus de fonctionnalités de haut niveau que Swing --> <!-- -
Animations, transitions, ... --> <!-- - Open Source, utilisable dans
n'importe quel projet --> <!-- - Commercial ou non --> <!-- -
Possibilité de décrire les interfaces en FXML plutôt qu'en Java et -->
<!-- de styliser son interface avec du CSS --> <!-- - Donne du code
plus simple et plus facile à maintenir --> <!-- - Ports Android/iOS
--> <!-- - Sent bon la framboise -->

# Programme GUI simple

```java
public class SimpleGUI extends Application {
    public static void main(String[] args) {
        SimpleGUI.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        VBox root = new VBox();
        Scene scene = new Scene(root, 320, 250);

        Text texte = new Text("Hello, World !");
        texte.setFont(Font.font("serif", 25));
        root.getChildren().add(texte);

        primaryStage.setTitle("Titre de la fenêtre");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
```

# Programme GUI simple

![Résultat affiché](img/javafx-simple.png){width=40%}

# Structure générale

Plus généralement, on aurait :

```java
public class SimpleGUI extends Application {
    public static void main(String[] args) {
        SimpleGUI.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // >> Définition de l'interface graphique ici <<
    }
}
```

- `Application` est une classe abstraite qui sert à définir un point
  de départ dans un programme graphique avec JavaFX
- La méthode `start` est redéfinie et sert à construire l'interface
- Pour lancer l'application graphique, on doit appeler
  `MonApplication.launch(...)` depuis le `main()`

# La méthode `start`

Regardons de plus près la méthode `start` de l'exemple :

```java
    @Override
    public void start(Stage primaryStage) {
        VBox root = new VBox();
        Scene scene = new Scene(root, 320, 250);

        Text texte = new Text("Hello, World !");
        texte.setFont(Font.font("serif", 25));
        root.getChildren().add(texte);

        primaryStage.setTitle("Titre de la fenêtre");
        primaryStage.setScene(scene);
        primaryStage.show(); // Important !
    }
```

[columns]

[column=0.5]

\center

`Stage` ?

`VBox` ?

[column=0.5]

\center

`Scene` ?

`Text` ?


[/columns]

# `Stage`

```java
public void start(Stage primaryStage) {

    // ...

    primaryStage.setTitle("Titre de la fenêtre");
    primaryStage.setScene(scene);
    primaryStage.show(); // Important !
}
```

- Le "`Stage`" représente la fenêtre de l'application
- Définit la barre avec le titre et les boutons

    [minimiser, maximiser, fermer]

- Il s'agit d'un conteneur qui doit contenir une *scène*

# `Scene`

```java
public void start(Stage primaryStage) {

    VBox root = new VBox();
    Scene scene = new Scene(root, 320, 250);

    Text texte = new Text("Hello, World !");
    texte.setFont(Font.font("serif", 25));
    root.getChildren().add(texte);

    // ...
```

- Une scène représente le contenu graphique à afficher dans la fenêtre
- Il s'agit d'un *arbre* d'objets qui composent la scène \newline &nbsp;


# `Scene`


```java
public void start(Stage primaryStage) {

    VBox root = new VBox();
    Scene scene = new Scene(root, 320, 250);

    Text texte = new Text("Hello, World !");
    texte.setFont(Font.font("serif", 25));
    root.getChildren().add(texte);

    // ...
```

- Dans l'exemple, l'objet `texte` est un noeud dans l'arbre
- `VBox` (Vertical Box) est un groupe de noeuds. Dans l'exemple, c'est
  le noeud parent de l'objet `texte` ainsi que la racine de l'arbre

# Visuellement...

\center

![](img/javafx-layers/1.png)&nbsp;


# Visuellement...

\center

![](img/javafx-layers/2.png)&nbsp;

# Visuellement...

\center

![](img/javafx-layers/3.png)&nbsp;

# Visuellement...

\center

![](img/javafx-layers/4.png)&nbsp;

# Scène sous forme d'arbre

Le dernier exemple était un exemple simple : on avait seulement du
texte dans une fenêtre.

&nbsp;

En pratique, on va avoir besoin de fenêtres plus complexes que ça :


\center

![Programme graphique un peu plus intéressant](img/javafx-fancy.png){width=50%}

# Scène sous forme d'arbre

\center

![](img/javafx-layers/fancy1.png)&nbsp;

# Scène sous forme d'arbre

\center

![](img/javafx-layers/fancy2.png)&nbsp;

# Scène sous forme d'arbre

\center

![](img/javafx-layers/fancy3.png)&nbsp;

# Scène sous forme d'arbre

\center

![](img/javafx-layers/fancy4.png)&nbsp;

# Scène sous forme d'arbre

Le code pour définir cette interface :

```java
public void start(Stage primaryStage) throws Exception {
    VBox root = new VBox();
    Scene scene = new Scene(root, 400, 150);

    Text titre = new Text("Ceci n'est pas un titre");
    root.getChildren().add(titre);
    root.getChildren().add(new Separator());

    HBox buttonGroup = new HBox();

    Button gauche = new Button("Gauche");
    Button centre = new Button("Centre");
    Button droite = new Button("Droite");

    buttonGroup.getChildren().add(gauche);
    buttonGroup.getChildren().add(centre);
    buttonGroup.getChildren().add(droite);
    buttonGroup.setAlignment(Pos.CENTER);
```

# Scène sous forme d'arbre

```java
    root.getChildren().add(buttonGroup);

    root.getChildren().add(new Separator());

    CheckBox checkBox = new CheckBox("Voulez-vous cocher ... ?");

    root.getChildren().add(checkBox);

    root.setAlignment(Pos.CENTER);
    root.setSpacing(10);

    primaryStage.setTitle("Fenêtre fancy");
    primaryStage.setScene(scene);
    primaryStage.show();
}
```


# Composantes

On a donc deux "catégories" de `Node` :

[columns]

[column=0.5]

&nbsp;

**Éléments graphiques**

- `Text`
- `Button`
- `CheckBox`
- `ImageView`
- `Slider`
- `Label`, `Separator`
- `ListView`, `PieChart`
- ...
- `=>` Feuilles de l'arbre
- Éléments à afficher

[column=0.5]

&nbsp;

**Conteneurs d'éléments**

- `HBox`, `VBox`
- `BorderPane`
- `StackPane`
- `GridPane`
- `AnchorPane`
- `FlowPane`
- `TilePane`
- ...
- `=>` Parents dans l'arbre
- Layout des éléments

[/columns]

# Éléments graphiques

On dispose de différentes composantes graphiques pour *présenter* des
informations et *interagir* avec l'utilisateur

- `Text` : texte à afficher
- `Button` : bouton (cliquable)
- `CheckBox` : case à cocher
- `ImageView` : pour afficher une image
- `Separator` : séparateur visuel (ligne verticale/horizontale)
- `Slider` : pour indiquer une valeur sur une échelle
- `ProgressBar` : affiche une proportion sur une échelle visuelle
- `ColorPicker` : permet de choisir une couleur
- ...


# Éléments graphiques

Les composantes graphiques ont toutes des **événements** auxquels
elles peuvent répondre, ex.:

- Cliquer sur un `Button`
- Cocher une `CheckBox`
- Passer la souris par-dessus un `ImageView`
- ...

&nbsp;

La `Scene` elle-même répond à des événements, ce qui peut être utile
lorsqu'on souhaite réagir à certaines actions qui ne concernent pas un
noeud de la scène en particulier, ex.:

- Raccourcis clavier tapé par l'utilisateur
- Déplacement du personnage dans un jeu

# `Text`

Un `Text` permet d'afficher du texte :

```java
    // On peut spécifier la valeur du texte à la création...
    Text titre = new Text("Ceci n'est pas un titre");

    // ... Ou plus tard pendant l'exécution du programme
    titre.setText("Ceci est un titre !");

    /* On peut également ajuster certains paramètres, comme la
       police d'écriture, l'alignement, la couleurs et autres */
    titre.setFont(Font.font("Purisa", 32));
    titre.setTextAlignment(TextAlignment.CENTER);
    titre.setFill(Color.RED);
```

\center

![](img/fx-text.png){width=40%}&nbsp;

# `Text`

Pour que le texte soit affiché, on doit l'inclure dans l'arbre de la
`Scene` d'une façon ou d'une autre, par exemple en l'ajoutant aux
enfants de la racine :

```java
    VBox root = new VBox();
    Scene scene = new Scene(root, 400, 150);

    Text titre = new Text("Ceci n'est pas un titre");
    root.getChildren().add(titre);
```

# `Text`

*Notez : une composante ne peut pas apparaître plus d'une fois dans
la scène*

&nbsp;

On ne pourrait \underline{pas} afficher le texte en double dans la
scène en faisant :

```java
    VBox root = new VBox();
    Scene scene = new Scene(root, 400, 150);

    Text titre = new Text("Ceci n'est pas un titre");
    root.getChildren().add(titre);
    // Interdit ! Va lancer une exception à l'exécution
    root.getChildren().add(titre);
```

On doit absolument créer deux `new Text("...")` différents

# `Button`

Un `Button` permet d'afficher un bouton cliquable avec un label
textuel :

```java
    Button monBouton = new Button("Cliquez ici !");
```

\center

![](img/fx-btn.png){width=60%}&nbsp;

# `Button`

```java
    // Comme pour Text, on peut ajuster des paramètres...
    monBouton.setFont(Font.font(18));

    // Mais plus important : on peut définir un événement au clic
    monBouton.setOnAction((event) -> {
        System.out.println("Clic sur le bouton !");
    });
```

\center

![](img/fx-btn-event.png){width=80%}&nbsp;

# `Button`

Ici, l'interface fonctionnelle utilisée définit une instance de
`EventHandler<ActionEvent>` :

```java
    // Équivalent... Mais moins joli
    monBouton.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("Clic sur le bouton !");
        }
    });
```

Le paramètre `ActionEvent event` contient des informations relatives à
l'événement de clic.

# `CheckBox`

Une `CheckBox` est un élément qui peut être coché ou non

```java
    CheckBox checkBox = new CheckBox("Cochez cette case");

    checkBox.setOnAction((event) -> {
        // La méthode .isSelected() indique si la case est cochée
        if (checkBox.isSelected()) {
            System.out.println("Case cochée");
        } else {
            System.out.println("Case pas cochée");
        }
    });
```

\center

![](img/fx-checkbox.png){width=70%}&nbsp;


# `ImageView`

`ImageView` permet d'afficher une image dans la scène \newline &nbsp; \newline &nbsp;

[columns]

[column=0.6]

```java
VBox root = new VBox();

Scene scene = new Scene(root, 200, 150);

Image img = new Image("/egg.png");
ImageView imageView = new ImageView(img);

// Définis la largeur/hauteur du ImageView
imageView.setFitWidth(200);
imageView.setFitHeight(150);


root.getChildren().add(imageView);
```

[column=0.4]

![](img/fx-img.png)&nbsp;

[/columns]


# `ImageView`

Notez qu'on peut utiliser `setPreserveRatio(true)` pour empêcher
l'image de se déformer si jamais les proportions de l'`ImageView` ne
sont pas les mêmes que celles de l'image

[columns]

[column=0.6]

```java
VBox root = new VBox();

Scene scene = new Scene(root, 200, 150);

Image img = new Image("/egg.png");
ImageView imageView = new ImageView(img);

// Définis la largeur/hauteur du ImageView
imageView.setFitWidth(200);
imageView.setFitHeight(150);
imageView.setPreserveRatio(true);

root.getChildren().add(imageView);
```

[column=0.4]

![](img/fx-img-ratio.png)&nbsp;

[/columns]

# `ImageView`

On peut modifier l'image affichée par l'`ImageView` avec la méthode
`imageView.setImage(...)` :

```java
    Image img2 = new Image("/pac-man.png");
    imageView.setImage(img2);
```

Notez : le `"/"` dans le chemin d'image a une signification spéciale
ici et représente le dossier où on retrouve les classes (fichiers
.java) et packages du programme.

&nbsp;

Pour que le programme s'exécute correctement, on devrait avoir des
fichiers organisés dans le style de :

```python
MonProjetJavaFX/
    src/
        MonProjetJavaFX.java
        pac-man.png
```

# `Separator`

Un `Separator` permet de séparer des éléments visuellement

```java
    VBox root = new VBox();
    Scene scene = new Scene(root, 400, 150);

    Text texte1 = new Text("Lorem ipsum dolor sit amet");
    root.getChildren().add(texte1);

    Separator sep = new Separator();
    root.getChildren().add(sep);

    Text texte2 = new Text("consectetuer adipiscing elit");
    root.getChildren().add(texte2);
```

\center

![](img/fx-sep.png){width=50%}&nbsp;

# *etc*

[columns]

[column=0.7]

Ça vous donne un petit aperçu du fonctionnement général des
composantes graphiques

&nbsp;

&nbsp;

`=>` Il y a *beaucoup plus* de composantes que ça... N'hésitez pas à
fouiller dans la documentation de JavaFX pour découvrir comment les
composantes plus avancées fonctionnent !

[column=0.3]

\center

![Une composante de type `ColorPicker`](img/fx-color.png)&nbsp;

Un `ColorPicker`

[/columns]

# Événements

Notez également qu'il existe plusieurs autres types d'événements :

- Clic de souris général sur n'importe quelles composantes (pas
  seulement les `Button`/`CheckBox`)

```java
    scene.setOnMouseClicked((event) -> {
        System.out.println("Clic en : (" +
            event.getX() + ", " + event.getY() +
            ")");
    });

/* Résultat de l'exécution en cliquant un peu partout :

Clic en : (59.0, 50.0)
Clic en : (59.0, 50.0)
Clic en : (130.0, 95.0)
Clic en : (130.0, 128.0)
Clic en : (170.0, 139.0)
*/
```

# Événements

Notez également qu'il existe plusieurs autres types d'événements :

- Touches du clavier enfoncées

```java
    // Traitement des caractères tapés
    scene.setOnKeyPressed((event) -> {
        System.out.println("Touche tapée : " + event.getText());
        if (event.getCode() == KeyCode.Q && event.isControlDown()) {
            System.out.println("Fin du programme");
            Platform.exit();
        }
    });
/* Résultat de l'exécution en tapant n'importe quoi :

Touche tapée : n
Touche tapée : m
Touche tapée : y
Touche tapée : ^Q (touche Ctrl)
Touche tapée : q
Fin du programme
*/
```

# Événements

La façon la plus efficace de savoir à quels événements les objets
répondent est regarder dans la documentation (incluse dans les IDE)

\center

![](img/javafx-ide-events.png)&nbsp;

# *Pitfall*

**Attention** : plusieurs noms de classes de JavaFX comme `Button`,
`Image`, `Label`, `Canvas`, ... existent également dans le package
`java.awt`\footnote{awt est la couche d'affichage plus bas niveau
derrière \textit{Swing}}

```java
import java.awt.Button;
```

vs.

```java
import javafx.scene.control.Button;
```

Prenez garde à ça si votre IDE importe des classes
automatiquement... Les différentes classes `Button` ne sont pas
compatibles !

# Layouts

Comment organiser les éléments dans la fenêtre ?

- Positionnement absolu
    - On donne un $(x, y)$ à l'élément
    - Pas pratique... La fenêtre doit absolument avoir une taille fixe
    - `=>` Plus petit écran ? Programme en *Full Screen* ?
- Positionnement automatique
    - Les éléments sont positionnés automatiquement selon des règles
      prédéfinies propres au layout utilisé


# Layouts

Quelques exemples d'éléments conteneurs :

- `HBox`, `VBox` : *Horizontal Box*, *Vertical Box*
- `Pane` : un conteneur générique sans positionnement prédéfini
- `BorderPane` : positionnement d'éléments en Haut/Bas/Gauche/Droite/Centre
- `StackPane` : affiche ses composantes les unes par-dessus les autres
- `GridPane` : permet d'avoir une grille régulière d'éléments
- `AnchorPane` : permet d'"attacher" des composantes à des régions
- `FlowPane`, `TilePane`
- ...

# `HBox`, `VBox`

- Positionne les composantes les unes à la suite des autres
  (horizontalement ou verticalement, respectivement)
    - Par défaut : dans l'ordre où elles ont été ajoutées
    - On peut insérer des composantes au début/à la fin/au milieu au
      besoin en précisant l'index au moment d'ajouter une composante

# `HBox`, `VBox`

[columns]

[column=0.78]

```java
public void start(Stage primaryStage) {
    VBox vertical = new VBox();
    HBox horizontal = new HBox();

    horizontal.getChildren().add(new Text("A"));
    horizontal.getChildren().add(0, new Text("B"));
    horizontal.getChildren().add(new Text("C"));
    vertical.getChildren().add(horizontal);

    vertical.getChildren().add(new Text("D"));
    vertical.getChildren().add(0, new Text("E"));

    Scene scene = new Scene(vertical, 100, 100);

    primaryStage.setScene(scene);
    primaryStage.show();
}
```

[column=0.22]

\center

![](img/fx-vhbox.png)&nbsp;

*Résultat*

[/columns]

# `Pane`

- Un `Pane` est un conteneur générique
- Par défaut, tous les éléments ajoutés sont ajoutés par rapport au
  point d'origine $(0, 0)$ du `Pane` (en haut à gauche)
    - Tous les éléments se retrouvent les uns par-dessus les autres

[columns]

[column=0.7]

```java
Pane root = new Pane();

Scene scene = new Scene(root, 200, 200);

Button b1 = new Button("Ceci est le bouton 1");
Button b2 = new Button("btn 2");

root.getChildren().add(b1);
root.getChildren().add(b2);

primaryStage.setScene(scene);
primaryStage.show();
```

[column=0.3]

\center

![](img/fx-pane1.png)&nbsp;

[/columns]

# `Pane`

On peut cependant se servir de ce conteneur pour positionner des
éléments à des positions $(x, y)$ arbitraires dans l'image, par
exemple pour faire suivre la souris par une composante :

[columns]

[column=0.7]

```java
Pane root = new Pane();
Scene scene = new Scene(root, 400, 400);

Image img = new Image("/queen.png");
ImageView imageView = new ImageView(img);

root.getChildren().add(imageView);

root.setOnMouseMoved((event) -> {
    imageView.setX(event.getX());
    imageView.setY(event.getY());
});
```

[column=0.3]

\center

![](img/fx-queen1.png)&nbsp;

[/columns]

# `Pane`

On peut cependant se servir de ce conteneur pour positionner des
éléments à des positions $(x, y)$ arbitraires dans l'image, par
exemple pour faire suivre la souris par une composante :

[columns]

[column=0.7]

```java
Pane root = new Pane();
Scene scene = new Scene(root, 400, 400);

Image img = new Image("/queen.png");
ImageView imageView = new ImageView(img);

root.getChildren().add(imageView);

root.setOnMouseMoved((event) -> {
    imageView.setX(event.getX());
    imageView.setY(event.getY());
});
```

[column=0.3]

\center

![](img/fx-queen2.png)&nbsp;

[/columns]

# `Pane`

On peut cependant se servir de ce conteneur pour positionner des
éléments à des positions $(x, y)$ arbitraires dans l'image, par
exemple pour faire suivre la souris par une composante :

[columns]

[column=0.7]

```java
Pane root = new Pane();
Scene scene = new Scene(root, 400, 400);

Image img = new Image("/queen.png");
ImageView imageView = new ImageView(img);

root.getChildren().add(imageView);

root.setOnMouseMoved((event) -> {
    imageView.setX(event.getX());
    imageView.setY(event.getY());
});
```

[column=0.3]

\center

![](img/fx-queen3.png)&nbsp;

[/columns]

# `Pane`

- Les méthodes `setX()` et `setY()` d'une composante permettent de
  définir la position *relativement à leur conteneur, en partant du
  coin en haut à gauche*
- Si on veut que l'image soit centrée sur le curseur, on doit décaler
la position selon la largeur/hauteur de l'image :

[columns]

[column=0.7]

```java
root.setOnMouseMoved((event) -> {
    double w =
        imageView.getBoundsInLocal().getWidth();
    double w =
        imageView.getBoundsInLocal().getHeight();
    double x = event.getX() - w / 2;
    double y = event.getY() - h / 2;

    imageView.setX(x);
    imageView.setY(y);
});
```

[column=0.3]

\center

![](img/fx-queen4.png)&nbsp;

[/columns]

# `Pane`

- Les méthodes `setX()` et `setY()` d'une composante permettent de
  définir la position *relativement à leur conteneur, en partant du
  coin en haut à gauche*
- Si on veut que l'image soit centrée sur le curseur, on doit décaler
la position selon la largeur/hauteur de l'image :

[columns]

[column=0.7]

```java
root.setOnMouseMoved((event) -> {
    double w =
        imageView.getBoundsInLocal().getWidth();
    double w =
        imageView.getBoundsInLocal().getHeight();
    double x = event.getX() - w / 2;
    double y = event.getY() - h / 2;

    imageView.setX(x);
    imageView.setY(y);
});
```

[column=0.3]

\center

![](img/fx-queen5.png)&nbsp;

[/columns]

# `Pane`

- Les méthodes `setX()` et `setY()` d'une composante permettent de
  définir la position *relativement à leur conteneur, en partant du
  coin en haut à gauche*
- Si on veut que l'image soit centrée sur le curseur, on doit décaler
la position selon la largeur/hauteur de l'image :

[columns]

[column=0.7]

```java
root.setOnMouseMoved((event) -> {
    double w =
        imageView.getBoundsInLocal().getWidth();
    double w =
        imageView.getBoundsInLocal().getHeight();
    double x = event.getX() - w / 2;
    double y = event.getY() - h / 2;

    imageView.setX(x);
    imageView.setY(y);
});
```

[column=0.3]

\center

![](img/fx-queen6.png)&nbsp;

[/columns]

# `BorderPane`

- `BorderPane` permet de positionner (jusqu'à) 5 éléments sur les points cardinaux + centre :
    - Haut, Bas, Gauche, Droite et Centre

![https://docs.oracle.com/javafx/2/layout/builtin_layouts.htm](img/fx-borderpane.png){width=65%}

# `BorderPane`

On peut se servir du `BorderPane` pour imbriquer d'autres layouts et
ainsi faire un menu de boutons :

\center

![](img/fx-queen7.png){width=50%}&nbsp;

# `BorderPane`

On peut se servir du `BorderPane` pour imbriquer d'autres layouts et
ainsi faire un menu de boutons :

\center

![](img/fx-queen8.png){width=50%}&nbsp;

# `BorderPane`

On peut se servir du `BorderPane` pour imbriquer d'autres layouts et
ainsi faire un menu de boutons :

\center

![](img/fx-queen9.png){width=50%}&nbsp;


# `BorderPane`

Code pour faire ça :

```java
@Override
public void start(Stage primaryStage) {

    BorderPane root = new BorderPane();
    Scene scene = new Scene(root, 400, 400);

    Pane pane = new Pane();
    /* L'élément du centre est le Pane où l'image
       se déplace */
    root.setCenter(pane);

    Image happy = new Image("/happy.png");
    Image sad = new Image("/sad.png");

    ImageView imageView = new ImageView(happy);

    pane.getChildren().add(imageView);
```

# `BorderPane`

```java
    // Au déplacement de souris dans le Pane...
    pane.setOnMouseMoved((event) -> {
        double w = imageView.getBoundsInLocal().getHeight();
        double h = imageView.getBoundsInLocal().getHeight();
        imageView.setX(event.getX() - w / 2);
        imageView.setY(event.getY() - h / 2);
    });

    // HBox pour le menu en haut (+ style pour rendre ça joli)
    HBox menu = new HBox();
    menu.setBackground(new Background(
        new BackgroundFill(
            Color.DARKSALMON,
            CornerRadii.EMPTY,
            Insets.EMPTY)
        ));
    menu.setAlignment(Pos.CENTER);
    menu.setSpacing(10);
    menu.setPadding(new Insets(10));
```

# `BorderPane`

```java
    // Définition des boutons
    Button buttonHappy = new Button("Happy :D");
    Button buttonSad = new Button("Sad :(");

    buttonHappy.setOnAction((event) -> {
        imageView.setImage(happy);
    });
    buttonSad.setOnAction((event) -> {
        imageView.setImage(sad);
    });
    menu.getChildren().add(buttonHappy);
    menu.getChildren().add(buttonSad);

    // L'élément "Top" du BorderPane est le menu
    root.setTop(menu);

    primaryStage.setScene(scene);
    primaryStage.show();
}
```

# `BorderPane`

Notez que les positions des éléments vont s'adapter si on resize la
fenêtre : \newline &nbsp;

&nbsp;

\center

![](img/fx-border-counter1.png){width=27%}&nbsp;

# `BorderPane`

Notez que les positions des éléments vont s'adapter si on resize la
fenêtre :

&nbsp;

\center

![](img/fx-border-counter2.png){width=58%}&nbsp;

# *etc*

- Comme pour les composantes graphiques, JavaFX définit plusieurs
  autres layouts, mais on ne s'attardera pas à chacun individuellement

&nbsp;

Fouillez dans la documentation et les tutoriels pour plus
d'informations

&nbsp;

Un bon point de départ pour en apprendre plus sur les layouts :

[https://docs.oracle.com/javafx/2/layout/builtin_layouts.htm](https://docs.oracle.com/javafx/2/layout/builtin_layouts.htm)


# Modifier la scène

- Lorsqu'on crée l'application, on doit absolument fournir une `Scene`
  au `Stage` par défaut

&nbsp;

- Rien ne nous empêche de créer d'autres scènes pour les afficher plus
  tard
- On peut modifier la scène active en utilisant `stage.setScene(...)`
  n'importe quand dans l'exécution du programme

# Modifier la scène : ajouter un `Splash Screen`

- On pourrait par exemple ajouter un *Splash Screen* à notre
application, soit une image qui s'affiche au début de l'exécution du
programme pour le logo et la version
- Dès qu'on clique quelque part, on veut changer la scène pour
  afficher sur notre "vrai" programme


```java
// ... définition de la scène principale plus haut ...
// Définition de la scène pour le Splash Screen
HBox splashScreenRoot = new HBox();
Scene splashScreenScene = new Scene(splashScreenRoot, 400, 400);

Image splashImg = new Image("/splash.png");
ImageView splashViewer = new ImageView(splashImg);
splashScreenRoot.getChildren().add(splashViewer);

splashScreenScene.setOnMouseClicked((value) -> {
    primaryStage.setScene(scene);
});

primaryStage.setScene(splashScreenScene);
```

# Modifier la scène : ajouter un `Splash Screen`

On afficherait alors avec fierté : \newline &nbsp;

\center

![](img/fx-splash.png)&nbsp;


# Modifier la scène : ajouter un `Splash Screen`

Dès qu'on clique quelque part, la scène changerait pour devenir notre
programme principal :

\center

![](img/fx-queen8.png)&nbsp;

# Organiser le code

- Vous commencez peut-être à réaliser que plus les programmes sont
  gros, plus le code de la fonction `start()` va devenir un gros
  ramassis de définitions de composantes graphiques et d'événements...

# Organiser le code

Prennons en exemple le programme montré il y a quelques slides...

On avait un compteur avec des boutons pour modifier la valeur :

&nbsp;

\center

![](img/fx-border-counter3.png){width=60%}&nbsp;

# Organiser le code

Prennons en exemple le programme montré il y a quelques slides...

On avait un compteur avec des boutons pour modifier la valeur :

&nbsp;

\center

![](img/fx-border-counter4.png){width=60%}&nbsp;

# Organiser le code

Code nécessaire :

```java
public class JavaFXCompteur extends Application {
    private int valeur;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 400, 400);

        valeur = 0;
        Button incrementer = new Button("+ 1");
        Button doubler = new Button("* 2");

        Text texteValeur = new Text("" + valeur);
```

# Organiser le code


```java

        Button diviser = new Button("/ 2");
        Button decrementer = new Button("- 1");

        incrementer.setOnAction((action) -> {
            valeur++;
            texteValeur.setText("" + valeur);
        });

        decrementer.setOnAction((action) -> {
            valeur--;
            texteValeur.setText("" + valeur);
        });

        doubler.setOnAction((action) -> {
            valeur *= 2;
            texteValeur.setText("" + valeur);
        });
```

# Organiser le code

```java
        diviser.setOnAction((action) -> {
            valeur /= 2;
            texteValeur.setText("" + valeur);
        });
        // Alignement (pour rendre ça joli)
        root.setAlignment(incrementer, Pos.CENTER);
        root.setAlignment(decrementer, Pos.CENTER);
        root.setAlignment(doubler, Pos.CENTER);
        root.setAlignment(diviser, Pos.CENTER);

        root.setTop(incrementer);
        root.setLeft(doubler);
        root.setCenter(texteValeur);
        root.setRight(diviser);
        root.setBottom(decrementer);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
```

# Organiser le code

- Les événements dans notre application peuvent se produire n'importe
  quand et dans n'importe quel ordre
- La logique finit par être éparpillée un peu partout

&nbsp;

- Pour un simple compteur, ça peut aller...
    - Mais pour un logiciel comptable ?
    - Pour un navigateur web ?
    - Pour une application de courriels ?

On va avoir beaucoup d'objets dont les états sont modifiables par
toutes sortes d'événements...

&nbsp;

*Question* : comment séparer le code pour garder le tout le plus
lisible et maintenable possible ?

# MVC : Modèle-Vue-Contrôleur

Solution : on peut organiser notre programme avec une *architecture*
de type *Modèle-Vue-Contrôleur*

&nbsp;

Plutôt que d'emmêler la logique avec la définition d'événements et de
composantes graphiques, on va **séparer le code** en fonction du rôle
joué dans le programme


# MVC : Modèle-Vue-Contrôleur

On va avoir différentes classes, regroupées en catégories :

1. **Modèle** : le code qui contient la logique interne de l'application
    - Regroupe les données, les calculs et toute la logique reliée au
      problème que votre programme doit résoudre
    - Exemples
        - Informations sur les employés
        - Code pour calculer les salaires à verser
        - ...
2. **Vue** : le code qui sert à définir ce qui doit être affiché
    - Exemples
        - Où est affichée chaque information sur une fiche de paie
        - Quel bouton est placé où dans l'interface
        - ...

# MVC : Modèle-Vue-Contrôleur

3. **Contrôleur** : le code qui relie les deux ensemble
    - Exemple, lorsqu'on clique sur "Afficher la fiche de paie"
    - Une fonction du contrôleur est appelée
    - `=>` Elle demande au modèle de calculer les valeurs à afficher
    - `=>` Elle demande à la vue d'afficher la scène qui correspond à
      la fiche de paie avec les valeurs calculées

<!-- # MVC : Modèle-Vue-Contrôleur -->

<!-- Résumé en un diagramme : -->

<!-- \center -->

<!-- ![](img/mvc.png)&nbsp; -->

# MVC : Modèle-Vue-Contrôleur

- Toute la *logique du code* devrait se trouver dans les modèles
    - Le modèle doit pouvoir *exister sans l'interface graphique*

&nbsp;

- Les contrôleurs devraient se limiter à *faire le pont* entre les
  différents événements qui surviennent dans la vue et le modèle

&nbsp;

- Les vues devraient être aussi simples et stupides que possible :
  aucune logique contenue, seulement de la *définition d'interface et
  d'événements*

# MVC : Exemple de décomposition

![Le lecteur de musique VLC](img/vlc.png){width=80%}

# MVC : Exemple de décomposition

## Vue

- Définition de la hiérarchie de composantes graphiques
    - Boutons
    - Barre de temps
    - Temps restant/temps total
    - Pochette de l'album...

# MVC : Exemple de décomposition

## Modèle

- Représentation **indépendante de l'affichage graphique**

- Classe `Song`
    - Contient les informations reliées (titre, album, durée, image
      de la pochette, fichier associé...)
    - Contient l'état de la chansons (temps restant, play/pause)
- Classe `MusicPlayer`
    - Contient une playlist
    - Connaît la chanson actuelle
    - Permet toutes les actions faisables pour un lecteur de musique
        - Passer à la prochaine chanson dans la playlist
        - Ajouter une chanson à la playlist
        - Mettre la chanson actuelle en pause
        - Télécharger la pochette
        - ...

# MVC : Exemple de décomposition

## Contrôleur

La "colle" qui lie les deux :

- Quand on clique sur un bouton, le Contrôleur est avisé et appelle
  les méthodes nécessaires du modèle
    - Exemple : quand on clique sur le bouton "next", la playlist doit
      passer à la prochaine chanson
- Lorsque le modèle est modifié, le Contrôleur est avisé et se charge
  de mettre à jour la vue
    - Exemple : quand une chanson termine, on passe automatiquement à
      la prochaine `->` le Contrôleur avise la vue qu'on doit affiche
      le titre de la prochaine chanson et afficher une autre pochette
      d'album

- `=>` *Aucune autre logique que celle qui fait la communication entre
  la vue et le modèle*


<!-- # Logiciel Comptable : **Modèle**-Vue-Contrôleur -->

<!-- Autre exemple : cas d'un logiciel comptable -->

<!-- &nbsp; -->

<!-- **Modèle** : définition des classes de l'application (comme d'habitude) -->

<!-- ```java -->
<!-- class Employe { -->
<!--     private String nom; -->
<!--     private double tauxHoraire; -->
<!--     private double heuresTravaillees; -->

<!--     public double getSalaire() { -->
<!--         return heuresTravaillees * tauxHoraire; -->
<!--     } -->

<!--     ... -->
<!-- } -->
<!-- ``` -->

<!-- # Logiciel Comptable : Modèle-**Vue**-Contrôleur -->

<!-- **Vue** : définition des scènes avec JavaFX -->

<!-- ```java -->
<!-- class UltraLogicielComptable extends Application { -->
<!--     private Scene sceneMenu; -->
<!--     private Scene sceneFiche; -->
<!--     private Text nom, heures, salaire, total; -->
<!--     private Stage primaryStage; -->

<!--     private Controleur controleur; // Accès au contrôleur -->

<!--     @Override -->
<!--     public void start(Stage stage) { -->
<!--         /* On stocke la référence du stage pour usage -->
<!--            futur dans d'autres méthodes que start */ -->
<!--         primaryStage = stage; -->

<!--         /* On crée une instance du contrôleur (this passé en -->
<!--            paramètre pour communiquer dans les deux sens */ -->
<!--         controleur = new Controleur(this); -->
<!--         //... -->
<!-- ``` -->

<!-- # Logiciel Comptable : Modèle-**Vue**-Contrôleur -->

<!-- ```java -->
<!--         // suite de la méthode start() -->

<!--         /* Préparation de la scène qui correspond -->
<!--            au menu d'actions */ -->
<!--         Scene sceneMenu = ... -->

<!--         Button payeJimmy = new Button("Fiche de paye de Jimmy"); -->
<!--         Button payeIsabelle = new Button("Fiche de paye d'Isabelle"); -->
<!--         Button payeCorinne = new Button("Fiche de paye de Corinne"); -->

<!--         payeJimmy.setOnAction((event) -> { -->
<!--             controleur.afficherFiche(0); // Jimmy est l'employé #0 -->
<!--         }); -->

<!--         payeIsabelle.setOnAction((event) -> { -->
<!--             // etc -->
<!--         }); -->

<!--         // ... -->

<!-- ``` -->

<!-- # Logiciel Comptable : Modèle-**Vue**-Contrôleur -->

<!-- ```java -->
<!--         // suite de la méthode start() -->

<!--         /* Préparation de la scène qui correspond à -->
<!--            une des fiches de paye */ -->
<!--         Scene sceneFiche = ... -->

<!--         nom = new Text(); // Pour afficher le nom -->
<!--         heures = new Text(); // Nombre d'heures travaillées -->
<!--         salaire = new Text(); // Salaire horaire -->
<!--         total = new Text(); // Montant total à payer -->
<!--         Button retour = new Button("Retour au menu"); -->

<!--         // + des boutons pour modifier le nombre d'heures... -->

<!--         retour.setOnAction((event) -> { -->
<!--             controleur.fermerEtSauvegarderFiche(); -->
<!--         }); -->
<!--         // ... -->
<!-- ``` -->

<!-- # Logiciel Comptable : Modèle-**Vue**-Contrôleur -->

<!-- ```java -->
<!--         // suite et fin de la méthode start() -->

<!--         // L'application commence avec le menu -->
<!--         currentStage.setScene(sceneMenu); -->
<!--         currentStage.show(); -->
<!--     } -->

<!--     public void afficherFiche(Employe e) { -->
<!--         nom.setText(e.getNom()); -->
<!--         heures.setText(e.getHeuresTravaillees()); -->
<!--         salaire.setText(e.getTauxHoraire()); -->
<!--         total.setText(e.getPaieTotale()); -->

<!--         // On affiche la scène "fiche de paie" -->
<!--         currentStage.setScene(sceneFiche); -->
<!--     } -->

<!--     ... -->
<!-- } -->
<!-- ``` -->

<!-- # Logiciel Comptable : Modèle-Vue-**Contrôleur** -->

<!-- **Contrôleur** : "colle" entre la vue et le modèle -->

<!-- ```java -->
<!-- public class Controleur { -->

<!--     private UltraLogicielComptable vue; -->
<!--     private Employe[] employes; -->

<!--     public Controleur(UltraLogicielComptable vue) { -->
<!--         /* On reçoit la vue en paramètre et on sauve la référence -->
<!--            pour pouvoir appeler ses méthodes plus tard */ -->
<!--         this.vue = vue; -->

<!--         // Création des modèles -->
<!--         this.employes = new Employe[3]; -->
<!--         this.employes[0] = new Employe(...); // Jimmy -->
<!--         this.employes[1] = new Employe(...); // Isabelle -->
<!--         this.employes[2] = new Employe(...); // Corinne -->
<!--     } -->
<!-- ``` -->

<!-- # Logiciel Comptable : Modèle-Vue-**Contrôleur** -->

<!-- ```java -->
<!--     /** -->
<!--      * Fonction appelée lorsqu'on clique sur un -->
<!--      * des boutons pour afficher une paye -->
<!--      */ -->
<!--     public void afficherFiche(int id) { -->
<!--         // On demande à la vue d'afficher la bonne fiche -->
<!--         Employe e = this.employes[id]; -->
<!--         this.vue.afficherFiche(e); -->
<!--     } -->

<!--     public void fermerEtSauvegarderFiche() { -->
<!--          /* Modifie l'employé sélectionné pour -->
<!--             refléter les changements dans la vue */ -->
<!--          // ... -->

<!--          this.vue.afficherMenu(); -->
<!--     } -->
<!--     // ...Autres méthodes ... -->
<!-- } -->
<!-- ``` -->

# Avantages de la décomposition *MVC*

La décomposition *MVC* offre un gros avantage au niveau de la
maintenabilité du code :

- Bug d'affichage ?
    - Besoin de chercher l'erreur seulement dans les classes reliées à
      la *Vue*
- Problème de logique ?
    - Chercher seulement dans les classes du *Modèle*

&nbsp;

Au niveau des tests unitaires : une application graphique est
difficile à tester!

&nbsp;

En MVC, le modèle est indépendant de la vue : on peut donc écrire des
tests exactement comme on le ferait du code non-GUI


# À quoi sert le Contrôleur?

Le rôle du contrôleur peut sembler flou/redondant...

&nbsp;

&nbsp;

Pourquoi ne pas avoir seulement une structure *Vue-Modèle*?

- Quand la vue change, elle avertit le modèle et vice versa?


&nbsp;

&nbsp;

`=>` La présence du Contrôleur est ce qui permet d'*isoler* le modèle
de la vue.


# Avantages de la décomposition *MVC* : Réutilisation

Le modèle *ne doit pas avoir besoin d'une vue précise pour exister*

`=>` On peut réutiliser un même modèle avec différentes vues

&nbsp;

![Le lecteur de musique VLC : interface en ligne de commande](img/vlc-cli.png){width=60%}

<!-- # Avantages de la décomposition *MVC* : Réutilisation -->

<!-- Exemple de réutilisation du même modèle avec plusieurs vues : -->

<!-- ![Exemple de jeu en ligne de commande](img/rfk1.png){width=60%} -->

<!-- &nbsp; -->

<!-- &nbsp; -->

<!-- &nbsp; -->

<!-- &nbsp; -->

<!-- &nbsp; -->


<!-- # Avantages de la décomposition *MVC* : Réutilisation -->

<!-- Exemple de réutilisation du même modèle avec plusieurs vues : -->

<!-- ![Même jeu (même modèle), avec une vue codée avec JavaFX](img/rfk2.png) -->


<!-- # Avantages de la décomposition *MVC* : Réutilisation -->

<!-- Exemple de réutilisation du même modèle avec plusieurs vues : -->

<!-- ![Toujours le même jeu (même modèle), avec une 3ème vue codée avec la librairie 3D de JavaFX](img/rfk3d.png){width=65%} -->


# Avantages de la décomposition *MVC* : Réutilisation

L'inverse est également vrai : on devrait pouvoir réutiliser une même
vue pour afficher différents modèles (similaires)

[columns]

[column=0.5]

\small

Logique de disposition des cartes différente pour chaque jeu, mais le
frame autour reste le même :

- Boutons dans la barre en haut :
    - Nouvelle partie
    - Annuler
    - Tirer une carte
    - ...
- Score + le temps écoulé affiché dans la barre en bas

[column=0.6]

\center

![](img/aiselriot1.png){width=80%}&nbsp;

\footnotesize

Images du programme https://wiki.gnome.org/Apps/Aisleriot

[/columns]


# Avantages de la décomposition *MVC* : Réutilisation

L'inverse est également vrai : on devrait pouvoir réutiliser une même
vue pour afficher différents modèles (similaires)

[columns]

[column=0.5]

\small

Logique de disposition des cartes différente pour chaque jeu, mais le
frame autour reste le même :

- Boutons dans la barre en haut :
    - Nouvelle partie
    - Annuler
    - Tirer une carte
    - ...
- Score + le temps écoulé affiché dans la barre en bas

[column=0.6]

\center

![](img/aiselriot2.png){width=80%}&nbsp;

\footnotesize

Images du programme https://wiki.gnome.org/Apps/Aisleriot

[/columns]


# Avantages de la décomposition *MVC* : Réutilisation

L'inverse est également vrai : on devrait pouvoir réutiliser une même
vue pour afficher différents modèles (similaires)

[columns]

[column=0.5]

\small

Logique de disposition des cartes différente pour chaque jeu, mais le
frame autour reste le même :

- Boutons dans la barre en haut :
    - Nouvelle partie
    - Annuler
    - Tirer une carte
    - ...
- Score + le temps écoulé affiché dans la barre en bas

[column=0.6]

\center

![](img/aiselriot3.png){width=80%}&nbsp;

\footnotesize

Images du programme https://wiki.gnome.org/Apps/Aisleriot

[/columns]


<!-- TODO : MVC, tel qu'expliqué par Normand Desmarais -->

<!-- Pour celles et ceux que ça pourrait interesser, voici comment on m'a expliqué le MVC il y a des lustres. -->

<!-- (Les personnes qui m'ont expliqués le concept avaient une longue expérience avec WebObjects qui s'appuyait énormément sur le MVC. Eh oui, Steve Jobs est indirectement responsable de la re-popularité du MCV — WebObjects ayant été créé sur NeXT, NeXT ayant été fondé par Steve Jobs.) -->

<!-- Le principe du MVC est de clairement séparer la vue du modèle de telle sorte que si l'on remplace la vue par une autre on n'ait rien à changer dans le modèle, et vice versa. -->

<!-- Par exemple, l'on pourrait utiliser la même vue pour afficher différents jeux. La seule contrainte étant que le jeu (le modèle) ait potentiellement deux messages à afficher (l'un en haut, l'autre en bas), que le contenu principal soit affiché sur une grille, et que les intéractions avec le joueur passent par les touches claviers. Pas nécessairement les mêmes touches, c'est le modèle (le jeu) qui impose les touches significatives et le sens associé. -->

<!-- L'on pourrait aussi avoir une vue en JavaFX, et une autre exploitant une librairie différente (SWING, par exemple). Ce qui implique, pour ne pas avoir à réécrire du code, que le modèle et le controleur ne doivent pas dépendrent des classes de JavaFX (même pas KeyType). Tout simplement parce qu'il pourrait y avoir collision entre SWING et JavaFX, ou que JavaFX ne soit pas disponible sur la plateforme d'intérêt (Android par exemple). -->

<!-- Le rôle du controlleur est quant à lui de correctement aiguiller les actions de la vue vers le modèle et de faire remonter les changements (notifications) du modèle vers la vue. C'est donc plus ou moins un rôle d'aiguilleur. Ce rôle devient évident si l'on considère plusieurs vues et plusieurs modèles en parallèle. -->

<!-- Par exemple, l'on pourrait avoir plusieurs jeux supportés par la même vue. Chaque jeu est un modèle, le rôle du controleur est de sélectionnner le bon modèle en fonction du choix de l'utilisateur communiqué par la vue. -->

<!-- L'on pourrait également avoir une vue statistique (et un modèle sous-jacent) affichant le meilleur score, le nombre de parties jouée, les durées moyenne/min/max, un graphique montrant la progresson au cours du temps, etc. L'utilisateur pourrait ainsi passer de la vue jeu à la vue statistique. Le controleur se doit alors de faire remonter les notifications du bon modèle vers la vue correspondante. Par exemple, le modèle statistique doit être constament actif pour accumuler les informations. Il notifie donc constament le controleur de tous changements. Le controleur ne propagera ces notifications que si la vue statistique est active, et les taira autrement. -->

<!-- Voilà, c'est en gros ma compréhension de ce design pattern, en espérant que ce soit utile à quelqu'un... -->


<!-- # MVC pour *The Legend of Zoe* ? -->

<!-- <\!-- TODO : Référence au cours H18 en particulier -\-> -->

<!-- Pour le TP1 que vous avez fait, *The Legend of Zoe* en ligne de -->
<!-- commande, on peut considérer : -->

<!-- &nbsp; -->

<!-- ## Modèles -->

<!-- - Personnages (*Zoe*, Monstres) -->
<!-- - Grille -->
<!-- - Tous les éléments dans la grille (trésors, murs, sortie) -->
<!-- - Logique du jeu -->
<!--     - *Zoe* peut se déplacer dans 4 directions -->
<!--     - Les monstres suivent une certaine logique pour se déplacer -->
<!--     - On ne peut pas avancer dans un mur -->
<!--     - On doit avoir trouvé l'hexaforce du niveau avant de passer au -->
<!--       prochain, etc. -->

<!-- # MVC pour *The Legend of Zoe* ? -->

<!-- Est-ce qu'on pouvait parler de MVC ? -->

<!-- &nbsp; -->

<!-- ## Vue + Contrôleur -->

<!-- - "Vue" : Affichage en console (avec des `System.out.println`) -->
<!-- - Pas vraiment de contrôleur -->
<!-- - Les modèles s'affichaient probablement eux-mêmes... -->
<!--     - `System.out.println` dans le code de *Zoe* ou des Monstres ? -->
<!--     - `=>` pas MVC, la logique et l'affichage étaient mêlés ensemble -->
<!--     - Pas très grave, puisque le projet était simple -->
<!--     - Ça serait un peu plus difficile à gérer si on commençait à mêler -->
<!--       des événements et des composantes graphiques dans le code en -->
<!--       plus du reste... -->

# Exemple de code MVC

**Examiner le code dans `MVC.zip`**

- Spécification: un petit programme qui permet d'entrer des nombres et
  qui va calculer le total

![Interface graphique pour le petit programme](img/monsupercompteur.png){width=50%}

# Exemple de code MVC

Notez que la façon dont le code est construit permet...

- D'avoir plusieurs vues (une en console, une graphique) pour le même
  compteur
- D'avoir plusieurs modèles différents en réutilisant la même vue
- Réutiliser le même contrôleur peu importe le modèle ou la vue qui
  sont choisis

&nbsp;

Également :

- On peut réutiliser l'objet Compteur tel quel dans un autre programme

# MVC en pratique...

Tout le monde s'entend sur le fait qu'il est une bonne idée de
séparer :

1. Le code qui définit l'interface
2. Le code qui gère les interactions *Affichage* $\leftrightarrow$ *Logique*
3. Le code qui s'occupe de la logique du programme

&nbsp;

Le concept n'est cependant pas défini de façon unique, précise et
parfaite.

# MVC en pratique...

D'une librairie à l'autre, d'un contexte à l'autre, vous allez tomber
sur toutes sortes d'implantations différentes...

![\footnotesize https://accu.org/index.php/journals/1524, Deltacen via Wikimedia Commons, https://commons.wikimedia.org/wiki/File:MVC-Process.svg, https://commons.wikimedia.org/wiki/File:ModelViewControllerDiagram2.svg](img/mvc-blend.png){width=80%}

# MVC en pratique...

Il y a plusieurs façons d'arriver au même résultat, l'idée
fondamentale reste la même : séparer le code selon les différentes
tâches.

&nbsp;

La structure de code qui est montrée dans le cours est simplement un
exemple d'application du modèle MVC qui peut ne pas s'appliquer à tous
les contextes.

&nbsp;

Exemple de contextes différents : Site web vs Logiciel GUI

- Site web : l'utilisateur interagit principalement via la barre du
  navigateur et les hyperliens (ex.: aller sur *http://perdu.com/*)
- Logiciel GUI : l'utilisateur interagit en cliquant sur des
  composantes de l'interface


# MVC en pratique...

*Notez finalement* : MVC est une des architectures logicielles
possibles... Il en existe d'autres !

&nbsp;

Le code MVC que vous devrez écrire dans le cadre du cours et le code
montré en exemple feront office de *cas d'école*, pour vous aider à
maîtriser le principe général.

&nbsp;

Si jamais vous vous intéressez à utiliser JavaFX en dehors du cours,
sachez que d'autres patterns de développement sont utilisés en
pratique.

&nbsp;

Les "bonnes pratiques de JavaFX" demandent plus de notions que ce
qu'on a le temps de voir et dépassent les buts de ce cours.
