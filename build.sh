#!/bin/bash

if test -z "$1"
then
    echo usage "./build.sh fichier.md"
    echo
    echo "  Exemple: ./build.sh 00-PlanDeCours.md"
    echo
    exit 1
fi
stem=${1%.md}

THEME=Rochester
# PANDOC_STYLE="--filter ./columns.py --no-tex-ligatures -t beamer+strikeout -V theme:$THEME -V colortheme:dolphin"
PANDOC_STYLE="--filter ./columns.py --from markdown-smart -t beamer -V theme:$THEME -V colortheme:dolphin"
PANDOC_CODE="-H make-code-smaller.tex --highlight-style=tango -H header_pagenbr.tex"

pandoc $PANDOC_STYLE $PANDOC_CODE "$stem.md" -o "$stem.pdf" || (echo Pas de code dans les slides; pandoc $PANDOC_STYLE "$stem.md" -o "$stem.pdf")
