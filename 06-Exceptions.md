---
title: Traitement d'erreurs - Les Exceptions
author: Par Nicolas Hurtubise \newline \footnotesize Basé sur les notes de Pascal Vincent et Jian-Yun Nie
header-includes:
    - \usepackage{textcomp}
date: IFT1025 - Programmation 2
---

# Au programme...

- Erreurs/Exceptions
- Lancer une exception
- Trajet de transmission d'une exception
- `finally`
- *Catch or Specify*

# Erreurs

On peut distinguer deux sortes d'erreurs dans les programmes :

1. Erreur à la *compilation*

```java
int x = 19;

if (x > 0 { // Erreur de syntaxe, il manque une parenthèse
    System.out.println("x est strictement positif");
}

System.out.println("Bonjour") // point-virgule manquant

// Erreur : les types sont incompatibles
Etudiant e = new Professeur("Jimmy", "Whooper");
```

- Ce genre d'erreur est une erreur de programmation et peut être
intercepté directement à la compilation
- Les IDEs sont généralement capables de détecter les erreurs pendant
  qu'on tape le code

# Erreurs

2. Erreur à l'*exécution*

```java
Scanner s = new Scanner(System.in);

int nombre = s.nextInt();
// => L'utilisateur entre "bonjour"...
```

- Ce genre d'erreur ne peut pas être intercepté avant d'exécuter le
  programme sur une certaine entrée
- On doit cependant prévoir que ce genre de situation puisse arriver
- Les utilisateurs *vont* faire des erreurs... On doit pouvoir réagir
  en conséquence quand ça arrive

# Erreurs

2. Erreur à l'*exécution*

&nbsp;

&nbsp;

Les erreurs à l'exécution peuvent être causées par les utilisateurs,
mais aussi par d'autres facteurs...


```java
PageWeb page = new PageWeb("https://en.wikipedia.org/wiki/Java");

System.out.println(page.titre);

/* Et si l'ordinateur n'est pas connecté à internet ?
   La page n'est pas accessible, notre programme plante... */


```


# Erreurs

Dans des langages de programmation comme le `C`, la gestion d'erreur à
l'exécution se fait généralement en retournant une valeur spéciale :

```c
int main() {
    FILE* fichier = fopen("fichier.txt","w");

    if(fichier == NULL) { // Erreur (ex.: ficher inaccessible)
        printf("Erreur à l'ouverture du fichier");
        exit(1);
    }

    // Écriture dans le fichier
    fprintf(fichier,"Bonjour !\n");

    // Fermeture du fichier
    fclose(fichier);
    return 0;
}
```

# Erreurs

C'est ce que vous avez fait dans l'exercice de `Matrice`s...

- Ce style mène à écrire du code qui contient plein de tests pour
vérifier si la valeur retournée est valide ou non :

```java
System.out.println("Nombre de lignes ?");
int l1 = scanner.nextInt();
...

Matrice m1 = new Matrice(l1, c1);
Matrice m2 = new Matrice(l2, c2);
Matrice m3 = m1.dotProduct(m2); // Retourne peut-être null

if(m3 == null) {
    System.err.println("Erreur");
    System.exit(1); // Fermer l'application
}

// Si c'est bon, on peut continuer
...
```

# Exceptions

Pour faciliter la gestion d'erreurs de ce genre, on a le mécanisme
d'`Exceptions` en Java :

- Apparition d'une situation **anormale** qui conduirait à l'échec du
programme
- On peut souvent **détecter** ces situations et réagir pour réparer
  le problème, par exemple :
    - Demander d'entrer un nombre entier positif pour le nombre de
      lignes/colonnes de la matrice
    - Demander de vérifier la connexion internet puis cliquer sur
      *Réessayer le téléchargement*
    - Avertir que le fichier n'a pas été sauvegardé parce que le
      disque dur n'a plus assez d'espace libre
    - etc.

# Types d'erreurs d'exécution (exceptions) les plus courants


- ArrayIndexOutOfBoundsException

```java
int[] tab = new int[5];

tab[5] = 123; // Erreur

int val = tab[10]; // Erreur
```

# Types d'erreurs d'exécution (exceptions) les plus courants

- NullPointerException

```java
Etudiant e;

e.etudier(); // Erreur, e contient null



```

# Types d'erreurs d'exécution (exceptions) les plus courants

- ArithmeticException

```java
int a = 1;

int b = 15/(a - 1); // Division par 0



```

# Types d'erreurs d'exécution (exceptions) les plus courants

- ClassCastException

```java
Etudiant e = new Etudiant();

Professeur p = (Professeur) e; // Cast invalide



```

# Types d'erreurs d'exécution (exceptions) les plus courants

Plusieurs autres exceptions existent également :

- `EOFException` : tenter de lire après la fin d'un fichier
- `IOException` : erreur d'entrée/sortie en général
- `IllegalArgumentException` : paramètres illégaux ou invalides passés
  à une fonction

# Types d'exceptions

Les Exceptions sont représentées en Java par une hiérarchie de classes (`extends ...`)

```java
* java.lang.Exception
    - java.lang.InterruptedException
    - java.lang.RuntimeException
        - java.lang.ArithmeticException
        - java.lang.ArrayStoreException
        - java.lang.ClassCastException
        - java.lang.EnumConstantNotPresentException
        - java.lang.IllegalArgumentException
            - java.lang.IllegalThreadStateException
            - java.lang.NumberFormatException
        - java.lang.IllegalMonitorStateException
        - java.lang.IllegalStateException
        - java.lang.IndexOutOfBoundsException
            - java.lang.ArrayIndexOutOfBoundsException
            - java.lang.StringIndexOutOfBoundsException
    ...
```

# Lancer une exception

On peut *lancer* une exception avec le mot-clé `throw`


```java
public class Cercle {
  ...

  public void setRayon(double rayon) {
    if(rayon < 0) {
      IllegalArgumentException e;
      e = new IllegalArgumentException("Le rayon doit être positif");

      throw e;
    }
    // ...
  }
}
```

- On crée une *nouvelle instance* de l'exception appropriée
- On la *lance* à la fonction appelante


# Lancer une exception

Notez que ça serait équivalent à :

```java
public class Cercle {
  ...

  public void setRayon(double rayon) {
    if(rayon < 0)
      throw new IllegalArgumentException("Le rayon doit être positif");
        
    // ...
  }
}
```

Typiquement, on crée une instance d'une sous-classe `Exception` au
moment de la `throw`er.

# Lancer une exception

```java
public static void main(String[] args) {
    Cercle c = new Cercle();

    c.setRayon(-150); // Le programme va planter ici
}

/* Dans la console :

Exception in thread "main" java.lang.IllegalArgumentException:
Le rayon doit être positif
	at Cercle.setRayon(Cercle.java:9)
	at Geometrie.main(Geometrie.java:54)
*/
```

Plutôt que de continuer l'exécution du programme avec une valeur
erronée, le programme plante avec une erreur appropriée


# Lancer une exception

- Dans certains cas, l'erreur est rattrapable
- On a un mécanisme pour **attraper** une exception lancée et trouver
  une solution au problème : bloc `try...catch`

```java
try {

    /* Traitement normal, incluant une opération
       qui pourrait se dérouler de façon anormale
       (Ex.: ouvrir une page web, écrire dans un fichier, ...)
    */

} catch(UneCertaineException e) {

    // Traitement d'exception s'il y a une anomalie

}
```

# Lancer une exception

```java
Cercle c = new Cercle();
Scanner scanner = new Scanner(System.in);

int rayon = scanner.nextInt();
// L'utilisateur tape -150...

try {
    
    c.setRayon(rayon);
    
} catch (IllegalArgumentException e) {
    /* Message d'erreur plus user-friendly que
       Exception in thread "main" java.lang.IllegalArgumentException */
    System.out.println("Erreur ! Le rayon entré n'est pas valide");
}
```


# Mécanisme général

- On a donc deux mécanismes en parallèle :
    - Traitement normal (si tout se passe bien)
    - Traitement d'exception (exception handler)
- Quand il y a une exception
    - Le traitement normal s'arrête
    - Le traitement d'exception prend le contrôle

# Mécanisme général

```java
try {

    System.out.print("Entrez le rayon du cercle : ")
    int rayon = scanner.nextInt();

    c.setRayon(rayon); /* *** Exception ici, on arrête tout
                          et on va poursuivre l'exécution dans
                          le bloc catch approprié */

    System.out.println("Super, le rayon est modifié !");
} catch (IllegalArgumentException e) {
    // *** On arrive dans le traitement d'exception
    System.out.println("Erreur ! Le rayon entré n'est pas valide");
}
```

Exécution :

```java
Entrez le rayon du cercle : [l'utilisateur entre -150]
Erreur ! Le rayon entré n'est pas valide
```

# Trajet de transmission d'une exception

- Les exceptions lancées suivent un trajet semblable aux valeurs
  de retour des fonctions
- `throw` est similaire à `return`
    - On quitte la fonction actuelle, on retourne à la fonction
      appelante

# Trajet de transmission d'une exception

```java
public static int diviser(int a, int b) {
    if(b == 0)
        throw new ArithmeticException("Division par 0 impossible");

    System.out.println("Super division en traitement !");

    return a/b;
}

public static void main(String[] args) {
    try {
        diviser(3, 0);
    } catch(ArithmeticException e) {
        System.out.println("Erreur dans la division...");
    }
}

/* Affiche seulement :
Erreur dans la division... */
```

# Trajet de transmission d'une exception

- Si une exception n'est pas attrapée par la fonction appelante (avec
  un `try...catch`), elle est relancée à la prochaine fonction dans le
  stack d'appels

# Trajet de transmission d'une exception

```java
public static void erreur() {
    int[] tab = new int[1];
    tab[10] = 100; // Cause une exception -> lancée à c()
    System.out.println("test"); // Pas affiché, on quitté à l'exception
}
public static void c() {
    erreur(); // L'exception n'est pas gérée : relancée à b()
    System.out.print("c"); // Pas affiché (on a relancé l'exception)
}
public static void b() {
    c(); // L'exception n'est toujours pas gérée : relancée à a()
    System.out.println("b"); // Pas affiché non plus
}
public static void a() {
    try {
        b();
    } catch(ArrayIndexOutOfBoundsException e) {
        System.out.println("L'exception est attrapée ici"); // Affiché!
    }
}
```

# Trajet de transmission d'une exception

- S'il n'y a aucun `try...catch` pour attraper l'exception,
  l'exception remonte jusqu'à plus haut que la fonction `main()`
- L'erreur est alors affichée directement dans la console


# Trajet de transmission d'une exception

```java
public static void erreur() {
    int[] tab = new int[1];
    tab[10] = 100; // Exception lancée à c()
}
public static void c() {
    erreur(); // Exception relancée à b()
    System.out.println("c");
}
public static void b() {
    c(); // Exception relancée à a()
    System.out.println("b");
}
public static void a() {
    b(); // Exception relancée à main()
    System.out.println("a");
}
public static void main(String[] args) {
    a(); // Exception in thread "main" ...
}
```

# Attraper différents types d'exceptions

On peut avoir plusieurs blocs `catch` spécialisés pour les différents
types d'exceptions qui pourraient être lancés :

```java
try {

    PageWeb page = new PageWeb("https://en.wikipedia.org/wiki/Java");

} catch(SSLException exception) {

    // Erreur au niveau du certificat SSL du site

} catch(UnknownHostException exception) {

    // Le nom de domaine n'existe pas
    
} catch(...) {
    ...
}
```

<!-- # Attraper différents types d'exceptions -->

<!-- *Attention* : l'ordre des `catch` est important. -->

<!-- - Le premier `catch` dont le type match l'exception est celui utilisé -->


<!-- ```java -->
<!-- try { -->
<!--     PageWeb page = new PageWeb("https://en.wikipedia.org/wiki/Java"); -->
<!-- } catch(Exception exception) { -->

<!--     /* La classe `Exception` est la classe parente de toutes -->
<!--        les exceptions, une exception lancée de type SSLException -->
<!--        serait donc catchée ici (puisqu'elle est instanceof Exception)  -->
<!--        et ne sera pas traitée dans le prochain bloc */ -->

<!-- } catch(SSLException exception) { -->
<!--     // Erreur au niveau du certificat SSL du site, catch pas exécuté -->
<!-- } catch(...) { -->
<!--     ... -->
<!-- } -->
<!-- ``` -->

<!-- <\!-- TODO -\-> -->
<!-- Notez qu'en Java, le code précédent cause une erreur à la compilation -->

# `finally`

- Le bloc `finally` est optionnel et permet d'exécuter quelque chose
  *peu importe si le `try`... s'est exécuté correctement ou non*

```java

try {

    trucQuiPlantePossiblement();
    System.out.println("Il n'y a pas d'erreur");
    
} catch(UneCertaineException e) {

    System.out.println("Il y a eu une erreur !");

} finally {
    System.out.println("Ceci s'exécute peu importe");
}

System.out.println("Ceci s'exécute après");
```

# `finally`

Cas 1 : s'il n'y a pas d'exception lors de l'exécution de
`trucQuiPlantePossiblement()` :

```java
Il n'y a pas d'erreur
Ceci s'exécute peu importe
Ceci s'exécute après
```


# `finally`

Cas 2 : il y a eu une erreur pendant `trucQuiPlantePossiblement()` :

```java
Il y a eu une erreur !
Ceci s'exécute peu importe
Ceci s'exécute après
```

# `finally`

- Le bloc `finally` permet d'exécuter quelque chose ***peu importe***
  ce qui s'est passé

Le mot-clé ici est : ***peu importe***

# `finally`

```java

try {

    trucQuiPlantePossiblement();
    System.out.println("Pas encore d'erreur");

    if(elementProblematique < 0)
        throw new UneException(); // *** on quitte la fonction ***

    System.out.println("Ok !");

} catch(UneCertaineException e) {
    System.out.println("Il y a eu une erreur !");
} finally {
    System.out.println("Ceci s'exécute peu importe");
}

System.out.println("Ceci s'exécute après");
```

# `finally`

Exécution si tout se passe bien :

```java
Pas encore d'erreur
Ok !
Ceci s'exécute peu importe
Ceci s'exécute après
```

# `finally`

Exécution s'il y a une erreur dans `trucQuiPlantePossiblement()` :

```java
Il y a eu une erreur !
Ceci s'exécute peu importe
Ceci s'exécute après
```


# `finally`

Exécution si `elementProblematique < 0` :

```java
Pas encore d'erreur
// On lance `UneException()`, on quitte la fonction actuelle
Ceci s'exécute peu importe
```

On exécute le code du `finally` peu importe ce qui s'est passé, même
s'il y a eu un `return`, un autre `throw`, ou une autre erreur...

# `finally`

C'est pratique dans certains cas *très spécifiques*, en particulier
quand on a besoin de libérer des ressources réservées

- Même dans des cas où on se retrouve devant une erreur non catchée
  sur le moment, le `finally` est exécuté

&nbsp;

En pratique, c'est plutôt rare d'utiliser un bloc `finally`


# Définir un nouveau type d'exception

On peut définir nos propres sous-classes de la classe `Exception`

- Pratique dans les bibliothèques de fonctions :
    - ex.: `HttpException404` dans un framework web pour dire que la
      ressource demandée n'existe pas
- *Mais*, les exceptions existantes sont souvent suffisantes pour
  exprimer un type d'erreur
    - `IllegalArgumentException` : l'argument passé à la fonction est
      invalide
    - `IndexOutOfBoundsException` : si on essaie d'accéder à un
      élément qui correspond conceptuellement à un *index invalide*
      (ex.: numéro de page inexistant dans une classe `Livre`)
    - ...


# The *Catch or Specify Requirement*

Si votre fonction peut lancer une exception :

- Soit parce que vous en lancez une manuellement avec `throw`
- Soit parce qu'il pourrait y en avoir une qui ne serait pas `catch`ée

Alors, vous devez la déclarer avec `throw`**s** `...` :

```java
public String skip() throws IOException {
    String s;


    /* Cette ligne pourrait lancer une IOException
       (ou une sous-classe de IOException) et
       le traitement d'erreur n'est pas fait ici */
    s = input.readLine();

    return s;
}
```


# The *Catch or Specify Requirement*

Si votre fonction peut lancer une exception :

- Soit parce que vous en lancez une manuellement avec `throw`
- Soit parce qu'il pourrait y en avoir une qui ne serait pas `catch`ée

Alors, vous devez la déclarer avec `throw`**s** `...` :

```java
public String skip() { // Rien à déclarer, on catch l'exception ici
    String s;

    try {
        s = input.readLine();
    } catch(IOException e) {
        // Traitement d'erreur fait ici
    }

    return s;
}
```

# Checked Exceptions vs Unchecked

Ce n'est bien sûr pas le cas de toutes les exceptions...

```java
public void diviser(int a, int b) {
    return a/b;
}
```

- On sait que `a/b` pourrait produire une `ArithmeticException` si on divise par zéro...
- Mais ça serait lourd de soit :
    - Faire des `try...catch` à chaque division
    - Déclarer toutes les méthodes qui font des divisions avec un
      `throws ArithmeticException`, tout le temps

# Checked Exceptions vs Unchecked

- *De façon générale* : les exceptions sont `check`ées par le
compilateur, pour s'assurer que si un erreur survient, votre code est
en mesure de la traiter

&nbsp;

- Dans le cas d'exceptions dues à des erreurs de programmation plutôt
  qu'à des circonstances *anormales*, les exceptions qui pourrait être
  lancées ne sont pas `check`ées, ex.:
    - `NullPointerException`
    - `IndexOutOfBoundsException`
    - ...

# Bonnes pratiques

Comme leur nom le suggère, les exceptions sont un mécanisme pour
proposer un *traitement alternatif* lors de cas *exceptionnels*... Une
situation normale ne devrait pas utiliser le système
d'exceptions\footnote{Exemple tiré du livre \textit{Effective Java}
(Joshua Bloch), Item 57}

```java
/* Le code fonctionne... Mais c'est un abus horrible des
   exceptions, à éviter absolument ! */
try {
    int i = 0;
    while(true) {
        tab[i] += 1;
        i++;
    }
} catch(ArrayIndexOutOfBoundsException e) {
    // rien
}
```

